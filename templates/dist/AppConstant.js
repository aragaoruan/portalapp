angular.module('portal.constants').constant("CONFIG_APP", {
  "public_token": 'ceb67e61be84d393a46495e36876bcd8d49e7e545a28de8a',
  "local_token": 'user_token',
  "isAuthenticated": false,
  "idSistema": 38,
  "url": 'AMBIENTE',
  "apiModule": '/api-v2',
  "googleProjectId": '652309714922',
  "entidadeAppId": "ENTIDADE"


  // "url": 'http://dev11g2.ead1.net', // ambiente de desenvolvimento Arthur
  //"url": 'http://dev5g2.unyleya.xyz', // ambiente de desenvolvimento Alex
  // "url": 'http://g2evolutiva1.unyleya.xyz', // ambiente de desenvolvimento do time Evolutiva 1
  // "url": 'http://g2s.unyleya.com.br/', // ambiente de produção
  // "url": 'http://g2staging.unyleya.xyz/', // ambiente de testes
  // "url": 'http://localhost:8100', // localhost
  // "url": 'http://g2release.unyleya.xyz', // ambiente do release
  //"entidadeAppId": "514"  // ID CORRESPONDENTE AO IMP CONCURSOS
  // "entidadeAppId": "14" // -> ID CORRESPONDENTE A FACULDADE UNYLEYA
  // "entidadeAppId": "262" // -> ID CORRESPONDENTE A UCAM
});
