angular.module('portal', [
  'ionic',
  'ionic.service.core',
  'portal.controllers',
  'portal.factories',
  'portal.services',
  'portal.constants',
  'portal.filters',
  'portal.directives',
  'angular-jwt',
  'chart.js',
  'angulartics',
  'angulartics.google.analytics',
  'ui.bootstrap',
  'jett.ionic.filter.bar',
  'angular-cache',
  'angular-md5',
  'ionic.native'
])
  .run(function ($ionicPlatform, $rootScope, $ionicLoading, $ionicFilterBar, AppService, $timeout, $window, $ionicHistory) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(false);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });

    //Abre a barra de pesquisa do header
    $rootScope.openSearch = function (list) {

      $ionicFilterBar.show({
        items: list,
        update: function (filteredItems, filterText) {
          list = filteredItems;
          if (filterText) {
          }
        }
      });
    };
    $rootScope.$on('loading:show', function () {
      $ionicLoading.show({
        template: '<ion-spinner>Carregando...</ion-spinner>'
      });
    });

    $rootScope.$on('loading:hide', function () {
      $ionicLoading.hide();
    });

    $rootScope.reloadScreen = function () {
      $timeout(function () {
        $rootScope.$broadcast('scroll.refreshComplete');
      }, 1000).then(function () {
        $window.location.reload();
      });
    };
    $rootScope.myGoBack = function () {
      $ionicHistory.goBack();
    };
    AppService.notifications();

  }).config(['$ionicAppProvider', '$ionicConfigProvider', '$analyticsProvider', '$ionicFilterBarConfigProvider', 'ChartJsProvider', 'CacheFactoryProvider',
    function ($ionicAppProvider, $ionicConfigProvider, $analyticsProvider, $ionicFilterBarConfigProvider, ChartJsProvider, CacheFactoryProvider) {
      angular.extend(CacheFactoryProvider.defaults, {
        maxAge: 60 * 60 * 1000
      });

      // Desliga o tracking automtático das pageviews virtuais.
      $analyticsProvider.virtualPageviews(false);

      //Configura o placeholder da barra de pesquisa
      $ionicFilterBarConfigProvider.placeholder('Pesquisar');

      // Identify app
      $ionicAppProvider.identify({

      // The App ID (from apps.ionic.io) for the server
        app_id: '382dbb82',

        // The public API key all services will use for this app
        api_key: 'ceb67e61be84d393a46495e36876bcd8d49e7e545a28de8a',

        // Set the app to use development pushes
        dev_push: false
      });

      $ionicConfigProvider.backButton.text('');
    }
  ]);


angular.module('portal.controllers', []);

angular.module('portal.services', []);

angular.module('portal.factories', ['portal.constants', 'angular-md5']);

angular.module('portal.constants', []);

angular.module('portal.filters', []);

angular.module('portal.directives', []);
