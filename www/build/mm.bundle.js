// (C) Copyright 2015 Martin Dougiamas
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

angular.module('mm', ['ionic', 'mm.core', 'mm.core.contentlinks', 'mm.core.course', 'mm.core.courses', 'mm.core.login', 'mm.core.sidemenu', 'mm.core.user', 'mm.addons.mod_forum', 'mm.addons.notifications', 'mm.addons.pushnotifications', 'ngCordova', 'angular-md5', 'pascalprecht.translate', 'ngAria', 'ngIOS9UIWebViewPatch'])
    .run(["$ionicPlatform", function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    }]);

angular.module('mm.core', ['pascalprecht.translate'])
.constant('mmCoreSessionExpired', 'mmCoreSessionExpired')
.constant('mmCoreUserDeleted', 'mmCoreUserDeleted')
.constant('mmCoreSecondsYear', 31536000)
.constant('mmCoreSecondsDay', 86400)
.constant('mmCoreSecondsHour', 3600)
.constant('mmCoreSecondsMinute', 60)
.constant('mmCoreDownloaded', 'downloaded')
.constant('mmCoreDownloading', 'downloading')
.constant('mmCoreNotDownloaded', 'notdownloaded')
.constant('mmCoreOutdated', 'outdated')
.constant('mmCoreNotDownloadable', 'notdownloadable')
.constant('mmCoreWifiDownloadThreshold', 104857600)
.constant('mmCoreDownloadThreshold', 10485760)
.config(["$stateProvider", "$provide", "$ionicConfigProvider", "$httpProvider", "$mmUtilProvider", "$mmLogProvider", "$compileProvider", "$mmInitDelegateProvider", "mmInitDelegateMaxAddonPriority", function($stateProvider, $provide, $ionicConfigProvider, $httpProvider, $mmUtilProvider,
        $mmLogProvider, $compileProvider, $mmInitDelegateProvider, mmInitDelegateMaxAddonPriority) {
    $ionicConfigProvider.platform.android.tabs.position('bottom');
    $provide.decorator('$ionicPlatform', ['$delegate', '$window', function($delegate, $window) {
        $delegate.isTablet = function() {
            var mq = 'only screen and (min-width: 768px) and (-webkit-min-device-pixel-ratio: 1)';
            return $window.matchMedia(mq).matches;
        };
        return $delegate;
    }]);
        $provide.decorator('$log', ['$delegate', $mmLogProvider.logDecorator]);
    $stateProvider
        .state('redirect', {
            url: '/redirect',
            params: {
                siteid: null,
                state: null,
                params: null
            },
            cache: false,
            controller: ["$scope", "$state", "$stateParams", "$mmSite", "$mmSitesManager", "$ionicHistory", function($scope, $state, $stateParams, $mmSite, $mmSitesManager, $ionicHistory) {
                $ionicHistory.nextViewOptions({disableBack: true});
                function loadSiteAndGo() {
                    $mmSitesManager.loadSite($stateParams.siteid).then(function() {
                        $state.go($stateParams.state, $stateParams.params);
                    }, function() {
                        $state.go('app.home');
                    });
                }
                $scope.$on('$ionicView.enter', function() {
                    if ($mmSite.isLoggedIn()) {
                        if ($stateParams.siteid && $stateParams.siteid != $mmSite.getId()) {
                            $mmSitesManager.logout().then(function() {
                                loadSiteAndGo();
                            });
                        } else {
                            $state.go($stateParams.state, $stateParams.params);
                        }
                    } else {
                        if ($stateParams.siteid) {
                            loadSiteAndGo();
                        } else {
                            $state.go('app.home');
                        }
                    }
                });
            }]
        });
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.transformRequest = [function(data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? $mmUtilProvider.param(data) : data;
    }];
    function addProtocolIfMissing(list, protocol) {
        if (list.indexOf(protocol) == -1) {
            list = list.replace('https?', 'https?|' + protocol);
        }
        return list;
    }
    var hreflist = $compileProvider.aHrefSanitizationWhitelist().source,
        imglist = $compileProvider.imgSrcSanitizationWhitelist().source;
    hreflist = addProtocolIfMissing(hreflist, 'file');
    hreflist = addProtocolIfMissing(hreflist, 'tel');
    hreflist = addProtocolIfMissing(hreflist, 'mailto');
    hreflist = addProtocolIfMissing(hreflist, 'geo');
    hreflist = addProtocolIfMissing(hreflist, 'filesystem');
    imglist = addProtocolIfMissing(imglist, 'filesystem');
    $compileProvider.aHrefSanitizationWhitelist(hreflist);
    $compileProvider.imgSrcSanitizationWhitelist(imglist);
    $mmInitDelegateProvider.registerProcess('mmAppInit', '$mmApp.initProcess', mmInitDelegateMaxAddonPriority + 400, true);
    $mmInitDelegateProvider.registerProcess('mmUpdateManager', '$mmUpdateManager.check', mmInitDelegateMaxAddonPriority + 300, true);
}])
.run(["$ionicPlatform", "$ionicBody", "$window", "$mmEvents", "$mmInitDelegate", "mmCoreEventKeyboardShow", "mmCoreEventKeyboardHide", function($ionicPlatform, $ionicBody, $window, $mmEvents, $mmInitDelegate, mmCoreEventKeyboardShow, mmCoreEventKeyboardHide) {
    $mmInitDelegate.executeInitProcesses();
    $ionicPlatform.ready(function() {
        var checkTablet = function() {
            $ionicBody.enableClass($ionicPlatform.isTablet(), 'tablet');
        };
        ionic.on('resize', checkTablet, $window);
        checkTablet();
        $window.addEventListener('native.keyboardshow', function(e) {
            $mmEvents.trigger(mmCoreEventKeyboardShow, e);
        });
        $window.addEventListener('native.keyboardhide', function(e) {
            $mmEvents.trigger(mmCoreEventKeyboardHide, e);
        });
    });
}]);

angular.module('mm.core')
.factory('$mmAddonManager', ["$log", "$injector", function($log, $injector) {
    $log = $log.getInstance('$mmAddonManager');
    var self = {},
        instances = {};
        self.get = function(name) {
        if (self.isAvailable(name)) {
            return instances[name];
        }
    };
        self.isAvailable = function(name) {
        if (!name) {
            return false;
        }
        if (instances[name]) {
            return true;
        }
        try {
            instances[name] = $injector.get(name);
            return true;
        } catch(ex) {
            $log.warn('Service not available: '+name);
            return false;
        }
    };
    return self;
}]);

angular.module('ngIOS9UIWebViewPatch', ['ng']).config(["$provide", function($provide) {
  $provide.decorator('$browser', ['$delegate', '$window', function($delegate, $window) {
    if (isIOS9UIWebView($window.navigator.userAgent)) {
      return applyIOS9Shim($delegate);
    }
    return $delegate;
    function isIOS9UIWebView(userAgent) {
      return /(iPhone|iPad|iPod).* OS 9_\d/.test(userAgent) && !/Version\/9\./.test(userAgent);
    }
    function applyIOS9Shim(browser) {
      var pendingLocationUrl = null;
      var originalUrlFn= browser.url;
      browser.url = function() {
        if (arguments.length) {
          pendingLocationUrl = arguments[0];
          return originalUrlFn.apply(browser, arguments);
        }
        return pendingLocationUrl || originalUrlFn.apply(browser, arguments);
      };
      window.addEventListener('popstate', clearPendingLocationUrl, false);
      window.addEventListener('hashchange', clearPendingLocationUrl, false);
      function clearPendingLocationUrl() {
        pendingLocationUrl = null;
      }
      return browser;
    }
  }]);
}]);
angular.module('mm.core')
.provider('$mmApp', ["$stateProvider", function($stateProvider) {
        var DBNAME = 'MoodleMobile',
        dbschema = {
            stores: []
        },
        dboptions = {
            autoSchema: true
        };
        this.registerStore = function(store) {
        if (typeof(store.name) === 'undefined') {
            console.log('$mmApp: Error: store name is undefined.');
            return;
        } else if (storeExists(store.name)) {
            console.log('$mmApp: Error: store ' + store.name + ' is already defined.');
            return;
        }
        dbschema.stores.push(store);
    };
        this.registerStores = function(stores) {
        var self = this;
        angular.forEach(stores, function(store) {
            self.registerStore(store);
        });
    };
        function storeExists(name) {
        var exists = false;
        angular.forEach(dbschema.stores, function(store) {
            if (store.name === name) {
                exists = true;
            }
        });
        return exists;
    }
    this.$get = ["$mmDB", "$cordovaNetwork", "$log", "$injector", "$ionicPlatform", function($mmDB, $cordovaNetwork, $log, $injector, $ionicPlatform) {
        $log = $log.getInstance('$mmApp');
        var db,
            self = {};
                self.createState = function(name, config) {
            $log.debug('Adding new state: '+name);
            $stateProvider.state(name, config);
        };
                self.closeKeyboard = function() {
            if (typeof cordova != 'undefined' && cordova.plugins && cordova.plugins.Keyboard && cordova.plugins.Keyboard.close) {
                cordova.plugins.Keyboard.close();
                return true;
            }
            return false;
        };
                self.getDB = function() {
            if (typeof db == 'undefined') {
                db = $mmDB.getDB(DBNAME, dbschema, dboptions);
            }
            return db;
        };
                self.getSchema = function() {
            return dbschema;
        };
                self.initProcess = function() {
            return $ionicPlatform.ready();
        };
                self.isDevice = function() {
            return !!window.device;
        };
                self.isOnline = function() {
            var online = typeof navigator.connection === 'undefined' || $cordovaNetwork.isOnline();
            if (!online && navigator.onLine) {
                online = true;
            }
            return online;
        };
                self.isNetworkAccessLimited = function() {
            if (typeof navigator.connection === 'undefined') {
                return false;
            }
            var type = $cordovaNetwork.getNetwork();
            var limited = [Connection.CELL_2G, Connection.CELL_3G, Connection.CELL_4G, Connection.CELL];
            return limited.indexOf(type) > -1;
        };
                self.isReady = function() {
            var promise = $injector.get('$mmInitDelegate').ready();
            return promise.$$state.status === 1;
        };
                self.openKeyboard = function() {
            if (typeof cordova != 'undefined' && cordova.plugins && cordova.plugins.Keyboard && cordova.plugins.Keyboard.show) {
                cordova.plugins.Keyboard.show();
                return true;
            }
            return false;
        };
                self.ready = function() {
            return $injector.get('$mmInitDelegate').ready();
        };
        return self;
    }];
}]);

angular.module('mm.core')
.constant('mmCoreConfigStore', 'config')
.config(["$mmAppProvider", "mmCoreConfigStore", function($mmAppProvider, mmCoreConfigStore) {
    var stores = [
        {
            name: mmCoreConfigStore,
            keyPath: 'name'
        }
    ];
    $mmAppProvider.registerStores(stores);
}])
.factory('$mmConfig', ["$q", "$log", "$mmApp", "mmCoreConfigStore", function($q, $log, $mmApp, mmCoreConfigStore) {
    $log = $log.getInstance('$mmConfig');
    var self = {};
        self.get = function(name, defaultValue) {
        return $mmApp.getDB().get(mmCoreConfigStore, name).then(function(entry) {
            return entry.value;
        }).catch(function() {
            if (typeof defaultValue != 'undefined') {
                return defaultValue;
            } else {
                return $q.reject();
            }
        });
    };
        self.set = function(name, value) {
        return $mmApp.getDB().insert(mmCoreConfigStore, {name: name, value: value});
    };
        self.delete = function(name) {
        return $mmApp.getDB().remove(mmCoreConfigStore, name);
    };
    return self;
}]);

angular.module('mm.core')
.factory('$mmDB', ["$q", "$log", function($q, $log) {
    $log = $log.getInstance('$mmDB');
    var self = {},
        dbInstances = {};
        function applyOrder(query, order, reverse) {
        if (order) {
            query = query.order(order);
            if (reverse) {
                query = query.reverse();
            }
        }
        return query;
    }
        function applyWhere(query, where) {
        if (where && where.length > 0) {
            query = query.where.apply(query, where);
        }
        return query;
    }
        function callDBFunction(db, func) {
        var deferred = $q.defer();
        try {
            if (typeof(db) != 'undefined') {
                db[func].apply(db, Array.prototype.slice.call(arguments, 2)).then(function(result) {
                    if (typeof(result) == 'undefined') {
                        deferred.reject();
                    } else {
                        deferred.resolve(result);
                    }
                });
            } else {
                deferred.reject();
            }
        } catch(ex) {
            $log.error('Error executing function '+func+' to DB '+db.getName());
            $log.error(ex.name+': '+ex.message);
            deferred.reject();
        }
        return deferred.promise;
    }
        function callCount(db, store, where) {
        var deferred = $q.defer(),
            query;
        try {
            if (typeof(db) != 'undefined') {
                query = db.from(store);
                query = applyWhere(query, where);
                query.count().then(function(count) {
                    deferred.resolve(count);
                }, function() {
                    deferred.reject();
                });
            } else {
                deferred.reject();
            }
        } catch(ex) {
            $log.error('Error querying db '+db.getName()+'. '+ex.name+': '+ex.message);
            deferred.reject();
        }
        return deferred.promise;
    }
        function callWhere(db, store, field_name, op, value, op2, value2) {
        var deferred = $q.defer();
        try {
            if (typeof(db) != 'undefined') {
                db.from(store).where(field_name, op, value, op2, value2).list().then(function(list) {
                    deferred.resolve(list);
                }, function() {
                    deferred.reject();
                });
            } else {
                deferred.reject();
            }
        } catch(ex) {
            $log.error('Error querying db '+db.getName()+'. '+ex.name+': '+ex.message);
            deferred.reject();
        }
        return deferred.promise;
    }
        function callWhereEqual(db, store, field_name, value) {
        var deferred = $q.defer();
        try {
            if (typeof(db) != 'undefined') {
                db.from(store).where(field_name, '=', value).list().then(function(list) {
                    deferred.resolve(list);
                }, function() {
                    deferred.reject();
                });
            } else {
                deferred.reject();
            }
        } catch(ex) {
            $log.error('Error getting where equal from db '+db.getName()+'. '+ex.name+': '+ex.message);
            deferred.reject();
        }
        return deferred.promise;
    }
        function callEach(db, store, callback) {
        var deferred = $q.defer();
        callDBFunction(db, 'values', store, undefined, 99999999).then(function(entries) {
            for (var i = 0; i < entries.length; i++) {
                callback(entries[i]);
            }
            deferred.resolve();
        }, function() {
            deferred.reject();
        });
        return deferred.promise;
    }
        function doQuery(db, store, where, order, reverse, limit) {
        var deferred = $q.defer(),
            query;
        try {
            if (typeof(db) != 'undefined') {
                query = db.from(store);
                query = applyWhere(query, where);
                query = applyOrder(query, order, reverse);
                query.list(limit).then(function(list) {
                    deferred.resolve(list);
                }, function() {
                    deferred.reject();
                });
            } else {
                deferred.reject();
            }
        } catch(ex) {
            $log.error('Error querying ' + store + ' on ' + db.getName() + '. ' + ex.name + ': ' + ex.message);
            deferred.reject();
        }
        return deferred.promise;
    }
        function doUpdate(db, store, values, where) {
        var deferred = $q.defer(),
            query;
        try {
            if (typeof(db) != 'undefined') {
                query = db.from(store);
                query = applyWhere(query, where);
                query.patch(values).then(function(count) {
                    deferred.resolve(count);
                }, function() {
                    deferred.reject();
                });
            } else {
                deferred.reject();
            }
        } catch(ex) {
            $log.error('Error querying ' + store + ' on ' + db.getName() + '. ' + ex.name + ': ' + ex.message);
            deferred.reject();
        }
        return deferred.promise;
    }
        self.getDB = function(name, schema, options) {
        if (typeof dbInstances[name] === 'undefined') {
            var isSafari = !ionic.Platform.isIOS() && !ionic.Platform.isAndroid() && navigator.userAgent.indexOf('Safari') != -1
                            && navigator.userAgent.indexOf('Chrome') == -1 && navigator.userAgent.indexOf('Firefox') == -1;
            if (typeof IDBObjectStore == 'undefined' || typeof IDBObjectStore.prototype.count == 'undefined' || isSafari) {
                if (typeof options.mechanisms == 'undefined') {
                    options.mechanisms = ['websql', 'sqlite', 'localstorage', 'sessionstorage', 'userdata', 'memory'];
                } else {
                    var position = options.mechanisms.indexOf('indexeddb');
                    if (position != -1) {
                        options.mechanisms.splice(position, 1);
                    }
                }
            }
            var db = new ydn.db.Storage(name, schema, options);
            dbInstances[name] = {
                                getName: function() {
                    return db.getName();
                },
                                get: function(store, id) {
                    return callDBFunction(db, 'get', store, id);
                },
                                getAll: function(store) {
                    return callDBFunction(db, 'values', store, undefined, 99999999);
                },
                                count: function(store, where) {
                    return callCount(db, store, where);
                },
                                insert: function(store, value, id) {
                    return callDBFunction(db, 'put', store, value, id);
                },
                                insertSync: function(store, value) {
                    if (db) {
                        try {
                            db.put(store, value);
                            return true;
                        } catch(ex) {
                            $log.error('Error executing function sync put to DB '+db.getName());
                            $log.error(ex.name+': '+ex.message);
                        }
                    }
                    return false;
                },
                                query: function(store, where, order, reverse, limit) {
                    return doQuery(db, store, where, order, reverse, limit);
                },
                                remove: function(store, id) {
                    return callDBFunction(db, 'remove', store, id);
                },
                                removeAll: function(store) {
                    return callDBFunction(db, 'clear', store);
                },
                                update: function(store, values, where) {
                    return doUpdate(db, store, values, where);
                },
                                where: function(store, field_name, op, value, op2, value2) {
                    return callWhere(db, store, field_name, op, value, op2, value2);
                },
                                whereEqual: function(store, field_name, value) {
                    return callWhereEqual(db, store, field_name, value);
                },
                                each: function(store, callback) {
                    return callEach(db, store, callback);
                },
                                close: function() {
                    db.close();
                    db = undefined;
                },
                                onReady: function(cb) {
                    db.onReady(cb);
                },
                                getType: function() {
                    return db.getType();
                }
            };
        }
        return dbInstances[name];
    };
        self.deleteDB = function(name) {
        var deferred = $q.defer();
        function deleteDB() {
            delete dbInstances[name];
            $q.when(ydn.db.deleteDatabase(name)).then(deferred.resolve, deferred.reject);
        }
        if (typeof dbInstances[name] != 'undefined') {
            dbInstances[name].onReady(deleteDB);
        } else {
            deleteDB();
        }
        return deferred.promise;
    };
    return self;
}]);

angular.module('mm.core')
.factory('$mmEmulatorManager', ["$log", "$q", "$http", "$mmFS", "$window", function($log, $q, $http, $mmFS, $window) {
    $log = $log.getInstance('$mmEmulatorManager');
    var self = {};
        self.loadHTMLAPI = function() {
        if ($mmFS.isAvailable()) {
            $log.debug('Stop loading HTML API, it was already loaded or the environment doesn\'t need it.');
            return $q.when();
        }
        var deferred = $q.defer(),
            basePath;
        $log.debug('Loading HTML API.');
        $window.requestFileSystem  = $window.requestFileSystem || $window.webkitRequestFileSystem;
        $window.resolveLocalFileSystemURL = $window.resolveLocalFileSystemURL || $window.webkitResolveLocalFileSystemURL;
        $window.LocalFileSystem = {
            PERSISTENT: 1
        };
        $window.FileTransfer = function() {};
        $window.FileTransfer.prototype.download = function(url, filePath, successCallback, errorCallback) {
            $http.get(url, {responseType: 'blob'}).then(function(data) {
                if (!data || !data.data) {
                    errorCallback();
                } else {
                    filePath = filePath.replace(basePath, '');
                    filePath = filePath.replace(/%20/g, ' ');
                    $mmFS.writeFile(filePath, data.data).then(function(e) {
                        successCallback(e);
                    }).catch(function(error) {
                        errorCallback(error);
                    });
                }
            }).catch(function(error) {
                errorCallback(error);
            });
        };
        $window.zip = {
            unzip: function(source, destination, callback, progressCallback) {
                source = source.replace(basePath, '');
                source = source.replace(/%20/g, ' ');
                destination = destination.replace(basePath, '');
                destination = destination.replace(/%20/g, ' ');
                $mmFS.readFile(source, $mmFS.FORMATARRAYBUFFER).then(function(data) {
                    var zip = new JSZip(data),
                        promises = [];
                    angular.forEach(zip.files, function(file, name) {
                        var filepath = $mmFS.concatenatePaths(destination, name),
                            type;
                        if (!file.dir) {
                            type = $mmFS.getMimeType($mmFS.getFileExtension(name));
                            promises.push($mmFS.writeFile(filepath, new Blob([file.asArrayBuffer()], {type: type})));
                        } else {
                            promises.push($mmFS.createDir(filepath));
                        }
                    });
                    return $q.all(promises).then(function() {
                        callback(0);
                    });
                }).catch(function() {
                    callback(-1);
                });
            }
        };
        $window.webkitStorageInfo.requestQuota(PERSISTENT, 500 * 1024 * 1024, function(granted) {
            $window.requestFileSystem(PERSISTENT, granted, function(entry) {
                basePath = entry.root.toURL();
                $mmFS.setHTMLBasePath(basePath);
                deferred.resolve();
            }, deferred.reject);
        }, deferred.reject);
        return deferred.promise;
    };
    return self;
}])
.config(["$mmInitDelegateProvider", "mmInitDelegateMaxAddonPriority", function($mmInitDelegateProvider, mmInitDelegateMaxAddonPriority) {
    if (!ionic.Platform.isWebView()) {
        $mmInitDelegateProvider.registerProcess('mmEmulator', '$mmEmulatorManager.loadHTMLAPI',
                mmInitDelegateMaxAddonPriority + 500, true);
    }
}]);

angular.module('mm.core')
.constant('mmCoreEventKeyboardShow', 'keyboard_show')
.constant('mmCoreEventKeyboardHide', 'keyboard_hide')
.constant('mmCoreEventSessionExpired', 'session_expired')
.constant('mmCoreEventLogin', 'login')
.constant('mmCoreEventLogout', 'logout')
.constant('mmCoreEventLanguageChanged', 'language_changed')
.constant('mmCoreEventSiteAdded', 'site_added')
.constant('mmCoreEventSiteUpdated', 'site_updated')
.constant('mmCoreEventSiteDeleted', 'site_deleted')
.constant('mmCoreEventQueueEmpty', 'filepool_queue_empty')
.constant('mmCoreEventCompletionModuleViewed', 'completion_module_viewed')
.constant('mmCoreEventUserDeleted', 'user_deleted')
.constant('mmCoreEventPackageStatusChanged', 'filepool_package_status_changed')
.constant('mmCoreEventSectionStatusChanged', 'section_status_changed')
.factory('$mmEvents', ["$log", "md5", function($log, md5) {
    $log = $log.getInstance('$mmEvents');
    var self = {},
        observers = {},
        uniqueEvents = {},
        uniqueEventsData = {};
        self.on = function(eventName, callBack) {
        if (uniqueEvents[eventName]) {
            callBack(uniqueEventsData[eventName]);
            return {
                id: -1,
                off: function() {}
            };
        }
        var observerID;
        if (typeof(observers[eventName]) === 'undefined') {
            observers[eventName] = {};
        }
        while (typeof(observerID) === 'undefined') {
            var candidateID = md5.createHash(Math.random().toString());
            if (typeof(observers[eventName][candidateID]) === 'undefined') {
                observerID = candidateID;
            }
        }
        $log.debug('Observer ' + observerID + ' listening to event '+eventName);
        observers[eventName][observerID] = callBack;
        var observer = {
            id: observerID,
            off: function() {
                $log.debug('Disable observer ' + observerID + ' for event '+eventName);
                delete observers[eventName][observerID];
            }
        };
        return observer;
    };
        self.trigger = function(eventName, data) {
        $log.debug('Event ' + eventName + ' triggered.');
        var affected = observers[eventName];
        for (var observerName in affected) {
            if (typeof(affected[observerName]) === 'function') {
                affected[observerName](data);
            }
        }
    };
        self.triggerUnique = function(eventName, data) {
        if (uniqueEvents[eventName]) {
            $log.debug('Unique event ' + eventName + ' ignored because it was already triggered.');
        } else {
            $log.debug('Unique event ' + eventName + ' triggered.');
            uniqueEvents[eventName] = true;
            uniqueEventsData[eventName] = data;
            var affected = observers[eventName];
            angular.forEach(affected, function(callBack) {
                if (typeof callBack === 'function') {
                    callBack(data);
                }
            });
        }
    };
    return self;
}]);

angular.module('mm.core')
.constant('mmFilepoolQueueProcessInterval', 0)
.constant('mmFilepoolFolder', 'filepool')
.constant('mmFilepoolStore', 'filepool')
.constant('mmFilepoolQueueStore', 'files_queue')
.constant('mmFilepoolLinksStore', 'files_links')
.constant('mmFilepoolPackagesStore', 'filepool_packages')
.constant('mmFilepoolWifiDownloadThreshold', 20971520)
.constant('mmFilepoolDownloadThreshold', 2097152)
.config(["$mmAppProvider", "$mmSitesFactoryProvider", "mmFilepoolStore", "mmFilepoolLinksStore", "mmFilepoolQueueStore", "mmFilepoolPackagesStore", function($mmAppProvider, $mmSitesFactoryProvider, mmFilepoolStore, mmFilepoolLinksStore, mmFilepoolQueueStore,
            mmFilepoolPackagesStore) {
    var siteStores = [
        {
            name: mmFilepoolStore,
            keyPath: 'fileId',
            indexes: []
        },
        {
            name: mmFilepoolLinksStore,
            keyPath: ['fileId', 'component', 'componentId'],
            indexes: [
                {
                    name: 'fileId',
                },
                {
                    name: 'component',
                },
                {
                    name: 'componentAndId',
                    generator: function(obj) {
                        return [obj.component, obj.componentId];
                    }
                }
            ]
        },
        {
            name: mmFilepoolPackagesStore,
            keyPath: 'id',
            indexes: [
                {
                    name: 'component',
                },
                {
                    name: 'componentId',
                },
                {
                    name: 'status',
                }
            ]
        }
    ];
    var appStores = [
        {
            name: mmFilepoolQueueStore,
            keyPath: ['siteId', 'fileId'],
            indexes: [
                {
                    name: 'siteId',
                },
                {
                    name: 'sortorder',
                    generator: function(obj) {
                        var sortorder = parseInt(obj.added, 10),
                            priority = 999 - Math.max(0, Math.min(parseInt(obj.priority || 0, 10), 999)),
                            padding = "000";
                        sortorder = "" + sortorder;
                        priority = "" + priority;
                        priority = padding.substring(0, padding.length - priority.length) + priority;
                        sortorder = priority + '-' + sortorder;
                        return sortorder;
                    }
                }
            ]
        }
    ];
    $mmAppProvider.registerStores(appStores);
    $mmSitesFactoryProvider.registerStores(siteStores);
}])
.factory('$mmFilepool', ["$q", "$log", "$timeout", "$mmApp", "$mmFS", "$mmWS", "$mmSitesManager", "$mmEvents", "md5", "mmFilepoolStore", "mmFilepoolLinksStore", "mmFilepoolQueueStore", "mmFilepoolFolder", "mmFilepoolQueueProcessInterval", "mmCoreEventQueueEmpty", "mmCoreDownloaded", "mmCoreDownloading", "mmCoreNotDownloaded", "mmCoreOutdated", "mmCoreNotDownloadable", "mmFilepoolPackagesStore", "mmCoreEventPackageStatusChanged", "$mmText", "$mmUtil", "mmFilepoolWifiDownloadThreshold", "mmFilepoolDownloadThreshold", function($q, $log, $timeout, $mmApp, $mmFS, $mmWS, $mmSitesManager, $mmEvents, md5, mmFilepoolStore,
        mmFilepoolLinksStore, mmFilepoolQueueStore, mmFilepoolFolder, mmFilepoolQueueProcessInterval, mmCoreEventQueueEmpty,
        mmCoreDownloaded, mmCoreDownloading, mmCoreNotDownloaded, mmCoreOutdated, mmCoreNotDownloadable, mmFilepoolPackagesStore,
        mmCoreEventPackageStatusChanged, $mmText, $mmUtil, mmFilepoolWifiDownloadThreshold, mmFilepoolDownloadThreshold) {
    $log = $log.getInstance('$mmFilepool');
    var self = {},
        extensionRegex = new RegExp('^[a-z0-9]+$'),
        tokenRegex = new RegExp('(\\?|&)token=([A-Za-z0-9]+)'),
        queueState,
        urlAttributes = [
            tokenRegex,
            new RegExp('(\\?|&)forcedownload=[0-1]')
        ],
        revisionRegex = new RegExp('/content/([0-9]+)/'),
        queueDeferreds = {},
        packagesPromises = {},
        filePromises = {},
        sizeCache = {};
    var QUEUE_RUNNING = 'mmFilepool:QUEUE_RUNNING',
        QUEUE_PAUSED = 'mmFilepool:QUEUE_PAUSED';
    var ERR_QUEUE_IS_EMPTY = 'mmFilepoolError:ERR_QUEUE_IS_EMPTY',
        ERR_FS_OR_NETWORK_UNAVAILABLE = 'mmFilepoolError:ERR_FS_OR_NETWORK_UNAVAILABLE',
        ERR_QUEUE_ON_PAUSE = 'mmFilepoolError:ERR_QUEUE_ON_PAUSE';
        self.FILEDOWNLOADED = 'downloaded';
    self.FILEDOWNLOADING = 'downloading';
    self.FILENOTDOWNLOADED = 'notdownloaded';
    self.FILEOUTDATED = 'outdated';
        function getSiteDb(siteId) {
        return $mmSitesManager.getSiteDb(siteId);
    }
        self._addFileLink = function(siteId, fileId, component, componentId) {
        if (!component) {
            return $q.reject();
        }
        componentId = self._fixComponentId(componentId);
        return getSiteDb(siteId).then(function(db) {
            return db.insert(mmFilepoolLinksStore, {
                fileId: fileId,
                component: component,
                componentId: componentId
            });
        });
    };
        self.addFileLinkByUrl = function(siteId, fileUrl, component, componentId) {
        return self._fixPluginfileURL(siteId, fileUrl).then(function(fileUrl) {
            var fileId = self._getFileIdByUrl(fileUrl);
            return self._addFileLink(siteId, fileId, component, componentId);
        });
    };
        self._addFileLinks = function(siteId, fileId, links) {
        var promises = [];
        angular.forEach(links, function(link) {
            promises.push(self._addFileLink(siteId, fileId, link.component, link.componentId));
        });
        return $q.all(promises);
    };
        self._addFileToPool = function(siteId, fileId, data) {
        var values = angular.copy(data) || {};
        values.fileId = fileId;
        return getSiteDb(siteId).then(function(db) {
            return db.insert(mmFilepoolStore, values);
        });
    };
        self.addToQueueByUrl = function(siteId, fileUrl, component, componentId, timemodified, filePath, priority) {
        var db = $mmApp.getDB(),
            fileId,
            now = new Date(),
            link,
            revision,
            queueDeferred;
        if (!$mmFS.isAvailable()) {
            return $q.reject();
        }
        return self._fixPluginfileURL(siteId, fileUrl).then(function(fileUrl) {
            timemodified = timemodified || 0;
            revision = self.getRevisionFromUrl(fileUrl);
            fileId = self._getFileIdByUrl(fileUrl);
            priority = priority || 0;
            if (typeof component !== 'undefined') {
                link = {
                    component: component,
                    componentId: componentId
                };
            }
            queueDeferred = self._getQueueDeferred(siteId, fileId, false);
            return db.get(mmFilepoolQueueStore, [siteId, fileId]).then(function(fileObject) {
                var foundLink = false,
                    update = false;
                if (fileObject) {
                    if (fileObject.priority < priority) {
                        update = true;
                        fileObject.priority = priority;
                    }
                    if (revision && fileObject.revision !== revision) {
                        update = true;
                        fileObject.revision = revision;
                    }
                    if (timemodified && fileObject.timemodified !== timemodified) {
                        update = true;
                        fileObject.timemodified = timemodified;
                    }
                    if (filePath && fileObject.path !== filePath) {
                        update = true;
                        fileObject.path = filePath;
                    }
                    if (link) {
                        angular.forEach(fileObject.links, function(fileLink) {
                            if (fileLink.component == link.component && fileLink.componentId == link.componentId) {
                                foundLink = true;
                            }
                        });
                        if (!foundLink) {
                            update = true;
                            fileObject.links.push(link);
                        }
                    }
                    if (update) {
                        $log.debug('Updating file ' + fileId + ' which is already in queue');
                        return db.insert(mmFilepoolQueueStore, fileObject).then(function() {
                            return self._getQueuePromise(siteId, fileId);
                        });
                    }
                    $log.debug('File ' + fileId + ' already in queue and does not require update');
                    if (queueDeferred) {
                        return queueDeferred.promise;
                    } else {
                        return self._getQueuePromise(siteId, fileId);
                    }
                } else {
                    return addToQueue();
                }
            }, function() {
                return addToQueue();
            });
            function addToQueue() {
                $log.debug('Adding ' + fileId + ' to the queue');
                return db.insert(mmFilepoolQueueStore, {
                    siteId: siteId,
                    fileId: fileId,
                    added: now.getTime(),
                    priority: priority,
                    url: fileUrl,
                    revision: revision,
                    timemodified: timemodified,
                    path: filePath,
                    links: link ? [link] : []
                }).then(function() {
                    self.checkQueueProcessing();
                    return self._getQueuePromise(siteId, fileId);
                });
            }
        });
    };
        self.checkQueueProcessing = function() {
        if (!$mmFS.isAvailable() || !$mmApp.isOnline()) {
            queueState = QUEUE_PAUSED;
            return;
        } else if (queueState === QUEUE_RUNNING) {
            return;
        }
        queueState = QUEUE_RUNNING;
        self._processQueue();
    };
        self.clearAllPackagesStatus = function(siteId) {
        var promises = [];
        $log.debug('Clear all packages status for site ' + siteId);
        return $mmSitesManager.getSite(siteId).then(function(site) {
            var db = site.getDb();
            return db.getAll(mmFilepoolPackagesStore).then(function(entries) {
                angular.forEach(entries, function(entry) {
                    promises.push(db.remove(mmFilepoolPackagesStore, entry.id).then(function() {
                        self._triggerPackageStatusChanged(siteId, entry.component, entry.componentId, mmCoreNotDownloaded);
                    }));
                });
                return $q.all(promises);
            });
        });
    };
        self.clearFilepool = function(siteId) {
        return getSiteDb(siteId).then(function(db) {
            return db.removeAll(mmFilepoolStore);
        });
    };
        self.componentHasFiles = function(siteId, component, componentId) {
        return getSiteDb(siteId).then(function(db) {
            var where;
            if (typeof componentId !== 'undefined') {
                where = ['componentAndId', '=', [component, self._fixComponentId(componentId)]];
            } else {
                where = ['component', '=', component];
            }
            return db.count(mmFilepoolLinksStore, where).then(function(count) {
                if (count > 0) {
                    return true;
                }
                return $q.reject();
            });
        });
    };
        self.determinePackagesStatus = function(current, packagestatus) {
        if (!current) {
            current = mmCoreNotDownloadable;
        }
        if (packagestatus === mmCoreNotDownloaded) {
            return mmCoreNotDownloaded;
        } else if (packagestatus === mmCoreDownloaded && current === mmCoreNotDownloadable) {
            return mmCoreDownloaded;
        } else if (packagestatus === mmCoreDownloading && (current === mmCoreNotDownloadable || current === mmCoreDownloaded)) {
            return mmCoreDownloading;
        } else if (packagestatus === mmCoreOutdated && current !== mmCoreNotDownloaded) {
            return mmCoreOutdated;
        }
        return current;
    };
        self._downloadOrPrefetchPackage = function(siteId, fileList, prefetch, component, componentId, revision, timemod, dirPath) {
        var packageId = self.getPackageId(component, componentId);
        if (packagesPromises[siteId] && packagesPromises[siteId][packageId]) {
            return packagesPromises[siteId][packageId];
        } else if (!packagesPromises[siteId]) {
            packagesPromises[siteId] = {};
        }
        revision = revision || self.getRevisionFromFileList(fileList);
        timemod = timemod || self.getTimemodifiedFromFileList(fileList);
        var dwnPromise,
            deleted = false;
        dwnPromise = self.storePackageStatus(siteId, component, componentId, mmCoreDownloading, revision, timemod).then(function() {
            var promises = [],
                deferred = $q.defer(),
                packageLoaded = 0;
            angular.forEach(fileList, function(file) {
                var path,
                    promise,
                    fileLoaded = 0;
                if (dirPath) {
                    path = file.filename;
                    if (file.filepath !== '/') {
                        path = file.filepath.substr(1) + path;
                    }
                    path = $mmFS.concatenatePaths(dirPath, path);
                }
                if (prefetch) {
                    promise = self.addToQueueByUrl(siteId, file.fileurl, component, componentId, file.timemodified, path);
                } else {
                    promise = self.downloadUrl(siteId, file.fileurl, false, component, componentId, file.timemodified, path);
                }
                promises.push(promise.then(undefined, undefined, function(progress) {
                    if (progress && progress.loaded) {
                        packageLoaded = packageLoaded + (progress.loaded - fileLoaded);
                        fileLoaded = progress.loaded;
                        deferred.notify({
                            packageDownload: true,
                            loaded: packageLoaded,
                            fileProgress: progress
                        });
                    }
                }));
            });
            $q.all(promises).then(function() {
                return self.storePackageStatus(siteId, component, componentId, mmCoreDownloaded, revision, timemod);
            }).catch(function() {
                return self.setPackagePreviousStatus(siteId, component, componentId).then(function() {
                    return $q.reject();
                });
            }).then(deferred.resolve, deferred.reject);
            return deferred.promise;
        }).finally(function() {
            delete packagesPromises[siteId][packageId];
            deleted = true;
        });
        if (!deleted) {
            packagesPromises[siteId][packageId] = dwnPromise;
        }
        return dwnPromise;
    };
        self.downloadPackage = function(siteId, fileList, component, componentId, revision, timemodified, dirPath) {
        return self._downloadOrPrefetchPackage(siteId, fileList, false, component, componentId, revision, timemodified, dirPath);
    };
        self.downloadUrl = function(siteId, fileUrl, ignoreStale, component, componentId, timemodified, filePath) {
        var fileId,
            revision,
            promise;
        if ($mmFS.isAvailable()) {
            return self._fixPluginfileURL(siteId, fileUrl).then(function(fixedUrl) {
                fileUrl = fixedUrl;
                timemodified = timemodified || 0;
                revision = self.getRevisionFromUrl(fileUrl);
                fileId = self._getFileIdByUrl(fileUrl);
                return self._restoreOldFileIfNeeded(siteId, fileId, fileUrl, filePath);
            }).then(function() {
                return self._hasFileInPool(siteId, fileId).then(function(fileObject) {
                    if (typeof fileObject === 'undefined') {
                        return self._downloadForPoolByUrl(siteId, fileUrl, revision, timemodified, filePath);
                    } else if (self._isFileOutdated(fileObject, revision, timemodified) && $mmApp.isOnline() && !ignoreStale) {
                        return self._downloadForPoolByUrl(siteId, fileUrl, revision, timemodified, filePath, fileObject);
                    }
                    if (filePath) {
                        promise = self._getInternalUrlByPath(filePath);
                    } else {
                        promise = self._getInternalUrlById(siteId, fileId);
                    }
                    return promise.then(function(response) {
                        return response;
                    }, function() {
                        return self._downloadForPoolByUrl(siteId, fileUrl, revision, timemodified, filePath, fileObject);
                    });
                }, function() {
                    return self._downloadForPoolByUrl(siteId, fileUrl, revision, timemodified, filePath);
                })
                .then(function(response) {
                    if (typeof component !== 'undefined') {
                        self._addFileLink(siteId, fileId, component, componentId);
                    }
                    self._notifyFileDownloaded(siteId, fileId);
                    return response;
                }, function(err) {
                    self._notifyFileDownloadError(siteId, fileId);
                    return $q.reject(err);
                });
            });
        } else {
            return $q.reject();
        }
    };
        self._downloadForPoolByUrl = function(siteId, fileUrl, revision, timemodified, filePath, poolFileObject) {
        var fileId = self._getFileIdByUrl(fileUrl);
        filePath = filePath || self._getFilePath(siteId, fileId);
        if (poolFileObject && poolFileObject.fileId !== fileId) {
            $log.error('Invalid object to update passed');
            return $q.reject();
        }
        var downloadId = self.getFileDownloadId(fileUrl, filePath),
            deleted = false,
            promise;
        if (filePromises[siteId] && filePromises[siteId][downloadId]) {
            return filePromises[siteId][downloadId];
        } else if (!filePromises[siteId]) {
            filePromises[siteId] = {};
        }
        promise = $mmSitesManager.getSite(siteId).then(function(site) {
            if (!site.canDownloadFiles()) {
                return $q.reject();
            }
            return $mmWS.downloadFile(fileUrl, filePath).then(function(fileEntry) {
                var now = new Date(),
                    data = poolFileObject || {};
                data.downloaded = now.getTime();
                data.stale = false;
                data.url = fileUrl;
                data.revision = revision;
                data.timemodified = timemodified;
                data.path = filePath;
                return self._addFileToPool(siteId, fileId, data).then(function() {
                    return fileEntry.toURL();
                });
            });
        }).finally(function() {
            delete filePromises[siteId][downloadId];
            deleted = true;
        });
        if (!deleted) {
            filePromises[siteId][downloadId] = promise;
        }
        return promise;
    };
        self._fixComponentId = function(componentId) {
        var id = parseInt(componentId, 10);
        if (isNaN(id)) {
            return -1;
        }
        return id;
    };
        self._fixPluginfileURL = function(siteId, fileUrl) {
        return $mmSitesManager.getSite(siteId).then(function(site) {
            return site.fixPluginfileURL(fileUrl);
        });
    };
        self._getFileLinks = function(siteId, fileId) {
        return getSiteDb(siteId).then(function(db) {
            return db.query(mmFilepoolLinksStore, ['fileId', '=', fileId]);
        });
    };
        self.getFileDownloadId = function(fileUrl, filePath) {
        return md5.createHash(fileUrl + '###' + filePath);
    };
        self._getFileEventName = function(siteId, fileId) {
        return 'mmFilepoolFile:'+siteId+':'+fileId;
    };
        self.getFileEventNameByUrl = function(siteId, fileUrl) {
        return self._fixPluginfileURL(siteId, fileUrl).then(function(fileUrl) {
            var fileId = self._getFileIdByUrl(fileUrl);
            return self._getFileEventName(siteId, fileId);
        });
    };
        self.getPackageDownloadPromise = function(siteId, component, componentId) {
        var packageId = self.getPackageId(component, componentId);
        if (packagesPromises[siteId] && packagesPromises[siteId][packageId]) {
            return packagesPromises[siteId][packageId];
        }
    };
        self.getPackageId = function(component, componentId) {
        return md5.createHash(component + '#' + self._fixComponentId(componentId));
    };
        self.getPackagePreviousStatus = function(siteId, component, componentId) {
        return $mmSitesManager.getSite(siteId).then(function(site) {
            var db = site.getDb(),
                packageId = self.getPackageId(component, componentId);
            return db.get(mmFilepoolPackagesStore, packageId).then(function(entry) {
                return entry.previous || mmCoreNotDownloaded;
            }, function() {
                return mmCoreNotDownloaded;
            });
        });
    };
        self.getPackageStatus = function(siteId, component, componentId, revision, timemodified) {
        revision = revision || 0;
        timemodified = timemodified || 0;
        return $mmSitesManager.getSite(siteId).then(function(site) {
            var db = site.getDb(),
                packageId = self.getPackageId(component, componentId);
            return db.get(mmFilepoolPackagesStore, packageId).then(function(entry) {
                if (entry.status === mmCoreDownloaded) {
                    if (revision != entry.revision || timemodified > entry.timemodified) {
                        entry.status = mmCoreOutdated;
                        entry.updated = new Date().getTime();
                        db.insert(mmFilepoolPackagesStore, entry).then(function() {
                            self._triggerPackageStatusChanged(siteId, component, componentId, mmCoreOutdated);
                        });
                    }
                } else if (entry.status === mmCoreOutdated) {
                    if (revision === entry.revision && timemodified === entry.timemodified) {
                        entry.status = mmCoreDownloaded;
                        entry.updated = new Date().getTime();
                        db.insert(mmFilepoolPackagesStore, entry).then(function() {
                            self._triggerPackageStatusChanged(siteId, component, componentId, mmCoreDownloaded);
                        });
                    }
                }
                return entry.status;
            }, function() {
                return mmCoreNotDownloaded;
            });
        });
    };
        self._getQueueDeferred = function(siteId, fileId, create) {
        if (typeof create == 'undefined') {
            create = true;
        }
        if (!queueDeferreds[siteId]) {
            if (!create) {
                return;
            }
            queueDeferreds[siteId] = {};
        }
        if (!queueDeferreds[siteId][fileId]) {
            if (!create) {
                return;
            }
            queueDeferreds[siteId][fileId] = $q.defer();
        }
        return queueDeferreds[siteId][fileId];
    };
        self._getQueuePromise = function(siteId, fileId, create) {
        return self._getQueueDeferred(siteId, fileId, create).promise;
    };
        self._hasFileInPool = function(siteId, fileId) {
        return getSiteDb(siteId).then(function(db) {
            return db.get(mmFilepoolStore, fileId).then(function(fileObject) {
                if (typeof fileObject === 'undefined') {
                    return $q.reject();
                }
                return fileObject;
            });
        });
    };
        self._hasFileInQueue = function(siteId, fileId) {
        return $mmApp.getDB().get(mmFilepoolQueueStore, [siteId, fileId]).then(function(fileObject) {
            if (typeof fileObject === 'undefined') {
                return $q.reject();
            }
            return fileObject;
        });
    };
        self.getDirectoryUrlByUrl = function(siteId, fileUrl) {
        if ($mmFS.isAvailable()) {
            return self._fixPluginfileURL(siteId, fileUrl).then(function(fileUrl) {
                var fileId = self._getFileIdByUrl(fileUrl);
                return $mmFS.getDir(self._getFilePath(siteId, fileId)).then(function(dirEntry) {
                    return dirEntry.toURL();
                });
            });
        }
        return $q.reject();
    };
        self._getFileIdByUrl = function(fileUrl) {
        var url = self._removeRevisionFromUrl(fileUrl),
            filename,
            candidate,
            extension = '';
        url = $mmText.decodeHTML(decodeURIComponent(url));
        if (url.indexOf('/webservice/pluginfile') !== -1) {
            angular.forEach(urlAttributes, function(regex) {
                url = url.replace(regex, '');
            });
        }
        filename = self._guessFilenameFromUrl(url);
        candidate = self._guessExtensionFromUrl(url);
        if (candidate && candidate !== 'php') {
            extension = '.' + candidate;
        }
        return filename + '_' + md5.createHash('url:' + url) + extension;
    };
        self._getNonReadableFileIdByUrl = function(fileUrl) {
        var url = self._removeRevisionFromUrl(fileUrl),
            candidate,
            extension = '';
        if (url.indexOf('/webservice/pluginfile') !== -1) {
            angular.forEach(urlAttributes, function(regex) {
                url = url.replace(regex, '');
            });
            candidate = self._guessExtensionFromUrl(url);
            if (candidate && candidate !== 'php') {
                extension = '.' + candidate;
            }
        }
        return md5.createHash('url:' + url) + extension;
    };
        self._getFileUrlByUrl = function(siteId, fileUrl, mode, component, componentId, timemodified, checkSize) {
        var fileId,
            revision;
        if (typeof checkSize == 'undefined') {
            checkSize = true;
        }
        return self._fixPluginfileURL(siteId, fileUrl).then(function(fixedUrl) {
            fileUrl = fixedUrl;
            timemodified = timemodified || 0;
            revision = self.getRevisionFromUrl(fileUrl);
            fileId = self._getFileIdByUrl(fileUrl);
            return self._restoreOldFileIfNeeded(siteId, fileId, fileUrl);
        }).then(function() {
            return self._hasFileInPool(siteId, fileId).then(function(fileObject) {
                var response,
                    fn;
                if (typeof fileObject === 'undefined') {
                    addToQueueIfNeeded();
                    response = fileUrl;
                } else if (self._isFileOutdated(fileObject, revision, timemodified) && $mmApp.isOnline()) {
                    addToQueueIfNeeded();
                    response = fileUrl;
                } else {
                    if (mode === 'src') {
                        fn = self._getInternalSrcById;
                    } else {
                        fn = self._getInternalUrlById;
                    }
                    response = fn(siteId, fileId).then(function(internalUrl) {
                        return internalUrl;
                    }, function() {
                        $log.debug('File ' + fileId + ' not found on disk');
                        self._removeFileById(siteId, fileId);
                        addToQueueIfNeeded();
                        if ($mmApp.isOnline()) {
                            return fileUrl;
                        }
                        return $q.reject();
                    });
                }
                return response;
            }, function() {
                addToQueueIfNeeded();
                return fileUrl;
            });
        });
        function addToQueueIfNeeded() {
            var promise;
            if (checkSize) {
                if (!$mmApp.isOnline()) {
                    return;
                }
                if (typeof sizeCache[fileUrl] != 'undefined') {
                    promise = $q.when(sizeCache[fileUrl]);
                } else {
                    promise = $mmWS.getRemoteFileSize(fileUrl);
                }
                promise.then(function(size) {
                    if (size > 0) {
                        sizeCache[fileUrl] = size;
                        var isWifi = !$mmApp.isNetworkAccessLimited();
                        if (size <= mmFilepoolDownloadThreshold || (isWifi && size <= mmFilepoolWifiDownloadThreshold)) {
                            self.addToQueueByUrl(siteId, fileUrl, component, componentId, timemodified);
                        }
                    }
                });
            } else {
                self.addToQueueByUrl(siteId, fileUrl, component, componentId, timemodified);
            }
        }
    };
        self._getFilePath = function(siteId, fileId) {
        return $mmFS.getSiteFolder(siteId) + '/' + mmFilepoolFolder + '/' + fileId;
    };
        self.getFilePathByUrl = function(siteId, fileUrl) {
        return self._fixPluginfileURL(siteId, fileUrl).then(function(fileUrl) {
            var fileId = self._getFileIdByUrl(fileUrl);
            return self._getFilePath(siteId, fileId);
        });
    };
        self.getFileStateByUrl = function(siteId, fileUrl, timemodified) {
        var fileId,
            revision;
        return self._fixPluginfileURL(siteId, fileUrl).then(function(fixedUrl) {
            fileUrl = fixedUrl;
            timemodified = timemodified || 0;
            revision = self.getRevisionFromUrl(fileUrl);
            fileId = self._getFileIdByUrl(fileUrl);
            return self._restoreOldFileIfNeeded(siteId, fileId, fileUrl);
        }).then(function() {
            return self._hasFileInQueue(siteId, fileId).then(function() {
                return mmCoreDownloading;
            }, function() {
                return self._hasFileInPool(siteId, fileId).then(function(fileObject) {
                    if (self._isFileOutdated(fileObject, revision, timemodified)) {
                        return mmCoreOutdated;
                    } else {
                        return mmCoreDownloaded;
                    }
                }, function() {
                    return mmCoreNotDownloaded;
                });
            });
        });
    };
        self._getInternalSrcById = function(siteId, fileId) {
        if ($mmFS.isAvailable()) {
            return $mmFS.getFile(self._getFilePath(siteId, fileId)).then(function(fileEntry) {
                return $mmFS.getInternalURL(fileEntry);
            });
        }
        return $q.reject();
    };
        self._getInternalUrlById = function(siteId, fileId) {
        if ($mmFS.isAvailable()) {
            return $mmFS.getFile(self._getFilePath(siteId, fileId)).then(function(fileEntry) {
                return fileEntry.toURL();
            });
        }
        return $q.reject();
    };
        self._getInternalUrlByPath = function(filePath) {
        if ($mmFS.isAvailable()) {
            return $mmFS.getFile(filePath).then(function(fileEntry) {
                return fileEntry.toURL();
            });
        }
        return $q.reject();
    };
        self.getPackageDirPathByUrl = function(siteId, url) {
        return self._fixPluginfileURL(siteId, url).then(function(fixedUrl) {
            var fileId = self._getNonReadableFileIdByUrl(fixedUrl);
            return self._getFilePath(siteId, fileId);
        });
    };
        self.getPackageDirUrlByUrl = function(siteId, url) {
        if ($mmFS.isAvailable()) {
            return self._fixPluginfileURL(siteId, url).then(function(fixedUrl) {
                var fileId = self._getNonReadableFileIdByUrl(fixedUrl);
                return $mmFS.getDir(self._getFilePath(siteId, fileId)).then(function(dirEntry) {
                    return dirEntry.toURL();
                });
            });
        }
        return $q.reject();
    };
        self.getRevisionFromFileList = function(files) {
        var revision = 0;
        angular.forEach(files, function(file) {
            if (file.fileurl) {
                var r = self.getRevisionFromUrl(file.fileurl);
                if (r > revision) {
                    revision = r;
                }
            }
        });
        return revision;
    };
        self.getRevisionFromUrl = function(url) {
        var matches = url.match(revisionRegex);
        if (matches && typeof matches[1] != 'undefined') {
            return parseInt(matches[1]);
        }
    };
        self.getSrcByUrl = function(siteId, fileUrl, component, componentId, timemodified, checkSize) {
        return self._getFileUrlByUrl(siteId, fileUrl, 'src', component, componentId, timemodified, checkSize);
    };
        self.getTimemodifiedFromFileList = function(files) {
        var timemod = 0;
        angular.forEach(files, function(file) {
            if (file.timemodified > timemod) {
                timemod = file.timemodified;
            }
        });
        return timemod;
    };
        self.getUrlByUrl = function(siteId, fileUrl, component, componentId, timemodified, checkSize) {
        return self._getFileUrlByUrl(siteId, fileUrl, 'url', component, componentId, timemodified, checkSize);
    };
        self._guessExtensionFromUrl = function(fileUrl) {
        var split = fileUrl.split('.'),
            candidate,
            extension;
        if (split.length > 1) {
            candidate = split.pop().toLowerCase();
            if (extensionRegex.test(candidate)) {
                extension = candidate;
            }
        }
        return extension;
    };
        self._guessFilenameFromUrl = function(fileUrl) {
        var filename = '';
        if (fileUrl.indexOf('/webservice/pluginfile') !== -1) {
            var params = $mmUtil.extractUrlParams(fileUrl);
            if (params.file) {
                filename = params.file.substr(params.file.lastIndexOf('/') + 1);
            } else {
                filename = $mmText.getLastFileWithoutParams(fileUrl);
            }
        } else if ($mmUtil.isGravatarUrl(fileUrl)) {
            filename = 'gravatar_' + $mmText.getLastFileWithoutParams(fileUrl);
        } else if ($mmUtil.isThemeImageUrl(fileUrl)) {
            var matches = fileUrl.match(/clean\/core\/([^\/]*)\//);
            if (matches && matches[1]) {
                filename = matches[1];
            }
            filename = 'default_' + filename + '_' + $mmText.getLastFileWithoutParams(fileUrl);
        } else {
            filename = $mmText.getLastFileWithoutParams(fileUrl);
        }
        var position = filename.lastIndexOf('.');
        if (position != -1) {
            filename = filename.substr(0, position);
        }
        return $mmText.removeSpecialCharactersForFiles(filename);
    };
        self.invalidateAllFiles = function(siteId) {
        return getSiteDb(siteId).then(function(db) {
            return db.getAll(mmFilepoolStore).then(function(items) {
                var promises = [];
                angular.forEach(items, function(item) {
                    item.stale = true;
                    promises.push(db.insert(mmFilepoolStore, item));
                });
                return $q.all(promises);
            });
        });
    };
        self.invalidateFileByUrl = function(siteId, fileUrl) {
        return self._fixPluginfileURL(siteId, fileUrl).then(function(fileUrl) {
            var fileId = self._getFileIdByUrl(fileUrl);
            return getSiteDb(siteId).then(function(db) {
                return db.get(mmFilepoolStore, fileId).then(function(fileObject) {
                    if (!fileObject) {
                        return;
                    }
                    fileObject.stale = true;
                    return db.insert(mmFilepoolStore, fileObject);
                });
            });
        });
    };
        self.invalidateFilesByComponent = function(siteId, component, componentId) {
        var values = { stale: true },
            where;
        if (typeof componentId !== 'undefined') {
            where = ['componentAndId', '=', [component, self._fixComponentId(componentId)]];
        } else {
            where = ['component', '=', component];
        }
        return getSiteDb(siteId).then(function(db) {
            return db.query(mmFilepoolLinksStore, where).then(function(items) {
                var promise,
                    promises = [];
                angular.forEach(items, function(item) {
                    promise = db.get(mmFilepoolStore, item.fileId).then(function(fileEntry) {
                        if (!fileEntry) {
                            return;
                        }
                        fileEntry.stale = true;
                        return db.insert(mmFilepoolStore, fileEntry);
                    });
                    promises.push(promise);
                });
                return $q.all(promises);
            });
        });
    };
        self.isFileDownloadingByUrl = function(siteId, fileUrl) {
        return self._fixPluginfileURL(siteId, fileUrl).then(function(fileUrl) {
            fileId = self._getFileIdByUrl(fileUrl);
            return self._hasFileInQueue(siteId, fileId);
        });
    };
        self._isFileOutdated = function(fileObject, revision, timemodified) {
        return fileObject.stale || revision > fileObject.revision || timemodified > fileObject.timemodified;
    };
        self._notifyFileDownloaded = function(siteId, fileId) {
        $mmEvents.trigger(self._getFileEventName(siteId, fileId), {success: true});
    };
        self._notifyFileDownloadError = function(siteId, fileId) {
        $mmEvents.trigger(self._getFileEventName(siteId, fileId), {success: false});
    };
        self.prefetchPackage = function(siteId, fileList, component, componentId, revision, timemodified, dirPath) {
        return self._downloadOrPrefetchPackage(siteId, fileList, true, component, componentId, revision, timemodified, dirPath);
    };
        self._processQueue = function() {
        var deferred = $q.defer(),
            promise;
        if (queueState !== QUEUE_RUNNING) {
            deferred.reject(ERR_QUEUE_ON_PAUSE);
            promise = deferred.promise;
        } else if (!$mmFS.isAvailable() || !$mmApp.isOnline()) {
            deferred.reject(ERR_FS_OR_NETWORK_UNAVAILABLE);
            promise = deferred.promise;
        } else {
            promise = self._processImportantQueueItem();
        }
        promise.then(function() {
            $timeout(self._processQueue, mmFilepoolQueueProcessInterval);
        }, function(error) {
            if (error === ERR_FS_OR_NETWORK_UNAVAILABLE) {
                $log.debug('Filesysem or network unavailable, pausing queue processing.');
            } else if (error === ERR_QUEUE_IS_EMPTY) {
                $log.debug('Queue is empty, pausing queue processing.');
                $mmEvents.trigger(mmCoreEventQueueEmpty);
            }
            queueState = QUEUE_PAUSED;
        });
    };
        self._processImportantQueueItem = function() {
        return $mmApp.getDB().query(mmFilepoolQueueStore, undefined, 'sortorder', undefined, 1)
        .then(function(items) {
            var item = items.pop();
            if (!item) {
                return $q.reject(ERR_QUEUE_IS_EMPTY);
            }
            return self._processQueueItem(item);
        }, function() {
            return $q.reject(ERR_QUEUE_IS_EMPTY);
        });
    };
        self._processQueueItem = function(item) {
        var siteId = item.siteId,
            fileId = item.fileId,
            fileUrl = item.url,
            revision = item.revision,
            timemodified = item.timemodified,
            filePath = item.path,
            links = item.links || [];
        $log.debug('Processing queue item: ' + siteId + ', ' + fileId);
        return getSiteDb(siteId).then(function(db) {
            return db.get(mmFilepoolStore, fileId).then(function(fileObject) {
                if (fileObject && !self._isFileOutdated(fileObject, revision, timemodified)) {
                    $log.debug('Queued file already in store, ignoring...');
                    self._addFileLinks(siteId, fileId, links);
                    self._removeFromQueue(siteId, fileId).finally(function() {
                        self._treatQueueDeferred(siteId, fileId, true);
                    });
                    self._notifyFileDownloaded(siteId, fileId);
                    return;
                }
                return download(siteId, fileUrl, fileObject, links);
            }, function() {
                return download(siteId, fileUrl, undefined, links);
            });
        }, function() {
            $log.debug('Item dropped from queue due to site DB not retrieved: ' + fileUrl);
            return self._removeFromQueue(siteId, fileId).catch(function() {}).finally(function() {
                self._treatQueueDeferred(siteId, fileId, false);
                self._notifyFileDownloadError(siteId, fileId);
            });
        });
                function download(siteId, fileUrl, fileObject, links) {
            return self._restoreOldFileIfNeeded(siteId, fileId, fileUrl, filePath).then(function() {
                return self._downloadForPoolByUrl(siteId, fileUrl, revision, timemodified, filePath, fileObject).then(function() {
                    var promise;
                    self._addFileLinks(siteId, fileId, links);
                    promise = self._removeFromQueue(siteId, fileId);
                    self._treatQueueDeferred(siteId, fileId, true);
                    self._notifyFileDownloaded(siteId, fileId);
                    return promise.catch(function() {});
                }, function(errorObject) {
                    var dropFromQueue = false;
                    if (typeof errorObject !== 'undefined' && errorObject.source === fileUrl) {
                        if (errorObject.code === 1) {
                            dropFromQueue = true;
                        } else if (errorObject.code === 2) {
                            dropFromQueue = true;
                        } else if (errorObject.code === 3) {
                            dropFromQueue = true;
                        } else if (errorObject.code === 4) {
                        } else if (errorObject.code === 5) {
                            dropFromQueue = true;
                        } else {
                            dropFromQueue = true;
                        }
                    } else {
                        dropFromQueue = true;
                    }
                    if (dropFromQueue) {
                        var promise;
                        $log.debug('Item dropped from queue due to error: ' + fileUrl);
                        promise = self._removeFromQueue(siteId, fileId);
                        return promise.catch(function() {}).finally(function() {
                            self._treatQueueDeferred(siteId, fileId, false);
                            self._notifyFileDownloadError(siteId, fileId);
                        });
                    } else {
                        self._treatQueueDeferred(siteId, fileId, false);
                        self._notifyFileDownloadError(siteId, fileId);
                        return $q.reject();
                    }
                }, function(progress) {
                    if (queueDeferreds[siteId] && queueDeferreds[siteId][fileId]) {
                        queueDeferreds[siteId][fileId].notify(progress);
                    }
                });
            });
        }
    };
        self._removeFromQueue = function(siteId, fileId) {
        return $mmApp.getDB().remove(mmFilepoolQueueStore, [siteId, fileId]);
    };
        self._removeFileById = function(siteId, fileId) {
        return getSiteDb(siteId).then(function(db) {
            var p1, p2, p3;
            p1 = db.remove(mmFilepoolStore, fileId);
            p2 = db.where(mmFilepoolLinksStore, 'fileId', '=', fileId).then(function(entries) {
                return $q.all(entries.map(function(entry) {
                    return db.remove(mmFilepoolLinksStore, [entry.fileId, entry.component, entry.componentId]);
                }));
            });
            p3 = $mmFS.isAvailable() ? $mmFS.removeFile(self._getFilePath(siteId, fileId)) : $q.when();
            return $q.all([p1, p2, p3]);
        });
    };
        self.removeFilesByComponent = function(siteId, component, componentId) {
        var where;
        if (typeof componentId !== 'undefined') {
            where = ['componentAndId', '=', [component, self._fixComponentId(componentId)]];
        } else {
            where = ['component', '=', component];
        }
        return getSiteDb(siteId).then(function(db) {
            return db.query(mmFilepoolLinksStore, where);
        }).then(function(items) {
            return $q.all(items.map(function(item) {
                return self._removeFileById(siteId, item.fileId);
            }));
        });
    };
        self.removeFileByUrl = function(siteId, fileUrl) {
        return self._fixPluginfileURL(siteId, fileUrl).then(function(fileUrl) {
            var fileId = self._getFileIdByUrl(fileUrl);
            return self._restoreOldFileIfNeeded(siteId, fileId, fileUrl).then(function() {
                return self._removeFileById(siteId, fileId);
            });
        });
    };
        self._removeRevisionFromUrl = function(url) {
        return url.replace(revisionRegex, '/content/0/');
    };
        self._restoreOldFileIfNeeded = function(siteId, fileId, fileUrl, filePath) {
        var fileObject,
            oldFileId = self._getNonReadableFileIdByUrl(fileUrl);
        if (fileId == oldFileId) {
            return $q.when();
        }
        return self._hasFileInPool(siteId, fileId).catch(function() {
            return self._hasFileInPool(siteId, oldFileId).then(function(entry) {
                fileObject = entry;
                if (filePath) {
                    return $q.when();
                } else {
                    return $mmFS.copyFile(self._getFilePath(siteId, oldFileId), self._getFilePath(siteId, fileId));
                }
            }).then(function() {
                return self._addFileToPool(siteId, fileId, fileObject);
            }).then(function() {
                return self._getFileLinks(siteId, fileId).then(function(links) {
                    var promises = [];
                    angular.forEach(links, function(link) {
                        promises.push(self._addFileLink(siteId, fileId, link.component, link.componentId));
                    });
                    return $q.all(promises);
                });
            }).then(function() {
                return self._removeFileById(siteId, oldFileId);
            }).catch(function() {
            });
        });
    };
        self.setPackagePreviousStatus = function(siteId, component, componentId) {
        $log.debug('Set previous status for package ' + component + ' ' + componentId);
        return $mmSitesManager.getSite(siteId).then(function(site) {
            var db = site.getDb(),
                packageId = self.getPackageId(component, componentId);
            return db.get(mmFilepoolPackagesStore, packageId).then(function(entry) {
                entry.status = entry.previous || mmCoreNotDownloaded;
                entry.updated = new Date().getTime();
                $log.debug('Set status \'' + entry.status + '\' for package ' + component + ' ' + componentId);
                return db.insert(mmFilepoolPackagesStore, entry).then(function() {
                    self._triggerPackageStatusChanged(siteId, component, componentId, entry.status);
                    return entry.status;
                });
            });
        });
    };
        self.storePackageStatus = function(siteId, component, componentId, status, revision, timemodified) {
        $log.debug('Set status \'' + status + '\' for package ' + component + ' ' + componentId);
        revision = revision || 0;
        timemodified = timemodified || 0;
        return $mmSitesManager.getSite(siteId).then(function(site) {
            var db = site.getDb(),
                packageId = self.getPackageId(component, componentId);
            return db.get(mmFilepoolPackagesStore, packageId).then(function(entry) {
                return entry.status;
            }, function() {
                return undefined;
            }).then(function(previousStatus) {
                var promise;
                if (previousStatus === status) {
                    promise = $q.when();
                } else {
                    promise = db.insert(mmFilepoolPackagesStore, {
                        id: packageId,
                        component: component,
                        componentId: componentId,
                        status: status,
                        previous: previousStatus,
                        revision: revision,
                        timemodified: timemodified,
                        updated: new Date().getTime()
                    });
                }
                return promise.then(function() {
                    self._triggerPackageStatusChanged(siteId, component, componentId, status);
                });
            });
        });
    };
        self._treatQueueDeferred = function(siteId, fileId, resolve) {
        if (queueDeferreds[siteId] && queueDeferreds[siteId][fileId]) {
            if (resolve) {
                queueDeferreds[siteId][fileId].resolve();
            } else {
                queueDeferreds[siteId][fileId].reject();
            }
            delete queueDeferreds[siteId][fileId];
        }
    };
        self._triggerPackageStatusChanged = function(siteId, component, componentId, status) {
        var data = {
            siteid: siteId,
            component: component,
            componentId: componentId,
            status: status
        };
        $mmEvents.trigger(mmCoreEventPackageStatusChanged, data);
    };
    return self;
}])
.run(["$log", "$ionicPlatform", "$timeout", "$mmFilepool", function($log, $ionicPlatform, $timeout, $mmFilepool) {
    $log = $log.getInstance('$mmFilepool');
    $ionicPlatform.ready(function() {
        $timeout($mmFilepool.checkQueueProcessing, 1000);
    });
}]);

angular.module('mm.core')
.constant('mmFsSitesFolder', 'sites')
.constant('mmFsTmpFolder', 'tmp')
.constant('mmModulePath', 'modules/moodle')
.factory('$mmFS', ["$ionicPlatform", "$cordovaFile", "$log", "$q", "$http", "$cordovaZip", "mmFsSitesFolder", "mmFsTmpFolder", "mmModulePath", function($ionicPlatform, $cordovaFile, $log, $q, $http, $cordovaZip, mmFsSitesFolder, mmFsTmpFolder,mmModulePath) {
    $log = $log.getInstance('$mmFS');
    var self = {},
        initialized = false,
        basePath = '',
        isHTMLAPI = false,
        mimeTypes = {};
    $http.get(mmModulePath+'/core/assets/mimetypes.json').then(function(response) {
        mimeTypes = response.data;
    }, function() {
    });
    self.FORMATTEXT         = 0;
    self.FORMATDATAURL      = 1;
    self.FORMATBINARYSTRING = 2;
    self.FORMATARRAYBUFFER  = 3;
        self.setHTMLBasePath = function(path) {
        isHTMLAPI = true;
        basePath = path;
    };
        self.usesHTMLAPI = function() {
        return isHTMLAPI;
    };
        self.init = function() {
        var deferred = $q.defer();
        if (initialized) {
            deferred.resolve();
            return deferred.promise;
        }
        $ionicPlatform.ready(function() {
            if (ionic.Platform.isAndroid()) {
                basePath = cordova.file.externalApplicationStorageDirectory;
            } else if (ionic.Platform.isIOS()) {
                basePath = cordova.file.documentsDirectory;
            } else if (!self.isAvailable() || basePath === '') {
                $log.error('Error getting device OS.');
                deferred.reject();
                return;
            }
            initialized = true;
            $log.debug('FS initialized: '+basePath);
            deferred.resolve();
        });
        return deferred.promise;
    };
        self.isAvailable = function() {
        return typeof window.resolveLocalFileSystemURL !== 'undefined' && typeof FileTransfer !== 'undefined';
    };
        self.getFile = function(path) {
        return self.init().then(function() {
            $log.debug('Get file: '+path);
            return $cordovaFile.checkFile(basePath, path);
        });
    };
        self.getDir = function(path) {
        return self.init().then(function() {
            $log.debug('Get directory: '+path);
            return $cordovaFile.checkDir(basePath, path);
        });
    };
        self.getSiteFolder = function(siteId) {
        return mmFsSitesFolder + '/' + siteId;
    };
        function create(isDirectory, path, failIfExists, base) {
        return self.init().then(function() {
            base = base || basePath;
            if (path.indexOf('/') == -1) {
                if (isDirectory) {
                    $log.debug('Create dir ' + path + ' in ' + base);
                    return $cordovaFile.createDir(base, path, !failIfExists);
                } else {
                    $log.debug('Create file ' + path + ' in ' + base);
                    return $cordovaFile.createFile(base, path, !failIfExists);
                }
            } else {
                var firstDir = path.substr(0, path.indexOf('/'));
                var restOfPath = path.substr(path.indexOf('/') + 1);
                $log.debug('Create dir ' + firstDir + ' in ' + base);
                return $cordovaFile.createDir(base, firstDir, true).then(function(newDirEntry) {
                    return create(isDirectory, restOfPath, failIfExists, newDirEntry.toURL());
                }, function(error) {
                    $log.error('Error creating directory ' + firstDir + ' in ' + base);
                    return $q.reject(error);
                });
            }
        });
    }
        self.createDir = function(path, failIfExists) {
        failIfExists = failIfExists || false;
        return create(true, path, failIfExists);
    };
        self.createFile = function(path, failIfExists) {
        failIfExists = failIfExists || false;
        return create(false, path, failIfExists);
    };
        self.removeDir = function(path) {
        return self.init().then(function() {
            $log.debug('Remove directory: ' + path);
            return $cordovaFile.removeRecursively(basePath, path);
        });
    };
        self.removeFile = function(path) {
        return self.init().then(function() {
            $log.debug('Remove file: ' + path);
            return $cordovaFile.removeFile(basePath, path);
        });
    };
        self.getDirectoryContents = function(path) {
        $log.debug('Get contents of dir: ' + path);
        return self.getDir(path).then(function(dirEntry) {
            var deferred = $q.defer();
            var directoryReader = dirEntry.createReader();
            directoryReader.readEntries(deferred.resolve, deferred.reject);
            return deferred.promise;
        });
    };
        function getSize(entry) {
        var deferred = $q.defer();
        if (entry.isDirectory) {
            var directoryReader = entry.createReader();
            directoryReader.readEntries(function(entries) {
                var promises = [];
                for (var i = 0; i < entries.length; i++) {
                    promises.push(getSize(entries[i]));
                }
                $q.all(promises).then(function(sizes) {
                    var directorySize = 0;
                    for (var i = 0; i < sizes.length; i++) {
                        var fileSize = parseInt(sizes[i]);
                        if (isNaN(fileSize)) {
                            deferred.reject();
                            return;
                        }
                        directorySize += fileSize;
                    }
                    deferred.resolve(directorySize);
                }, deferred.reject);
            }, deferred.reject);
        } else if (entry.isFile) {
            entry.file(function(file) {
                deferred.resolve(file.size);
            }, deferred.reject);
        }
        return deferred.promise;
    }
        self.getDirectorySize = function(path) {
        $log.debug('Get size of dir: ' + path);
        return self.getDir(path).then(function(dirEntry) {
           return getSize(dirEntry);
        });
    };
        self.getFileSize = function(path) {
        $log.debug('Get size of file: ' + path);
        return self.getFile(path).then(function(fileEntry) {
           return getSize(fileEntry);
        });
    };
        self.getFileObjectFromFileEntry = function(entry) {
        $log.debug('Get file object of: ' + entry.fullPath);
        var deferred = $q.defer();
        entry.file(function(file) {
            deferred.resolve(file);
        }, deferred.reject);
        return deferred.promise;
    };
        self.calculateFreeSpace = function() {
        if (ionic.Platform.isIOS() || isHTMLAPI) {
            if (window.requestFileSystem) {
                var iterations = 0,
                    maxIterations = 50,
                    deferred = $q.defer();
                function calculateByRequest(size, ratio) {
                    var deferred = $q.defer();
                    window.requestFileSystem(LocalFileSystem.PERSISTENT, size, function() {
                        iterations++;
                        if (iterations > maxIterations) {
                            deferred.resolve(size);
                            return;
                        }
                        calculateByRequest(size * ratio, ratio).then(deferred.resolve);
                    }, function() {
                        deferred.resolve(size / ratio);
                    });
                    return deferred.promise;
                };
                calculateByRequest(1048576, 1.3).then(function(size) {
                    iterations = 0;
                    maxIterations = 10;
                    calculateByRequest(size, 1.1).then(deferred.resolve);
                });
                return deferred.promise;
            } else {
                return $q.reject();
            }
        } else {
            return $cordovaFile.getFreeDiskSpace().then(function(size) {
                return size * 1024;
            });
        }
    };
        self.normalizeFileName = function(filename) {
        filename = decodeURIComponent(filename);
        return filename;
    };
        self.readFile = function(path, format) {
        format = format || self.FORMATTEXT;
        $log.debug('Read file ' + path + ' with format '+format);
        switch (format) {
            case self.FORMATDATAURL:
                return $cordovaFile.readAsDataURL(basePath, path);
            case self.FORMATBINARYSTRING:
                return $cordovaFile.readAsBinaryString(basePath, path);
            case self.FORMATARRAYBUFFER:
                return $cordovaFile.readAsArrayBuffer(basePath, path);
            default:
                return $cordovaFile.readAsText(basePath, path);
        }
    };
        self.readFileData = function(fileData, format) {
        format = format || self.FORMATTEXT;
        $log.debug('Read file from file data with format '+format);
        var deferred = $q.defer();
        var reader = new FileReader();
        reader.onloadend = function(evt) {
            if (evt.target.result !== undefined || evt.target.result !== null) {
                deferred.resolve(evt.target.result);
            } else if (evt.target.error !== undefined || evt.target.error !== null) {
                deferred.reject(evt.target.error);
            } else {
                deferred.reject({code: null, message: 'READER_ONLOADEND_ERR'});
            }
        };
        switch (format) {
            case self.FORMATDATAURL:
                reader.readAsDataURL(fileData);
                break;
            case self.FORMATBINARYSTRING:
                reader.readAsBinaryString(fileData);
                break;
            case self.FORMATARRAYBUFFER:
                reader.readAsArrayBuffer(fileData);
                break;
            default:
                reader.readAsText(fileData);
        }
        return deferred.promise;
    };
        self.writeFile = function(path, data) {
        $log.debug('Write file: ' + path);
        return self.init().then(function() {
            return self.createFile(path).then(function(fileEntry) {
                if (isHTMLAPI && typeof data == 'string') {
                    var type = self.getMimeType(self.getFileExtension(path));
                    data = new Blob([data], {type: type || 'text/plain'});
                }
                return $cordovaFile.writeFile(basePath, path, data, true).then(function() {
                    return fileEntry;
                });
            });
        });
    };
        self.getExternalFile = function(fullPath) {
        return $cordovaFile.checkFile(fullPath, '');
    };
        self.removeExternalFile = function(fullPath) {
        var directory = fullPath.substring(0, fullPath.lastIndexOf('/') );
        var filename = fullPath.substr(fullPath.lastIndexOf('/') + 1);
        return $cordovaFile.removeFile(directory, filename);
    };
        self.getBasePath = function() {
        return self.init().then(function() {
            if (basePath.slice(-1) == '/') {
                return basePath;
            } else {
                return basePath + '/';
            }
        });
    };
        self.getBasePathToDownload = function() {
        return self.init().then(function() {
            if (ionic.Platform.isIOS()) {
                return $cordovaFile.checkDir(basePath, '').then(function(dirEntry) {
                    return dirEntry.toInternalURL();
                });
            } else {
                return basePath;
            }
        });
    };
        self.getTmpFolder = function() {
        return mmFsTmpFolder;
    };
        self.moveFile = function(originalPath, newPath) {
        return self.init().then(function() {
            if (isHTMLAPI) {
                var commonPath = basePath,
                    dirsA = originalPath.split('/'),
                    dirsB = newPath.split('/');
                for (var i = 0; i < dirsA.length; i++) {
                    var dir = dirsA[i];
                    if (dirsB[i] === dir) {
                        dir = dir + '/';
                        commonPath = self.concatenatePaths(commonPath, dir);
                        originalPath = originalPath.replace(dir, '');
                        newPath = newPath.replace(dir, '');
                    } else {
                        break;
                    }
                }
                return $cordovaFile.moveFile(commonPath, originalPath, commonPath, newPath);
            } else {
                return $cordovaFile.moveFile(basePath, originalPath, basePath, newPath);
            }
        });
    };
        self.copyFile = function(from, to) {
        return self.init().then(function() {
            if (isHTMLAPI) {
                var commonPath = basePath,
                    dirsA = from.split('/'),
                    dirsB = to.split('/');
                for (var i = 0; i < dirsA.length; i++) {
                    var dir = dirsA[i];
                    if (dirsB[i] === dir) {
                        dir = dir + '/';
                        commonPath = self.concatenatePaths(commonPath, dir);
                        from = from.replace(dir, '');
                        to = to.replace(dir, '');
                    } else {
                        break;
                    }
                }
                return $cordovaFile.copyFile(commonPath, from, commonPath, to);
            } else {
                var toFile = self.getFileAndDirectoryFromPath(to);
                if (toFile.directory == '') {
                    return $cordovaFile.copyFile(basePath, from, basePath, to);
                } else {
                    return self.createDir(toFile.directory).then(function() {
                        return $cordovaFile.copyFile(basePath, from, basePath, to);
                    });
                }
            }
        });
    };
        self.getFileAndDirectoryFromPath = function(path) {
        var file = {
            directory: '',
            name: ''
        };
        file.directory = path.substring(0, path.lastIndexOf('/') );
        file.name = path.substr(path.lastIndexOf('/') + 1);
        return file;
    };
        self.concatenatePaths = function(leftPath, rightPath) {
        var lastCharLeft = leftPath.slice(-1),
            firstCharRight = rightPath.charAt(0);
        if (lastCharLeft === '/' && firstCharRight === '/') {
            return leftPath + rightPath.substr(1);
        } else if(lastCharLeft !== '/' && firstCharRight !== '/') {
            return leftPath + '/' + rightPath;
        } else {
            return leftPath + rightPath;
        }
    };
        self.getInternalURL = function(fileEntry) {
        if (isHTMLAPI) {
            return fileEntry.toURL();
        }
        return fileEntry.toInternalURL();
    };
        self.getFileIcon = function(filename) {
        var ext = self.getFileExtension(filename),
            icon;
        if (ext && mimeTypes[ext] && mimeTypes[ext].icon) {
            icon = mimeTypes[ext].icon + '-64.png';
        } else {
            icon = 'unknown-64.png';
        }
        return mmModulePath+'/img/files/' + icon;
    };
        self.getFolderIcon = function() {
        return mmModulePath+'/img/files/folder-64.png';
    };
        self.getFileExtension = function(filename) {
        var dot = filename.lastIndexOf("."),
            ext;
        if (dot > -1) {
            ext = filename.substr(dot + 1).toLowerCase();
        }
        return ext;
    };
        self.getMimeType = function(extension) {
        if (mimeTypes[extension] && mimeTypes[extension].type) {
            return mimeTypes[extension].type;
        }
    };
        self.removeExtension = function(path) {
        var index = path.lastIndexOf('.');
        if (index > -1) {
            return path.substr(0, index);
        }
        return path;
    };
        self.addBasePathIfNeeded = function(path) {
        if (path.indexOf(basePath) > -1) {
            return path;
        } else {
            return self.concatenatePaths(basePath, path);
        }
    };
        self.unzipFile = function(path, destFolder) {
        return self.getFile(path).then(function(fileEntry) {
            destFolder = self.addBasePathIfNeeded(destFolder || self.removeExtension(path));
            return $cordovaZip.unzip(fileEntry.toURL(), destFolder);
        });
    };
    return self;
}]);

angular.module('mm.core')
.factory('$mmGroups', ["$log", "$q", "$mmSite", "$mmSitesManager", function($log, $q, $mmSite, $mmSitesManager) {
    $log = $log.getInstance('$mmGroups');
    self.NOGROUPS       = 0;
    self.SEPARATEGROUPS = 1;
    self.VISIBLEGROUPS  = 2;
        self.getActivityAllowedGroups = function(cmid, userid) {
        userid = userid || $mmSite.getUserId();
        var params = {
                cmid: cmid,
                userid: userid
            },
            preSets = {
                cacheKey: getActivityAllowedGroupsCacheKey(cmid, userid)
            };
        return $mmSite.read('core_group_get_activity_allowed_groups', params, preSets).then(function(response) {
            if (!response || !response.groups) {
                return $q.reject();
            }
            return response.groups;
        });
    };
        function getActivityAllowedGroupsCacheKey(cmid, userid) {
        return 'mmGroups:allowedgroups:' + cmid + ':' + userid;
    }
        self.getActivityGroupMode = function(cmid) {
        var params = {
                cmid: cmid
            },
            preSets = {
                cacheKey: getActivityGroupModeCacheKey(cmid)
            };
        return $mmSite.read('core_group_get_activity_groupmode', params, preSets).then(function(response) {
            if (!response || typeof response.groupmode == 'undefined') {
                return $q.reject();
            }
            return response.groupmode;
        });
    };
        function getActivityGroupModeCacheKey(cmid) {
        return 'mmGroups:groupmode:' + cmid;
    }
        self.getUserGroups = function(courses, refresh, siteid, userid) {
        var promises = [],
            groups = [],
            deferred = $q.defer();
        angular.forEach(courses, function(course) {
            var courseid;
            if (typeof course == 'object') {
                courseid = course.id;
            } else {
                courseid = course;
            }
            var promise = self.getUserGroupsInCourse(courseid, refresh, siteid, userid).then(function(coursegroups) {
                groups = groups.concat(coursegroups);
            });
            promises.push(promise);
        });
        $q.all(promises).finally(function() {
            deferred.resolve(groups);
        });
        return deferred.promise;
    };
        self.getUserGroupsInCourse = function(courseid, refresh, siteid, userid) {
        siteid = siteid || $mmSite.getId();
        return $mmSitesManager.getSite(siteid).then(function(site) {
            var presets = {},
                data = {
                    userid: userid || site.getUserId(),
                    courseid: courseid
                };
            if (refresh) {
                presets.getFromCache = false;
            }
            return site.read('core_group_get_course_user_groups', data, presets).then(function(response) {
                if (response && response.groups) {
                    return response.groups;
                } else {
                    return $q.reject();
                }
            });
        });
    };
        self.invalidateActivityAllowedGroups = function(cmid, userid) {
        userid = userid || $mmSite.getUserId();
        return $mmSite.invalidateWsCacheForKey(getActivityAllowedGroupsCacheKey(cmid, userid));
    };
        self.invalidateActivityGroupMode = function(cmid) {
        return $mmSite.invalidateWsCacheForKey(getActivityGroupModeCacheKey(cmid));
    };
    return self;
}]);

angular.module('mm.core')
.constant('mmInitDelegateDefaultPriority', 100)
.constant('mmInitDelegateMaxAddonPriority', 599)
.provider('$mmInitDelegate', ["mmInitDelegateDefaultPriority", function(mmInitDelegateDefaultPriority) {
    var initProcesses = {},
        self = {};
        self.registerProcess = function(name, callable, priority, blocking) {
        priority = typeof priority === 'undefined' ? mmInitDelegateDefaultPriority : priority;
        if (typeof initProcesses[name] !== 'undefined') {
            console.log('$mmInitDelegateProvider: Process \'' + name + '\' already defined.');
            return;
        }
        console.log('$mmInitDelegateProvider: Registered process \'' + name + '\'.');
        initProcesses[name] = {
            blocking: blocking,
            callable: callable,
            name: name,
            priority: priority
        };
    };
    self.$get = ["$q", "$log", "$injector", "$mmUtil", function($q, $log, $injector, $mmUtil) {
        $log = $log.getInstance('$mmInitDelegate');
        var self = {},
            readiness;
                function prepareProcess(data) {
            return function() {
                var promise,
                    fn;
                $log.debug('Executing init process \'' + data.name + '\'');
                try {
                    fn = $mmUtil.resolveObject(data.callable);
                } catch (e) {
                    $log.error('Could not resolve object of init process \'' + data.name + '\'. ' + e);
                    return;
                }
                try {
                    promise = fn($injector);
                } catch (e) {
                    $log.error('Error while calling the init process \'' + data.name + '\'. ' + e);
                    return;
                }
                return promise;
            };
        }
                self.executeInitProcesses = function() {
            var ordered = [],
                promises = [],
                dependency = $q.when();
            if (typeof readiness === 'undefined') {
                readiness = $q.defer();
            }
            angular.forEach(initProcesses, function(data) {
                ordered.push(data);
            });
            ordered.sort(function(a, b) {
                return b.priority - a.priority;
            });
            angular.forEach(ordered, function(data) {
                var promise;
                promise = dependency.finally(prepareProcess(data));
                promises.push(promise);
                if (data.blocking) {
                    dependency = promise;
                }
            });
            $q.all(promises).finally(readiness.resolve);
        };
                self.ready = function() {
            if (typeof readiness === 'undefined') {
                readiness = $q.defer();
            }
            return readiness.promise;
        };
        return self;
    }];
    return self;
}]);

angular.module('ionic').directive('ionRadioFix', function() {
  return {
    restrict: 'E',
    replace: true,
    require: '?ngModel',
    transclude: true,
    template:
      '<label class="item item-radio">' +
        '<input type="radio" name="radio-group">' +
        '<div class="radio-content">' +
          '<div class="item-content disable-pointer-events" ng-transclude></div>' +
          '<i class="radio-icon disable-pointer-events icon ion-checkmark"></i>' +
        '</div>' +
      '</label>',
    compile: function(element, attr) {
      if (attr.icon) {
        var iconElm = element.find('i');
        iconElm.removeClass('ion-checkmark').addClass(attr.icon);
      }
      var input = element.find('input');
      angular.forEach({
          'name': attr.name,
          'value': attr.value,
          'disabled': attr.disabled,
          'ng-value': attr.ngValue,
          'ng-model': attr.ngModel,
          'ng-disabled': attr.ngDisabled,
          'ng-change': attr.ngChange,
          'ng-required': attr.ngRequired,
          'required': attr.required
      }, function(value, name) {
        if (angular.isDefined(value)) {
            input.attr(name, value);
          }
      });
      return function(scope, element, attr) {
        scope.getValue = function() {
          return scope.ngValue || attr.value;
        };
      };
    }
  };
});
angular.module('mm.core')
.factory('$mmLang', ["$translate", "$translatePartialLoader", "$mmConfig", "$cordovaGlobalization", "$q", "mmCoreConfigConstants", function($translate, $translatePartialLoader, $mmConfig, $cordovaGlobalization, $q, mmCoreConfigConstants) {
    var self = {},
        currentLanguage;
        self.registerLanguageFolder = function(path) {
        $translatePartialLoader.addPart(path);
        return $translate.refresh();
    };
        self.getCurrentLanguage = function() {
        if (typeof currentLanguage != 'undefined') {
            return $q.when(currentLanguage);
        }
        return $mmConfig.get('current_language').then(function(language) {
            return language;
        }, function() {
            try {
                return $cordovaGlobalization.getPreferredLanguage().then(function(result) {
                    var language = result.value.toLowerCase();
                    if (language.indexOf('-') > -1) {
                        if (mmCoreConfigConstants.languages && typeof mmCoreConfigConstants.languages[language] == 'undefined') {
                            language = language.substr(0, language.indexOf('-'));
                        }
                    }
                    return language;
                }, function() {
                    return mmCoreConfigConstants.default_lang || 'en';
                });
            } catch(err) {
                return mmCoreConfigConstants.default_lang || 'en';
            }
        }).then(function(language) {
            currentLanguage = language;
            return language;
        });
    };
        self.changeCurrentLanguage = function(language) {
        var p1 = $translate.use(language),
            p2 = $mmConfig.set('current_language', language);
        moment.locale(language);
        currentLanguage = language;
        return $q.all([p1, p2]);
    };
        self.translateAndReject = function(errorkey) {
        return $translate(errorkey).then(function(errorMessage) {
            return $q.reject(errorMessage);
        }, function() {
            return $q.reject(errorkey);
        });
    };
        self.translateAndRejectDeferred = function(deferred, errorkey) {
        $translate(errorkey).then(function(errorMessage) {
            deferred.reject(errorMessage);
        }, function() {
            deferred.reject(errorkey);
        });
    };
    return self;
}])
.config(["$translateProvider", "$translatePartialLoaderProvider", function($translateProvider, $translatePartialLoaderProvider) {
    $translateProvider.useLoader('$translatePartialLoader', {
        urlTemplate: '{part}/{lang}.json'
    });
    $translatePartialLoaderProvider.addPart('build/lang');
    $translateProvider.fallbackLanguage('en');
    $translateProvider.preferredLanguage('en');
}])
.run(["$ionicPlatform", "$translate", "$mmLang", function($ionicPlatform, $translate, $mmLang) {
    $ionicPlatform.ready(function() {
        $mmLang.getCurrentLanguage().then(function(language) {
            $translate.use(language);
            moment.locale(language);
        });
    });
}]);

angular.module('mm.core')
.constant('mmCoreNotificationsSitesStore', 'notification_sites')
.constant('mmCoreNotificationsComponentsStore', 'notification_components')
.constant('mmCoreNotificationsTriggeredStore', 'notifications_triggered')
.config(["$mmAppProvider", "mmCoreNotificationsSitesStore", "mmCoreNotificationsComponentsStore", "mmCoreNotificationsTriggeredStore", function($mmAppProvider, mmCoreNotificationsSitesStore, mmCoreNotificationsComponentsStore,
        mmCoreNotificationsTriggeredStore) {
    var stores = [
        {
            name: mmCoreNotificationsSitesStore,
            keyPath: 'id',
            indexes: [
                {
                    name: 'code',
                }
            ]
        },
        {
            name: mmCoreNotificationsComponentsStore,
            keyPath: 'id',
            indexes: [
                {
                    name: 'code',
                }
            ]
        },
        {
            name: mmCoreNotificationsTriggeredStore,
            keyPath: 'id',
            indexes: []
        }
    ];
    $mmAppProvider.registerStores(stores);
}])
.factory('$mmLocalNotifications', ["$log", "$cordovaLocalNotification", "$mmApp", "$q", "mmCoreNotificationsSitesStore", "mmCoreNotificationsComponentsStore", "mmCoreNotificationsTriggeredStore", function($log, $cordovaLocalNotification, $mmApp, $q,
        mmCoreNotificationsSitesStore, mmCoreNotificationsComponentsStore, mmCoreNotificationsTriggeredStore) {
    $log = $log.getInstance('$mmLocalNotifications');
    var self = {},
        observers = {},
        codes = {};
    var codeRequestsQueue = {};
        function getCode(store, id) {
        var db = $mmApp.getDB(),
            key = store + '#' + id;
        if (typeof codes[key] != 'undefined') {
            return $q.when(codes[key]);
        }
        return db.get(store, id).then(function(entry) {
            var code = parseInt(entry.code);
            codes[key] = code;
            return code;
        }, function() {
            return db.query(store, undefined, 'code', true).then(function(entries) {
                var newCode = 0;
                if (entries.length > 0) {
                    newCode = parseInt(entries[0].code) + 1;
                }
                return db.insert(store, {id: id, code: newCode}).then(function() {
                    codes[key] = newCode;
                    return newCode;
                });
            });
        });
    }
        function getSiteCode(siteid) {
        return requestCode(mmCoreNotificationsSitesStore, siteid);
    }
        function getComponentCode(component) {
        return requestCode(mmCoreNotificationsComponentsStore, component);
    }
        function getUniqueNotificationId(notificationid, component, siteid) {
        if (!siteid || !component) {
            return $q.reject();
        }
        return getSiteCode(siteid).then(function(sitecode) {
            return getComponentCode(component).then(function(componentcode) {
                return (sitecode * 100000000 + componentcode * 10000000 + parseInt(notificationid)) % 2147483647;
            });
        });
    }
        function processNextRequest() {
        var nextKey = Object.keys(codeRequestsQueue)[0],
            request,
            promise;
        if (typeof nextKey == 'undefined') {
            return;
        }
        request = codeRequestsQueue[nextKey];
        if (angular.isObject(request) && typeof request.store != 'undefined' && typeof request.id != 'undefined') {
            promise = getCode(request.store, request.id).then(function(code) {
                angular.forEach(request.promises, function(p) {
                    p.resolve(code);
                });
            }, function(error) {
                angular.forEach(request.promises, function(p) {
                    p.reject(error);
                });
            });
        } else {
            promise = $q.when();
        }
        promise.finally(function() {
            delete codeRequestsQueue[nextKey];
            processNextRequest();
        });
    }
        function requestCode(store, id) {
        var deferred = $q.defer(),
            key = store+'#'+id,
            isQueueEmpty = Object.keys(codeRequestsQueue).length == 0;
        if (typeof codeRequestsQueue[key] != 'undefined') {
            codeRequestsQueue[key].promises.push(deferred);
        } else {
            codeRequestsQueue[key] = {
                store: store,
                id: id,
                promises: [deferred]
            };
        }
        if (isQueueEmpty) {
            processNextRequest();
        }
        return deferred.promise;
    }
        self.cancel = function(id, component, siteid) {
        return getUniqueNotificationId(id, component, siteid).then(function(uniqueId) {
            return $cordovaLocalNotification.cancel(uniqueId);
        });
    };
        self.cancelSiteNotifications = function(siteid) {
        if (!self.isAvailable()) {
            return $q.when();
        } else if (!siteid) {
            return $q.reject();
        }
        return $cordovaLocalNotification.getAllScheduled().then(function(scheduled) {
            var ids = [];
            angular.forEach(scheduled, function(notif) {
                if (typeof notif.data == 'string') {
                    notif.data = JSON.parse(notif.data);
                }
                if (typeof notif.data == 'object' && notif.data.siteid === siteid) {
                    ids.push(notif.id);
                }
            });
            return $cordovaLocalNotification.cancel(ids);
        });
    };
        self.isAvailable = function() {
        return window.plugin && window.plugin.notification && window.plugin.notification.local ? true: false;
    };
        self.isTriggered = function(notification) {
        return $mmApp.getDB().get(mmCoreNotificationsTriggeredStore, notification.id).then(function(stored) {
            var notifTime = notification.at.getTime() / 1000;
            return stored.at === notifTime;
        }, function() {
            return false;
        });
    };
        self.notifyClick = function(data) {
        var component = data.component;
        if (component) {
            var callback = observers[component];
            if (typeof callback == 'function') {
                callback(data);
            }
        }
    };
        self.registerClick = function(component, callback) {
        $log.debug("Register observer '"+component+"' for notification click.");
        observers[component] = callback;
    };
        self.removeTriggered = function(id) {
        return $mmApp.getDB().remove(mmCoreNotificationsTriggeredStore, id);
    };
        self.schedule = function(notification, component, siteid) {
        return getUniqueNotificationId(notification.id, component, siteid).then(function(uniqueId) {
            notification.id = uniqueId;
            notification.data = notification.data || {};
            notification.data.component = component;
            notification.data.siteid = siteid;
            return self.isTriggered(notification).then(function(triggered) {
                if (!triggered) {
                    self.removeTriggered(notification.id);
                    return $cordovaLocalNotification.schedule(notification);
                }
            });
        });
    };
        self.trigger = function(notification) {
        var id = parseInt(notification.id);
        if (!isNaN(id)) {
            return $mmApp.getDB().insert(mmCoreNotificationsTriggeredStore, {
                id: id,
                at: parseInt(notification.at)
            });
        } else {
            return $q.reject();
        }
    };
    return self;
}])
.run(["$rootScope", "$log", "$mmLocalNotifications", "$mmEvents", "mmCoreEventSiteDeleted", function($rootScope, $log, $mmLocalNotifications, $mmEvents, mmCoreEventSiteDeleted) {
    $log = $log.getInstance('$mmLocalNotifications');
    $rootScope.$on('$cordovaLocalNotification:trigger', function(e, notification, state) {
        $mmLocalNotifications.trigger(notification);
    });
    $rootScope.$on('$cordovaLocalNotification:click', function(e, notification, state) {
        if (notification && notification.data) {
            $log.debug('Notification clicked: '+notification.data);
            var data = JSON.parse(notification.data);
            $mmLocalNotifications.notifyClick(data);
        }
    });
    $mmEvents.on(mmCoreEventSiteDeleted, function(site) {
        if (site) {
            $mmLocalNotifications.cancelSiteNotifications(site.id);
        }
    });
}]);

angular.module('mm.core')
.constant('mmCoreLogEnabledDefault', true)
.constant('mmCoreLogEnabledConfigName', 'debug_enabled')
.provider('$mmLog', ["mmCoreLogEnabledDefault", function(mmCoreLogEnabledDefault) {
    var isEnabled = mmCoreLogEnabledDefault,
        self = this;
    function prepareLogFn(logFn, className) {
        className = className || '';
        var enhancedLogFn = function() {
            if (isEnabled) {
                var args = Array.prototype.slice.call(arguments),
                    now  = moment().format('l LTS');
                args[0] = now + ' ' + className + ': ' + args[0];
                logFn.apply(null, args);
            }
        };
        enhancedLogFn.logs = [];
        return enhancedLogFn;
    }
        self.logDecorator = function($log) {
        var _$log = (function($log) {
            return {
                log   : $log.log,
                info  : $log.info,
                warn  : $log.warn,
                debug : $log.debug,
                error : $log.error
            };
        })($log);
        var getInstance = function(className) {
            return {
                log   : prepareLogFn(_$log.log, className),
                info  : prepareLogFn(_$log.info, className),
                warn  : prepareLogFn(_$log.warn, className),
                debug : prepareLogFn(_$log.debug, className),
                error : prepareLogFn(_$log.error, className)
            };
        };
        $log.log   = prepareLogFn($log.log);
        $log.info  = prepareLogFn($log.info);
        $log.warn  = prepareLogFn($log.warn);
        $log.debug = prepareLogFn($log.debug);
        $log.error = prepareLogFn($log.error);
        $log.getInstance = getInstance;
        return $log;
    };
    this.$get = ["$mmConfig", "mmCoreLogEnabledDefault", "mmCoreLogEnabledConfigName", function($mmConfig, mmCoreLogEnabledDefault, mmCoreLogEnabledConfigName) {
        var self = {};
                self.init = function() {
            $mmConfig.get(mmCoreLogEnabledConfigName).then(function(enabled) {
                isEnabled = enabled;
            }, function() {
                isEnabled = mmCoreLogEnabledDefault;
            });
        }
                self.enabled = function(flag) {
            $mmConfig.set(mmCoreLogEnabledConfigName, flag);
            isEnabled = flag;
        };
                self.isEnabled = function() {
            return isEnabled;
        };
        return self;
    }];
}])
.run(["$mmLog", function($mmLog) {
    $mmLog.init();
}]);

angular.module('mm.core')
.factory('$mmSite', ["$mmSitesManager", "$mmSitesFactory", function($mmSitesManager, $mmSitesFactory) {
    var self = {},
        siteMethods = $mmSitesFactory.getSiteMethods();
    angular.forEach(siteMethods, function(method) {
        self[method] = function() {
            var currentSite = $mmSitesManager.getCurrentSite();
            if (typeof currentSite == 'undefined') {
                return undefined;
            } else {
                return currentSite[method].apply(currentSite, arguments);
            }
        };
    });
        self.isLoggedIn = function() {
        var currentSite = $mmSitesManager.getCurrentSite();
        return typeof currentSite != 'undefined' && typeof currentSite.token != 'undefined' && currentSite.token != '';
    };
    return self;
}]);

angular.module('mm.core')
.value('mmCoreWSPrefix', 'local_mobile_')
.constant('mmCoreWSCacheStore', 'wscache')
.config(["$mmSitesFactoryProvider", "mmCoreWSCacheStore", function($mmSitesFactoryProvider, mmCoreWSCacheStore) {
    var stores = [
        {
            name: mmCoreWSCacheStore,
            keyPath: 'id',
            indexes: [
                {
                    name: 'key'
                }
            ]
        }
    ];
    $mmSitesFactoryProvider.registerStores(stores);
}])
.provider('$mmSitesFactory', function() {
        var siteSchema = {
            stores: []
        },
        dboptions = {
            autoSchema: true
        };
        this.registerStore = function(store) {
        if (typeof(store.name) === 'undefined') {
            console.log('$mmSite: Error: store name is undefined.');
            return;
        } else if (storeExists(store.name)) {
            console.log('$mmSite: Error: store ' + store.name + ' is already defined.');
            return;
        }
        siteSchema.stores.push(store);
    };
        this.registerStores = function(stores) {
        var self = this;
        angular.forEach(stores, function(store) {
            self.registerStore(store);
        });
    };
        function storeExists(name) {
        var exists = false;
        angular.forEach(siteSchema.stores, function(store) {
            if (store.name === name) {
                exists = true;
            }
        });
        return exists;
    }
    this.$get = ["$http", "$q", "$mmWS", "$mmDB", "$log", "md5", "$mmApp", "$mmLang", "$mmUtil", "$mmFS", "mmCoreWSCacheStore", "mmCoreWSPrefix", "mmCoreSessionExpired", "$mmEvents", "mmCoreEventSessionExpired", "mmCoreUserDeleted", "mmCoreEventUserDeleted", "$mmText", "mmCoreConfigConstants", function($http, $q, $mmWS, $mmDB, $log, md5, $mmApp, $mmLang, $mmUtil, $mmFS, mmCoreWSCacheStore,
            mmCoreWSPrefix, mmCoreSessionExpired, $mmEvents, mmCoreEventSessionExpired, mmCoreUserDeleted, mmCoreEventUserDeleted,
            $mmText, mmCoreConfigConstants) {
        $log = $log.getInstance('$mmSite');
                var deprecatedFunctions = {
            "core_grade_get_definitions": "core_grading_get_definitions",
            "moodle_course_create_courses": "core_course_create_courses",
            "moodle_course_get_courses": "core_course_get_courses",
            "moodle_enrol_get_users_courses": "core_enrol_get_users_courses",
            "moodle_file_get_files": "core_files_get_files",
            "moodle_file_upload": "core_files_upload",
            "moodle_group_add_groupmembers": "core_group_add_group_members",
            "moodle_group_create_groups": "core_group_create_groups",
            "moodle_group_delete_groupmembers": "core_group_delete_group_members",
            "moodle_group_delete_groups": "core_group_delete_groups",
            "moodle_group_get_course_groups": "core_group_get_course_groups",
            "moodle_group_get_groupmembers": "core_group_get_group_members",
            "moodle_group_get_groups": "core_group_get_groups",
            "moodle_message_send_instantmessages": "core_message_send_instant_messages",
            "moodle_notes_create_notes": "core_notes_create_notes",
            "moodle_role_assign": "core_role_assign_role",
            "moodle_role_unassign": "core_role_unassign_role",
            "moodle_user_create_users": "core_user_create_users",
            "moodle_user_delete_users": "core_user_delete_users",
            "moodle_user_get_course_participants_by_id": "core_user_get_course_user_profiles",
            "moodle_user_get_users_by_courseid": "core_enrol_get_enrolled_users",
            "moodle_user_get_users_by_id": "core_user_get_users_by_id",
            "moodle_user_update_users": "core_user_update_users",
            "moodle_webservice_get_siteinfo": "core_webservice_get_site_info",
        };
        var self = {};
                function Site(id, siteurl, token, infos) {
            this.id = id;
            this.siteurl = siteurl;
            this.token = token;
            this.infos = infos;
            if (this.id) {
                this.db = $mmDB.getDB('Site-' + this.id, siteSchema, dboptions);
            }
        }
                Site.prototype.getId = function() {
            return this.id;
        };
                Site.prototype.getURL = function() {
            return this.siteurl;
        };
                Site.prototype.getToken = function() {
            return this.token;
        };
                Site.prototype.getInfo = function() {
            return this.infos;
        };
                Site.prototype.getDb = function() {
            return this.db;
        };
                Site.prototype.getUserId = function() {
            if (typeof this.infos != 'undefined' && typeof this.infos.userid != 'undefined') {
                return this.infos.userid;
            } else {
                return undefined;
            }
        };
                Site.prototype.setId = function(id) {
            this.id = id;
            this.db = $mmDB.getDB('Site-' + this.id, siteSchema, dboptions);
        };
                Site.prototype.setToken = function(token) {
            this.token = token;
        };
                Site.prototype.setInfo = function(infos) {
            this.infos = infos;
        };
                Site.prototype.canAccessMyFiles = function() {
            var infos = this.getInfo();
            return infos && (typeof infos.usercanmanageownfiles === 'undefined' || infos.usercanmanageownfiles);
        };
                Site.prototype.canDownloadFiles = function() {
            var infos = this.getInfo();
            return infos && infos.downloadfiles;
        };
                Site.prototype.canUseAdvancedFeature = function(feature, whenUndefined) {
            var infos = this.getInfo(),
                canUse = true;
            whenUndefined = (typeof whenUndefined === 'undefined') ? true : whenUndefined;
            if (typeof infos.advancedfeatures === 'undefined') {
                canUse = whenUndefined;
            } else {
                angular.forEach(infos.advancedfeatures, function(item) {
                    if (item.name === feature && parseInt(item.value, 10) === 0) {
                        canUse = false;
                    }
                });
            }
            return canUse;
        };
                Site.prototype.canUploadFiles = function() {
            var infos = this.getInfo();
            return infos && infos.uploadfiles;
        };
                Site.prototype.fetchSiteInfo = function() {
            var deferred = $q.defer(),
                site = this;
            var preSets = {
                getFromCache: 0,
                saveToCache: 0
            };
            site.read('core_webservice_get_site_info', {}, preSets).then(deferred.resolve, function(error) {
                site.read('moodle_webservice_get_siteinfo', {}, preSets).then(deferred.resolve, function(error) {
                    deferred.reject(error);
                });
            });
            return deferred.promise;
        };
                Site.prototype.read = function(method, data, preSets) {
            preSets = preSets || {};
            if (typeof(preSets.getFromCache) === 'undefined') {
                preSets.getFromCache = 1;
            }
            if (typeof(preSets.saveToCache) === 'undefined') {
                preSets.saveToCache = 1;
            }
            if (typeof(preSets.sync) === 'undefined') {
                preSets.sync = 0;
            }
            return this.request(method, data, preSets);
        };
                Site.prototype.write = function(method, data, preSets) {
            preSets = preSets || {};
            if (typeof(preSets.getFromCache) === 'undefined') {
                preSets.getFromCache = 0;
            }
            if (typeof(preSets.saveToCache) === 'undefined') {
                preSets.saveToCache = 0;
            }
            if (typeof(preSets.sync) === 'undefined') {
                preSets.sync = 0;
            }
            return this.request(method, data, preSets);
        };
                Site.prototype.request = function(method, data, preSets) {
            var deferred = $q.defer(),
                site = this;
            data = data || {};
            method = site.getCompatibleFunction(method);
            if (site.getInfo() && !site.wsAvailable(method, false)) {
                if (site.wsAvailable(mmCoreWSPrefix + method, false)) {
                    $log.info("Using compatibility WS method '" + mmCoreWSPrefix + method + "'");
                    method = mmCoreWSPrefix + method;
                } else {
                    $log.error("WS function '" + method + "' is not available, even in compatibility mode.");
                    $mmLang.translateAndRejectDeferred(deferred, 'mm.core.wsfunctionnotavailable');
                    return deferred.promise;
                }
            }
            preSets = angular.copy(preSets) || {};
            preSets.wstoken = site.token;
            preSets.siteurl = site.siteurl;
            data.moodlewssettingfilter = true;
            getFromCache(site, method, data, preSets).then(function(data) {
                deferred.resolve(data);
            }, function() {
                var wsPreSets = angular.copy(preSets);
                delete wsPreSets.getFromCache;
                delete wsPreSets.saveToCache;
                delete wsPreSets.omitExpires;
                delete wsPreSets.cacheKey;
                delete wsPreSets.emergencyCache;
                delete wsPreSets.getCacheUsingCacheKey;
                $mmWS.call(method, data, wsPreSets).then(function(response) {
                    if (preSets.saveToCache) {
                        saveToCache(site, method, data, response, preSets.cacheKey);
                    }
                    deferred.resolve(angular.copy(response));
                }, function(error) {
                    if (error === mmCoreSessionExpired) {
                        $mmLang.translateAndRejectDeferred(deferred, 'mm.core.lostconnection');
                        $mmEvents.trigger(mmCoreEventSessionExpired, site.id);
                    } else if (error === mmCoreUserDeleted) {
                        $mmLang.translateAndRejectDeferred(deferred, 'mm.core.userdeleted');
                        $mmEvents.trigger(mmCoreEventUserDeleted, {siteid: site.id, params: data});
                    } else if (typeof preSets.emergencyCache !== 'undefined' && !preSets.emergencyCache) {
                        $log.debug('WS call ' + method + ' failed. Emergency cache is forbidden, rejecting.');
                        deferred.reject(error);
                    } else {
                        $log.debug('WS call ' + method + ' failed. Trying to use the emergency cache.');
                        preSets.omitExpires = true;
                        preSets.getFromCache = true;
                        getFromCache(site, method, data, preSets).then(function(data) {
                            deferred.resolve(data);
                        }, function() {
                            deferred.reject(error);
                        });
                    }
                });
            });
            return deferred.promise;
        };
                Site.prototype.wsAvailable = function(method, checkPrefix) {
            checkPrefix = (typeof checkPrefix === 'undefined') ? true : checkPrefix;
            if (typeof this.infos == 'undefined') {
                return false;
            }
            for (var i = 0; i < this.infos.functions.length; i++) {
                var f = this.infos.functions[i];
                if (f.name == method) {
                    return true;
                }
            }
            if (checkPrefix) {
                return this.wsAvailable(mmCoreWSPrefix + method, false);
            }
            return false;
        };
                Site.prototype.uploadFile = function(uri, options) {
            return $mmWS.uploadFile(uri, options, {
                siteurl: this.siteurl,
                token: this.token
            });
        };
                Site.prototype.invalidateWsCache = function() {
            var db = this.db;
            if (!db) {
                return $q.reject();
            }
            $log.debug('Invalidate all the cache for site: '+ this.id);
            return db.getAll(mmCoreWSCacheStore).then(function(entries) {
                if (entries && entries.length > 0) {
                    return invalidateWsCacheEntries(db, entries);
                }
            });
        };
                Site.prototype.invalidateWsCacheForKey = function(key) {
            var db = this.db;
            if (!db || !key) {
                return $q.reject();
            }
            $log.debug('Invalidate cache for key: '+key);
            return db.whereEqual(mmCoreWSCacheStore, 'key', key).then(function(entries) {
                if (entries && entries.length > 0) {
                    return invalidateWsCacheEntries(db, entries);
                }
            });
        };
                Site.prototype.invalidateWsCacheForKeyStartingWith = function(key) {
            var db = this.db;
            if (!db || !key) {
                return $q.reject();
            }
            $log.debug('Invalidate cache for key starting with: '+key);
            return db.where(mmCoreWSCacheStore, 'key', '^', key).then(function(entries) {
                if (entries && entries.length > 0) {
                    return invalidateWsCacheEntries(db, entries);
                }
            });
        };
                Site.prototype.fixPluginfileURL = function(url) {
            return $mmUtil.fixPluginfileURL(url, this.token);
        };
                Site.prototype.deleteDB = function() {
            return $mmDB.deleteDB('Site-' + this.id);
        };
                Site.prototype.deleteFolder = function() {
            if ($mmFS.isAvailable()) {
                var siteFolder = $mmFS.getSiteFolder(this.id);
                return $mmFS.removeDir(siteFolder).catch(function() {
                });
            } else {
                return $q.when();
            }
        };
                Site.prototype.getSpaceUsage = function() {
            if ($mmFS.isAvailable()) {
                var siteFolderPath = $mmFS.getSiteFolder(this.id);
                return $mmFS.getDirectorySize(siteFolderPath).catch(function() {
                    return 0;
                });
            } else {
                return $q.when(0);
            }
        };
                Site.prototype.getDocsUrl = function(page) {
            var release = this.infos.release ? this.infos.release : undefined;
            return $mmUtil.getDocsUrl(release, page);
        };
                Site.prototype.checkLocalMobilePlugin = function(retrying) {
            var siteurl = this.siteurl,
                self = this,
                service = mmCoreConfigConstants.wsextservice;
            if (!service) {
                return $q.when({code: 0});
            }
            return $http.post(siteurl + '/local/mobile/check.php', {service: service}).then(function(response) {
                var data = response.data;
                if (typeof data != 'undefined' && data.errorcode === 'requirecorrectaccess') {
                    if (!retrying) {
                        self.siteurl = $mmText.addOrRemoveWWW(siteurl);
                        return self.checkLocalMobilePlugin(true);
                    } else {
                        return $q.reject(data.error);
                    }
                } else if (typeof data == 'undefined' || typeof data.code == 'undefined') {
                    return {code: 0, warning: 'mm.login.localmobileunexpectedresponse'};
                }
                var code = parseInt(data.code, 10);
                if (data.error) {
                    switch (code) {
                        case 1:
                            return $mmLang.translateAndReject('mm.login.siteinmaintenance');
                        case 2:
                            return $mmLang.translateAndReject('mm.login.webservicesnotenabled');
                        case 3:
                            return {code: 0};
                        case 4:
                            return $mmLang.translateAndReject('mm.login.mobileservicesnotenabled');
                        default:
                            return $mmLang.translateAndReject('mm.core.unexpectederror');
                    }
                } else {
                    return {code: code, service: service};
                }
            }, function() {
                return {code: 0};
            });
        };
                Site.prototype.checkIfLocalMobileInstalledAndNotUsed = function() {
            var appUsesLocalMobile = false;
            angular.forEach(this.infos.functions, function(func) {
                if (func.name.indexOf(mmCoreWSPrefix) != -1) {
                    appUsesLocalMobile = true;
                }
            });
            if (appUsesLocalMobile) {
                return $q.reject();
            }
            return this.checkLocalMobilePlugin().then(function(data) {
                if (typeof data.service == 'undefined') {
                    return $q.reject();
                }
                return data;
            });
        };
                Site.prototype.containsUrl = function(url) {
            if (!url) {
                return false;
            }
            var siteurl = $mmText.removeProtocolAndWWW(this.siteurl);
            url = $mmText.removeProtocolAndWWW(url);
            return url.indexOf(siteurl) == 0;
        };
                function invalidateWsCacheEntries(db, entries) {
            var promises = [];
            angular.forEach(entries, function(entry) {
                entry.expirationtime = 0;
                var promise = db.insert(mmCoreWSCacheStore, entry);
                promises.push(promise);
            });
            return $q.all(promises);
        }
                Site.prototype.getCompatibleFunction = function(method) {
            if (typeof deprecatedFunctions[method] !== "undefined") {
                if (this.wsAvailable(deprecatedFunctions[method])) {
                    $log.warn("You are using deprecated Web Services: " + method +
                        " you must replace it with the newer function: " + deprecatedFunctions[method]);
                    return deprecatedFunctions[method];
                } else {
                    $log.warn("You are using deprecated Web Services. " +
                        "Your remote site seems to be outdated, consider upgrade it to the latest Moodle version.");
                }
            } else if (!this.wsAvailable(method)) {
                for (var oldFunc in deprecatedFunctions) {
                    if (deprecatedFunctions[oldFunc] === method && this.wsAvailable(oldFunc)) {
                        $log.warn("Your remote site doesn't support the function " + method +
                            ", it seems to be outdated, consider upgrade it to the latest Moodle version.");
                        return oldFunc;
                    }
                }
            }
            return method;
        };
                function getFromCache(site, method, data, preSets) {
            var result,
                db = site.db,
                deferred = $q.defer(),
                id,
                promise;
            if (!db) {
                deferred.reject();
                return deferred.promise;
            } else if (!preSets.getFromCache) {
                deferred.reject();
                return deferred.promise;
            }
            id = md5.createHash(method + ':' + JSON.stringify(data));
            if (preSets.getCacheUsingCacheKey) {
                promise = db.whereEqual(mmCoreWSCacheStore, 'key', preSets.cacheKey).then(function(entries) {
                    if (entries.length == 0) {
                        return db.get(mmCoreWSCacheStore, id);
                    }
                    return entries[0];
                });
            } else {
                promise = db.get(mmCoreWSCacheStore, id);
            }
            promise.then(function(entry) {
                var now = new Date().getTime();
                preSets.omitExpires = preSets.omitExpires || !$mmApp.isOnline();
                if (!preSets.omitExpires) {
                    if (now > entry.expirationtime) {
                        $log.debug('Cached element found, but it is expired');
                        deferred.reject();
                        return;
                    }
                }
                if (typeof entry != 'undefined' && typeof entry.data != 'undefined') {
                    var expires = (entry.expirationtime - now) / 1000;
                    $log.info('Cached element found, id: ' + id + ' expires in ' + expires + ' seconds');
                    deferred.resolve(entry.data);
                    return;
                }
                deferred.reject();
            }, function() {
                deferred.reject();
            });
            return deferred.promise;
        }
                function saveToCache(site, method, data, response, cacheKey) {
            var db = site.db,
                id = md5.createHash(method + ':' + JSON.stringify(data)),
                cacheExpirationTime = mmCoreConfigConstants.cache_expiration_time,
                entry = {
                        id: id,
                        data: response
                    };
            if (!db) {
                return $q.reject();
            } else {
                cacheExpirationTime = isNaN(cacheExpirationTime) ? 300000 : cacheExpirationTime;
                entry.expirationtime = new Date().getTime() + cacheExpirationTime;
                if (cacheKey) {
                    entry.key = cacheKey;
                }
                return db.insert(mmCoreWSCacheStore, entry);
            }
        }
                self.makeSite = function(id, siteurl, token, infos) {
            return new Site(id, siteurl, token, infos);
        };
                self.getSiteMethods = function() {
            var methods = [];
            for (var name in Site.prototype) {
                methods.push(name);
            }
            return methods;
        };
        return self;
    }];
});

angular.module('mm.core')
.constant('mmCoreSitesStore', 'sites')
.constant('mmCoreCurrentSiteStore', 'current_site')
.config(["$mmAppProvider", "mmCoreSitesStore", "mmCoreCurrentSiteStore", function($mmAppProvider, mmCoreSitesStore, mmCoreCurrentSiteStore) {
    var stores = [
        {
            name: mmCoreSitesStore,
            keyPath: 'id'
        },
        {
            name: mmCoreCurrentSiteStore,
            keyPath: 'id'
        }
    ];
    $mmAppProvider.registerStores(stores);
}])
.factory('$mmSitesManager', ["$http", "$q", "$mmSitesFactory", "md5", "$mmLang", "$mmApp", "$mmUtil", "$mmEvents", "$state", "$translate", "mmCoreSitesStore", "mmCoreCurrentSiteStore", "mmCoreEventLogin", "mmCoreEventLogout", "$log", "mmCoreWSPrefix", "mmCoreEventSiteUpdated", "mmCoreEventSiteAdded", "mmCoreEventSessionExpired", "mmCoreEventSiteDeleted", "$mmText", "mmCoreConfigConstants", function($http, $q, $mmSitesFactory, md5, $mmLang, $mmApp, $mmUtil, $mmEvents, $state,
            $translate, mmCoreSitesStore, mmCoreCurrentSiteStore, mmCoreEventLogin, mmCoreEventLogout, $log, mmCoreWSPrefix,
            mmCoreEventSiteUpdated, mmCoreEventSiteAdded, mmCoreEventSessionExpired, mmCoreEventSiteDeleted, $mmText,
            mmCoreConfigConstants) {
    $log = $log.getInstance('$mmSitesManager');
    var self = {},
        services = {},
        sessionRestored = false,
        currentSite,
        sites = {};
        self.getDemoSiteData = function(siteurl) {
        var demoSites = mmCoreConfigConstants.demo_sites;
        if (typeof demoSites != 'undefined' && typeof demoSites[siteurl] != 'undefined') {
            return demoSites[siteurl];
        }
    };
        self.checkSite = function(siteurl, protocol) {
        siteurl = $mmUtil.formatURL(siteurl);
        if (siteurl.indexOf('://localhost') == -1 && !$mmUtil.isValidURL(siteurl)) {
            return $mmLang.translateAndReject('mm.login.invalidsite');
        } else if (!$mmApp.isOnline()) {
            return $mmLang.translateAndReject('mm.core.networkerrormsg');
        } else {
            protocol = protocol || "https://";
            siteurl = siteurl.replace(/^http(s)?\:\/\//i, protocol);
            return self.siteExists(siteurl).then(function() {
                var temporarySite = $mmSitesFactory.makeSite(undefined, siteurl);
                return temporarySite.checkLocalMobilePlugin().then(function(data) {
                    siteurl = temporarySite.getURL();
                    services[siteurl] = data.service;
                    return {siteurl: siteurl, code: data.code, warning: data.warning};
                });
            }, function() {
                if (siteurl.indexOf("https://") === 0) {
                    return self.checkSite(siteurl, "http://");
                } else{
                    return $mmLang.translateAndReject('mm.core.cannotconnect');
                }
            });
        }
    };
        self.siteExists = function(siteurl) {
        var url = siteurl + '/login/token.php';
        if (!ionic.Platform.isWebView()) {
            url = url + '?username=a&password=b&service=c';
        }
        return $http.get(url, {timeout: 30000});
    };
        self.getUserToken = function(siteurl, username, password, service, retry) {
        retry = retry || false;
        if (!$mmApp.isOnline()) {
            return $mmLang.translateAndReject('mm.core.networkerrormsg');
        }
        if (!service) {
            service = determineService(siteurl);
        }
        var loginurl = siteurl + '/login/token.php';
        var data = {
            username: username,
            password: password,
            service: service
        };
        return $http.post(loginurl, data).then(function(response) {
            var data = response.data;
            if (typeof data == 'undefined') {
                return $mmLang.translateAndReject('mm.core.cannotconnect');
            } else {
                if (typeof data.token != 'undefined') {
                    return {token: data.token, siteurl: siteurl};
                } else {
                    if (typeof data.error != 'undefined') {
                        if (!retry && data.errorcode == "requirecorrectaccess") {
                            siteurl = $mmText.addOrRemoveWWW(siteurl);
                            return self.getUserToken(siteurl, username, password, service, true);
                        } else {
                            return $q.reject(data.error);
                        }
                    } else {
                        return $mmLang.translateAndReject('mm.login.invalidaccount');
                    }
                }
            }
        }, function() {
            return $mmLang.translateAndReject('mm.core.cannotconnect');
        });
    };
        self.newSite = function(siteurl, token) {
        var candidateSite = $mmSitesFactory.makeSite(undefined, siteurl, token);
        return candidateSite.fetchSiteInfo().then(function(infos) {
            if (isValidMoodleVersion(infos)) {
                var validation = validateSiteInfo(infos);
                if (validation === true) {
                    var siteid = self.createSiteID(infos.siteurl, infos.username);
                    self.addSite(siteid, siteurl, token, infos);
                    candidateSite.setId(siteid);
                    candidateSite.setInfo(infos);
                    currentSite = candidateSite;
                    self.login(siteid);
                    $mmEvents.trigger(mmCoreEventSiteAdded);
                } else {
                    return $translate(validation.error, validation.params).then(function(error) {
                        return $q.reject(error);
                    });
                }
            } else {
                return $mmLang.translateAndReject('mm.login.invalidmoodleversion');
            }
        });
    };
        self.createSiteID = function(siteurl, username) {
        return md5.createHash(siteurl + username);
    };
        function determineService(siteurl) {
        siteurl = siteurl.replace("https://", "http://");
        if (services[siteurl]) {
            return services[siteurl];
        }
        siteurl = siteurl.replace("http://", "https://");
        if (services[siteurl]) {
            return services[siteurl];
        }
        return mmCoreConfigConstants.wsservice;
    }
        function isValidMoodleVersion(infos) {
        if (!infos) {
            return false;
        }
        var minVersion = 2012120300,
            minRelease = "2.4";
        if (infos.version) {
            var version = parseInt(infos.version);
            if (!isNaN(version)) {
                return version >= minVersion;
            }
        }
        if (infos.release) {
            var matches = infos.release.match(/^([\d|\.]*)/);
            if (matches && matches.length > 1) {
                return matches[1] >= minRelease;
            }
        }
        var appUsesLocalMobile = false;
        angular.forEach(infos.functions, function(func) {
            if (func.name.indexOf(mmCoreWSPrefix) != -1) {
                appUsesLocalMobile = true;
            }
        });
        return appUsesLocalMobile;
    }
        function validateSiteInfo(infos) {
        if (!infos.firstname || !infos.lastname) {
            var moodleLink = '<a mm-browser href="' + infos.siteurl + '">' + infos.siteurl + '</a>';
            return {error: 'mm.core.requireduserdatamissing', params: {'$a': moodleLink}};
        }
        return true;
    }
        self.addSite = function(id, siteurl, token, infos) {
        return $mmApp.getDB().insert(mmCoreSitesStore, {
            id: id,
            siteurl: siteurl,
            token: token,
            infos: infos
        });
    };
        self.loadSite = function(siteid) {
        $log.debug('Load site '+siteid);
        return self.getSite(siteid).then(function(site) {
            currentSite = site;
            self.login(siteid);
            return site.checkIfLocalMobileInstalledAndNotUsed().then(function() {
                $mmEvents.trigger(mmCoreEventSessionExpired, siteid);
            }, function() {
                self.updateSiteInfo(siteid).finally(function() {
                    var infos = site.getInfo(),
                        validation = validateSiteInfo(infos);
                    if (validation !== true) {
                        self.logout();
                        $state.go('mm_login.sites');
                        $translate(validation.error, validation.params).then(function(error) {
                            $mmUtil.showErrorModal(error);
                        });
                    }
                });
            });
        });
    };
        self.getCurrentSite = function() {
        return currentSite;
    };
        self.deleteSite = function(siteid) {
        $log.debug('Delete site '+siteid);
        if (typeof currentSite != 'undefined' && currentSite.id == siteid) {
            self.logout();
        }
        return self.getSite(siteid).then(function(site) {
            return site.deleteDB().then(function() {
                delete sites[siteid];
                return $mmApp.getDB().remove(mmCoreSitesStore, siteid).then(function() {
                    return site.deleteFolder();
                }, function() {
                    return site.deleteFolder();
                }).then(function() {
                    $mmEvents.trigger(mmCoreEventSiteDeleted, site);
                });
            });
        });
    };
        self.hasNoSites = function() {
        return $mmApp.getDB().count(mmCoreSitesStore).then(function(count) {
            if (count > 0) {
                return $q.reject();
            }
        });
    };
        self.hasSites = function() {
        return $mmApp.getDB().count(mmCoreSitesStore).then(function(count) {
            if (count == 0) {
                return $q.reject();
            }
        });
    };
        self.getSite = function(siteId) {
        if (!siteId) {
            return $q.reject();
        } else if (currentSite && currentSite.getId() === siteId) {
            return $q.when(currentSite);
        } else if (typeof sites[siteId] != 'undefined') {
            return $q.when(sites[siteId]);
        } else {
            return $mmApp.getDB().get(mmCoreSitesStore, siteId).then(function(data) {
                var site = $mmSitesFactory.makeSite(siteId, data.siteurl, data.token, data.infos);
                sites[siteId] = site;
                return site;
            });
        }
    };
        self.getSiteDb = function(siteId) {
        return self.getSite(siteId).then(function(site) {
            return site.getDb();
        });
    };
        self.getSites = function(ids) {
        return $mmApp.getDB().getAll(mmCoreSitesStore).then(function(sites) {
            var formattedSites = [];
            angular.forEach(sites, function(site) {
                if (!ids || ids.indexOf(site.id) > -1) {
                    formattedSites.push({
                        id: site.id,
                        siteurl: site.siteurl,
                        fullname: site.infos.fullname,
                        sitename: site.infos.sitename,
                        avatar: site.infos.userpictureurl
                    });
                }
            });
            return formattedSites;
        });
    };
        self.getSitesIds = function() {
        return $mmApp.getDB().getAll(mmCoreSitesStore).then(function(sites) {
            var ids = [];
            angular.forEach(sites, function(site) {
                ids.push(site.id);
            });
            return ids;
        });
    };
        self.login = function(siteid) {
        return $mmApp.getDB().insert(mmCoreCurrentSiteStore, {
            id: 1,
            siteid: siteid
        }).then(function() {
            $mmEvents.trigger(mmCoreEventLogin);
        });
    };
        self.logout = function() {
        currentSite = undefined;
        $mmEvents.trigger(mmCoreEventLogout);
        return $mmApp.getDB().remove(mmCoreCurrentSiteStore, 1);
    }
        self.restoreSession = function() {
        if (sessionRestored) {
            return $q.reject();
        }
        sessionRestored = true;
        return $mmApp.getDB().get(mmCoreCurrentSiteStore, 1).then(function(current_site) {
            var siteid = current_site.siteid;
            $log.debug('Restore session in site '+siteid);
            return self.loadSite(siteid);
        }, function() {
            return $q.reject();
        });
    };
        self.updateSiteToken = function(siteurl, username, token) {
        var siteid = self.createSiteID(siteurl, username);
        return self.getSite(siteid).then(function(site) {
            site.token = token;
            return $mmApp.getDB().insert(mmCoreSitesStore, {
                id: siteid,
                siteurl: site.getURL(),
                token: token,
                infos: site.getInfo()
            });
        });
    };
        self.updateSiteInfo = function(siteid) {
        return self.getSite(siteid).then(function(site) {
            return site.fetchSiteInfo().then(function(infos) {
                site.setInfo(infos);
                return $mmApp.getDB().insert(mmCoreSitesStore, {
                    id: siteid,
                    siteurl: site.getURL(),
                    token: site.getToken(),
                    infos: infos
                }).finally(function() {
                    $mmEvents.trigger(mmCoreEventSiteUpdated, siteid);
                });
            });
        });
    };
        self.updateSiteInfoByUrl = function(siteurl, username) {
        var siteid = self.createSiteID(siteurl, username);
        return self.updateSiteInfo(siteid);
    };
        self.getSiteIdsFromUrl = function(url, prioritize, username) {
        if (prioritize && currentSite && currentSite.containsUrl(url)) {
            if (!username || currentSite.getInfo().username == username) {
                return $q.when([currentSite.getId()]);
            }
        }
        if (!url.match(/^https?:\/\//i)) {
            if (url.match(/^[^:]{2,10}:\/\//i)) {
                return $q.when([]);
            } else {
                if (currentSite) {
                    return $q.when([currentSite.getId()]);
                } else {
                    return $q.when([]);
                }
            }
        }
        return $mmApp.getDB().getAll(mmCoreSitesStore).then(function(sites) {
            var ids = [];
            angular.forEach(sites, function(site) {
                if (!sites[site.id]) {
                    sites[site.id] = $mmSitesFactory.makeSite(site.id, site.siteurl, site.token, site.infos);
                }
                if (sites[site.id].containsUrl(url)) {
                    if (!username || sites[site.id].getInfo().username == username) {
                        ids.push(site.id);
                    }
                }
            });
            return ids;
        }).catch(function() {
            return [];
        });
    };
    return self;
}]);

angular.module('mm.core')
.factory('$mmText', ["$q", "$mmLang", "$translate", function($q, $mmLang, $translate) {
    var self = {};
        self.bytesToSize = function(bytes, precision) {
        if (typeof bytes == 'undefined' || bytes < 0) {
            return $translate.instant('mm.core.notapplicable');
        }
        if (typeof precision == 'undefined' || precision < 0) {
            precision = 2;
        }
        var keys = ['mm.core.sizeb', 'mm.core.sizekb', 'mm.core.sizemb', 'mm.core.sizegb', 'mm.core.sizetb'];
        var units = $translate.instant(keys);
        var posttxt = 0;
        if (bytes >= 1024) {
            while (bytes >= 1024) {
                posttxt++;
                bytes = bytes / 1024;
            }
            bytes = Number(Math.round(bytes+'e+'+precision) + 'e-'+precision);
        }
        return $translate.instant('mm.core.humanreadablesize', {size: Number(bytes), unit: units[keys[posttxt]]});
    };
        self.cleanTags = function(text, singleLine) {
        text = text.replace(/(<([^>]+)>)/ig,"");
        text = angular.element('<p>').html(text).text();
        text = self.replaceNewLines(text, singleLine ? ' ' : '<br />');
        return text;
    };
        self.replaceNewLines = function(text, newValue) {
        return text.replace(/(?:\r\n|\r|\n)/g, newValue);
    };
        self.formatText = function(text, clean, singleLine, shortenLength) {
        return self.treatMultilangTags(text).then(function(formatted) {
            if (clean) {
                formatted = self.cleanTags(formatted, singleLine);
            }
            if (shortenLength && parseInt(shortenLength) > 0) {
                formatted = self.shortenText(formatted, parseInt(shortenLength));
            }
            return formatted;
        });
    };
        self.shortenText = function(text, length) {
        if (text.length > length) {
            text = text.substr(0, length);
            var lastWordPos = text.lastIndexOf(' ');
            if (lastWordPos > 0) {
                text = text.substr(0, lastWordPos);
            }
            text += '&hellip;';
        }
        return text;
    };
        self.treatMultilangTags = function(text) {
        if (!text) {
            return $q.when('');
        }
        return $mmLang.getCurrentLanguage().then(function(language) {
            var currentLangRe = new RegExp('<(?:lang|span)[^>]+lang="' + language + '"[^>]*>(.*?)<\/(?:lang|span)>', 'g'),
                anyLangRE = /<(?:lang|span)[^>]+lang="[a-zA-Z0-9_-]+"[^>]*>(.*?)<\/(?:lang|span)>/g;
            if (!text.match(currentLangRe)) {
                var matches = text.match(anyLangRE);
                if (matches && matches[0]) {
                    language = matches[0].match(/lang="([a-zA-Z0-9_-]+)"/)[1];
                    currentLangRe = new RegExp('<(?:lang|span)[^>]+lang="' + language + '"[^>]*>(.*?)<\/(?:lang|span)>', 'g');
                } else {
                    return text;
                }
            }
            text = text.replace(currentLangRe, '$1');
            text = text.replace(anyLangRE, '');
            return text;
        });
    };
        self.escapeHTML = function(text) {
        if (typeof text == 'undefined' || text === null || (typeof text == 'number' && isNaN(text))) {
            return '';
        } else if (typeof text != 'string') {
            return '' + text;
        }
        return text
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
    };
        self.decodeHTML = function(text) {
        if (typeof text == 'undefined' || text === null || (typeof text == 'number' && isNaN(text))) {
            return '';
        } else if (typeof text != 'string') {
            return '' + text;
        }
        return text
            .replace(/&amp;/g, '&')
            .replace(/&lt;/g, '<')
            .replace(/&gt;/g, '>')
            .replace(/&quot;/g, '"')
            .replace(/&#039;/g, "'")
            .replace(/&nbsp;/g, ' ');
    };
        self.addOrRemoveWWW = function(url) {
        if (typeof url == 'string') {
            if (url.match(/http(s)?:\/\/www\./)) {
                url = url.replace('www.', '');
            } else {
                url = url.replace('https://', 'https://www.');
                url = url.replace('http://', 'http://www.');
            }
        }
        return url;
    };
        self.removeProtocolAndWWW = function(url) {
        url = url.replace(/.*?:\/\//g, '');
        url = url.replace(/^www./, '');
        return url;
    };
        self.getUsernameFromUrl = function(url) {
        if (url.indexOf('@') > -1) {
            var withoutProtocol = url.replace(/.*?:\/\//, ''),
                matches = withoutProtocol.match(/[^@]*/);
            if (matches && matches.length && !matches[0].match(/[\/|?]/)) {
                return matches[0];
            }
        }
    };
        self.removeSpecialCharactersForFiles = function(text) {
        return text.replace(/[#:\/\?\\]+/g, '_');
    };
        self.getLastFileWithoutParams = function(url) {
        var filename = url.substr(url.lastIndexOf('/') + 1);
        if (filename.indexOf('?') != -1) {
            filename = filename.substr(0, filename.indexOf('?'));
        }
        return filename;
    };
    return self;
}]);

angular.module('mm.core')
.constant('mmCoreVersionApplied', 'version_applied')
.factory('$mmUpdateManager', ["$log", "$q", "$mmConfig", "$mmSitesManager", "$mmFS", "$cordovaLocalNotification", "$mmLocalNotifications", "$mmApp", "$mmEvents", "mmCoreSitesStore", "mmCoreVersionApplied", "mmCoreEventSiteAdded", "mmCoreEventSiteUpdated", "mmCoreEventSiteDeleted", "$injector", "$mmFilepool", "mmCoreCourseModulesStore", "mmFilepoolLinksStore", "mmFilepoolPackagesStore", "mmCoreConfigConstants", function($log, $q, $mmConfig, $mmSitesManager, $mmFS, $cordovaLocalNotification, $mmLocalNotifications,
            $mmApp, $mmEvents, mmCoreSitesStore, mmCoreVersionApplied, mmCoreEventSiteAdded, mmCoreEventSiteUpdated,
            mmCoreEventSiteDeleted, $injector, $mmFilepool, mmCoreCourseModulesStore, mmFilepoolLinksStore,
            mmFilepoolPackagesStore, mmCoreConfigConstants) {
    $log = $log.getInstance('$mmUpdateManager');
    var self = {},
        sitesFilePath = 'migration/sites.json';
        self.check = function() {
        var promises = [],
            versionCode = mmCoreConfigConstants.versioncode;
        return $mmConfig.get(mmCoreVersionApplied, 0).then(function(versionApplied) {
            if (versionCode >= 391 && versionApplied < 391) {
                promises.push(migrateMM1Sites());
                promises.push(clearAppFolder().catch(function() {}));
            }
            if (versionCode >= 2003 && versionApplied < 2003) {
                promises.push(cancelAndroidNotifications());
            }
            if (versionCode >= 2003) {
                setStoreSitesInFile();
            }
            if (versionCode >= 2007 && versionApplied < 2007) {
                promises.push(migrateModulesStatus());
            }
            return $q.all(promises).then(function() {
                return $mmConfig.set(mmCoreVersionApplied, versionCode);
            }).catch(function() {
                $log.error('Error applying update from ' + versionApplied + ' to ' + versionCode);
            });
        });
    };
        function clearAppFolder() {
        if ($mmFS.isAvailable()) {
            return $mmFS.getDirectoryContents('').then(function(entries) {
                var promises = [];
                angular.forEach(entries, function(entry) {
                    var canDeleteAndroid = ionic.Platform.isAndroid() && entry.name !== 'cache' && entry.name !== 'files';
                    var canDeleteIOS = ionic.Platform.isIOS() && entry.name !== 'NoCloud';
                    if (canDeleteIOS || canDeleteAndroid) {
                        promises.push($mmFS.removeDir(entry.name));
                    }
                });
                return $q.all(promises);
            });
        } else {
            return $q.when();
        }
    }
        function migrateMM1Sites() {
        var sites = localStorage.getItem('sites'),
            promises = [];
        if (sites) {
            sites = sites.split(',');
            angular.forEach(sites, function(siteid) {
                if (!siteid) {
                    return;
                }
                $log.debug('Migrating site from MoodleMobile 1: ' + siteid);
                var site = localStorage.getItem('sites-'+siteid),
                    infos;
                if (site) {
                    try {
                        site = JSON.parse(site);
                    } catch(ex) {
                        $log.warn('Site ' + siteid + ' data is invalid. Ignoring.');
                        return;
                    }
                    infos = angular.copy(site);
                    delete infos.id;
                    delete infos.token;
                    promises.push($mmSitesManager.addSite(site.id, site.siteurl, site.token, infos));
                } else {
                    $log.warn('Site ' + siteid + ' not found in local storage. Ignoring.');
                }
            });
        }
        return $q.all(promises).then(function() {
            if (sites) {
                localStorage.clear();
            }
        });
    }
        function cancelAndroidNotifications() {
        if ($mmLocalNotifications.isAvailable() && ionic.Platform.isAndroid()) {
            return $cordovaLocalNotification.cancelAll().catch(function() {
                $log.error('Error cancelling Android notifications.');
            });
        }
        return $q.when();
    }
        function setStoreSitesInFile() {
        $mmEvents.on(mmCoreEventSiteAdded, storeSitesInFile);
        $mmEvents.on(mmCoreEventSiteUpdated, storeSitesInFile);
        $mmEvents.on(mmCoreEventSiteDeleted, storeSitesInFile);
        storeSitesInFile();
    }
        function getSitesStoredInFile() {
        if ($mmFS.isAvailable()) {
            return $mmFS.readFile(sitesFilePath).then(function(sites) {
                try {
                    sites = JSON.parse(sites);
                } catch (ex) {
                    sites = [];
                }
                return sites;
            }).catch(function() {
                return [];
            });
        } else {
            return $q.when([]);
        }
    }
        function storeSitesInFile() {
        if ($mmFS.isAvailable()) {
            return $mmApp.getDB().getAll(mmCoreSitesStore).then(function(sites) {
                angular.forEach(sites, function(site) {
                    site.token = 'private';
                });
                return $mmFS.writeFile(sitesFilePath, JSON.stringify(sites));
            });
        } else {
            return $q.when();
        }
    }
        function deleteSitesFile() {
        if ($mmFS.isAvailable()) {
            return $mmFS.removeFile(sitesFilePath);
        } else {
            return $q.when();
        }
    }
        function migrateModulesStatus() {
        var components = [];
        components.push($injector.get('mmaModBookComponent'));
        components.push($injector.get('mmaModImscpComponent'));
        components.push($injector.get('mmaModPageComponent'));
        components.push($injector.get('mmaModResourceComponent'));
        return $mmSitesManager.getSitesIds().then(function(sites) {
            var promises = [];
            angular.forEach(sites, function(siteId) {
                promises.push(migrateSiteModulesStatus(siteId, components));
            });
            return $q.all(promises);
        });
    }
        function migrateSiteModulesStatus(siteId, components) {
        $log.debug('Migrate site modules status from site ' + siteId);
        return $mmSitesManager.getSiteDb(siteId).then(function(db) {
            return db.getAll(mmCoreCourseModulesStore).then(function(entries) {
                var promises = [];
                angular.forEach(entries, function(entry) {
                    if (!parseInt(entry.id)) {
                        return;
                    }
                    promises.push(determineComponent(db, entry.id, components).then(function(component) {
                        if (component) {
                            entry.component = component;
                            entry.componentId = entry.id;
                            entry.id = $mmFilepool.getPackageId(component, entry.id);
                            promises.push(db.insert(mmFilepoolPackagesStore, entry));
                        }
                    }));
                });
                return $q.all(promises).then(function() {
                    return db.removeAll(mmCoreCourseModulesStore).catch(function() {
                    });
                });
            });
        });
    }
        function determineComponent(db, componentId, components) {
        var promises = [],
            component;
        angular.forEach(components, function(c) {
            if (c) {
                promises.push(db.query(mmFilepoolLinksStore, ['componentAndId', '=', [c, componentId]]).then(function(items) {
                    if (items.length) {
                        component = c;
                    }
                }).catch(function() {
                }));
            }
        });
        return $q.all(promises).then(function() {
            return component;
        });
    }
    return self;
}]);

angular.module('mm.core')
.factory('$mmURLDelegate', ["$log", function($log) {
    $log = $log.getInstance('$mmURLDelegate');
    var observers = {},
        self = {};
        self.register = function(name, callback) {
        $log.debug("Register observer '"+name+"' for custom URL.");
        observers[name] = callback;
    };
        self.notify = function(url) {
        var treated = false;
        angular.forEach(observers, function(callback, name) {
            if (!treated && typeof(callback) === 'function') {
                treated = callback(url);
            }
        });
    };
    return self;
}])
.run(["$mmURLDelegate", "$log", function($mmURLDelegate, $log) {
    window.handleOpenURL = function(url) {
        $log.debug('App launched by URL.');
        $mmURLDelegate.notify(url);
    };
}]);

angular.module('mm.core')
.provider('$mmUtil', ["mmCoreSecondsYear", "mmCoreSecondsDay", "mmCoreSecondsHour", "mmCoreSecondsMinute", function(mmCoreSecondsYear, mmCoreSecondsDay, mmCoreSecondsHour, mmCoreSecondsMinute) {
    var self = this,
        provider = this;
        self.param = function(obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;
        for (name in obj) {
            value = obj[name];
            if (value instanceof Array) {
                for (i = 0; i < value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += self.param(innerObj) + '&';
                }
            }
            else if (value instanceof Object) {
                for (subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += self.param(innerObj) + '&';
                }
            }
            else if (value !== undefined && value !== null) query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }
        return query.length ? query.substr(0, query.length - 1) : query;
    };
    this.$get = ["$ionicLoading", "$ionicPopup", "$injector", "$translate", "$http", "$log", "$q", "$mmLang", "$mmFS", "$timeout", "$mmApp", "$mmText", "mmCoreWifiDownloadThreshold", "mmCoreDownloadThreshold", function($ionicLoading, $ionicPopup, $injector, $translate, $http, $log, $q, $mmLang, $mmFS, $timeout, $mmApp,
                $mmText, mmCoreWifiDownloadThreshold, mmCoreDownloadThreshold) {
        $log = $log.getInstance('$mmUtil');
        var self = {};
                self.formatURL = function(url) {
            url = url.trim();
            if (! /^http(s)?\:\/\/.*/i.test(url)) {
                url = "https://" + url;
            }
            url = url.replace(/^http/i, 'http');
            url = url.replace(/^https/i, 'https');
            url = url.replace(/\/$/, "");
            return url;
        };
                self.resolveObject = function(object, instantiate) {
            var toInject,
                resolved;
            instantiate = angular.isUndefined(instantiate) ? false : instantiate;
            if (angular.isFunction(object) || angular.isObject(object)) {
                resolved = object;
            } else if (angular.isString(object)) {
                toInject = object.split('.');
                resolved = $injector.get(toInject[0]);
                if (toInject.length > 1) {
                    resolved = resolved[toInject[1]];
                }
            }
            if (angular.isFunction(resolved) && instantiate) {
                resolved = resolved();
            }
            if (typeof resolved === 'undefined') {
                throw new Error('Unexpected argument passed passed');
            }
            return resolved;
        };
                self.isDownloadableUrl = function(url) {
            return self.isPluginFileUrl(url) || self.isThemeImageUrl(url) || self.isGravatarUrl(url);
        };
                self.isGravatarUrl = function(url) {
            return url && url.indexOf('gravatar.com/avatar') !== -1;
        };
                self.isPluginFileUrl = function(url) {
            return url && url.indexOf('/pluginfile.php') !== -1;
        };
                self.isThemeImageUrl = function(url) {
            return url && url.indexOf('/theme/image.php') !== -1;
        };
                self.isValidURL = function(url) {
            return /^http(s)?\:\/\/([\da-zA-Z\.-]+)\.([\da-zA-Z\.]{2,6})([\/\w \.-]*)*\/?/i.test(url);
        };
                self.fixPluginfileURL = function(url, token) {
            if (!url) {
                return '';
            }
            if (url.indexOf('token=') != -1) {
                return url;
            }
            if (url.indexOf('pluginfile') == -1) {
                return url;
            }
            if (!token) {
                return '';
            }
            if (url.indexOf('?file=') != -1 || url.indexOf('?forcedownload=') != -1 || url.indexOf('?rev=') != -1) {
                url += '&';
            } else {
                url += '?';
            }
            url += 'token=' + token;
            if (url.indexOf('/webservice/pluginfile') == -1) {
                url = url.replace('/pluginfile', '/webservice/pluginfile');
            }
            return url;
        };
                self.openFile = function(path) {
            var deferred = $q.defer();
            if (false) {
                deferred.resolve();
            } else if (window.plugins) {
                var extension = $mmFS.getFileExtension(path),
                    mimetype = $mmFS.getMimeType(extension);
                if (ionic.Platform.isAndroid() && window.plugins.webintent) {
                    var iParams = {
                        action: "android.intent.action.VIEW",
                        url: path,
                        type: mimetype
                    };
                    window.plugins.webintent.startActivity(
                        iParams,
                        function() {
                            $log.debug('Intent launched');
                            deferred.resolve();
                        },
                        function() {
                            $log.debug('Intent launching failed.');
                            $log.debug('action: ' + iParams.action);
                            $log.debug('url: ' + iParams.url);
                            $log.debug('type: ' + iParams.type);
                            if (!extension || extension.indexOf('/') > -1 || extension.indexOf('\\') > -1) {
                                $mmLang.translateAndRejectDeferred(deferred, 'mm.core.erroropenfilenoextension');
                            } else {
                                $mmLang.translateAndRejectDeferred(deferred, 'mm.core.erroropenfilenoapp');
                            }
                        }
                    );
                } else if (ionic.Platform.isIOS() && typeof handleDocumentWithURL == 'function') {
                    $mmFS.getBasePath().then(function(fsRoot) {
                        if (path.indexOf(fsRoot > -1)) {
                            path = path.replace(fsRoot, "");
                            path = encodeURIComponent(decodeURIComponent(path));
                            path = fsRoot + path;
                        }
                        handleDocumentWithURL(
                            function() {
                                $log.debug('File opened with handleDocumentWithURL' + path);
                                deferred.resolve();
                            },
                            function(error) {
                                $log.debug('Error opening with handleDocumentWithURL' + path);
                                if(error == 53) {
                                    $log.error('No app that handles this file type.');
                                }
                                self.openInBrowser(path);
                                deferred.resolve();
                            },
                            path
                        );
                    }, deferred.reject);
                } else {
                    self.openInBrowser(path);
                    deferred.resolve();
                }
            } else {
                $log.debug('Opening external file using window.open()');
                window.open(path, '_blank');
                deferred.resolve();
            }
            return deferred.promise;
        };
                self.openInBrowser = function(url) {
            window.open(url, '_system');
        };
                self.openInApp = function(url) {
            window.open(url, '_blank');
        };
                self.showModalLoading = function(text, needsTranslate) {
            var modalClosed = false,
                modalShown = false;
            if (!text) {
                text = 'mm.core.loading';
                needsTranslate = true;
            }
            function showModal(text) {
                if (!modalClosed) {
                    $ionicLoading.show({
                        template:   '<ion-spinner></ion-spinner>' +
                                    '<p>'+text+'</p>'
                    });
                    modalShown = true;
                }
            }
            if (needsTranslate) {
                $translate(text).then(showModal);
            } else {
                showModal(text);
            }
            return {
                dismiss: function() {
                    modalClosed = true;
                    if (modalShown) {
                        $ionicLoading.hide();
                    }
                }
            };
        };
                self.showErrorModal = function(errorMessage, needsTranslate, autocloseTime) {
            var errorKey = 'mm.core.error',
                langKeys = [errorKey],
                matches;
            if (angular.isObject(errorMessage)) {
                if (typeof errorMessage.content != 'undefined') {
                    errorMessage = errorMessage.content;
                } else if (typeof errorMessage.body != 'undefined') {
                    errorMessage = errorMessage.body;
                } else if (typeof errorMessage.message != 'undefined') {
                    errorMessage = errorMessage.message;
                } else if (typeof errorMessage.error != 'undefined') {
                    errorMessage = errorMessage.error;
                } else {
                    errorMessage = JSON.stringify(errorMessage);
                }
                matches = errorMessage.match(/token"?[=|:]"?(\w*)/, '');
                if (matches && matches[1]) {
                    errorMessage = errorMessage.replace(new RegExp(matches[1], 'g'), 'secret');
                }
            }
            if (needsTranslate) {
                langKeys.push(errorMessage);
            }
            $translate(langKeys).then(function(translations) {
                var popup = $ionicPopup.alert({
                    title: translations[errorKey],
                    template: needsTranslate ? translations[errorMessage] : errorMessage
                });
                if (typeof autocloseTime != 'undefined' && !isNaN(parseInt(autocloseTime))) {
                    $timeout(function() {
                        popup.close();
                    }, parseInt(autocloseTime));
                } else {
                    delete popup;
                }
            });
        };
                self.showModal = function(title, message) {
            var promises = [
                $translate(title),
                $translate(message),
            ];
            $q.all(promises).then(function(translations) {
                $ionicPopup.alert({
                    title: translations[0],
                    template: translations[1]
                });
            });
        };
                self.showConfirm = function(template, title) {
            return $ionicPopup.confirm({template: template, title: title}).then(function(confirmed) {
                if (!confirmed) {
                    return $q.reject();
                }
            });
        };
                self.showPrompt = function(body, title, inputPlaceholder, inputType) {
            inputType = inputType || 'password';
            var options = {
                template: body,
                title: title,
                inputPlaceholder: inputPlaceholder,
                inputType: inputType
            };
            return $ionicPopup.prompt(options).then(function(data) {
                if (typeof data == 'undefined') {
                    return $q.reject();
                }
                return data;
            });
        };
                self.readJSONFile = function(path) {
            return $http.get(path).then(function(response) {
                return response.data;
            });
        };
                self.getCountryName = function(code) {
            var countryKey = 'mm.core.country-' + code,
                countryName = $translate.instant(countryKey);
            return countryName !== countryKey ? countryName : code;
        };
                self.getDocsUrl = function(release, page) {
            page = page || 'Mobile_app';
            var docsurl = 'https://docs.moodle.org/en/' + page;
            if (typeof release != 'undefined') {
                var version = release.substr(0, 3).replace(".", "");
                if (parseInt(version) >= 24) {
                    docsurl = docsurl.replace('https://docs.moodle.org/', 'https://docs.moodle.org/' + version + '/');
                }
            }
            return $mmLang.getCurrentLanguage().then(function(lang) {
                return docsurl.replace('/en/', '/' + lang + '/');
            }, function() {
                return docsurl;
            });
        };
                self.timestamp = function() {
            return Math.round(new Date().getTime() / 1000);
        };
                self.isFalseOrZero = function(value) {
            return typeof value != 'undefined' && (value === false || parseInt(value) === 0);
        };
                self.isTrueOrOne = function(value) {
            return typeof value != 'undefined' && (value === true || parseInt(value) === 1);
        };
                self.formatTime = function(seconds) {
            var langKeys = ['mm.core.day', 'mm.core.days', 'mm.core.hour', 'mm.core.hours', 'mm.core.min', 'mm.core.mins',
                            'mm.core.sec', 'mm.core.secs', 'mm.core.year', 'mm.core.years', 'mm.core.now'];
            return $translate(langKeys).then(function(translations) {
                totalSecs = Math.abs(seconds);
                var years     = Math.floor(totalSecs / mmCoreSecondsYear);
                var remainder = totalSecs - (years * mmCoreSecondsYear);
                var days      = Math.floor(remainder / mmCoreSecondsDay);
                remainder = totalSecs - (days * mmCoreSecondsDay);
                var hours     = Math.floor(remainder / mmCoreSecondsHour);
                remainder = remainder - (hours * mmCoreSecondsHour);
                var mins      = Math.floor(remainder / mmCoreSecondsMinute);
                var secs      = remainder - (mins * mmCoreSecondsMinute);
                var ss = (secs == 1)  ? translations['mm.core.sec']  : translations['mm.core.secs'];
                var sm = (mins == 1)  ? translations['mm.core.min']  : translations['mm.core.mins'];
                var sh = (hours == 1) ? translations['mm.core.hour'] : translations['mm.core.hours'];
                var sd = (days == 1)  ? translations['mm.core.day']  : translations['mm.core.days'];
                var sy = (years == 1) ? translations['mm.core.year'] : translations['mm.core.years'];
                var oyears = '',
                    odays = '',
                    ohours = '',
                    omins = '',
                    osecs = '';
                if (years) {
                    oyears  = years + ' ' + sy;
                }
                if (days) {
                    odays  = days + ' ' + sd;
                }
                if (hours) {
                    ohours = hours + ' ' + sh;
                }
                if (mins) {
                    omins  = mins + ' ' + sm;
                }
                if (secs) {
                    osecs  = secs + ' ' + ss;
                }
                if (years) {
                    return oyears + ' ' + odays;
                }
                if (days) {
                    return odays + ' ' + ohours;
                }
                if (hours) {
                    return ohours + ' ' + omins;
                }
                if (mins) {
                    return omins + ' ' + osecs;
                }
                if (secs) {
                    return osecs;
                }
                return translations['mm.core.now'];
            });
        };
                self.emptyArray = function(array) {
            array.length = 0;
        };
                self.allPromises = function(promises) {
            if (!promises || !promises.length) {
                return $q.when();
            }
            var count = 0,
                failed = false,
                deferred = $q.defer();
            angular.forEach(promises, function(promise) {
                promise.catch(function() {
                    failed = true;
                }).finally(function() {
                    count++;
                    if (count === promises.length) {
                        if (failed) {
                            deferred.reject();
                        } else {
                            deferred.resolve();
                        }
                    }
                });
            });
            return deferred.promise;
        };
                self.basicLeftCompare = function(itemA, itemB, maxLevels, level) {
            level = level || 0;
            maxLevels = maxLevels || 0;
            if (angular.isFunction(itemA) || angular.isFunction(itemB)) {
                return true;
            } else if (angular.isObject(itemA) && angular.isObject(itemB)) {
                if (level >= maxLevels) {
                    return true;
                }
                var equal = true;
                angular.forEach(itemA, function(value, name) {
                    if (!self.basicLeftCompare(value, itemB[name], maxLevels, level + 1)) {
                        equal = false;
                    }
                });
                return equal;
            } else {
                var floatA = parseFloat(itemA),
                    floatB = parseFloat(itemB);
                if (!isNaN(floatA) && !isNaN(floatB)) {
                    return floatA == floatB;
                }
                return itemA === itemB;
            }
        };
                self.confirmDownloadSize = function(size, message, unknownsizemessage, wifiThreshold, limitedThreshold) {
            wifiThreshold = typeof wifiThreshold == 'undefined' ? mmCoreWifiDownloadThreshold : wifiThreshold;
            limitedThreshold = typeof limitedThreshold == 'undefined' ? mmCoreDownloadThreshold : limitedThreshold;
            message = message || 'mm.course.confirmdownload';
            unknownsizemessage = unknownsizemessage || 'mm.course.confirmdownloadunknownsize';
            if (size <= 0) {
                return self.showConfirm($translate(unknownsizemessage));
            }
            else if (size >= wifiThreshold || ($mmApp.isNetworkAccessLimited() && size >= limitedThreshold)) {
                var readableSize = $mmText.bytesToSize(size, 2);
                return self.showConfirm($translate(message, {size: readableSize}));
            }
            return $q.when();
        };
                self.formatPixelsSize = function(size) {
            if (typeof size == 'string' && (size.indexOf('px') > -1 || size.indexOf('%') > -1)) {
                return size;
            }
            size = parseInt(size, 10);
            if (!isNaN(size)) {
                return size + 'px';
            }
            return '';
        };
                self.param = function(obj) {
            return provider.param(obj);
        };
                self.roundToDecimals = function(number, decimals) {
            if (typeof decimals == 'undefined') {
                decimals = 2;
            }
            var multiplier = Math.pow(10, decimals);
            return Math.round(parseFloat(number) * multiplier) / multiplier;
        };
                self.extractUrlParams = function(url) {
            var regex = /[?&]+([^=&]+)=?([^&]*)?/gi,
                params = {};
            url.replace(regex, function(match, key, value) {
                params[key] = value !== undefined ? value : '';
            });
            return params;
        };
                self.restoreSourcesInHtml = function(html, paths, anchorFn) {
            var div = angular.element('<div>'),
                media;
            div.html(html);
            media = div[0].querySelectorAll('img, video, audio, source');
            angular.forEach(media, function(el) {
                var src = paths[decodeURIComponent(el.getAttribute('src'))];
                if (typeof src !== 'undefined') {
                    el.setAttribute('src', src);
                }
            });
            angular.forEach(div.find('a'), function(anchor) {
                var href = decodeURIComponent(anchor.getAttribute('href')),
                    url = paths[href];
                if (typeof url !== 'undefined') {
                    anchor.setAttribute('href', url);
                    if (angular.isFunction(anchorFn)) {
                        anchorFn(anchor, href);
                    }
                }
            });
            return div.html();
        };
        return self;
    }];
}]);

angular.module('mm.core')
.factory('$mmWS', ["$http", "$q", "$log", "$mmLang", "$cordovaFileTransfer", "$mmApp", "$mmFS", "$mmText", "mmCoreSessionExpired", "mmCoreUserDeleted", "$translate", "$window", "$mmUtil", function($http, $q, $log, $mmLang, $cordovaFileTransfer, $mmApp, $mmFS, $mmText, mmCoreSessionExpired,
            mmCoreUserDeleted, $translate, $window, $mmUtil) {
    $log = $log.getInstance('$mmWS');
    var self = {};
        self.call = function(method, data, preSets) {
        var siteurl;
        data = convertValuesToString(data);
        if (typeof preSets == 'undefined' || preSets === null ||
                typeof preSets.wstoken == 'undefined' || typeof preSets.siteurl == 'undefined') {
            return $mmLang.translateAndReject('mm.core.unexpectederror');
        } else if (!$mmApp.isOnline()) {
            return $mmLang.translateAndReject('mm.core.networkerrormsg');
        }
        preSets.typeExpected = preSets.typeExpected || 'object';
        if (typeof preSets.responseExpected == 'undefined') {
            preSets.responseExpected = true;
        }
        data.wsfunction = method;
        data.wstoken = preSets.wstoken;
        siteurl = preSets.siteurl + '/webservice/rest/server.php?moodlewsrestformat=json';
        var ajaxData = data;
        return $http.post(siteurl, ajaxData).then(function(data) {
            if ((!data || !data.data) && !preSets.responseExpected) {
                data = {};
            } else {
                data = data.data;
            }
            if (!data) {
                return $mmLang.translateAndReject('mm.core.serverconnection');
            } else if (typeof data != preSets.typeExpected) {
                $log.warn('Response of type "' + typeof data + '" received, expecting "' + preSets.typeExpected + '"');
                return $mmLang.translateAndReject('mm.core.errorinvalidresponse');
            }
            if (typeof(data.exception) !== 'undefined') {
                if (data.errorcode == 'invalidtoken' ||
                        (data.errorcode == 'accessexception' && data.message.indexOf('Invalid token - token expired') > -1)) {
                    $log.error("Critical error: " + JSON.stringify(data));
                    return $q.reject(mmCoreSessionExpired);
                } else if (data.errorcode === 'userdeleted') {
                    return $q.reject(mmCoreUserDeleted);
                } else {
                    return $q.reject(data.message);
                }
            }
            if (typeof(data.debuginfo) != 'undefined') {
                return $q.reject('Error. ' + data.message);
            }
            $log.info('WS: Data received from WS ' + typeof(data));
            if (typeof(data) == 'object' && typeof(data.length) != 'undefined') {
                $log.info('WS: Data number of elements '+ data.length);
            }
            return data;
        }, function() {
            return $mmLang.translateAndReject('mm.core.serverconnection');
        });
    };
        function convertValuesToString(data) {
        var result = [];
        if (!angular.isArray(data) && angular.isObject(data)) {
            result = {};
        }
        for (var el in data) {
            if (angular.isObject(data[el])) {
                result[el] = convertValuesToString(data[el]);
            } else {
                result[el] = data[el] + '';
            }
        }
        return result;
    }
        self.downloadFile = function(url, path, background) {
        $log.debug('Downloading file ' + url);
        var tmpPath = path + '.tmp';
        return $mmFS.createFile(tmpPath).then(function(fileEntry) {
            return $cordovaFileTransfer.download(url, fileEntry.toURL(), { encodeURI: false }, true).then(function() {
                return $mmFS.moveFile(tmpPath, path).then(function(movedEntry) {
                    $log.debug('Success downloading file ' + url + ' to ' + path);
                    return movedEntry;
                });
            });
        }).catch(function(err) {
            $log.error('Error downloading ' + url + ' to ' + path);
            $log.error(JSON.stringify(err));
            return $q.reject(err);
        });
    };
        self.uploadFile = function(uri, options, presets) {
        $log.debug('Trying to upload file: ' + uri);
        var ftOptions = {},
            deferred = $q.defer();
        ftOptions.fileKey = options.fileKey;
        ftOptions.fileName = options.fileName;
        ftOptions.httpMethod = 'POST';
        ftOptions.mimeType = options.mimeType;
        ftOptions.params = {
            token: presets.token
        };
        ftOptions.chunkedMode = false;
        ftOptions.headers = {
            Connection: "close"
        };
        $log.debug('Initializing upload');
        $cordovaFileTransfer.upload(presets.siteurl + '/webservice/upload.php', uri, ftOptions, true).then(function(success) {
            $log.debug('Successfully uploaded file');
            deferred.resolve(success);
        }, function(error) {
            $log.error('Error while uploading file: ' + error.exception);
            deferred.reject(error);
        }, function(progress) {
            deferred.notify(progress);
        });
        return deferred.promise;
    };
        self.getRemoteFileSize = function(url) {
        return $http.head(url).then(function(data) {
            var size = parseInt(data.headers('Content-Length'), 10);
            if (size) {
                return size;
            }
            return -1;
        }).catch(function() {
            return -1;
        });
    };
        self.syncCall = function(method, data, preSets) {
        var siteurl,
            xhr,
            errorResponse = {
                error: true,
                message: ''
            };
        data = convertValuesToString(data);
        if (typeof preSets == 'undefined' || preSets === null ||
                typeof preSets.wstoken == 'undefined' || typeof preSets.siteurl == 'undefined') {
            errorResponse.message = $translate.instant('mm.core.unexpectederror');
            return errorResponse;
        } else if (!$mmApp.isOnline()) {
            errorResponse.message = $translate.instant('mm.core.networkerrormsg');
            return errorResponse;
        }
        preSets.typeExpected = preSets.typeExpected || 'object';
        if (typeof preSets.responseExpected == 'undefined') {
            preSets.responseExpected = true;
        }
        data.wsfunction = method;
        data.wstoken = preSets.wstoken;
        siteurl = preSets.siteurl + '/webservice/rest/server.php?moodlewsrestformat=json';
        data = $mmUtil.param(data);
        xhr = new $window.XMLHttpRequest();
        xhr.open('post', siteurl, false);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        xhr.send(data);
        data = ('response' in xhr) ? xhr.response : xhr.responseText;
        xhr.status = Math.max(xhr.status === 1223 ? 204 : xhr.status, 0);
        if (xhr.status < 200 || xhr.status >= 300) {
            errorResponse.message = data;
            return errorResponse;
        }
        try {
            data = JSON.parse(data);
        } catch(ex) {}
        if ((!data || !data.data) && !preSets.responseExpected) {
            data = {};
        }
        if (!data) {
            errorResponse.message = $translate.instant('mm.core.serverconnection');
        } else if (typeof data != preSets.typeExpected) {
            $log.warn('Response of type "' + typeof data + '" received, expecting "' + preSets.typeExpected + '"');
            errorResponse.message = $translate.instant('mm.core.errorinvalidresponse');
        }
        if (typeof data.exception != 'undefined' || typeof data.debuginfo != 'undefined') {
            errorResponse.message = data.message;
        }
        if (errorResponse.message !== '') {
            return errorResponse;
        }
        $log.info('Synchronous: Data received from WS ' + typeof data);
        if (typeof(data) == 'object' && typeof(data.length) != 'undefined') {
            $log.info('Synchronous: Data number of elements '+ data.length);
        }
        return data;
    };
    return self;
}]);

angular.module('mm.core')
.filter('mmBytesToSize', ["$mmText", function($mmText) {
    return function(text) {
        return $mmText.bytesToSize(text);
    };
}]);
angular.module('mm.core')
.filter('mmCreateLinks', function() {
    var replacePattern = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])(?![^<]*>|[^<>]*<\/)/gim;
    return function(text) {
        return text.replace(replacePattern, '<a href="$1">$1</a>');
    };
});
angular.module('mm.core')
.filter('mmDateDayOrTime', ["$translate", function($translate) {
    return function(timestamp) {
        return moment(timestamp * 1000).calendar(null, {
            sameDay: $translate.instant('mm.core.dftimedate'),
            lastDay: $translate.instant('mm.core.dflastweekdate'),
            lastWeek: $translate.instant('mm.core.dflastweekdate')
        });
    };
}]);

angular.module('mm.core')
.filter('mmFormatDate', ["$translate", function($translate) {
    return function(timestamp, format) {
        if (format.indexOf('.') == -1) {
            format = 'mm.core.' + format;
        }
        return moment(timestamp).format($translate.instant(format));
    };
}]);

angular.module('mm.core')
.filter('mmNoTags', function() {
    return function(text) {
        return String(text).replace(/(<([^>]+)>)/ig, '');
    }
});
angular.module('mm.core')
.filter('mmTimeAgo', function() {
    return function(timestamp) {
        return moment(timestamp * 1000).fromNow(true);
    };
});

angular.module('mm.core')
.filter('mmToLocaleString', function() {
    return function(text) {
        var timestamp = parseInt(text);
        if (isNaN(timestamp) || timestamp < 0) {
            return '';
        }
        if (timestamp < 100000000000) {
            timestamp = timestamp * 1000;
        }
        return new Date(timestamp).toLocaleString();
    };
});

angular.module('mm.core')
.directive('mmAutoFocus', ["$mmApp", function($mmApp) {
    return {
        restrict: 'A',
        link: function(scope, el) {
            var unregister = scope.$watch(function() {
                return ionic.transition.isActive;
            }, function(isActive) {
                if (!isActive) {
                    el[0].focus();
                    unregister();
                    if (ionic.Platform.isAndroid()) {
                        $mmApp.openKeyboard();
                    }
                }
            });
        }
    };
}]);

angular.module('mm.core')
.directive('mmBrowser', ["$mmUtil", "$mmContentLinksHelper", function($mmUtil, $mmContentLinksHelper) {
    return {
        restrict: 'A',
        priority: 100,
        link: function(scope, element, attrs) {
            element.on('click', function(event) {
                var href = element[0].getAttribute('href');
                if (href) {
                    event.preventDefault();
                    event.stopPropagation();
                    $mmContentLinksHelper.handleLink(href).then(function(treated) {
                        if (!treated) {
                           if (href.indexOf('cdvfile://') === 0 || href.indexOf('file://') === 0) {
                                $mmUtil.openFile(href).catch(function(error) {
                                    $mmUtil.showErrorModal(error);
                                });
                            } else {
                                $mmUtil.openInBrowser(href);
                            }
                        }
                    });
                }
            });
        }
    };
}]);

angular.module('mm.core')
.directive('mmCompletion', ["$mmSite", "$mmUtil", "$mmText", "$translate", "$q", function($mmSite, $mmUtil, $mmText, $translate, $q) {
    function showStatus(scope) {
        var langKey,
            moduleName = scope.moduleName || '';
        if (scope.completion.tracking === 1 && scope.completion.state === 0) {
            scope.completionImage = 'img/completion/completion-manual-n.svg';
            langKey = 'mm.core.completion-alt-manual-n';
        } else if(scope.completion.tracking === 1 && scope.completion.state === 1) {
            scope.completionImage = 'img/completion/completion-manual-y.svg';
            langKey = 'mm.core.completion-alt-manual-y';
        } else if(scope.completion.tracking === 2 && scope.completion.state === 0) {
            scope.completionImage = 'img/completion/completion-auto-n.svg';
            langKey = 'mm.core.completion-alt-auto-n';
        } else if(scope.completion.tracking === 2 && scope.completion.state === 1) {
            scope.completionImage = 'img/completion/completion-auto-y.svg';
            langKey = 'mm.core.completion-alt-auto-y';
        } else if(scope.completion.tracking === 2 && scope.completion.state === 2) {
            scope.completionImage = 'img/completion/completion-auto-pass.svg';
            langKey = 'mm.core.completion-alt-auto-pass';
        } else if(scope.completion.tracking === 2 && scope.completion.state === 3) {
            scope.completionImage = 'img/completion/completion-auto-fail.svg';
            langKey = 'mm.core.completion-alt-auto-fail';
        }
        if (moduleName) {
            $mmText.formatText(moduleName, true, true, 50).then(function(formatted) {
                $translate(langKey, {$a: formatted}).then(function(translated) {
                    scope.completionDescription = translated;
                });
            });
        }
    }
    return {
        restrict: 'E',
        priority: 100,
        scope: {
            completion: '=',
            afterChange: '=',
            moduleName: '=?'
        },
        templateUrl: 'modules/moodle/core/templates/completion.html',
        link: function(scope, element, attrs) {
            if (scope.completion) {
                showStatus(scope);
                element.on('click', function(e) {
                    if (typeof scope.completion.cmid == 'undefined' || scope.completion.tracking !== 1) {
                        return;
                    }
                    e.preventDefault();
                    e.stopPropagation();
                    var modal = $mmUtil.showModalLoading(),
                        params = {
                            cmid: scope.completion.cmid,
                            completed: scope.completion.state === 1 ? 0 : 1
                        };
                    $mmSite.write('core_completion_update_activity_completion_status_manually', params).then(function(response) {
                        if (!response.status) {
                            return $q.reject();
                        }
                        if (angular.isFunction(scope.afterChange)) {
                            scope.afterChange();
                        }
                    }).catch(function(error) {
                        if (error) {
                            $mmUtil.showErrorModal(error);
                        } else {
                            $mmUtil.showErrorModal('mm.core.errorchangecompletion', true);
                        }
                    }).finally(function() {
                        modal.dismiss();
                    });
                });
            }
        }
    };
}]);

angular.module('mm.core')
.directive('mmExternalContent', ["$log", "$mmFilepool", "$mmSite", "$mmSitesManager", "$mmUtil", "$q", function($log, $mmFilepool, $mmSite, $mmSitesManager, $mmUtil, $q) {
    $log = $log.getInstance('mmExternalContent');
        function addSource(dom, url) {
        if (dom.tagName !== 'SOURCE') {
            return;
        }
        var e = document.createElement('source'),
            type = dom.getAttribute('type');
        e.setAttribute('src', url);
        if (type) {
            e.setAttribute('type', type);
        }
        dom.parentNode.insertBefore(e, dom);
    }
        function handleExternalContent(siteId, dom, targetAttr, url, component, componentId) {
        if (!url || !$mmUtil.isDownloadableUrl(url)) {
            $log.debug('Ignoring non-downloadable URL: ' + url);
            if (dom.tagName === 'SOURCE') {
                addSource(dom, url);
            }
            return $q.reject();
        }
        return $mmSitesManager.getSite(siteId).then(function(site) {
            if (!site.canDownloadFiles() && $mmUtil.isPluginFileUrl(url)) {
                dom.remove();
                return $q.reject();
            }
            var fn;
            if (targetAttr === 'src' && dom.tagName !== 'SOURCE') {
                fn = $mmFilepool.getSrcByUrl;
            } else {
                fn = $mmFilepool.getUrlByUrl;
            }
            return fn(siteId, url, component, componentId).then(function(finalUrl) {
                $log.debug('Using URL ' + finalUrl + ' for ' + url);
                if (dom.tagName === 'SOURCE') {
                    addSource(dom, finalUrl);
                } else {
                    dom.setAttribute(targetAttr, finalUrl);
                }
            });
        });
    }
    return {
        restrict: 'A',
        scope: {
            siteid: '='
        },
        link: function(scope, element, attrs) {
            var dom = element[0],
                siteid = scope.siteid || $mmSite.getId(),
                component = attrs.component,
                componentId = attrs.componentId,
                targetAttr,
                sourceAttr,
                observe = false;
            if (dom.tagName === 'A') {
                targetAttr = 'href';
                sourceAttr = 'href';
                if (attrs.hasOwnProperty('ngHref')) {
                    observe = true;
                }
            } else if (dom.tagName === 'IMG') {
                targetAttr = 'src';
                sourceAttr = 'src';
                if (attrs.hasOwnProperty('ngSrc')) {
                    observe = true;
                }
            } else if (dom.tagName === 'AUDIO' || dom.tagName === 'VIDEO' || dom.tagName === 'SOURCE') {
                targetAttr = 'src';
                sourceAttr = 'targetSrc';
                if (attrs.hasOwnProperty('ngSrc')) {
                    observe = true;
                }
            } else {
                $log.warn('Directive attached to non-supported tag: ' + dom.tagName);
                return;
            }
            if (observe) {
                attrs.$observe(targetAttr, function(url) {
                    if (!url) {
                        return;
                    }
                    handleExternalContent(siteid, dom, targetAttr, url, component, componentId);
                });
            } else {
                handleExternalContent(siteid, dom, targetAttr, attrs[sourceAttr] || attrs[targetAttr], component, componentId);
            }
        }
    };
}]);

angular.module('mm.core')
.directive('mmFile', ["$q", "$mmUtil", "$mmFilepool", "$mmSite", "$mmApp", "$mmEvents", "$mmFS", "mmCoreDownloaded", "mmCoreDownloading", "mmCoreNotDownloaded", "mmCoreOutdated", function($q, $mmUtil, $mmFilepool, $mmSite, $mmApp, $mmEvents, $mmFS, mmCoreDownloaded, mmCoreDownloading,
            mmCoreNotDownloaded, mmCoreOutdated) {
        function getState(scope, siteid, fileurl, timemodified) {
        return $mmFilepool.getFileStateByUrl(siteid, fileurl, timemodified).then(function(state) {
            var canDownload = $mmSite.canDownloadFiles();
            scope.isDownloaded = state === mmCoreDownloaded || state === mmCoreOutdated;
            scope.isDownloading = canDownload && state === mmCoreDownloading;
            scope.showDownload = canDownload && (state === mmCoreNotDownloaded || state === mmCoreOutdated);
        });
    }
        function downloadFile(scope, siteid, fileurl, component, componentid, timemodified) {
        if (!$mmSite.canDownloadFiles()) {
            $mmUtil.showErrorModal('mm.core.cannotdownloadfiles', true);
            return $q.reject();
        }
        scope.isDownloading = true;
        return $mmFilepool.downloadUrl(siteid, fileurl, true, component, componentid, timemodified).then(function(localUrl) {
            getState(scope, siteid, fileurl, timemodified);
            return localUrl;
        }, function() {
            return getState(scope, siteid, fileurl, timemodified).then(function() {
                if (scope.isDownloaded) {
                    return localUrl;
                } else {
                    return $q.reject();
                }
            });
        });
    }
    return {
        restrict: 'E',
        templateUrl: 'modules/moodle/core/templates/file.html',
        scope: {
            file: '='
        },
        link: function(scope, element, attrs) {
            var fileurl = scope.file.fileurl || scope.file.url,
                filename = scope.file.filename,
                filesize = scope.file.filesize,
                timemodified = attrs.timemodified || 0,
                siteid = $mmSite.getId(),
                component = attrs.component,
                componentid = attrs.componentId,
                observer;
            scope.filename = filename;
            scope.fileicon = $mmFS.getFileIcon(filename);
            getState(scope, siteid, fileurl, timemodified);
            $mmFilepool.getFileEventNameByUrl(siteid, fileurl).then(function(eventName) {
                observer = $mmEvents.on(eventName, function(data) {
                    getState(scope, siteid, fileurl, timemodified);
                    if (!data.success) {
                        $mmUtil.showErrorModal('mm.core.errordownloading', true);
                    }
                });
            });
            scope.download = function(e, openAfterDownload) {
                e.preventDefault();
                e.stopPropagation();
                var promise;
                if (scope.isDownloading) {
                    return;
                }
                if (!$mmApp.isOnline() && (!openAfterDownload || (openAfterDownload && !scope.isDownloaded))) {
                    $mmUtil.showErrorModal('mm.core.networkerrormsg', true);
                    return;
                }
                if (openAfterDownload) {
                    downloadFile(scope, siteid, fileurl, component, componentid, timemodified).then(function(localUrl) {
                        $mmUtil.openFile(localUrl).catch(function(error) {
                            $mmUtil.showErrorModal(error);
                        });
                    });
                } else {
                    promise = filesize ? $mmUtil.confirmDownloadSize(filesize) : $q.when();
                    promise.then(function() {
                        $mmFilepool.invalidateFileByUrl(siteid, fileurl).finally(function() {
                            scope.isDownloading = true;
                            $mmFilepool.addToQueueByUrl(siteid, fileurl, component, componentid, timemodified);
                        });
                    });
                }
            };
            scope.$on('$destroy', function() {
                if (observer && observer.off) {
                    observer.off();
                }
            });
        }
    };
}]);

angular.module('mm.core')
.directive('mmFormatText', ["$interpolate", "$mmText", "$compile", "$translate", "$state", function($interpolate, $mmText, $compile, $translate, $state) {
    var extractVariableRegex = new RegExp('{{([^|]+)(|.*)?}}', 'i'),
        tagsToIgnore = ['AUDIO', 'VIDEO', 'BUTTON', 'INPUT', 'SELECT', 'TEXTAREA', 'A'];
        function addExternalContent(el, component, componentId, siteId) {
        el.setAttribute('mm-external-content', '');
        if (component) {
            el.setAttribute('component', component);
            if (componentId) {
                el.setAttribute('component-id', componentId);
            }
        }
        if (siteId) {
            el.setAttribute('siteid', siteId);
        }
    }
        function calculateShorten(element, shorten) {
        var multiplier;
        if (typeof shorten == 'string' && shorten.indexOf('%') > -1) {
            multiplier = parseInt(shorten.replace(/%/g, '').trim()) / 100;
            if (isNaN(multiplier)) {
                multiplier = 0.3;
            }
        } else if (typeof shorten != 'undefined' && shorten === '') {
            multiplier = 0.3;
        } else {
            var number = parseInt(shorten);
            if (isNaN(number)) {
                return;
            } else {
                return number;
            }
        }
        var el = element[0],
            elWidth = el.offsetWidth || el.width || el.clientWidth;
        if (!elWidth) {
            return 100;
        } else {
            return Math.round(elWidth * multiplier);
        }
    }
        function addMediaAdaptClass(el) {
        angular.element(el).addClass('mm-media-adapt-width');
    }
        function formatAndRenderContents(scope, element, attrs, text) {
        if (typeof text == 'undefined') {
            element.removeClass('hide');
            return;
        }
        attrs.shorten = calculateShorten(element, attrs.shorten);
        var shorten = (attrs.expandOnClick || attrs.fullviewOnClick) ? 0 : attrs.shorten;
        text = $interpolate(text)(scope);
        text = text.trim();
        formatContents(scope, element, attrs, text, shorten).then(function(fullText) {
            if (attrs.shorten && (attrs.expandOnClick || attrs.fullviewOnClick)) {
                var shortened = $mmText.shortenText($mmText.cleanTags(fullText, false), parseInt(attrs.shorten)),
                    expanded = false;
                if (shortened.trim() === '') {
                    var hasContent = false,
                        meaningfulTags = ['img', 'video', 'audio'];
                    angular.forEach(meaningfulTags, function(tag) {
                        if (fullText.indexOf('<'+tag) > -1) {
                            hasContent = true;
                        }
                    });
                    if (hasContent) {
                        shortened = $translate.instant(attrs.expandOnClick ? 'mm.core.clicktohideshow' : 'mm.core.clicktoseefull');
                    }
                }
                element.on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var target = e.target;
                    if (tagsToIgnore.indexOf(target.tagName) === -1 || (target.tagName === 'A' && !target.getAttribute('href'))) {
                        if (attrs.expandOnClick) {
                            expanded = !expanded;
                            element.html( expanded ? fullText : shortened);
                            if (expanded) {
                                $compile(element.contents())(scope);
                            }
                        } else {
                            $state.go('site.mm_textviewer', {
                                title: $translate.instant('mm.core.description'),
                                content: text
                            });
                        }
                    }
                });
                renderText(scope, element, shortened, attrs.afterRender);
            } else {
                renderText(scope, element, fullText, attrs.afterRender);
            }
        });
    }
        function formatContents(scope, element, attrs, text, shorten) {
        var siteId = scope.siteid,
            component = attrs.component,
            componentId = attrs.componentId;
        return $mmText.formatText(text, attrs.clean, attrs.singleline, shorten).then(function(formatted) {
            var el = element[0],
                elWidth = el.offsetWidth || el.width || el.clientWidth,
                dom = angular.element('<div>').html(formatted);
            angular.forEach(dom.find('a'), function(anchor) {
                anchor.setAttribute('mm-browser', '');
                addExternalContent(anchor, component, componentId, siteId);
            });
            angular.forEach(dom.find('img'), function(img) {
                addMediaAdaptClass(img);
                addExternalContent(img, component, componentId, siteId);
                var imgWidth = img.offsetWidth || img.width || img.clientWidth;
                if (imgWidth > elWidth) {
                    var div = angular.element('<div class="mm-adapted-img-container"></div>'),
                        jqImg = angular.element(img),
                        label = $mmText.escapeHTML($translate.instant('mm.core.openfullimage')),
                        imgSrc = $mmText.escapeHTML(img.getAttribute('src'));
                    img.style.float = '';
                    jqImg.wrap(div);
                    jqImg.after('<a href="#" class="mm-image-viewer-icon" mm-image-viewer img="' + imgSrc +
                                    '" aria-label="' + label + '"><i class="icon ion-ios-search-strong"></i></a>');
                }
            });
            angular.forEach(dom.find('audio'), function(el) {
                treatMedia(el, component, componentId, siteId);
            });
            angular.forEach(dom.find('video'), function(el) {
                treatMedia(el, component, componentId, siteId);
            });
            angular.forEach(dom.find('iframe'), addMediaAdaptClass);
            return dom.html();
        });
    }
        function renderText(scope, element, text, afterRender) {
        element.html(text);
        element.removeClass('hide');
        $compile(element.contents())(scope);
        if (afterRender && scope[afterRender]) {
            scope[afterRender](scope);
        }
    }
        function treatMedia(el, component, componentId, siteId) {
        addMediaAdaptClass(el);
        addExternalContent(el, component, componentId, siteId);
        angular.forEach(angular.element(el).find('source'), function(source) {
            source.setAttribute('target-src', source.getAttribute('src'));
            source.removeAttribute('src');
            addExternalContent(source, component, componentId, siteId);
        });
    }
    return {
        restrict: 'E',
        scope: true,
        link: function(scope, element, attrs) {
            element.addClass('hide');
            var content = element.html();
            if (attrs.watch) {
                var matches = content.match(extractVariableRegex);
                if (matches && typeof matches[1] == 'string') {
                    var variable = matches[1].trim();
                    scope.$watch(variable, function() {
                        formatAndRenderContents(scope, element, attrs, content);
                    });
                }
            } else {
                formatAndRenderContents(scope, element, attrs, content);
            }
        }
    };
}]);

angular.module('mm.core')
.directive('mmIframe', ["$mmUtil", function($mmUtil) {
    var errorShownTime = 0,
        tags = ['iframe', 'frame', 'object', 'embed'];
        function treatFrame(element) {
        if (element) {
            redefineWindowOpen(element);
            treatLinks(element);
            element.on('load', function() {
                redefineWindowOpen(element);
                treatLinks(element);
            });
        }
    }
        function redefineWindowOpen(element) {
        var el = element[0],
            contentWindow = element.contentWindow || el.contentWindow,
            contents = element.contents();
        if (!contentWindow && el && el.contentDocument) {
            contentWindow = el.contentDocument.defaultView;
        }
        if (!contentWindow && el && el.getSVGDocument) {
            var svgDoc = el.getSVGDocument;
            if (svgDoc && svgDoc.defaultView) {
                contents = angular.element(svgdoc);
                contentWindow = svgdoc.defaultView;
            } else if (el.window) {
                contentWindow = el.window;
            } else if (el.getWindow) {
                contentWindow = el.getWindow();
            }
        }
        if (contentWindow) {
            contentWindow.open = function () {
                var currentTime = new Date().getTime();
                if (currentTime - errorShownTime > 500) {
                    errorShownTime = currentTime;
                    $mmUtil.showErrorModal('mm.core.erroropenpopup', true);
                }
                return {};
            };
        }
        angular.forEach(tags, function(tag) {
            angular.forEach(contents.find(tag), function(subelement) {
                treatFrame(angular.element(subelement));
            });
        });
    }
        function treatLinks(element) {
        var links = element.contents().find('a');
        angular.forEach(links, function(el) {
            var href = el.href;
            if (href) {
                if (href.indexOf('http') === 0) {
                    angular.element(el).on('click', function(e) {
                        e.preventDefault();
                        $mmUtil.openInBrowser(href);
                    });
                } else if (el.target == '_parent' || el.target == '_top' || el.target == '_blank') {
                    angular.element(el).on('click', function(e) {
                        e.preventDefault();
                        $mmUtil.openInApp(href);
                    });
                }
            }
        });
    }
    return {
        restrict: 'E',
        template: '<div class="iframe-wrapper"><iframe class="mm-iframe" ng-style="{\'width\': width, \'height\': height}" ng-src="{{src}}"></iframe></div>',
        scope: {
            src: '='
        },
        link: function(scope, element, attrs) {
            scope.width = $mmUtil.formatPixelsSize(attrs.iframeWidth) || '100%';
            scope.height = $mmUtil.formatPixelsSize(attrs.iframeHeight) || '100%';
            var iframe = angular.element(element.find('iframe')[0]);
            treatFrame(iframe);
        }
    };
}]);

angular.module('mm.core')
.directive('mmImageViewer', ["$ionicModal", function($ionicModal) {
    return {
        restrict: 'A',
        priority: 500,
        scope: true,
        link: function(scope, element, attrs) {
            if (attrs.img) {
                scope.img = attrs.img;
                scope.closeModal = function(){
                    scope.modal.hide();
                };
                element.on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (!scope.modal) {
                        $ionicModal.fromTemplateUrl('core/templates/imageviewer.html', {
                            scope: scope,
                            animation: 'slide-in-up'
                        }).then(function(m) {
                            scope.modal = m;
                            scope.modal.show();
                        });
                    } else {
                        scope.modal.show();
                    }
                });
                scope.$on('$destroy', function() {
                    if (scope.modal) {
                        scope.modal.remove();
                    }
                });
            }
        }
    };
}]);

angular.module('mm.core')
.directive('mmLoading', ["$translate", function($translate) {
    return {
        restrict: 'E',
        templateUrl: 'modules/moodle/core/templates/loading.html',
        transclude: true,
        scope: {
            hideUntil: '=?',
            message: '@?',
            dynMessage: '=?',
            loadingPaddingTop: '=?'
        },
        link: function(scope, element, attrs) {
            var el = element[0],
                loading = angular.element(el.querySelector('.mm-loading-container'));
            if (!attrs.message) {
                $translate('mm.core.loading').then(function(loadingString) {
                    scope.message = loadingString;
                });
            }
            if (attrs.loadingPaddingTop) {
                scope.$watch('loadingPaddingTop', function(newValue) {
                    var num = parseInt(newValue);
                    if (num >= 0 || num < 0) {
                        loading.css('padding-top', newValue + 'px');
                    } else if(typeof newValue == 'string') {
                        loading.css('padding-top', newValue);
                    }
                });
            }
        }
    };
}]);

angular.module('mm.core')
.directive('mmNavigationBar', ["$state", "$translate", function($state, $translate) {
    return {
        restrict: 'E',
        scope: {
            previous: '=?',
            next: '=?',
            action: '=?',
            info: '=?'
        },
        templateUrl: 'modules/moodle/core/templates/navigationbar.html',
        link: function(scope, element, attrs) {
            scope.title = attrs.title || $translate.instant('mm.core.info');
            scope.showInfo = function() {
                $state.go('site.mm_textviewer', {
                    title: scope.title,
                    content: scope.info
                });
            };
        }
    };
}]);

angular.module('mm.core')
.directive('mmNoInputValidation', function() {
    return {
        restrict: 'A',
        priority: 500,
        compile: function(el, attrs) {
            attrs.$set('type',
                null,               
                false               
            );
        }
    }
});

angular.module('mm.core')
.constant('mmCoreSplitViewLoad', 'mmSplitView:load')
.directive('mmSplitView', ["$log", "$state", "$ionicPlatform", "$timeout", "$mmUtil", "$interpolate", "mmCoreSplitViewLoad", function($log, $state, $ionicPlatform, $timeout, $mmUtil, $interpolate, mmCoreSplitViewLoad) {
    $log = $log.getInstance('mmSplitView');
        function triggerClick(link) {
        if (link && link.length && link.triggerHandler) {
            link.triggerHandler('click');
            return true;
        }
        return false;
    }
    function controller() {
        var self = this,
            element,
            menuState,
            linkToLoad,
            component;
                this.clearMarkedLinks = function() {
            angular.element(element.querySelectorAll('[mm-split-view-link]')).removeClass('mm-split-item-selected');
        };
                this.getComponent = function() {
            return component;
        };
                this.getMenuState = function() {
            return menuState || $state.current.name;
        };
                this.loadLink = function(scope, loadAttr, retrying) {
            if ($ionicPlatform.isTablet()) {
                if (!linkToLoad) {
                    if (typeof loadAttr != 'undefined') {
                        var position = parseInt(loadAttr);
                        if (!position) {
                            position = parseInt($interpolate(loadAttr)(scope), 10);
                        }
                        if (position) {
                            var links = element.querySelectorAll('[mm-split-view-link]');
                            position = position > links.length ? 0 : position - 1;
                            linkToLoad = angular.element(links[position]);
                        } else {
                            linkToLoad = angular.element(element.querySelector('[mm-split-view-link]'));
                        }
                    } else {
                        linkToLoad = angular.element(element.querySelector('[mm-split-view-link]'));
                    }
                }
                if (!triggerClick(linkToLoad)) {
                    if (!retrying) {
                        linkToLoad = undefined;
                        $timeout(function() {
                            self.loadLink(scope, loadAttr, true);
                        });
                    }
                }
            }
        };
                this.setComponent = function(cmp) {
            component = cmp;
        };
                this.setElement = function(el) {
            element = el;
        };
                this.setLink = function(link) {
            linkToLoad = link;
        };
                this.setMenuState = function(state) {
            menuState = state;
        };
    }
    return {
        restrict: 'E',
        templateUrl: 'modules/moodle/core/templates/splitview.html',
        transclude: true,
        controller: controller,
        link: function(scope, element, attrs, controller) {
            var el = element[0],
                menu = angular.element(el.querySelector('.mm-split-pane-menu')),
                menuState = $state.$current.name,
                menuParams = $state.params,
                menuWidth = attrs.menuWidth,
                component = attrs.component || 'tablet';
            scope.component = component;
            controller.setComponent(component);
            controller.setElement(el);
            controller.setMenuState(menuState);
            if (menuWidth && $ionicPlatform.isTablet()) {
                menu.css('width', menuWidth);
                menu.css('-webkit-flex-basis', menuWidth);
                menu.css('-moz-flex-basis', menuWidth);
                menu.css('-ms-flex-basis', menuWidth);
                menu.css('flex-basis', menuWidth);
            }
            if (attrs.loadWhen) {
                scope.$watch(attrs.loadWhen, function(newValue) {
                    if (newValue) {
                        controller.loadLink(scope, attrs.load);
                    }
                });
            } else {
                controller.loadLink(scope, attrs.load);
            }
            scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                if (toState.name === menuState && $mmUtil.basicLeftCompare(toParams, menuParams, 1)) {
                    controller.loadLink();
                }
            });
            scope.$on(mmCoreSplitViewLoad, function(e, data) {
                if (data && data.load) {
                    controller.loadLink(scope, data.load);
                } else {
                    controller.loadLink(scope, attrs.load);
                }
            });
        }
    };
}]);

angular.module('mm.core')
.directive('mmSplitViewLink', ["$log", "$ionicPlatform", "$state", "$mmApp", function($log, $ionicPlatform, $state, $mmApp) {
    $log = $log.getInstance('mmSplitViewLink');
    var srefRegex = new RegExp(/([^\(]*)(\((.*)\))?$/);
        function createTabletState(stateName, tabletStateName, newViewName) {
        var targetState = $state.get(stateName),
            newConfig,
            viewName;
        if (targetState) {
            newConfig = angular.copy(targetState);
            viewName = Object.keys(newConfig.views)[0];
            newConfig.views[newViewName] = newConfig.views[viewName];
            delete newConfig.views[viewName];
            delete newConfig['name'];
            $mmApp.createState(tabletStateName, newConfig);
            return true;
        } else {
            $log.error('State doesn\'t exist: '+stateName);
            return false;
        }
    }
        function scopeEval(scope, value) {
        if (typeof value == 'string') {
            try {
                return scope.$eval(value);
            } catch(ex) {
                $log.error('Error evaluating string: ' + param);
            }
        }
    }
    return {
        restrict: 'A',
        require: '^mmSplitView',
        link: function(scope, element, attrs, splitViewController) {
            var sref = attrs.mmSplitViewLink,
                menuState = splitViewController.getMenuState(),
                matches,
                stateName,
                stateParams,
                stateParamsString,
                tabletStateName;
            if (sref) {
                matches = sref.match(srefRegex);
                if (matches && matches.length) {
                    stateName = matches[1];
                    tabletStateName = menuState + '.' + stateName.substr(stateName.lastIndexOf('.') + 1);
                    stateParamsString = matches[3];
                    stateParams = scopeEval(scope, stateParamsString);
                    scope.$watch(stateParamsString, function(newVal) {
                        stateParams = newVal;
                    });
                    element.on('click', function(event) {
                        event.stopPropagation();
                        event.preventDefault();
                        if ($ionicPlatform.isTablet()) {
                            if (!$state.get(tabletStateName)) {
                                if (!createTabletState(stateName, tabletStateName, splitViewController.getComponent())) {
                                    return;
                                }
                            }
                            splitViewController.setLink(element);
                            splitViewController.clearMarkedLinks();
                            element.addClass('mm-split-item-selected');
                            $state.go(tabletStateName, stateParams, {location:'replace'});
                        } else {
                            $state.go(stateName, stateParams);
                        }
                    });
                } else {
                    $log.error('Invalid sref.');
                }
            } else {
                $log.error('Invalid sref.');
            }
        }
    };
}]);

angular.module('mm.core')
.directive('mmStateClass', ["$state", function($state) {
    return {
        restrict: 'A',
        link: function(scope, el) {
            var current = $state.$current.name,
                split,
                className = 'mm-';
            if (typeof current == 'string') {
                split = current.split('.');
                className += split.shift();
                if (split.length) {
                    className += '_' + split.pop();
                }
                el.addClass(className);
            }
        }
    };
}]);

angular.module('mm.core.contentlinks', [])
.run(["$log", "$mmURLDelegate", "$mmContentLinksHelper", function($log, $mmURLDelegate, $mmContentLinksHelper) {
    $log = $log.getInstance('mmContentLinks');
    $mmURLDelegate.register('mmContentLinks', $mmContentLinksHelper.handleCustomUrl);
}]);

angular.module('mm.core.course', ['mm.core.courses'])
.constant('mmCoreCoursePriority', 800)
.constant('mmCoreCourseAllSectionsId', -1)
.config(["$stateProvider", "$mmCoursesDelegateProvider", "mmCoreCoursePriority", function($stateProvider, $mmCoursesDelegateProvider, mmCoreCoursePriority) {
    $stateProvider
    .state('site.mm_course', {
        url: '/mm_course',
        params: {
            courseid: null,
            sid: null,
            moduleid: null
        },
        views: {
            'site': {
                templateUrl: 'modules/moodle/core/components/course/templates/sections.html',
                controller: 'mmCourseSectionsCtrl'
            }
        }
    })
    .state('site.mm_course-section', {
        url: '/mm_course-section',
        params: {
            sectionid: null,
            cid: null,
            mid: null
        },
        views: {
            'site': {
                templateUrl: 'modules/moodle/core/components/course/templates/section.html',
                controller: 'mmCourseSectionCtrl'
            }
        }
    })
    .state('site.mm_course-modcontent', {
        url: '/mm_course-modcontent',
        params: {
            module: null
        },
        views: {
            site: {
                templateUrl: 'modules/moodle/core/components/course/templates/modcontent.html',
                controller: 'mmCourseModContentCtrl'
            }
        }
    });
    $mmCoursesDelegateProvider.registerNavHandler('mmCourse', '$mmCourseCoursesNavHandler', mmCoreCoursePriority);
}])
.run(["$mmEvents", "mmCoreEventLogin", "mmCoreEventSiteUpdated", "$mmCourseDelegate", function($mmEvents, mmCoreEventLogin, mmCoreEventSiteUpdated, $mmCourseDelegate) {
    $mmEvents.on(mmCoreEventLogin, $mmCourseDelegate.updateContentHandlers);
    $mmEvents.on(mmCoreEventSiteUpdated, $mmCourseDelegate.updateContentHandlers);
}]);

angular.module('mm.core.courses', [])
.constant('mmCoursesSearchComponent', 'mmCoursesSearch')
.constant('mmCoursesSearchPerPage', 20)
.constant('mmCoursesEnrolInvalidKey', 'mmCoursesEnrolInvalidKey')
.constant('mmCoursesEventMyCoursesUpdated', 'my_courses_updated')
.constant('mmCoursesAccessMethods', {
     guest: 'guest',
     default: 'default'
})
.config(["$mmContentLinksDelegateProvider", function($mmContentLinksDelegateProvider) {
    $mmContentLinksDelegateProvider.registerLinkHandler('mmCourses', '$mmCoursesHandlers.linksHandler');
}])
.run(["$mmEvents", "mmCoreEventLogin", "mmCoreEventSiteUpdated", "mmCoreEventLogout", "$mmCoursesDelegate", "$mmCourses", function($mmEvents, mmCoreEventLogin, mmCoreEventSiteUpdated, mmCoreEventLogout, $mmCoursesDelegate, $mmCourses) {
    $mmEvents.on(mmCoreEventLogin, $mmCoursesDelegate.updateNavHandlers);
    $mmEvents.on(mmCoreEventSiteUpdated, $mmCoursesDelegate.updateNavHandlers);
    $mmEvents.on(mmCoreEventLogout, function() {
        $mmCoursesDelegate.clearCoursesHandlers();
        $mmCourses.clearCurrentCourses();
    });
}]);

angular.module('mm.core.login', [])
.config(["$stateProvider", "$urlRouterProvider", "$mmInitDelegateProvider", "mmInitDelegateMaxAddonPriority", function($stateProvider, $urlRouterProvider, $mmInitDelegateProvider, mmInitDelegateMaxAddonPriority) {
    $urlRouterProvider.otherwise(function($injector) {
        var $state = $injector.get('$state');
        return $state.href('app.home').replace('#', '');
    });
    $mmInitDelegateProvider.registerProcess('mmLogin', '$mmSitesManager.restoreSession', mmInitDelegateMaxAddonPriority + 200);
}])
.run(["$log", "$state", "$mmUtil", "$translate", "$mmSitesManager", "$rootScope", "$mmSite", "$mmURLDelegate", "$ionicHistory", "$mmEvents", "$mmLoginHelper", "mmCoreEventSessionExpired", "$mmApp", "mmCoreConfigConstants", function($log, $state, $mmUtil, $translate, $mmSitesManager, $rootScope, $mmSite, $mmURLDelegate, $ionicHistory,
                $mmEvents, $mmLoginHelper, mmCoreEventSessionExpired, $mmApp, mmCoreConfigConstants) {
    $log = $log.getInstance('mmLogin');
    $mmEvents.on(mmCoreEventSessionExpired, sessionExpired);
    $mmURLDelegate.register('mmLoginSSO', appLaunchedByURL);
    function sessionExpired(siteid) {
        var siteurl = $mmSite.getURL();
        if (typeof(siteurl) !== 'undefined') {
            if (siteid && siteid !== $mmSite.getId()) {
                return;
            }
            $mmSitesManager.checkSite(siteurl).then(function(result) {
                if (result.warning) {
                    $mmUtil.showErrorModal(result.warning, true, 4000);
                }
                if ($mmLoginHelper.isSSOLoginNeeded(result.code)) {
                    $mmUtil.showConfirm($translate('mm.login.reconnectssodescription')).then(function() {
                        $mmLoginHelper.openBrowserForSSOLogin(result.siteurl);
                    });
                } else {
                    var info = $mmSite.getInfo();
                    if (typeof(info) !== 'undefined' && typeof(info.username) !== 'undefined') {
                        $ionicHistory.nextViewOptions({disableBack: true});
                        $state.go('mm_login.reconnect',
                                        {siteurl: result.siteurl, username: info.username, infositeurl: info.siteurl});
                    }
                }
            });
        }
    }
    function appLaunchedByURL(url) {
        var ssoScheme = mmCoreConfigConstants.customurlscheme + '://token=';
        if (url.indexOf(ssoScheme) == -1) {
            return false;
        }
        $mmLoginHelper.setSSOLoginOngoing(true);
        $log.debug('App launched by URL');
        var modal = $mmUtil.showModalLoading('mm.login.authenticating', true);
        url = url.replace(ssoScheme, '');
        try {
            url = atob(url);
        } catch(err) {
            $log.error('Error decoding parameter received for login SSO');
            return false;
        }
        $mmLoginHelper.validateBrowserSSOLogin(url).then(function(sitedata) {
            return $mmLoginHelper.handleSSOLoginAuthentication(sitedata.siteurl, sitedata.token).then(function() {
                return $mmLoginHelper.goToSiteInitialPage();
            });
        }).catch(function(errorMessage) {
            if (typeof errorMessage === 'string' && errorMessage !== '') {
                $mmUtil.showErrorModal(errorMessage);
            }
        }).finally(function() {
            modal.dismiss();
            $mmLoginHelper.setSSOLoginOngoing(false);
        });
        return true;
    }
}]);

angular.module('mm.core.sidemenu', [])
.config(["$stateProvider", function($stateProvider) {
    $stateProvider
    .state('site', {
        url: '/site',
        templateUrl: 'modules/portal/components/index/tpl/index.html',
        controller: 'IndexCtrl',
        abstract: true,
        cache: false,
        onEnter: ["$ionicHistory", "$state", "$mmSite", "$timeout", function($ionicHistory, $state, $mmSite, $timeout) {
            $ionicHistory.clearHistory();
        }]
    });
}])
.run(["$mmEvents", "mmCoreEventLogin", "mmCoreEventSiteUpdated", "mmCoreEventLogout", "$mmSideMenuDelegate", function($mmEvents, mmCoreEventLogin, mmCoreEventSiteUpdated, mmCoreEventLogout, $mmSideMenuDelegate) {
    $mmEvents.on(mmCoreEventLogin, $mmSideMenuDelegate.updateNavHandlers);
    $mmEvents.on(mmCoreEventSiteUpdated, $mmSideMenuDelegate.updateNavHandlers);
    $mmEvents.on(mmCoreEventLogout, $mmSideMenuDelegate.clearSiteHandlers);
}]);

angular.module('mm.core.user', [])
.value('mmUserProfileState', 'site.mm_user-profile')
.config(["$stateProvider", "$mmContentLinksDelegateProvider", function($stateProvider, $mmContentLinksDelegateProvider) {
    $mmContentLinksDelegateProvider.registerLinkHandler('mmUser', '$mmUserHandlers.linksHandler');
}])
.run(["$mmEvents", "mmCoreEventLogin", "mmCoreEventSiteUpdated", "$mmUserDelegate", "$mmSite", "mmCoreEventUserDeleted", "$mmUser", function($mmEvents, mmCoreEventLogin, mmCoreEventSiteUpdated, $mmUserDelegate, $mmSite, mmCoreEventUserDeleted, $mmUser) {
    $mmEvents.on(mmCoreEventLogin, $mmUserDelegate.updateProfileHandlers);
    $mmEvents.on(mmCoreEventSiteUpdated, $mmUserDelegate.updateProfileHandlers);
    $mmEvents.on(mmCoreEventUserDeleted, function(data) {
        if (data.siteid && data.siteid === $mmSite.getId() && data.params) {
            var params = data.params,
                userid = 0;
            if (params.userid) {
                userid = params.userid;
            } else if (params.userids) {
                userid = params.userids[0];
            } else if (params.field === 'id' && params.values && params.values.length) {
                userid = params.values[0];
            } else if (params.userlist && params.userlist.length) {
                userid = params.userlist[0].userid;
            }
            userid = parseInt(userid);
            if (userid > 0) {
                $mmUser.deleteStoredUser(userid);
            }
        }
    });
}]);

angular.module('mm.core.contentlinks')
.provider('$mmContentLinksDelegate', function() {
    var linkHandlers = {},
        self = {};
        self.registerLinkHandler = function(name, handler, priority) {
        if (typeof linkHandlers[name] !== 'undefined') {
            console.log("$mmContentLinksDelegateProvider: Addon '" + linkHandlers[name].name +
                        "' already registered as link handler");
            return false;
        }
        console.log("$mmContentLinksDelegateProvider: Registered handler '" + name + "' as link handler.");
        linkHandlers[name] = {
            name: name,
            handler: handler,
            instance: undefined,
            priority: typeof priority === 'undefined' ? 100 : priority
        };
        return true;
    };
    self.$get = ["$mmUtil", "$log", "$q", "$mmSitesManager", function($mmUtil, $log, $q, $mmSitesManager) {
        var self = {};
        $log = $log.getInstance('$mmContentLinksDelegate');
                self.getActionsFor = function(url, courseId, username) {
            if (!url) {
                return $q.when([]);
            }
            return $mmSitesManager.getSiteIdsFromUrl(url, true, username).then(function(siteIds) {
                var linkActions = [],
                    promises = [];
                angular.forEach(linkHandlers, function(handler) {
                    if (typeof handler.instance === 'undefined') {
                        handler.instance = $mmUtil.resolveObject(handler.handler, true);
                    }
                    if (handler.instance) {
                        promises.push($q.when(handler.instance.getActions(siteIds, url, courseId)).then(function(actions) {
                            if (actions && actions.length) {
                                linkActions.push({
                                    priority: handler.priority,
                                    actions: actions
                                });
                            }
                        }));
                    }
                });
                return $mmUtil.allPromises(promises).catch(function() {}).then(function() {
                    return sortActionsByPriority(linkActions);
                });
            });
        };
                self.getSiteUrl = function(url) {
            if (!url) {
                return;
            }
            for (var name in linkHandlers) {
                var handler = linkHandlers[name];
                if (typeof handler.instance === 'undefined') {
                    handler.instance = $mmUtil.resolveObject(handler.handler, true);
                }
                if (handler.instance && handler.instance.handles) {
                    var siteUrl = handler.instance.handles(url);
                    if (siteUrl) {
                        return siteUrl;
                    }
                }
            }
        };
                function sortActionsByPriority(actions) {
            var sorted = [];
            actions = actions.sort(function(a, b) {
                return a.priority > b.priority;
            });
            actions.forEach(function(entry) {
                sorted = sorted.concat(entry.actions);
            });
            return sorted;
        }
        return self;
    }];
    return self;
});

angular.module('mm.core.contentlinks')
.factory('$mmContentLinksHelper', ["$log", "$ionicHistory", "$state", "$mmSite", "$mmContentLinksDelegate", "$mmUtil", "$translate", "$mmCourseHelper", "$mmSitesManager", "$q", "$mmLoginHelper", "$mmText", "mmCoreConfigConstants", function($log, $ionicHistory, $state, $mmSite, $mmContentLinksDelegate, $mmUtil, $translate,
            $mmCourseHelper, $mmSitesManager, $q, $mmLoginHelper, $mmText, mmCoreConfigConstants) {
    $log = $log.getInstance('$mmContentLinksHelper');
    var self = {};
        self.filterSupportedSites = function(siteIds, isEnabledFn, checkAll) {
        var promises = [],
            supported = [],
            extraParams = Array.prototype.slice.call(arguments, 3);
        angular.forEach(siteIds, function(siteId) {
            if (checkAll || !promises.length) {
                promises.push(isEnabledFn.apply(isEnabledFn, [siteId].concat(extraParams)).then(function(enabled) {
                    if (enabled) {
                        supported.push(siteId);
                    }
                }));
            }
        });
        return $mmUtil.allPromises(promises).catch(function() {}).then(function() {
            if (!checkAll) {
                if (supported.length) {
                    return siteIds;
                } else {
                    return [];
                }
            } else {
                return supported;
            }
        });
    };
        self.getFirstValidAction = function(actions) {
        if (actions) {
            for (var i = 0; i < actions.length; i++) {
                var action = actions[i];
                if (action && action.sites && action.sites.length && angular.isFunction(action.action)) {
                    return action;
                }
            }
        }
    };
        self.goInSite = function(stateName, stateParams, siteId) {
        siteId = siteId || $mmSite.getId();
        if (siteId == $mmSite.getId()) {
            return $state.go(stateName, stateParams);
        } else {
            return $state.go('redirect', {
                siteid: siteId,
                state: stateName,
                params: stateParams
            });
        }
    };
        self.goToChooseSite = function(url) {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        return $state.go('mm_contentlinks.choosesite', {url: url});
    };
        self.handleCustomUrl = function(url) {
        var contentLinksScheme = mmCoreConfigConstants.customurlscheme + '://link=';
        if (url.indexOf(contentLinksScheme) == -1) {
            return false;
        }
        $log.debug('Treating custom URL scheme: ' + url);
        var modal = $mmUtil.showModalLoading(),
            username;
        url = url.replace(contentLinksScheme, '');
        username = $mmText.getUsernameFromUrl(url);
        if (username) {
            url = url.replace(username + '@', '');
        }
        $mmSitesManager.getSiteIdsFromUrl(url, false, username).then(function(siteIds) {
            if (siteIds.length) {
                modal.dismiss();
                return self.handleLink(url, username).then(function(treated) {
                    if (!treated) {
                        $mmUtil.showErrorModal('mm.contentlinks.errornoactions', true);
                    }
                });
            } else {
                var siteUrl = $mmContentLinksDelegate.getSiteUrl(url),
                    formatted = $mmUtil.formatURL(siteUrl);
                if (!siteUrl || !$mmUtil.isValidURL(formatted)) {
                    $mmUtil.showErrorModal('mm.login.invalidsite', true);
                    return;
                }
                return $mmSitesManager.checkSite(siteUrl).then(function(result) {
                    var promise,
                        ssoNeeded = $mmLoginHelper.isSSOLoginNeeded(result.code);
                    modal.dismiss();
                    if (!$mmSite.isLoggedIn()) {
                        if (ssoNeeded) {
                            promise = $mmUtil.showConfirm($translate('mm.login.logininsiterequired'));
                        } else {
                            promise = $q.when();
                        }
                    } else {
                        promise = $mmUtil.showConfirm($translate('mm.contentlinks.confirmurlothersite')).then(function() {
                            if (!ssoNeeded) {
                                return $mmSitesManager.logout().catch(function() {
                                });
                            }
                        });
                    }
                    return promise.then(function() {
                        if (ssoNeeded) {
                            $mmLoginHelper.openBrowserForSSOLogin(result.siteurl);
                        } else {
                            $state.go('mm_login.credentials', {
                                siteurl: result.siteurl,
                                username: username,
                                urltoopen: url
                            });
                        }
                    });
                }, function(error) {
                    $mmUtil.showErrorModal(error);
                });
            }
        }).finally(function() {
            modal.dismiss();
        });
        return true;
    };
        self.handleLink = function(url, username) {
        return $mmContentLinksDelegate.getActionsFor(url, undefined, username).then(function(actions) {
            var action = self.getFirstValidAction(actions);
            if (action) {
                if (!$mmSite.isLoggedIn()) {
                    if (action.sites.length == 1) {
                        action.action(action.sites[0]);
                    } else {
                        self.goToChooseSite(url);
                    }
                } else if (action.sites.length == 1 && action.sites[0] == $mmSite.getId()) {
                    action.action(action.sites[0]);
                } else {
                    $mmUtil.showConfirm($translate('mm.contentlinks.confirmurlothersite')).then(function() {
                        if (action.sites.length == 1) {
                            action.action(action.sites[0]);
                        } else {
                            self.goToChooseSite(url);
                        }
                    });
                }
                return true;
            }
            return false;
        }).catch(function() {
            return false;
        });
    };
        self.treatModuleIndexUrl = function(siteIds, url, isEnabled, courseId) {
        var params = $mmUtil.extractUrlParams(url);
        if (typeof params.id != 'undefined') {
            courseId = courseId || params.courseid || params.cid;
            return self.filterSupportedSites(siteIds, isEnabled, false, courseId).then(function(ids) {
                if (!ids.length) {
                    return [];
                } else {
                    return [{
                        message: 'mm.core.view',
                        icon: 'ion-eye',
                        sites: ids,
                        action: function(siteId) {
                            $mmCourseHelper.navigateToModule(parseInt(params.id, 10), siteId, courseId);
                        }
                    }];
                }
            });
        }
        return $q.when([]);
    };
    return self;
}]);

angular.module('mm.core.course')
.controller('mmCourseModContentCtrl', ["$log", "$stateParams", "$scope", function($log, $stateParams, $scope) {
    $log = $log.getInstance('mmCourseModContentCtrl');
    var module = $stateParams.module || {};
    $scope.description = module.description;
    $scope.title = module.name;
    $scope.url = module.url;
}]);

angular.module('mm.core.course')
.controller('mmCourseSectionCtrl', ["$mmCourseDelegate", "$mmCourse", "$mmUtil", "$scope", "$stateParams", "$translate", "$mmSite", "$mmEvents", "$ionicScrollDelegate", "$mmCourses", "$q", "mmCoreEventCompletionModuleViewed", "$controller", function($mmCourseDelegate, $mmCourse, $mmUtil, $scope, $stateParams, $translate, $mmSite,
            $mmEvents, $ionicScrollDelegate, $mmCourses, $q, mmCoreEventCompletionModuleViewed, $controller) {
    var courseId = $stateParams.cid || 1,
        sectionId = $stateParams.sectionid || -1,
        moduleId = $stateParams.mid;
    $scope.sitehome = (courseId === 1);
    $scope.sections = [];
    if (sectionId < 0) {
        if ($scope.sitehome) {
            $scope.title = $translate.instant('mma.frontpage.sitehome');
        } else {
            $scope.title = $translate.instant('mm.course.allsections');
        }
        $scope.summary = null;
    }
    function loadContent(sectionId) {
        return $mmCourses.getUserCourse(courseId, true).catch(function() {
        }).then(function(course) {
            var promise;
            if (course && course.enablecompletion === false) {
                promise = $q.when([]);
            } else {
                promise = $mmCourse.getActivitiesCompletionStatus(courseId).catch(function() {
                    return [];
                });
            }
            return promise.then(function(statuses) {
                var promise,
                    sectionnumber;
                if (sectionId < 0) {
                    sectionnumber = 0;
                    promise = $mmCourse.getSections(courseId);
                } else {
                    sectionnumber = sectionId;
                    promise = $mmCourse.getSection(courseId, sectionId).then(function(section) {
                        $scope.title = section.name;
                        $scope.summary = section.summary;
                        return [section];
                    });
                }
                return promise.then(function(sections) {
                    if ($scope.sitehome) {
                        sections.reverse();
                    }
                    var hasContent = false;
                    angular.forEach(sections, function(section) {
                        if (section.summary != '' || section.modules.length) {
                            hasContent = true;
                        }
                        angular.forEach(section.modules, function(module) {
                            module._controller =
                                    $mmCourseDelegate.getContentHandlerControllerFor(module.modname, module, courseId, section.id);
                            var status = statuses[module.id];
                            if (typeof status != 'undefined') {
                                module.completionstatus = status;
                            }
                            if (module.id == moduleId) {
                                var scope = $scope.$new();
                                $controller(module._controller, {$scope: scope});
                                if (scope.action) {
                                    scope.action();
                                }
                            }
                        });
                    });
                    $scope.sections = sections;
                    $scope.hasContent = hasContent;
                    $mmSite.write('core_course_view_course', {
                        courseid: courseId,
                        sectionnumber: sectionnumber
                    });
                }, function(error) {
                    if (error) {
                        $mmUtil.showErrorModal(error);
                    } else {
                        $mmUtil.showErrorModal('mm.course.couldnotloadsectioncontent', true);
                    }
                });
            });
        });
    }
    loadContent(sectionId).finally(function() {
        $scope.sectionLoaded = true;
    });
    $scope.doRefresh = function() {
        $mmCourse.invalidateSections(courseId).finally(function() {
            loadContent(sectionId).finally(function() {
                $scope.$broadcast('scroll.refreshComplete');
            });
        });
    };
    function refreshAfterCompletionChange() {
        var scrollView = $ionicScrollDelegate.$getByHandle('mmSectionScroll');
        if (scrollView && scrollView.getScrollPosition()) {
            $scope.loadingPaddingTop = scrollView.getScrollPosition().top;
        }
        $scope.sectionLoaded = false;
        $scope.sections = [];
        loadContent(sectionId).finally(function() {
            $scope.sectionLoaded = true;
            $scope.loadingPaddingTop = 0;
        });
    }
    $scope.completionChanged = function() {
        $mmCourse.invalidateSections(courseId).finally(function() {
            refreshAfterCompletionChange();
        });
    };
    var observer = $mmEvents.on(mmCoreEventCompletionModuleViewed, function(cid) {
        if (cid === courseId) {
            refreshAfterCompletionChange();
        }
    });
    $scope.$on('$destroy', function() {
        if (observer && observer.off) {
            observer.off();
        }
    });
}]);

angular.module('mm.core.course')
.controller('mmCourseSectionsCtrl', ["$mmCourse", "$mmUtil", "$scope", "$stateParams", "$translate", "$mmCourseHelper", "$mmEvents", "$mmSite", "$mmCoursePrefetchDelegate", "$mmCourses", "$q", "$ionicHistory", "$ionicPlatform", "mmCoreCourseAllSectionsId", "mmCoreEventSectionStatusChanged", "$mmConfig", "mmCoreSettingsDownloadSection", "$state", "$timeout", function($mmCourse, $mmUtil, $scope, $stateParams, $translate, $mmCourseHelper, $mmEvents,
            $mmSite, $mmCoursePrefetchDelegate, $mmCourses, $q, $ionicHistory, $ionicPlatform, mmCoreCourseAllSectionsId,
            mmCoreEventSectionStatusChanged, $mmConfig, mmCoreSettingsDownloadSection, $state, $timeout) {
    var courseId = $stateParams.courseid,
        sectionId = $stateParams.sid,
        moduleId = $stateParams.moduleid,
        downloadSectionsEnabled;
    $scope.courseId = courseId;
    $scope.sectionToLoad = 2;
    function checkDownloadSectionsEnabled() {
        return $mmConfig.get(mmCoreSettingsDownloadSection, true).then(function(enabled) {
            downloadSectionsEnabled = enabled;
        }).catch(function() {
            downloadSectionsEnabled = false;
        });
    }
    function loadSections(refresh) {
        return $mmCourses.getUserCourse(courseId).then(function(course) {
            $scope.fullname = course.fullname;
            return $mmCourse.getSections(courseId).then(function(sections) {
                return $translate('mm.course.allsections').then(function(str) {
                    var result = [{
                        name: str,
                        id: mmCoreCourseAllSectionsId
                    }].concat(sections);
                    $scope.sections = result;
                    if (downloadSectionsEnabled) {
                        return $mmCourseHelper.calculateSectionsStatus(result, courseId, true, refresh).catch(function() {
                        }).then(function(downloadpromises) {
                            if (downloadpromises && downloadpromises.length) {
                                $mmUtil.allPromises(downloadpromises).catch(function() {
                                    if (!$scope.$$destroyed) {
                                        $mmUtil.showErrorModal('mm.course.errordownloadingsection', true);
                                    }
                                }).finally(function() {
                                    if (!$scope.$$destroyed) {
                                        $mmCourseHelper.calculateSectionsStatus($scope.sections, courseId, false);
                                    }
                                });
                            }
                        });
                    }
                });
            });
        }).catch(function(error) {
            if (error) {
                $mmUtil.showErrorModal(error);
            } else {
                $mmUtil.showErrorModal('mm.course.couldnotloadsections', true);
            }
        });
    }
    function prefetch(section, manual) {
        $mmCourseHelper.prefetch(section, courseId, $scope.sections).catch(function() {
            if ($scope.$$destroyed) {
                return;
            }
            var current = $ionicHistory.currentStateName(),
                isCurrent = ($ionicPlatform.isTablet() && current == 'site.mm_course.mm_course-section') ||
                            (!$ionicPlatform.isTablet() && current == 'site.mm_course');
            if (!manual && !isCurrent) {
                return;
            }
            $mmUtil.showErrorModal('mm.course.errordownloadingsection', true);
        }).finally(function() {
            if (!$scope.$$destroyed) {
                $mmCourseHelper.calculateSectionsStatus($scope.sections, courseId, false);
            }
        });
    }
    function autoloadSection() {
        if (sectionId) {
            if ($ionicPlatform.isTablet()) {
                angular.forEach($scope.sections, function(section, index) {
                    if (section.id == sectionId) {
                        $scope.sectionToLoad = index + 1;
                    }
                });
                $scope.moduleId = moduleId;
                $timeout(function() {
                    $scope.moduleId = null;
                }, 500);
            } else {
                $state.go('site.mm_course-section', {
                    sectionid: sectionId,
                    cid: courseId,
                    mid: moduleId
                });
            }
        }
    }
    $scope.doRefresh = function() {
        var promises = [];
        promises.push($mmCourses.invalidateUserCourses());
        promises.push($mmCourse.invalidateSections(courseId));
        $q.all(promises).finally(function() {
            loadSections(true).finally(function() {
                $scope.$broadcast('scroll.refreshComplete');
            });
        });
    };
    $scope.prefetch = function(e, section) {
        e.preventDefault();
        e.stopPropagation();
        $mmCourseHelper.confirmDownloadSize(courseId, section, $scope.sections).then(function() {
            prefetch(section, true);
        });
    };
    checkDownloadSectionsEnabled().then(function() {
        loadSections().finally(function() {
            autoloadSection();
            $scope.sectionsLoaded = true;
        });
    });
    var statusObserver = $mmEvents.on(mmCoreEventSectionStatusChanged, function(data) {
        if (downloadSectionsEnabled && $scope.sections && $scope.sections.length && data.siteid === $mmSite.getId() &&
                    !$scope.$$destroyed&& data.sectionid) {
            if ($mmCoursePrefetchDelegate.isBeingDownloaded($mmCourseHelper.getSectionDownloadId({id: data.sectionid}))) {
                return;
            }
            $mmCourseHelper.calculateSectionsStatus($scope.sections, courseId, false).then(function() {
                var section;
                angular.forEach($scope.sections, function(s) {
                    if (s.id === data.sectionid) {
                        section = s;
                    }
                });
                if (section) {
                    var downloadid = $mmCourseHelper.getSectionDownloadId(section);
                    if (section.isDownloading && !$mmCoursePrefetchDelegate.isBeingDownloaded(downloadid)) {
                        prefetch(section, false);
                    }
                }
            });
        }
    });
    $scope.$on('$destroy', function() {
        statusObserver && statusObserver.off && statusObserver.off();
    });
}]);

angular.module('mm.core.course')
.directive('mmCourseModDescription', function() {
    return {
        compile: function(element, attrs) {
            if (attrs.watch) {
                element.find('mm-format-text').attr('watch', attrs.watch);
            }
            return function(scope) {
                scope.showfull = !!attrs.showfull;
            };
        },
        restrict: 'E',
        scope: {
            description: '=',
            note: '='
        },
        templateUrl: 'modules/moodle/core/components/course/templates/mod_description.html'
    };
});

angular.module('mm.core.course')
.factory('$mmCourseContentHandler', ["$mmCourse", "$mmUtil", function($mmCourse, $mmUtil) {
    return {
        getController: function(module) {
            return function($scope, $state) {
                $scope.icon = $mmCourse.getModuleIconSrc(module.modname);
                $scope.title = module.name;
                $scope.action = function(e) {
                    $state.go('site.mm_course-modcontent', {module: module});
                    e.preventDefault();
                    e.stopPropagation();
                };
                if (module.url) {
                    $scope.buttons = [{
                        icon: 'ion-ios-browsers-outline',
                        label: 'mm.core.openinbrowser',
                        action: function(e) {
                            $mmUtil.openInBrowser(module.url);
                            e.preventDefault();
                            e.stopPropagation();
                        }
                    }];
                }
            };
        }
    };
}]);

angular.module('mm.core.course')
.constant('mmCoreCourseModulesStore', 'course_modules')
.config(["$mmSitesFactoryProvider", "mmCoreCourseModulesStore", function($mmSitesFactoryProvider, mmCoreCourseModulesStore) {
    var stores = [
        {
            name: mmCoreCourseModulesStore,
            keyPath: 'id'
        }
    ];
    $mmSitesFactoryProvider.registerStores(stores);
}])
.factory('$mmCourse', ["$mmSite", "$translate", "$q", "$log", "$mmEvents", "$mmSitesManager", "mmCoreEventCompletionModuleViewed", function($mmSite, $translate, $q, $log, $mmEvents, $mmSitesManager, mmCoreEventCompletionModuleViewed) {
    $log = $log.getInstance('$mmCourse');
    var self = {},
        mods = ["assign", "assignment", "book", "chat", "choice", "data", "database", "date", "external-tool",
            "feedback", "file", "folder", "forum", "glossary", "ims", "imscp", "label", "lesson", "lti", "page", "quiz",
            "resource", "scorm", "survey", "url", "wiki", "workshop"
        ],
        modsWithContent = ['book', 'folder', 'imscp', 'page', 'resource', 'url'];
        function addContentsIfNeeded(module) {
        if (modsWithContent.indexOf(module.modname) > -1) {
            module.contents = module.contents || [];
        }
        return module;
    }
        self.canGetModuleWithoutCourseId = function(siteId) {
        siteId = siteId || $mmSite.getId();
        return $mmSitesManager.getSite(siteId).then(function(site) {
            return site.wsAvailable('core_course_get_course_module');
        });
    };
        self.canGetModuleByInstance = function(siteId) {
        siteId = siteId || $mmSite.getId();
        return $mmSitesManager.getSite(siteId).then(function(site) {
            return site.wsAvailable('core_course_get_course_module_by_instance');
        });
    };
        self.checkModuleCompletion = function(courseId, completion) {
        if (completion && completion.tracking === 2 && completion.state === 0) {
            self.invalidateSections(courseId).finally(function() {
                $mmEvents.trigger(mmCoreEventCompletionModuleViewed, courseId);
            });
        }
    };
        self.getActivitiesCompletionStatus = function(courseid, userid) {
        userid = userid || $mmSite.getUserId();
        $log.debug('Getting completion status for user ' + userid + ' in course ' + courseid);
        var params = {
                courseid: courseid,
                userid: userid
            },
            preSets = {
                cacheKey: getActivitiesCompletionCacheKey(courseid, userid)
            };
        return $mmSite.read('core_completion_get_activities_completion_status', params, preSets).then(function(data) {
            if (data && data.statuses) {
                var formattedStatuses = {};
                angular.forEach(data.statuses, function(status) {
                    formattedStatuses[status.cmid] = status;
                });
                return formattedStatuses;
            }
            return $q.reject();
        });
    };
        function getActivitiesCompletionCacheKey(courseid, userid) {
        return 'mmCourse:activitiescompletion:' + courseid + ':' + userid;
    }
        self.getModuleBasicInfo = function(moduleId, siteId) {
        siteId = siteId || $mmSite.getId();
        return $mmSitesManager.getSite(siteId).then(function(site) {
            var params = {
                    cmid: moduleId
                },
                preSets = {
                    cacheKey: getModuleCacheKey(moduleId)
                };
            return site.read('core_course_get_course_module', params, preSets).then(function(response) {
                if (response.cm && (!response.warnings || !response.warnings.length)) {
                    return response.cm;
                }
                return $q.reject();
            });
        });
    };
        self.getModuleBasicInfoByInstance = function(id, module, siteId) {
        siteId = siteId || $mmSite.getId();
        return $mmSitesManager.getSite(siteId).then(function(site) {
            var params = {
                    instance: id,
                    module: module
                },
                preSets = {
                    cacheKey: getModuleByInstanceCacheKey(id, module)
                };
            return site.read('core_course_get_course_module_by_instance', params, preSets).then(function(response) {
                if (response.cm && (!response.warnings || !response.warnings.length)) {
                    return response.cm;
                }
                return $q.reject();
            });
        });
    };
        self.getModule = function(moduleId, courseId, sectionId) {
        if (!moduleId) {
            return $q.reject();
        }
        var promise;
        if (!courseId) {
            promise = self.getModuleBasicInfo(moduleId).then(function(module) {
                return module.course;
            });
        } else {
            promise = $q.when(courseId);
        }
        return promise.then(function(courseId) {
            $log.debug('Getting module ' + moduleId + ' in course ' + courseId);
            params = {
                courseid: courseId,
                options: [
                    {
                        name: 'cmid',
                        value: moduleId
                    }
                ]
            };
            preSets = {
                cacheKey: getModuleCacheKey(moduleId)
            };
            if (sectionId) {
                params.options.push({
                    name: 'sectionid',
                    value: sectionId
                });
            }
            return $mmSite.read('core_course_get_contents', params, preSets).catch(function() {
                params.options = [];
                preSets.cacheKey = getSectionsCacheKey(courseId);
                return $mmSite.read('core_course_get_contents', params, preSets);
            }).then(function(sections) {
                var section,
                    module;
                for (var i = 0; i < sections.length; i++) {
                    section = sections[i];
                    for (var j = 0; j < section.modules.length; j++) {
                        module = section.modules[j];
                        if (module.id == moduleId) {
                            module.course = courseId;
                            return addContentsIfNeeded(module);
                        }
                    }
                }
                return $q.reject();
            });
        });
    };
        function getModuleByInstanceCacheKey(id, module) {
        return 'mmCourse:moduleByInstance:' + module + ':' + id;
    }
        function getModuleCacheKey(moduleid) {
        return 'mmCourse:module:' + moduleid;
    }
        self.getModuleIconSrc = function(moduleName) {
        if (mods.indexOf(moduleName) < 0) {
            moduleName = "external-tool";
        }
        return "modules/moodle/img/mod/" + moduleName + ".svg";
    };
        self.getModuleSectionId = function(moduleId, courseId, siteId) {
        if (!moduleId) {
            return $q.reject();
        }
        return self.getModuleBasicInfo(moduleId, siteId).then(function(module) {
            return module.section;
        }).catch(function() {
            if (!courseId) {
                return $q.reject();
            }
            return self.getSections(courseId, {}, siteId).then(function(sections) {
                for (var i = 0, seclen = sections.length; i < seclen; i++) {
                    var section = sections[i];
                    for (var j = 0, modlen = section.modules.length; j < modlen; j++) {
                        if (section.modules[j].id == moduleId) {
                            return section.id;
                        }
                    }
                }
                return $q.reject();
            });
        });
    };
        self.getSection = function(courseid, sectionid) {
        var deferred = $q.defer();
        if (sectionid < 0) {
            deferred.reject('Invalid section ID');
            return deferred.promise;
        }
        self.getSections(courseid).then(function(sections) {
            for (var i = 0; i < sections.length; i++) {
                if (sections[i].id == sectionid) {
                    deferred.resolve(sections[i]);
                    return;
                }
            }
            deferred.reject('Unkown section');
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };
        self.getSections = function(courseid, preSets, siteId) {
        preSets = preSets || {};
        siteId = siteId || $mmSite.getId();
        preSets.cacheKey = getSectionsCacheKey(courseid);
        return $mmSitesManager.getSite(siteId).then(function(site) {
            return site.read('core_course_get_contents', {
                courseid: courseid,
                options: []
            }, preSets).then(function(sections) {
                angular.forEach(sections, function(section) {
                    angular.forEach(section.modules, function(module) {
                        addContentsIfNeeded(module);
                    });
                });
                return sections;
            });
        });
    };
        function getSectionsCacheKey(courseid) {
        return 'mmCourse:sections:' + courseid;
    }
        self.invalidateModule = function(moduleid) {
        return $mmSite.invalidateWsCacheForKey(getModuleCacheKey(moduleid));
    };
        self.invalidateModuleByInstance = function(id, module) {
        return $mmSite.invalidateWsCacheForKey(getModuleByInstanceCacheKey(id, module));
    };
        self.invalidateSections = function(courseid, userid) {
        userid = userid || $mmSite.getUserId();
        var p1 = $mmSite.invalidateWsCacheForKey(getSectionsCacheKey(courseid)),
            p2 = $mmSite.invalidateWsCacheForKey(getActivitiesCompletionCacheKey(courseid, userid));
        return $q.all([p1, p2]);
    };
        self.translateModuleName = function(moduleName) {
        if (mods.indexOf(moduleName) < 0) {
            moduleName = "external-tool";
        }
        var langkey = 'mm.core.mod_'+moduleName;
        return $translate(langkey).then(function(translated) {
            return translated !== langkey ? translated : moduleName;
        });
    };
    return self;
}]);

angular.module('mm.core.course')
.factory('$mmCourseCoursesNavHandler', function() {
    return {
                isEnabled: function() {
            return true;
        },
                isEnabledForCourse: function() {
            return true;
        },
                getController: function(courseId) {
            return function($scope, $state) {
                $scope.icon = 'ion-briefcase';
                $scope.title = 'mm.course.contents';
                $scope.action = function(e, course) {
                    $state.go('site.mm_course', {courseid: course.id});
                    e.preventDefault();
                    e.stopPropagation();
                };
            };
        }
    };
});

angular.module('mm.core.course')
.provider('$mmCourseDelegate', function() {
    var contentHandlers = {},
        self = {};
        self.registerContentHandler = function(addon, handles, handler) {
        if (typeof contentHandlers[handles] !== 'undefined') {
            console.log("$mmCourseDelegateProvider: Addon '" + contentHandlers[handles].addon + "' already registered as handler for '" + handles + "'");
            return false;
        }
        console.log("$mmCourseDelegateProvider: Registered addon '" + addon + "' as course content handler.");
        contentHandlers[handles] = {
            addon: addon,
            handler: handler,
            instance: undefined
        };
        return true;
    };
    self.$get = ["$q", "$log", "$mmSite", "$mmUtil", "$mmCourseContentHandler", function($q, $log, $mmSite, $mmUtil, $mmCourseContentHandler) {
        var enabledHandlers = {},
            self = {};
        $log = $log.getInstance('$mmCourseDelegate');
                self.getContentHandlerControllerFor = function(handles, module, courseid, sectionid) {
            if (typeof enabledHandlers[handles] !== 'undefined') {
                return enabledHandlers[handles].getController(module, courseid, sectionid);
            }
            return $mmCourseContentHandler.getController(module, courseid, sectionid);
        };
                self.updateContentHandler = function(handles, handlerInfo) {
            var promise;
            if (typeof handlerInfo.instance === 'undefined') {
                handlerInfo.instance = $mmUtil.resolveObject(handlerInfo.handler, true);
            }
            if (!$mmSite.isLoggedIn()) {
                promise = $q.reject();
            } else {
                promise = $q.when(handlerInfo.instance.isEnabled());
            }
            return promise.then(function(enabled) {
                if (enabled) {
                    enabledHandlers[handles] = handlerInfo.instance;
                } else {
                    return $q.reject();
                }
            }).catch(function() {
                delete enabledHandlers[handles];
            });
        };
                self.updateContentHandlers = function() {
            var promises = [],
                enabledHandlers = {};
            $log.debug('Updating content handlers for current site.');
            angular.forEach(contentHandlers, function(handlerInfo, handles) {
                promises.push(self.updateContentHandler(handles, handlerInfo));
            });
            return $q.all(promises).then(function() {
                return true;
            }, function() {
                return true;
            });
        };
        return self;
    }];
    return self;
});

angular.module('mm.core.course')
.factory('$mmCourseHelper', ["$q", "$mmCoursePrefetchDelegate", "$mmFilepool", "$mmUtil", "$mmCourse", "$mmSite", "$state", "mmCoreNotDownloaded", "mmCoreOutdated", "mmCoreDownloading", "mmCoreCourseAllSectionsId", function($q, $mmCoursePrefetchDelegate, $mmFilepool, $mmUtil, $mmCourse, $mmSite, $state,
            mmCoreNotDownloaded, mmCoreOutdated, mmCoreDownloading, mmCoreCourseAllSectionsId) {
    var self = {};
        self.calculateSectionStatus = function(section, courseid, restoreDownloads, refresh, dwnpromises) {
        if (section.id !== mmCoreCourseAllSectionsId) {
            return $mmCoursePrefetchDelegate.getModulesStatus(section.id, section.modules, courseid, refresh, restoreDownloads)
                    .then(function(result) {
                var downloadid = self.getSectionDownloadId(section);
                if ($mmCoursePrefetchDelegate.isBeingDownloaded(downloadid)) {
                    result.status = mmCoreDownloading;
                }
                section.showDownload = result.status === mmCoreNotDownloaded;
                section.showRefresh = result.status === mmCoreOutdated;
                if (result.status !== mmCoreDownloading) {
                    section.isDownloading = false;
                    section.total = 0;
                } else if (!restoreDownloads) {
                    section.count = 0;
                    section.total = result[mmCoreOutdated].length + result[mmCoreNotDownloaded].length +
                                    result[mmCoreDownloading].length;
                    section.isDownloading = true;
                } else {
                    var promise = self.startOrRestorePrefetch(section, result, courseid).then(function() {
                        return self.calculateSectionStatus(section, courseid);
                    });
                    if (dwnpromises) {
                        dwnpromises.push(promise);
                    }
                }
                return result;
            });
        }
        return $q.reject();
    };
        self.calculateSectionsStatus = function(sections, courseid, restoreDownloads, refresh) {
        var allsectionssection,
            allsectionsstatus,
            downloadpromises = [],
            statuspromises = [];
        angular.forEach(sections, function(section) {
            if (section.id === mmCoreCourseAllSectionsId) {
                allsectionssection = section;
            } else {
                statuspromises.push(self.calculateSectionStatus(section, courseid, restoreDownloads, refresh, downloadpromises)
                        .then(function(result) {
                    allsectionsstatus = $mmFilepool.determinePackagesStatus(allsectionsstatus, result.status);
                }));
            }
        });
        return $q.all(statuspromises).then(function() {
            if (allsectionssection) {
                allsectionssection.showDownload = allsectionsstatus === mmCoreNotDownloaded;
                allsectionssection.showRefresh = allsectionsstatus === mmCoreOutdated;
                allsectionssection.isDownloading = allsectionsstatus === mmCoreDownloading;
            }
            return downloadpromises;
        });
    };
        self.confirmDownloadSize = function(courseid, section, sections) {
        var sizePromise;
        if (section.id != mmCoreCourseAllSectionsId) {
            sizePromise = $mmCoursePrefetchDelegate.getDownloadSize(section.modules, courseid);
        } else {
            var promises = [],
                size = 0;
            angular.forEach(sections, function(s) {
                if (s.id != mmCoreCourseAllSectionsId) {
                    promises.push($mmCoursePrefetchDelegate.getDownloadSize(s.modules, courseid).then(function(sectionsize) {
                        size = size + sectionsize;
                    }));
                }
            });
            sizePromise = $q.all(promises).then(function() {
                return size;
            });
        }
        return sizePromise.then(function(size) {
            return $mmUtil.confirmDownloadSize(size);
        });
    };
        self.getModuleCourseIdByInstance = function(id, module, siteId) {
        return $mmCourse.getModuleBasicInfoByInstance(id, module, siteId).then(function(cm) {
            return cm.course;
        }).catch(function(error) {
            if (error) {
                $mmUtil.showErrorModal(error);
            } else {
                $mmUtil.showErrorModal('mm.course.errorgetmodule', true);
            }
            return $q.reject();
        });
    };
        self.getSectionDownloadId = function(section) {
        return 'Section-'+section.id;
    };
        self.navigateToModule = function(moduleId, siteId, courseId, sectionId) {
        siteId = siteId || $mmSite.getId();
        var modal = $mmUtil.showModalLoading(),
            promise;
        return $mmCourse.canGetModuleWithoutCourseId(siteId).then(function(enabled) {
            if (courseId && sectionId) {
                promise = $q.when();
            } else if (!courseId && !enabled) {
                promise = $q.reject();
            } else if (!courseId) {
                promise = $mmCourse.getModuleBasicInfo(moduleId, siteId).then(function(module) {
                    courseId = module.course;
                    sectionId = module.section;
                });
            } else {
                promise = $mmCourse.getModuleSectionId(moduleId, courseId, siteId).then(function(id) {
                    sectionId = id;
                });
            }
            return promise.then(function() {
                return $state.go('redirect', {
                    siteid: siteId,
                    state: 'site.mm_course',
                    params: {
                        courseid: courseId,
                        moduleid: moduleId,
                        sid: sectionId
                    }
                });
            });
        }).catch(function(error) {
            if (error) {
                $mmUtil.showErrorModal(error);
            } else {
                $mmUtil.showErrorModal('mm.course.errorgetmodule', true);
            }
            return $q.reject();
        }).finally(function() {
            modal.dismiss();
        });
    };
        self.prefetch = function(section, courseid, sections) {
        if (section.id != mmCoreCourseAllSectionsId) {
            return self.prefetchSection(section, courseid, true, sections);
        } else {
            var promises = [];
            section.isDownloading = true;
            angular.forEach(sections, function(s) {
                if (s.id != mmCoreCourseAllSectionsId) {
                    promises.push(self.prefetchSection(s, courseid, false, sections).then(function() {
                        return self.calculateSectionStatus(s, courseid);
                    }));
                }
            });
            return $mmUtil.allPromises(promises);
        }
    };
        self.prefetchModule = function(service, module, size, refresh) {
        return $mmUtil.confirmDownloadSize(size).then(function() {
            var promise = refresh ? service.invalidateContent(module.id) : $q.when();
            return promise.catch(function() {
            }).then(function() {
                return service.prefetchContent(module).catch(function() {
                    if (!$scope.$$destroyed) {
                        $mmUtil.showErrorModal('mm.core.errordownloading', true);
                    }
                });
            });
        });
    };
        self.prefetchSection = function(section, courseid, singleDownload, sections) {
        if (section.id == mmCoreCourseAllSectionsId) {
            return $q.when();
        }
        section.isDownloading = true;
        return $mmCoursePrefetchDelegate.getModulesStatus(section.id, section.modules, courseid).then(function(result) {
            if (result.status === mmCoreNotDownloaded || result.status === mmCoreOutdated || result.status === mmCoreDownloading) {
                var promise = self.startOrRestorePrefetch(section, result, courseid);
                if (singleDownload) {
                    self.calculateSectionsStatus(sections, courseid, false);
                }
                return promise;
            }
        }, function() {
            section.isDownloading = false;
            return $q.reject();
        });
    };
        self.startOrRestorePrefetch = function(section, status, courseid) {
        if (section.id == mmCoreCourseAllSectionsId) {
            return $q.when();
        }
        var modules = status[mmCoreOutdated].concat(status[mmCoreNotDownloaded]).concat(status[mmCoreDownloading]),
            downloadid = self.getSectionDownloadId(section),
            moduleids;
        moduleids = modules.map(function(m) {
            return m.id;
        });
        section.count = 0;
        section.total = modules.length;
        section.isDownloading = true;
        return $mmCoursePrefetchDelegate.prefetchAll(downloadid, modules, courseid).then(function() {}, function() {
            return $q.reject();
        }, function(id) {
            var index = moduleids.indexOf(id);
            if (index > -1) {
                moduleids.splice(index, 1);
                section.count++;
            }
        });
    };
    return self;
}]);

angular.module('mm.core')
.provider('$mmCoursePrefetchDelegate', function() {
    var prefetchHandlers = {},
        self = {};
        self.registerPrefetchHandler = function(addon, handles, handler) {
        if (typeof prefetchHandlers[handles] !== 'undefined') {
            console.log("$mmCoursePrefetchDelegateProvider: Addon '" + prefetchHandlers[handles].addon +
                            "' already registered as handler for '" + handles + "'");
            return false;
        }
        console.log("$mmCoursePrefetchDelegateProvider: Registered addon '" + addon + "' as prefetch handler.");
        prefetchHandlers[handles] = {
            addon: addon,
            handler: handler,
            instance: undefined
        };
        return true;
    };
    self.$get = ["$q", "$log", "$mmSite", "$mmUtil", "$mmFilepool", "$mmEvents", "mmCoreDownloaded", "mmCoreDownloading", "mmCoreNotDownloaded", "mmCoreOutdated", "mmCoreNotDownloadable", "mmCoreEventSectionStatusChanged", function($q, $log, $mmSite, $mmUtil, $mmFilepool, $mmEvents, mmCoreDownloaded, mmCoreDownloading,
                mmCoreNotDownloaded, mmCoreOutdated, mmCoreNotDownloadable, mmCoreEventSectionStatusChanged) {
        var enabledHandlers = {},
            self = {},
            deferreds = {},
            statusCache = {};
        $log = $log.getInstance('$mmCoursePrefetchDelegate');
                self.clearStatusCache = function() {
            statusCache = {};
        };
                self.determineModuleStatus = function(module, status, restoreDownloads) {
            var handler = enabledHandlers[module.modname];
            if (handler) {
                if (status == mmCoreDownloading && restoreDownloads) {
                    if (!$mmFilepool.getPackageDownloadPromise($mmSite.getId(), handler.component, module.id)) {
                        handler.prefetch(module);
                    }
                } else if (handler.determineStatus) {
                    return handler.determineStatus(status);
                }
            }
            return status;
        };
                self.getDownloadSize = function(modules, courseid) {
            var size = 0,
                promises = [];
            angular.forEach(modules, function(module) {
                module.contents = module.contents || [];
                var handler = enabledHandlers[module.modname];
                if (handler) {
                    promises.push(self.getModuleStatus(module, courseid).then(function(modstatus) {
                        if (modstatus === mmCoreNotDownloaded || modstatus === mmCoreOutdated) {
                            return $q.when(handler.getDownloadSize(module, courseid)).then(function(modulesize) {
                                size = size + modulesize;
                            }).catch(function() {
                            });
                        }
                    }));
                }
            });
            return $q.all(promises).then(function() {
                return size;
            });
        };
                self.getModuleStatus = function(module, courseid, revision, timemodified) {
            var handler = enabledHandlers[module.modname],
                siteid = $mmSite.getId();
            module.contents = module.contents || [];
            if (handler) {
                var promise = handler.getFiles ? $q.when(handler.getFiles(module, courseid)) : $q.when(module.contents);
                return promise.then(function(files) {
                    if (files.length === 0) {
                        return $q.when(mmCoreDownloaded);
                    }
                    var promises = [];
                    if (typeof revision == 'undefined') {
                        if (handler.getRevision) {
                            promises.push($q.when(handler.getRevision(module, courseid)).then(function(rev) {
                                revision = rev;
                            }));
                        } else {
                            revision = $mmFilepool.getRevisionFromFileList(files);
                        }
                    }
                    if (typeof timemodified == 'undefined') {
                        if (handler.getTimemodified) {
                            promises.push($q.when(handler.getTimemodified(module, courseid)).then(function(timemod) {
                                timemodified = timemod;
                            }));
                        } else {
                            timemodified = $mmFilepool.getTimemodifiedFromFileList(files);
                        }
                    }
                    return $q.all(promises).then(function() {
                        return $mmFilepool.getPackageStatus(siteid, handler.component, module.id, revision, timemodified)
                                .then(function(status) {
                            return self.determineModuleStatus(module, status, true);
                        });
                    });
                });
            }
            return $q.reject();
        };
                self.getModulesStatus = function(sectionid, modules, courseid, refresh, restoreDownloads) {
            var promises = [],
                status = mmCoreNotDownloadable,
                result = {};
            result[mmCoreNotDownloaded] = [];
            result[mmCoreDownloaded] = [];
            result[mmCoreDownloading] = [];
            result[mmCoreOutdated] = [];
            result.total = 0;
            angular.forEach(modules, function(module) {
                var handler = enabledHandlers[module.modname],
                    promise;
                module.contents = module.contents || [];
                if (handler) {
                    var packageId = $mmFilepool.getPackageId(handler.component, module.id);
                    if (!refresh && statusCache[packageId] && statusCache[packageId].status) {
                        promise = $q.when(self.determineModuleStatus(module, statusCache[packageId].status, restoreDownloads));
                    } else {
                        promise = self.getModuleStatus(module, courseid);
                    }
                    promises.push(promise.then(function(modstatus) {
                        statusCache[packageId] = {
                            status: modstatus,
                            sectionid: sectionid
                        };
                        status = $mmFilepool.determinePackagesStatus(status, modstatus);
                        result[modstatus].push(module);
                        result.total++;
                    }));
                }
            });
            return $q.all(promises).then(function() {
                result.status = status;
                return result;
            });
        };
                self.getPrefetchHandlerFor = function(handles) {
            return enabledHandlers[handles];
        };
                self.isBeingDownloaded = function(id) {
            return deferreds[$mmSite.getId()] && deferreds[$mmSite.getId()][id];
        };
                self.prefetchAll = function(id, modules, courseid) {
            var siteid = $mmSite.getId();
            if (deferreds[siteid] && deferreds[siteid][id]) {
                return deferreds[siteid][id].promise;
            }
            var deferred = $q.defer(),
                promises = [];
            if (!deferreds[siteid]) {
                deferreds[siteid] = {};
            }
            deferreds[siteid][id] = deferred;
            angular.forEach(modules, function(module) {
                module.contents = module.contents || [];
                var handler = enabledHandlers[module.modname];
                if (handler) {
                    promises.push(handler.prefetch(module, courseid).then(function() {
                        deferred.notify(module.id);
                    }));
                }
            });
            $q.all(promises).then(function() {
                delete deferreds[siteid][id];
                deferred.resolve();
            }, function() {
                delete deferreds[siteid][id];
                deferred.reject();
            });
            return deferred.promise;
        };
                self.updatePrefetchHandler = function(handles, handlerInfo) {
            var promise;
            if (typeof handlerInfo.instance === 'undefined') {
                handlerInfo.instance = $mmUtil.resolveObject(handlerInfo.handler, true);
            }
            if (!$mmSite.isLoggedIn()) {
                promise = $q.reject();
            } else {
                promise = $q.when(handlerInfo.instance.isEnabled());
            }
            return promise.then(function(enabled) {
                if (enabled) {
                    enabledHandlers[handles] = handlerInfo.instance;
                } else {
                    return $q.reject();
                }
            }).catch(function() {
                delete enabledHandlers[handles];
            });
        };
                self.updatePrefetchHandlers = function() {
            var promises = [];
            $log.debug('Updating prefetch handlers for current site.');
            angular.forEach(prefetchHandlers, function(handlerInfo, handles) {
                promises.push(self.updatePrefetchHandler(handles, handlerInfo));
            });
            return $q.all(promises).then(function() {
                return true;
            }, function() {
                return true;
            });
        };
                self.updateStatusCache = function(component, componentId, status) {
            var notify = false,
                packageid = $mmFilepool.getPackageId(component, componentId);
            if (statusCache[packageid]) {
                notify = statusCache[packageid].status !== status;
            } else {
                statusCache[packageid] = {};
            }
            statusCache[packageid].status = status;
            if (notify) {
                $mmEvents.trigger(mmCoreEventSectionStatusChanged, {
                    sectionid: statusCache[packageid].sectionid,
                    siteid: $mmSite.getId()
                });
            }
        };
        return self;
    }];
    return self;
})
.run(["$mmEvents", "mmCoreEventLogin", "mmCoreEventSiteUpdated", "mmCoreEventLogout", "$mmCoursePrefetchDelegate", "$mmSite", "mmCoreEventPackageStatusChanged", function($mmEvents, mmCoreEventLogin, mmCoreEventSiteUpdated, mmCoreEventLogout, $mmCoursePrefetchDelegate, $mmSite,
            mmCoreEventPackageStatusChanged) {
    $mmEvents.on(mmCoreEventLogin, $mmCoursePrefetchDelegate.updatePrefetchHandlers);
    $mmEvents.on(mmCoreEventSiteUpdated, $mmCoursePrefetchDelegate.updatePrefetchHandlers);
    $mmEvents.on(mmCoreEventLogout, $mmCoursePrefetchDelegate.clearStatusCache);
    $mmEvents.on(mmCoreEventPackageStatusChanged, function(data) {
        if (data.siteid === $mmSite.getId()) {
            $mmCoursePrefetchDelegate.updateStatusCache(data.component, data.componentId, data.status);
        }
    });
}]);

angular.module('mm.core.courses')
.factory('$mmCourses', ["$q", "$mmSite", "$log", "$mmSitesManager", "mmCoursesSearchPerPage", "mmCoursesEnrolInvalidKey", function($q, $mmSite, $log, $mmSitesManager, mmCoursesSearchPerPage, mmCoursesEnrolInvalidKey) {
    $log = $log.getInstance('$mmCourses');
    var self = {},
        currentCourses = {};
        self.clearCurrentCourses = function() {
        currentCourses = {};
    };
        self.getCourse = function(id, siteid) {
        return self.getCourses([id], siteid).then(function(courses) {
            if (courses && courses.length > 0) {
                return courses[0];
            }
            return $q.reject();
        });
    };
        self.getCourseEnrolmentMethods = function(id) {
        var params = {
                courseid: id
            },
            preSets = {
                cacheKey: getCourseEnrolmentMethodsCacheKey(id)
            };
        return $mmSite.read('core_enrol_get_course_enrolment_methods', params, preSets);
    };
        function getCourseEnrolmentMethodsCacheKey(id) {
        return 'mmCourses:enrolmentmethods:' + id;
    }
        self.getCourseGuestEnrolmentInfo = function(instanceId) {
        var params = {
                instanceid: instanceId
            },
            preSets = {
                cacheKey: getCourseGuestEnrolmentInfoCacheKey(instanceId)
            };
        return $mmSite.read('enrol_guest_get_instance_info', params, preSets).then(function(response) {
            return response.instanceinfo;
        });
    };
        function getCourseGuestEnrolmentInfoCacheKey(instanceId) {
        return 'mmCourses:guestinfo:' + instanceId;
    }
        self.getCourses = function(ids, siteid) {
        siteid = siteid || $mmSite.getId();
        if (!angular.isArray(ids)) {
            return $q.reject();
        } else if (ids.length === 0) {
            return $q.when([]);
        }
        return $mmSitesManager.getSite(siteid).then(function(site) {
            var data = {
                    options: {
                        ids: ids
                    }
                },
                preSets = {
                    cacheKey: getCoursesCacheKey(ids)
                };
            return site.read('core_course_get_courses', data, preSets).then(function(courses) {
                if (typeof courses != 'object' && !angular.isArray(courses)) {
                    return $q.reject();
                }
                return courses;
            });
        });
    };
        function getCoursesCacheKey(ids) {
        return 'mmCourses:course:' + JSON.stringify(ids);
    }
        self.getStoredCourse = function(id) {
        $log.warn('The function \'getStoredCourse\' is deprecated. Please use \'getUserCourse\' instead');
        return currentCourses[id];
    };
        self.getUserCourse = function(id, preferCache, siteid) {
        siteid = siteid || $mmSite.getId();
        if (!id) {
            return $q.reject();
        }
        if (typeof preferCache == 'undefined') {
            preferCache = false;
        }
        return self.getUserCourses(preferCache, siteid).then(function(courses) {
            var course;
            angular.forEach(courses, function(c) {
                if (c.id == id) {
                    course = c;
                }
            });
            return course ? course : $q.reject();
        });
    };
        self.getUserCourses = function(preferCache, siteid) {
        siteid = siteid || $mmSite.getId();
        if (typeof preferCache == 'undefined') {
            preferCache = false;
        }
        return $mmSitesManager.getSite(siteid).then(function(site) {
            var userid = site.getUserId(),
                presets = {
                    cacheKey: getUserCoursesCacheKey(),
                    omitExpires: preferCache
                },
                data = {userid: userid};
            if (typeof userid === 'undefined') {
                return $q.reject();
            }
            return site.read('core_enrol_get_users_courses', data, presets).then(function(courses) {
                if (siteid === $mmSite.getId()) {
                    storeCoursesInMemory(courses);
                }
                return courses;
            });
        });
    };
        function getUserCoursesCacheKey() {
        return 'mmCourses:usercourses';
    }
        self.invalidateCourse = function(id, siteid) {
        return self.invalidateCourses([id], siteid);
    };
        self.invalidateCourseEnrolmentMethods = function(id) {
        return $mmSite.invalidateWsCacheForKey(getCourseEnrolmentMethodsCacheKey(id));
    };
        self.invalidateCourseGuestEnrolmentInfo = function(instanceId) {
        return $mmSite.invalidateWsCacheForKey(getCourseGuestEnrolmentInfoCacheKey(instanceId));
    };
        self.invalidateCourses = function(ids, siteid) {
        siteid = siteid || $mmSite.getId();
        return $mmSitesManager.getSite(siteid).then(function(site) {
            return site.invalidateWsCacheForKey(getCoursesCacheKey(ids));
        });
    };
        self.invalidateUserCourses = function(siteid) {
        siteid = siteid || $mmSite.getId();
        return $mmSitesManager.getSite(siteid).then(function(site) {
            return site.invalidateWsCacheForKey(getUserCoursesCacheKey());
        });
    };
        self.isGuestWSAvailable = function() {
        return $mmSite.wsAvailable('enrol_guest_get_instance_info');
    };
        self.isSearchCoursesAvailable = function() {
        return $mmSite.wsAvailable('core_course_search_courses');
    };
        self.isSelfEnrolmentEnabled = function() {
        return $mmSite.wsAvailable('enrol_self_enrol_user');
    };
        self.search = function(text, page, perpage) {
        page = page || 0;
        perpage = perpage || mmCoursesSearchPerPage;
        var params = {
                criterianame: 'search',
                criteriavalue: text,
                page: page,
                perpage: perpage
            }, preSets = {
                getFromCache: false
            };
        return $mmSite.read('core_course_search_courses', params, preSets).then(function(response) {
            if (typeof response == 'object') {
                return {total: response.total, courses: response.courses};
            }
            return $q.reject();
        });
    };
        self.selfEnrol = function(courseid, password, instanceId) {
        if (typeof password == 'undefined') {
            password = '';
        }
        var params = {
            courseid: courseid,
            password: password
        };
        if (instanceId) {
            params.instanceid = instanceId;
        }
        return $mmSite.write('enrol_self_enrol_user', params).then(function(response) {
            if (response) {
                if (response.status) {
                    return true;
                } else if (response.warnings && response.warnings.length) {
                    var message;
                    angular.forEach(response.warnings, function(warning) {
                        if (warning.warningcode == '2' || warning.warningcode == '4') {
                            message = warning.message;
                        }
                    });
                    if (message) {
                        return $q.reject({code: mmCoursesEnrolInvalidKey, message: message});
                    }
                }
            }
            return $q.reject();
        });
    };
        function storeCoursesInMemory(courses) {
        angular.forEach(courses, function(course) {
            currentCourses[course.id] = angular.copy(course);
        });
    }
    return self;
}]);

angular.module('mm.core.courses')
.provider('$mmCoursesDelegate', function() {
    var navHandlers = {},
        self = {};
        self.registerNavHandler = function(addon, handler, priority) {
        if (typeof navHandlers[addon] !== 'undefined') {
            console.log("$mmCoursesDelegateProvider: Addon '" + navHandlers[addon].addon + "' already registered as navigation handler");
            return false;
        }
        console.log("$mmCoursesDelegateProvider: Registered addon '" + addon + "' as navibation handler.");
        navHandlers[addon] = {
            addon: addon,
            handler: handler,
            instance: undefined,
            priority: priority
        };
        return true;
    };
    self.$get = ["$mmUtil", "$q", "$log", "$mmSite", "mmCoursesAccessMethods", function($mmUtil, $q, $log, $mmSite, mmCoursesAccessMethods) {
        var enabledNavHandlers = {},
            coursesHandlers = {},
            self = {},
            loaded = {};
        $log = $log.getInstance('$mmCoursesDelegate');
                self.areNavHandlersLoadedFor = function(courseId) {
            return loaded[courseId];
        };
                self.clearCoursesHandlers = function() {
            coursesHandlers = {};
            loaded = {};
        };
                function getNavHandlersForAccess(courseId, refresh, accessData) {
            if (refresh || !coursesHandlers[courseId] || coursesHandlers[courseId].access.type != accessData.type) {
                coursesHandlers[courseId] = {
                    access: accessData,
                    handlers: []
                };
                self.updateNavHandlersForCourse(courseId, accessData);
            }
            return coursesHandlers[courseId].handlers;
        }
                self.getNavHandlersFor = function(courseId, refresh) {
            var accessData = {
                type: mmCoursesAccessMethods.default
            };
            return getNavHandlersForAccess(courseId, refresh, accessData);
        };
                self.getNavHandlersForGuest = function(courseId, refresh) {
            var accessData = {
                type: mmCoursesAccessMethods.guest
            };
            return getNavHandlersForAccess(courseId, refresh, accessData);
        };
                self.updateNavHandler = function(addon, handlerInfo) {
            var promise;
            if (typeof handlerInfo.instance === 'undefined') {
                handlerInfo.instance = $mmUtil.resolveObject(handlerInfo.handler, true);
            }
            if (!$mmSite.isLoggedIn()) {
                promise = $q.reject();
            } else {
                promise = $q.when(handlerInfo.instance.isEnabled());
            }
            return promise.then(function(enabled) {
                if (enabled) {
                    enabledNavHandlers[addon] = {
                        instance: handlerInfo.instance,
                        priority: handlerInfo.priority
                    };
                } else {
                    return $q.reject();
                }
            }).catch(function() {
                delete enabledNavHandlers[addon];
            });
        };
                self.updateNavHandlers = function() {
            var promises = [];
            $log.debug('Updating navigation handlers for current site.');
            angular.forEach(navHandlers, function(handlerInfo, addon) {
                promises.push(self.updateNavHandler(addon, handlerInfo));
            });
            return $q.all(promises).then(function() {
                return true;
            }, function() {
                return true;
            }).finally(function() {
                angular.forEach(coursesHandlers, function(handler, courseId) {
                    self.updateNavHandlersForCourse(parseInt(courseId), handler.access);
                });
            });
        };
                self.updateNavHandlersForCourse = function(courseId, accessData) {
            var promises = [],
                enabledForCourse = [];
            angular.forEach(enabledNavHandlers, function(handler) {
                var promise = $q.when(handler.instance.isEnabledForCourse(courseId, accessData)).then(function(enabled) {
                    if (enabled) {
                        enabledForCourse.push(handler);
                    } else {
                        return $q.reject();
                    }
                }).catch(function() {
                });
                promises.push(promise);
            });
            return $q.all(promises).then(function() {
                return true;
            }).catch(function() {
                return true;
            }).finally(function() {
                $mmUtil.emptyArray(coursesHandlers[courseId].handlers);
                angular.forEach(enabledForCourse, function(handler) {
                    coursesHandlers[courseId].handlers.push({
                        controller: handler.instance.getController(courseId),
                        priority: handler.priority
                    });
                });
                loaded[courseId] = true;
            });
        };
        return self;
    }];
    return self;
});

angular.module('mm.core.courses')
.factory('$mmCoursesHandlers', ["$mmSite", "$state", "$mmCourses", "$q", "$mmUtil", "$translate", "$timeout", "mmCoursesEnrolInvalidKey", function($mmSite, $state, $mmCourses, $q, $mmUtil, $translate, $timeout,
            mmCoursesEnrolInvalidKey) {
    var self = {};
        self.linksHandler = function() {
        var self = {};
                function actionEnrol(courseId, url) {
            var modal = $mmUtil.showModalLoading();
            $mmCourses.getUserCourse(courseId).catch(function() {
                return canSelfEnrol(courseId).then(function() {
                    modal.dismiss();
                    return selfEnrol(courseId).catch(function() {
                        if (typeof error == 'string') {
                            $mmUtil.showErrorModal(error);
                        }
                        return $q.reject();
                    });
                }, function(error) {
                    modal.dismiss();
                    if (typeof error != 'string') {
                        error = $translate.instant('mm.courses.notenroled');
                    }
                    var body = $translate('mm.core.twoparagraphs',
                                    {p1: error, p2: $translate.instant('mm.core.confirmopeninbrowser')});
                    $mmUtil.showConfirm(body).then(function() {
                        $mmUtil.openInBrowser(url);
                    });
                    return $q.reject();
                });
            }).then(function() {
                modal.dismiss();
                $state.go('redirect', {
                    siteid: $mmSite.getId(),
                    state: 'site.mm_course',
                    params: {courseid: courseId}
                });
            });
        }
                function canSelfEnrol(courseId) {
            if (!$mmCourses.isSelfEnrolmentEnabled()) {
                return $q.reject();
            }
            return $mmCourses.getCourseEnrolmentMethods(courseId).then(function(methods) {
                var isSelfEnrolEnabled = false,
                    instances = 0;
                angular.forEach(methods, function(method) {
                    if (method.type == 'self' && method.status) {
                        isSelfEnrolEnabled = true;
                        instances++;
                    }
                });
                if (!isSelfEnrolEnabled || instances != 1) {
                    return $q.reject();
                }
            });
        }
                function selfEnrol(courseId, password) {
            var modal = $mmUtil.showModalLoading();
            return $mmCourses.selfEnrol(courseId, password).then(function() {
                return $mmCourses.invalidateUserCourses().catch(function() {
                }).then(function() {
                    return $timeout(function() {}, 4000).finally(function() {
                        modal.dismiss();
                    });
                });
            }).catch(function(error) {
                modal.dismiss();
                if (error && error.code === mmCoursesEnrolInvalidKey) {
                    var title = $translate.instant('mm.courses.selfenrolment'),
                        body = ' ',
                        placeholder = $translate.instant('mm.courses.password');
                    if (typeof password != 'undefined') {
                        $mmUtil.showErrorModal(error.message);
                    }
                    return $mmUtil.showPrompt(body, title, placeholder).then(function(password) {
                        return selfEnrol(courseId, password);
                    });
                } else {
                    return $q.reject(error);
                }
            });
        }
                self.getActions = function(siteIds, url) {
            if (typeof self.handles(url) != 'undefined') {
                var params = $mmUtil.extractUrlParams(url);
                if (typeof params.id != 'undefined') {
                    return [{
                        message: 'mm.core.view',
                        icon: 'ion-eye',
                        sites: siteIds,
                        action: function(siteId) {
                            siteId = siteId || $mmSite.getId();
                            if (siteId == $mmSite.getId()) {
                                actionEnrol(parseInt(params.id, 10), url);
                            } else {
                                $state.go('redirect', {
                                    siteid: siteId,
                                    state: 'site.mm_course',
                                    params: {courseid: parseInt(params.id, 10)}
                                });
                            }
                        }
                    }];
                }
            }
            return [];
        };
                self.handles = function(url) {
            var patterns = ['/enrol/index.php', '/course/enrol.php', '/course/view.php'];
            for (var i = 0; i < patterns.length; i++) {
                var position = url.indexOf(patterns[i]);
                if (position > -1) {
                    return url.substr(0, position);
                }
            }
        };
        return self;
    };
    return self;
}]);

angular.module('mm.core.login')
.constant('mmLoginSSOCode', 2)
.constant('mmLoginLaunchSiteURL', 'mmLoginLaunchSiteURL')
.constant('mmLoginLaunchPassport', 'mmLoginLaunchPassport')
.factory('$mmLoginHelper', ["$q", "$log", "$mmConfig", "mmLoginSSOCode", "mmLoginLaunchSiteURL", "mmLoginLaunchPassport", "md5", "$mmSite", "$mmSitesManager", "$mmLang", "$mmUtil", "$state", "$mmAddonManager", "mmCoreConfigConstants", function($q, $log, $mmConfig, mmLoginSSOCode, mmLoginLaunchSiteURL, mmLoginLaunchPassport,
            md5, $mmSite, $mmSitesManager, $mmLang, $mmUtil, $state, $mmAddonManager, mmCoreConfigConstants) {
    $log = $log.getInstance('$mmLoginHelper');
    var self = {},
        isSSOLoginOngoing = false;
        self.goToAddSite = function() {
            return $state.go('app.home');
 };
        self.goToSiteInitialPage = function() {
        if ($mmSite.getInfo() && $mmSite.getInfo().userhomepage === 0) {
            var $mmaFrontpage = $mmAddonManager.get('$mmaFrontpage');
            if ($mmaFrontpage) {
                return $state.go('site.mm_course-section');
            }
        }
        return $state.go('site.mm_courses');
    };
        self.isFixedUrlSet = function() {
        return typeof mmCoreConfigConstants.siteurl != 'undefined';
    };
        self.isSSOLoginNeeded = function(code) {
        return code == mmLoginSSOCode;
    };
        self.isSSOLoginOngoing = function() {
        return isSSOLoginOngoing;
    };
        self.openBrowserForSSOLogin = function(siteurl) {
        var passport = Math.random() * 1000;
        var loginurl = siteurl + "/local/mobile/launch.php?service=" + mmCoreConfigConstants.wsextservice;
        loginurl += "&passport=" + passport;
        $mmConfig.set(mmLoginLaunchSiteURL, siteurl);
        $mmConfig.set(mmLoginLaunchPassport, passport);
        $mmUtil.openInBrowser(loginurl);
        if (navigator.app) {
            navigator.app.exitApp();
        }
    };
        self.setSSOLoginOngoing = function(value) {
        isSSOLoginOngoing = value;
    };
        self.validateBrowserSSOLogin = function(url) {
        var params = url.split(":::");
        return $mmConfig.get(mmLoginLaunchSiteURL).then(function(launchSiteURL) {
            return $mmConfig.get(mmLoginLaunchPassport).then(function(passport) {
                $mmConfig.delete(mmLoginLaunchSiteURL);
                $mmConfig.delete(mmLoginLaunchPassport);
                var signature = md5.createHash(launchSiteURL + passport);
                if (signature != params[0]) {
                    if (launchSiteURL.indexOf("https://") != -1) {
                        launchSiteURL = launchSiteURL.replace("https://", "http://");
                    } else {
                        launchSiteURL = launchSiteURL.replace("http://", "https://");
                    }
                    signature = md5.createHash(launchSiteURL + passport);
                }
                if (signature == params[0]) {
                    $log.debug('Signature validated');
                    return { siteurl: launchSiteURL, token: params[1] };
                } else {
                    $log.debug('Inalid signature in the URL request yours: ' + params[0] + ' mine: '
                                    + signature + ' for passport ' + passport);
                    return $mmLang.translateAndReject('mm.core.unexpectederror');
                }
            });
        });
    };
        self.handleSSOLoginAuthentication = function(siteurl, token) {
        if ($mmSite.isLoggedIn()) {
            var deferred = $q.defer();
            var info = $mmSite.getInfo();
            if (typeof(info) !== 'undefined' && typeof(info.username) !== 'undefined') {
                $mmSitesManager.updateSiteToken(info.siteurl, info.username, token).then(function() {
                    $mmSitesManager.updateSiteInfoByUrl(info.siteurl, info.username).finally(deferred.resolve);
                }, function() {
                    $mmLang.translateAndRejectDeferred(deferred, 'mm.login.errorupdatesite');
                });
            } else {
                $mmLang.translateAndRejectDeferred(deferred, 'mm.login.errorupdatesite');
            }
            return deferred.promise;
        } else {
            return $mmSitesManager.newSite(siteurl, token);
        }
    };
    return self;
}]);

angular.module('mm.core.sidemenu')
.controller('mmSideMenuCtrl', ["$scope", "$state", "$mmSideMenuDelegate", "$mmSitesManager", "$mmSite", "$mmEvents", "$timeout", "mmCoreEventLanguageChanged", "mmCoreEventSiteUpdated", function($scope, $state, $mmSideMenuDelegate, $mmSitesManager, $mmSite, $mmEvents,
            $timeout, mmCoreEventLanguageChanged, mmCoreEventSiteUpdated) {
    $scope.handlers = $mmSideMenuDelegate.getNavHandlers();
    $scope.areNavHandlersLoaded = $mmSideMenuDelegate.areNavHandlersLoaded;
    $scope.siteinfo = $mmSite.getInfo();
    $scope.logout = function() {
        $mmSitesManager.logout().finally(function() {
            $state.go('mm_login.sites');
        });
    };
    $mmSite.getDocsUrl().then(function(docsurl) {
        $scope.docsurl = docsurl;
    });
    function updateSiteInfo() {
        $scope.siteinfo = undefined;
        $timeout(function() {
            $scope.siteinfo = $mmSite.getInfo();
            $mmSite.getDocsUrl().then(function(docsurl) {
                $scope.docsurl = docsurl;
            });
        });
    }
    var langObserver = $mmEvents.on(mmCoreEventLanguageChanged, updateSiteInfo);
    var updateSiteObserver = $mmEvents.on(mmCoreEventSiteUpdated, function(siteid) {
        if ($mmSite.getId() === siteid) {
            updateSiteInfo();
        }
    });
    $scope.$on('$destroy', function() {
        if (langObserver && langObserver.off) {
            langObserver.off();
        }
        if (updateSiteObserver && updateSiteObserver.off) {
            updateSiteObserver.off();
        }
    });
}]);

angular.module('mm.core.sidemenu')
.provider('$mmSideMenuDelegate', function() {
    var navHandlers = {},
        self = {};
        self.registerNavHandler = function(addon, handler, priority) {
        if (typeof navHandlers[addon] !== 'undefined') {
            console.log("$mmSideMenuDelegateProvider: Addon '" + navHandlers[addon].addon + "' already registered as navigation handler");
            return false;
        }
        console.log("$mmSideMenuDelegateProvider: Registered addon '" + addon + "' as navigation handler.");
        navHandlers[addon] = {
            addon: addon,
            handler: handler,
            instance: undefined,
            priority: priority
        };
        return true;
    };
    self.$get = ["$mmUtil", "$q", "$log", "$mmSite", function($mmUtil, $q, $log, $mmSite) {
        var enabledNavHandlers = {},
            currentSiteHandlers = [],
            self = {},
            loaded = false;
        $log = $log.getInstance('$mmSideMenuDelegate');
                self.areNavHandlersLoaded = function() {
            return loaded;
        };
                self.clearSiteHandlers = function() {
            loaded = false;
            $mmUtil.emptyArray(currentSiteHandlers);
        };
                self.getNavHandlers = function() {
            return currentSiteHandlers;
        };
                self.updateNavHandler = function(addon, handlerInfo) {
            var promise;
            if (typeof handlerInfo.instance === 'undefined') {
                handlerInfo.instance = $mmUtil.resolveObject(handlerInfo.handler, true);
            }
            if (!$mmSite.isLoggedIn()) {
                promise = $q.reject();
            } else {
                promise = $q.when(handlerInfo.instance.isEnabled());
            }
            return promise.then(function(enabled) {
                if (enabled) {
                    enabledNavHandlers[addon] = {
                        instance: handlerInfo.instance,
                        priority: handlerInfo.priority
                    };
                } else {
                    return $q.reject();
                }
            }).catch(function() {
                delete enabledNavHandlers[addon];
            });
        };
                self.updateNavHandlers = function() {
            var promises = [];
            $log.debug('Updating navigation handlers for current site.');
            angular.forEach(navHandlers, function(handlerInfo, addon) {
                promises.push(self.updateNavHandler(addon, handlerInfo));
            });
            return $q.all(promises).then(function() {
                return true;
            }, function() {
                return true;
            }).finally(function() {
                $mmUtil.emptyArray(currentSiteHandlers);
                angular.forEach(enabledNavHandlers, function(handler) {
                    currentSiteHandlers.push({
                        controller: handler.instance.getController(),
                        priority: handler.priority
                    });
                });
                loaded = true;
            });
        };
        return self;
    }];
    return self;
});

angular.module('mm.core')
.directive('mmUserLink', ["$state", "mmUserProfileState", function($state, mmUserProfileState) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.on('click', function(event) {
                event.preventDefault();
                event.stopPropagation();
                $state.go(mmUserProfileState, {courseid: attrs.courseid, userid: attrs.userid});
            });
        }
    };
}]);

angular.module('mm.core.user')
.provider('$mmUserDelegate', function() {
    var profileHandlers = {},
        self = {};
        self.registerProfileHandler = function(component, handler, priority) {
        if (typeof profileHandlers[component] !== 'undefined') {
            console.log("$mmUserDelegateProvider: Handler '" + profileHandlers[component].component + "' already registered as profile handler");
            return false;
        }
        console.log("$mmUserDelegateProvider: Registered component '" + component + "' as profile handler.");
        profileHandlers[component] = {
            component: component,
            handler: handler,
            instance: undefined,
            priority: typeof priority === 'undefined' ? 100 : priority
        };
        return true;
    };
    self.$get = ["$q", "$log", "$mmSite", "$mmUtil", function($q, $log, $mmSite, $mmUtil) {
        var enabledProfileHandlers = {},
            self = {};
        $log = $log.getInstance('$mmUserDelegate');
                self.getProfileHandlersFor = function(user, courseId) {
            var handlers = [],
                promises = [];
            angular.forEach(enabledProfileHandlers, function(handler) {
                var promise = $q.when(handler.instance.isEnabledForUser(user, courseId)).then(function(enabled) {
                    if (enabled) {
                        handlers.push({
                            controller: handler.instance.getController(user, courseId),
                            priority: handler.priority
                        });
                    } else {
                        return $q.reject();
                    }
                }).catch(function() {
                });
                promises.push(promise);
            });
            return $q.all(promises).then(function() {
                return handlers;
            }).catch(function() {
                return handlers;
            });
        };
                self.updateProfileHandler = function(component, handlerInfo) {
            var promise;
            if (typeof handlerInfo.instance === 'undefined') {
                handlerInfo.instance = $mmUtil.resolveObject(handlerInfo.handler, true);
            }
            if (!$mmSite.isLoggedIn()) {
                promise = $q.reject();
            } else {
                promise = $q.when(handlerInfo.instance.isEnabled());
            }
            return promise.then(function(enabled) {
                if (enabled) {
                    enabledProfileHandlers[component] = {
                        instance: handlerInfo.instance,
                        priority: handlerInfo.priority
                    };
                } else {
                    return $q.reject();
                }
            }).catch(function() {
                delete enabledProfileHandlers[component];
            });
        };
                self.updateProfileHandlers = function() {
            var promises = [];
            $log.debug('Updating profile handlers for current site.');
            angular.forEach(profileHandlers, function(handlerInfo, component) {
                promises.push(self.updateProfileHandler(component, handlerInfo));
            });
            return $q.all(promises).then(function() {
                return true;
            }, function() {
                return true;
            });
        };
        return self;
    }];
    return self;
});

angular.module('mm.core.user')
.factory('$mmUserHandlers', ["$mmUtil", "$mmContentLinksHelper", function($mmUtil, $mmContentLinksHelper) {
    var self = {};
        self.linksHandler = function() {
        var self = {};
                self.getActions = function(siteIds, url) {
            if (url.indexOf('grade/report/user') == -1 &&
                    (url.indexOf('/user/view.php') > -1 || url.indexOf('/user/profile.php') > -1)) {
                var params = $mmUtil.extractUrlParams(url);
                if (typeof params.id != 'undefined') {
                    return [{
                        message: 'mm.core.view',
                        icon: 'ion-eye',
                        sites: siteIds,
                        action: function(siteId) {
                            var stateParams = {
                                courseid: params.course,
                                userid: parseInt(params.id, 10)
                            };
                            $mmContentLinksHelper.goInSite('site.mm_user-profile', stateParams, siteId);
                        }
                    }];
                }
            }
            return [];
        };
                self.handles = function(url) {
            var patterns = ['/user/view.php', '/user/profile.php'];
            if (url.indexOf('grade/report/user') == -1) {
                for (var i = 0; i < patterns.length; i++) {
                    var position = url.indexOf(patterns[i]);
                    if (position > -1) {
                        return url.substr(0, position);
                    }
                }
            }
        };
        return self;
    };
    return self;
}]);

angular.module('mm.core.user')
.constant('mmCoreUsersStore', 'users')
.config(["$mmSitesFactoryProvider", "mmCoreUsersStore", function($mmSitesFactoryProvider, mmCoreUsersStore) {
    var stores = [
        {
            name: mmCoreUsersStore,
            keyPath: 'id'
        }
    ];
    $mmSitesFactoryProvider.registerStores(stores);
}])
.factory('$mmUser', ["$log", "$q", "$mmSite", "$mmUtil", "$translate", "mmCoreUsersStore", function($log, $q, $mmSite, $mmUtil, $translate, mmCoreUsersStore) {
    $log = $log.getInstance('$mmUser');
    var self = {};
        self.deleteStoredUser = function(id) {
        if (!$mmSite.isLoggedIn()) {
            return $q.reject();
        }
        self.invalidateUserCache(id);
        return $mmSite.getDb().remove(mmCoreUsersStore, parseInt(id));
    };
        self.formatAddress = function(address, city, country) {
        if (address) {
            address += city ? ', ' + city : '';
            address += country ? ', ' + country : '';
        }
        return address;
    };
        self.formatRoleList = function(roles) {
        var deferred = $q.defer();
        if (roles && roles.length > 0) {
            $translate('mm.core.elementseparator').then(function(separator) {
                var rolekeys = roles.map(function(el) {
                    return 'mm.user.'+el.shortname;
                });
                $translate(rolekeys).then(function(roleNames) {
                    var roles = '';
                    for (var roleKey in roleNames) {
                        var roleName = roleNames[roleKey];
                        if (roleName.indexOf('mm.user.') > -1) {
                            roleName = roleName.replace('mm.user.', '');
                        }
                        roles += (roles != '' ? separator: '') + roleName;
                    }
                    deferred.resolve(roles);
                });
            });
        } else {
            deferred.resolve('');
        }
        return deferred.promise;
    };
        self.getProfile = function(userid, courseid, forceLocal) {
        var deferred = $q.defer();
        if (forceLocal) {
            self.getUserFromLocal(userid).then(deferred.resolve, function() {
                self.getUserFromWS(userid, courseid).then(deferred.resolve, deferred.reject);
            });
        } else {
            self.getUserFromWS(userid, courseid).then(deferred.resolve, function() {
                self.getUserFromLocal(userid).then(deferred.resolve, deferred.reject);
            });
        }
        return deferred.promise;
    };
        function getUserCacheKey(userid) {
        return 'mmUser:data:'+userid;
    }
        self.getUserFromLocal = function(id) {
        if (!$mmSite.isLoggedIn()) {
            return $q.reject();
        }
        return $mmSite.getDb().get(mmCoreUsersStore, parseInt(id));
    };
        self.getUserFromWS = function(userid, courseid) {
        var wsName,
            data,
            preSets ={
                cacheKey: getUserCacheKey(userid)
            };
        if (courseid > 1) {
            $log.debug('Get participant with ID ' + userid + ' in course '+courseid);
            wsName = 'core_user_get_course_user_profiles';
            data = {
                "userlist[0][userid]": userid,
                "userlist[0][courseid]": courseid
            };
        } else {
            $log.debug('Get user with ID ' + userid);
            if ($mmSite.wsAvailable('core_user_get_users_by_field')) {
                wsName = 'core_user_get_users_by_field';
                data = {
                    'field': 'id',
                    'values[0]': userid
                };
            } else {
                wsName = 'core_user_get_users_by_id';
                data = {
                    'userids[0]': userid
                };
            }
        }
        return $mmSite.read(wsName, data, preSets).then(function(users) {
            if (users.length == 0) {
                return $q.reject();
            }
            var user = users.shift();
            if (user.country) {
                user.country = $mmUtil.getCountryName(user.country);
            }
            self.storeUser(user.id, user.fullname, user.profileimageurl);
            return user;
        });
    };
        self.invalidateUserCache = function(userid) {
        return $mmSite.invalidateWsCacheForKey(getUserCacheKey(userid));
    };
        self.storeUser = function(id, fullname, avatar) {
        if (!$mmSite.isLoggedIn()) {
            return $q.reject();
        }
        return $mmSite.getDb().insert(mmCoreUsersStore, {
            id: parseInt(id),
            fullname: fullname,
            profileimageurl: avatar
        });
    };
        self.storeUsers = function(users) {
        var promises = [];
        angular.forEach(users, function(user) {
            var userid = user.id || user.userid,
                img = user.profileimageurl || user.profileimgurl;
            if (typeof userid != 'undefined') {
                promises.push(self.storeUser(userid, user.fullname, img));
            }
        });
        return $q.all(promises);
    };
    return self;
}]);

angular.module('mm.addons.mod_forum', [])
.constant('mmaModForumDiscPerPage', 10)
.constant('mmaModForumComponent', 'mmaModForum')
.constant('mmaModForumNewDiscussionEvent', 'mma-mod_forum_new_discussion')
.config(["$stateProvider", function($stateProvider) {
    $stateProvider
    .state('site.mod_forum', {
        url: '/mod_forum',
        params: {
            module: null,
            courseid: null
        },
        views: {
            'site': {
                controller: 'mmaModForumDiscussionsCtrl',
                templateUrl: 'modules/moodle/addons/mod_forum/templates/discussions.html'
            }
        }
    })
    .state('site.mod_forum-discussion', {
        url: '/mod_forum-discussion',
        params: {
            discussionid: null,
            cid: null
        },
        views: {
            'site': {
                controller: 'mmaModForumDiscussionCtrl',
                templateUrl: 'modules/moodle/addons/mod_forum/templates/discussion.html'
            }
        }
    })
    .state('site.mod_forum-newdiscussion', {
        url: '/mod_forum-newdiscussion',
        params: {
            cid: null,
            forumid: null,
            cmid: null
        },
        views: {
            'site': {
                controller: 'mmaModForumNewDiscussionCtrl',
                templateUrl: 'modules/moodle/addons/mod_forum/templates/newdiscussion.html'
            }
        }
    });
}])
.config(["$mmCourseDelegateProvider", "$mmContentLinksDelegateProvider", function($mmCourseDelegateProvider, $mmContentLinksDelegateProvider) {
    $mmCourseDelegateProvider.registerContentHandler('mmaModForum', 'forum', '$mmaModForumHandlers.courseContent');
    $mmContentLinksDelegateProvider.registerLinkHandler('mmaModForum', '$mmaModForumHandlers.linksHandler');
}]);

angular.module('mm.addons.notifications', [])
.constant('mmaNotificationsListLimit', 20)
.constant('mmaNotificationsPriority', 800)
.config(["$stateProvider", "$mmSideMenuDelegateProvider", "mmaNotificationsPriority", function($stateProvider, $mmSideMenuDelegateProvider, mmaNotificationsPriority) {
    $stateProvider
    .state('site.notifications', {
        url: '/notifications',
        views: {
            'site': {
                templateUrl: 'modules/moodle/addons/notifications/templates/list.html',
                controller: 'mmaNotificationsListCtrl'
            }
        }
    });
    $mmSideMenuDelegateProvider.registerNavHandler('mmaNotifications', '$mmaNotificationsHandlers.sideMenuNav', mmaNotificationsPriority);
}])
.run(["$log", "$mmaNotifications", "$mmUtil", "$state", "$mmAddonManager", function($log, $mmaNotifications, $mmUtil, $state, $mmAddonManager) {
    $log = $log.getInstance('mmaNotifications');
    var $mmPushNotificationsDelegate = $mmAddonManager.get('$mmPushNotificationsDelegate');
    if ($mmPushNotificationsDelegate) {
        $mmPushNotificationsDelegate.registerHandler('mmaNotifications', function(notification) {
            if ($mmUtil.isTrueOrOne(notification.notif)) {
                $mmaNotifications.isPluginEnabledForSite(notification.site).then(function() {
                    $mmaNotifications.invalidateNotificationsList().finally(function() {
                        $state.go('redirect', {siteid: notification.site, state: 'site.notifications'});
                    });
                });
                return true;
            }
        });
    }
}]);

angular.module('mm.addons.pushnotifications', [])
.constant('mmaPushNotificationsComponent', 'mmaPushNotifications')
.run(["$mmaPushNotifications", "$ionicPlatform", "$rootScope", "$mmEvents", "$mmLocalNotifications", "mmCoreEventLogin", "mmaPushNotificationsComponent", "mmCoreEventSiteDeleted", function($mmaPushNotifications, $ionicPlatform, $rootScope, $mmEvents, $mmLocalNotifications, mmCoreEventLogin,
            mmaPushNotificationsComponent, mmCoreEventSiteDeleted) {
    $ionicPlatform.ready(function() {
        $mmaPushNotifications.registerDevice();
    });
    $rootScope.$on('$cordovaPush:notificationReceived', function(e, notification) {
        if (ionic.Platform.isAndroid()) {
            $mmaPushNotifications.onGCMReceived(notification);
        } else if (ionic.Platform.isIOS()) {
            $mmaPushNotifications.onMessageReceived(notification);
        }
    });
    $mmEvents.on(mmCoreEventLogin, function() {
        $mmaPushNotifications.registerDeviceOnMoodle();
    });
    $mmEvents.on(mmCoreEventSiteDeleted, function(site) {
        $mmaPushNotifications.unregisterDeviceOnMoodle(site);
    });
    $mmLocalNotifications.registerClick(mmaPushNotificationsComponent, $mmaPushNotifications.notificationClicked);
}]);

angular.module('mm.addons.mod_forum')
.controller('mmaModForumDiscussionCtrl', ["$q", "$scope", "$stateParams", "$mmaModForum", "$mmSite", "$mmUtil", "$translate", "$ionicScrollDelegate", "mmaModForumComponent", function($q, $scope, $stateParams, $mmaModForum, $mmSite, $mmUtil, $translate,
            $ionicScrollDelegate, mmaModForumComponent) {
    var discussionid = $stateParams.discussionid,
        courseid = $stateParams.cid,
        scrollView;
    $scope.component = mmaModForumComponent;
    $scope.courseid = courseid;
    $scope.newpost = {
        replyingto: undefined,
        subject: '',
        message: ''
    };
    function fetchPosts() {
        return $mmaModForum.getDiscussionPosts(discussionid).then(function(posts) {
            $scope.discussion = $mmaModForum.extractStartingPost(posts);
            $scope.posts = posts;
            return $translate('mma.mod_forum.re').then(function(strReplyPrefix) {
                $scope.defaultSubject = strReplyPrefix + ' ' + $scope.discussion.subject;
                $scope.newpost.subject = $scope.defaultSubject;
            });
        }, function(message) {
            $mmUtil.showErrorModal(message);
            return $q.reject();
        });
    }
    function refreshPosts() {
        return $mmaModForum.invalidateDiscussionPosts(discussionid).finally(function() {
            return fetchPosts();
        });
    }
    fetchPosts().then(function() {
        $mmSite.write('mod_forum_view_forum_discussion', {
            discussionid: discussionid
        });
    }).finally(function() {
        $scope.discussionLoaded = true;
    });
    $scope.refreshPosts = function() {
        refreshPosts().finally(function() {
            $scope.$broadcast('scroll.refreshComplete');
        });
    };
    $scope.newPostAdded = function() {
        if (!scrollView) {
            scrollView = $ionicScrollDelegate.$getByHandle('mmaModForumPostsScroll');
        }
        scrollView && scrollView.scrollTop && scrollView.scrollTop();
        $scope.newpost.replyingto = undefined;
        $scope.newpost.subject = $scope.defaultSubject;
        $scope.newpost.message = '';
        $scope.discussionLoaded = false;
        refreshPosts().finally(function() {
            $scope.discussionLoaded = true;
        });
    };
}]);

angular.module('mm.addons.mod_forum')
.controller('mmaModForumDiscussionsCtrl', ["$q", "$scope", "$stateParams", "$mmaModForum", "$mmCourse", "$mmUtil", "$mmGroups", "$mmEvents", "$ionicScrollDelegate", "$ionicPlatform", "mmUserProfileState", "mmaModForumNewDiscussionEvent", "$ionicHistory", function($q, $scope, $stateParams, $mmaModForum, $mmCourse, $mmUtil, $mmGroups,
            $mmEvents, $ionicScrollDelegate, $ionicPlatform, mmUserProfileState, mmaModForumNewDiscussionEvent, $ionicHistory ) {
    var module = $stateParams.module || {},
        courseid = $stateParams.courseid,
        forum,
        page = 0,
        scrollView = $ionicScrollDelegate.$getByHandle('mmaModForumDiscussionsScroll'),
        shouldScrollTop = false,
        usesGroups = false;
    $scope.title = module.name;
    $scope.description = module.description;
    $scope.moduleurl = module.url;
    $scope.courseid = courseid;
    $scope.userStateName = mmUserProfileState;
    $scope.isCreateEnabled = $mmaModForum.isCreateDiscussionEnabled();
    function fetchForumDataAndDiscussions(refresh) {
        return $mmaModForum.getForum(courseid, module.id).then(function(forumdata) {
            forum = forumdata;
            $scope.title = forum.name || $scope.title;
            $scope.description = forum.intro || $scope.description;
            $scope.forum = forum;
            return $mmGroups.getActivityGroupMode(forum.cmid).then(function(mode) {
                usesGroups = mode === $mmGroups.SEPARATEGROUPS || mode === $mmGroups.VISIBLEGROUPS;
            }).finally(function() {
                return fetchDiscussions(refresh);
            });
        }, function(message) {
            if (!refresh) {
                return refreshData();
            }
            if (message) {
                $mmUtil.showErrorModal(message);
            } else {
                $mmUtil.showErrorModal('mma.mod_forum.errorgetforum', true);
            }
            $scope.canLoadMore = false;
            return $q.reject();
        });
    }
    function fetchDiscussions(refresh) {
        if (refresh) {
            page = 0;
        }
        return $mmaModForum.getDiscussions(forum.id, page).then(function(response) {
            var promise = usesGroups ?
                    $mmaModForum.formatDiscussionsGroups(forum.cmid, response.discussions) : $q.when(response.discussions);
            return promise.then(function(discussions) {
                if (page == 0) {
                    $scope.discussions = discussions;
                } else {
                    $scope.discussions = $scope.discussions.concat(discussions);
                }
                $scope.count = $scope.discussions.length;
                $scope.canLoadMore = response.canLoadMore;
                page++;
                preFetchDiscussionsPosts(discussions);
            });
        }, function(message) {
            $mmUtil.showErrorModal(message);
            $scope.canLoadMore = false;
            return $q.reject();
        });
    }
    function preFetchDiscussionsPosts(discussions) {
        angular.forEach(discussions, function(discussion) {
            var discussionid = discussion.discussion;
            $mmaModForum.getDiscussionPosts(discussionid);
        });
    }
    function refreshData() {
        var promises = [];
        promises.push($mmaModForum.invalidateForumData(courseid));
        if (forum) {
            promises.push($mmaModForum.invalidateDiscussionsList(forum.id));
            promises.push($mmGroups.invalidateActivityGroupMode(forum.cmid));
        }
        return $q.all(promises).finally(function() {
            return fetchForumDataAndDiscussions(true);
        });
    }
    fetchForumDataAndDiscussions().then(function() {
        $mmaModForum.logView(forum.id).then(function() {
            $mmCourse.checkModuleCompletion(courseid, module.completionstatus);
        });
    }).finally(function() {
        $scope.discussionsLoaded = true;
    });
    $scope.loadMoreDiscussions = function() {
        fetchDiscussions().finally(function() {
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });
    };
    $scope.refreshDiscussions = function() {
        refreshData().finally(function() {
            $scope.$broadcast('scroll.refreshComplete');
        });
    };
    var obsNewDisc = $mmEvents.on(mmaModForumNewDiscussionEvent, function(data) {
        if ((forum && forum.id === data.forumid) || data.cmid === module.id) {
            if ($ionicPlatform.isTablet()) {
                scrollView.scrollTop();
            } else {
                shouldScrollTop = true;
            }
            $scope.discussionsLoaded = false;
            refreshData().finally(function() {
                $scope.discussionsLoaded = true;
            });
        }
    });
    $scope.goPortal = function(){
      $ionicHistory.goBack();
    }
    $scope.$on('$ionicView.enter', function() {
        if (shouldScrollTop) {
            shouldScrollTop = false;
            scrollView.scrollTop();
        }
    });
    $scope.$on('$destroy', function(){
        if (obsNewDisc && obsNewDisc.off) {
            obsNewDisc.off();
        }
    });
}]);

angular.module('mm.addons.mod_forum')
.controller('mmaModForumNewDiscussionCtrl', ["$scope", "$stateParams", "$mmGroups", "$q", "$mmaModForum", "$mmEvents", "$ionicPlatform", "$mmUtil", "$ionicHistory", "$translate", "mmaModForumNewDiscussionEvent", function($scope, $stateParams, $mmGroups, $q, $mmaModForum, $mmEvents, $ionicPlatform,
            $mmUtil, $ionicHistory, $translate, mmaModForumNewDiscussionEvent) {
    var courseid = $stateParams.cid,
        forumid = $stateParams.forumid,
        cmid = $stateParams.cmid;
    $scope.newdiscussion = {
        subject: '',
        message: '',
        subscribe: true
    };
    function fetchGroups(refresh) {
        return $mmGroups.getActivityGroupMode(cmid).then(function(mode) {
            if (mode === $mmGroups.SEPARATEGROUPS || mode === $mmGroups.VISIBLEGROUPS) {
                return $mmGroups.getActivityAllowedGroups(cmid).then(function(forumgroups) {
                    var promise;
                    if (mode === $mmGroups.VISIBLEGROUPS) {
                        promise = validateVisibleGroups(forumgroups, refresh);
                    } else {
                        promise = $q.when(forumgroups);
                    }
                    return promise.then(function(forumgroups) {
                        if (forumgroups.length > 0) {
                            $scope.groups = forumgroups;
                            $scope.newdiscussion.groupid = forumgroups[0].id;
                            $scope.showGroups = true;
                            $scope.showForm = true;
                        } else {
                            var message = mode === $mmGroups.SEPARATEGROUPS ?
                                                'mma.mod_forum.cannotadddiscussionall' : 'mma.mod_forum.cannotadddiscussion';
                            return $q.reject($translate.instant(message));
                        }
                    });
                });
            } else {
                $scope.showGroups = false;
                $scope.showForm = true;
            }
        }).catch(function(message) {
            if (message) {
                $mmUtil.showErrorModal(message);
            } else {
                $mmUtil.showErrorModal('mma.mod_forum.errorgetgroups', true);
            }
            $scope.showForm = false;
            return $q.reject();
        });
    }
    function validateVisibleGroups(forumgroups, refresh) {
        if ($mmaModForum.isCanAddDiscussionAvailable()) {
            return $mmaModForum.canAddDiscussionToAll(forumid).catch(function() {
                return false;
            }).then(function(canAdd) {
                if (canAdd) {
                    return forumgroups;
                } else {
                    var promises = [],
                        filtered = [];
                    angular.forEach(forumgroups, function(group) {
                        promises.push($mmaModForum.canAddDiscussion(forumid, group.id).catch(function() {
                            return true;
                        }).then(function(canAdd) {
                            if (canAdd) {
                                filtered.push(group);
                            }
                        }));
                    });
                    return $q.all(promises).then(function() {
                        return filtered;
                    });
                }
            });
        } else {
            return $mmGroups.getUserGroupsInCourse(courseid, refresh).then(function(usergroups) {
                if (usergroups.length === 0) {
                    return forumgroups;
                }
                return filterGroups(forumgroups, usergroups);
            });
        }
    }
    function filterGroups(forumgroups, usergroups) {
        var filtered = [],
            usergroupsids = usergroups.map(function(g) {
                return g.id;
            });
        angular.forEach(forumgroups, function(fg) {
            if (usergroupsids.indexOf(fg.id) > -1) {
                filtered.push(fg);
            }
        });
        return filtered;
    }
    fetchGroups().finally(function() {
        $scope.groupsLoaded = true;
    });
    $scope.refreshGroups = function() {
        var p1 = $mmGroups.invalidateActivityGroupMode(cmid),
            p2 = $mmGroups.invalidateActivityAllowedGroups(cmid),
            p3 = $mmaModForum.invalidateCanAddDiscussion(forumid);
        $q.all([p1, p2]).finally(function() {
            fetchGroups(true).finally(function() {
                $scope.$broadcast('scroll.refreshComplete');
            });
        });
    };
    $scope.add = function() {
        var subject = $scope.newdiscussion.subject,
            message = $scope.newdiscussion.message,
            subscribe = $scope.newdiscussion.subscribe,
            groupid = $scope.newdiscussion.groupid;
        if (!subject) {
            $mmUtil.showErrorModal('mma.mod_forum.erroremptysubject', true);
            return;
        }
        if (!message) {
            $mmUtil.showErrorModal('mma.mod_forum.erroremptymessage', true);
            return;
        }
        message = '<p>' + message + '<p>';
        $mmaModForum.addNewDiscussion(forumid, subject, message, subscribe, groupid).then(function(discussionid) {
            var data = {
                forumid: forumid,
                discussionid: discussionid,
                cmid: cmid
            };
            $mmEvents.trigger(mmaModForumNewDiscussionEvent, data);
            if ($ionicPlatform.isTablet()) {
                $scope.newdiscussion.subject = '';
                $scope.newdiscussion.message = '';
            } else {
                $ionicHistory.goBack();
            }
        }).catch(function(message) {
            if (message) {
                $mmUtil.showErrorModal(message);
            } else {
                $mmUtil.showErrorModal('mma.mod_forum.cannotcreatediscussion', true);
            }
        });
    };
}]);

angular.module('mm.addons.mod_forum')
.directive('mmaModForumDiscussionPost', ["$mmaModForum", "$mmUtil", "$translate", "$q", function($mmaModForum, $mmUtil, $translate, $q) {
    return {
        restrict: 'E',
        scope: {
            post: '=',
            courseid: '=',
            title: '=',
            subject: '=',
            component: '=',
            newpost: '=',
            showdivider: '=?',
            titleimportant: '=?',
            postadded: '&?',
            defaultsubject: '=?'
        },
        templateUrl: 'modules/moodle/addons/mod_forum/templates/discussionpost.html',
        transclude: true,
        link: function(scope) {
            scope.isReplyEnabled = $mmaModForum.isReplyPostEnabled();
            scope.showReply = function() {
                scope.newpost.replyingto = scope.post.id;
            };
            scope.reply = function() {
                if (!scope.newpost.subject) {
                    $mmUtil.showErrorModal('mma.mod_forum.erroremptysubject', true);
                    return;
                }
                if (!scope.newpost.message) {
                    $mmUtil.showErrorModal('mma.mod_forum.erroremptymessage', true);
                    return;
                }
                var message = '<p>' + scope.newpost.message.replace(/\n/g, '<br>') + '</p>',
                    modal = $mmUtil.showModalLoading('mm.core.sending', true);
                $mmaModForum.replyPost(scope.newpost.replyingto, scope.newpost.subject, message).then(function() {
                    if (scope.postadded) {
                        scope.postadded();
                    }
                }).catch(function(message) {
                    if (message) {
                        $mmUtil.showErrorModal(message);
                    } else {
                        $mmUtil.showErrorModal('mma.mod_forum.couldnotadd', true);
                    }
                }).finally(function() {
                    modal.dismiss();
                });
            };
            scope.cancel = function() {
                var promise;
                if (!scope.newpost.subject && !scope.newpost.message) {
                    promise = $q.when();
                } else {
                    promise = $mmUtil.showConfirm($translate('mm.core.areyousure'));
                }
                promise.then(function() {
                    scope.newpost.replyingto = undefined;
                    scope.newpost.subject = scope.defaultsubject || '';
                    scope.newpost.message = '';
                });
            };
        }
    };
}]);

angular.module('mm.addons.mod_forum')
.factory('$mmaModForum', ["$q", "$mmSite", "$mmUser", "$mmGroups", "$translate", "$mmSitesManager", "mmaModForumDiscPerPage", function($q, $mmSite, $mmUser, $mmGroups, $translate, $mmSitesManager, mmaModForumDiscPerPage) {
    var self = {};
        function getCanAddDiscussionCacheKey(forumid, groupid) {
        return getCommonCanAddDiscussionCacheKey(forumid) + ':' + groupid;
    }
        function getCommonCanAddDiscussionCacheKey(forumid) {
        return 'mmaModForum:canadddiscussion:' + forumid;
    }
        function getForumDataCacheKey(courseid) {
        return 'mmaModForum:forum:' + courseid;
    }
        function getDiscussionPostsCacheKey(discussionid) {
        return 'mmaModForum:discussion:' + discussionid;
    }
        function getDiscussionsListCacheKey(forumid) {
        return 'mmaModForum:discussions:' + forumid;
    }
        self.addNewDiscussion = function(forumid, subject, message, subscribe, groupid) {
        var params = {
            forumid: forumid,
            subject: subject,
            message: message,
            options: [
                {
                    name: 'discussionsubscribe',
                    value: !!subscribe
                }
            ]
        };
        if (groupid) {
            params.groupid = groupid;
        }
        return $mmSite.write('mod_forum_add_discussion', params).then(function(response) {
            if (!response || !response.discussionid) {
                return $q.reject();
            } else {
                return response.discussionid;
            }
        });
    };
        self.canAddDiscussion = function(forumid, groupid) {
        var params = {
                forumid: forumid,
                groupid: groupid
            },
            preSets = {
                cacheKey: getCanAddDiscussionCacheKey(forumid, groupid)
            };
        return $mmSite.read('mod_forum_can_add_discussion', params, preSets).then(function(result) {
            if (result) {
                return !!result.status;
            }
            return $q.reject();
        });
    };
        self.canAddDiscussionToAll = function(forumid) {
        return self.canAddDiscussion(forumid, -1);
    };
        self.extractStartingPost = function(posts) {
        var lastPost = posts[posts.length - 1];
        if (lastPost.parent == 0) {
            posts.pop();
            return lastPost;
        }
        for (var i = 0; i < posts.length; i++) {
            if (posts[i].parent == 0) {
                array.splice(i, 1);
                return posts[i];
            }
        }
        return undefined;
    };
        self.isCanAddDiscussionAvailable = function() {
        return $mmSite.wsAvailable('mod_forum_can_add_discussion');
    };
        self.isPluginEnabled = function(siteId) {
        siteId = siteId || $mmSite.getId();
        return $mmSitesManager.getSite(siteId).then(function(site) {
            return  site.wsAvailable('mod_forum_get_forums_by_courses') &&
                    site.wsAvailable('mod_forum_get_forum_discussions_paginated') &&
                    site.wsAvailable('mod_forum_get_forum_discussion_posts');
        });
    };
        self.formatDiscussionsGroups = function(cmid, discussions) {
        discussions = angular.copy(discussions);
        return $translate('mm.core.allparticipants').then(function(strAllParts) {
            return $mmGroups.getActivityAllowedGroups(cmid).then(function(forumgroups) {
                var groups = {};
                angular.forEach(forumgroups, function(fg) {
                    groups[fg.id] = fg;
                });
                angular.forEach(discussions, function(disc) {
                    if (disc.groupid === -1) {
                        disc.groupname = strAllParts;
                    } else {
                        var group = groups[disc.groupid];
                        if (group) {
                            disc.groupname = group.name;
                        }
                    }
                });
                return discussions;
            });
        }).catch(function() {
            return discussions;
        });
    };
        self.getForum = function(courseid, cmid) {
        var params = {
                courseids: [courseid]
            },
            preSets = {
                cacheKey: getForumDataCacheKey(courseid)
            };
        return $mmSite.read('mod_forum_get_forums_by_courses', params, preSets).then(function(forums) {
            var currentForum;
            angular.forEach(forums, function(forum) {
                if (forum.cmid == cmid) {
                    currentForum = forum;
                }
            });
            if (currentForum) {
                return currentForum;
            }
            return $q.reject();
        });
    };
        self.getDiscussionPosts = function(discussionid) {
        var params = {
                discussionid: discussionid
            },
            preSets = {
                cacheKey: getDiscussionPostsCacheKey(discussionid)
            };
        return $mmSite.read('mod_forum_get_forum_discussion_posts', params, preSets).then(function(response) {
            if (response) {
                storeUserData(response.posts);
                return response.posts;
            } else {
                return $q.reject();
            }
        });
    };
        self.getDiscussions = function(forumid, page) {
        page = page || 0;
        var params = {
                forumid: forumid,
                sortby:  'timemodified',
                sortdirection:  'DESC',
                page: page,
                perpage: mmaModForumDiscPerPage
            },
            preSets = {
                cacheKey: getDiscussionsListCacheKey(forumid)
            };
        return $mmSite.read('mod_forum_get_forum_discussions_paginated', params, preSets).then(function(response) {
            if (response) {
                var canLoadMore = response.discussions.length >= mmaModForumDiscPerPage;
                storeUserData(response.discussions);
                return {discussions: response.discussions, canLoadMore: canLoadMore};
            } else {
                return $q.reject();
            }
        });
    };
        self.invalidateCanAddDiscussion = function(forumid) {
        return $mmSite.invalidateWsCacheForKeyStartingWith(getCommonCanAddDiscussionCacheKey(forumid));
    };
        self.invalidateDiscussionPosts = function(discussionid) {
        return $mmSite.invalidateWsCacheForKey(getDiscussionPostsCacheKey(discussionid));
    };
        self.invalidateDiscussionsList = function(forumid) {
        return $mmSite.invalidateWsCacheForKey(getDiscussionsListCacheKey(forumid));
    };
        self.invalidateForumData = function(courseid) {
        return $mmSite.invalidateWsCacheForKey(getForumDataCacheKey(courseid));
    };
        self.isCreateDiscussionEnabled = function() {
        return $mmSite.wsAvailable('core_group_get_activity_groupmode') &&
                $mmSite.wsAvailable('core_group_get_activity_allowed_groups') &&
                $mmSite.wsAvailable('mod_forum_add_discussion');
    };
        self.isReplyPostEnabled = function() {
        return $mmSite.wsAvailable('mod_forum_add_discussion_post');
    };
        self.logView = function(id) {
        if (id) {
            var params = {
                forumid: id
            };
            return $mmSite.write('mod_forum_view_forum', params);
        }
        return $q.reject();
    };
        self.replyPost = function(postid, subject, message) {
        var params = {
            postid: postid,
            subject: subject,
            message: message
        };
        return $mmSite.write('mod_forum_add_discussion_post', params).then(function(response) {
            if (!response || !response.postid) {
                return $q.reject();
            } else {
                return response.postid;
            }
        });
    };
        function storeUserData(list) {
        var ids = [];
        angular.forEach(list, function(entry) {
            var id = parseInt(entry.userid);
            if (!isNaN(id) && ids.indexOf(id) === -1) {
                ids.push(id);
                $mmUser.storeUser(id, entry.userfullname, entry.userpictureurl);
            }
            if (typeof entry.usermodified != 'undefined') {
                id = parseInt(entry.usermodified);
                if(!isNaN(id) && ids.indexOf(id) === -1) {
                    ids.push(id);
                    $mmUser.storeUser(id, entry.usermodifiedfullname, entry.usermodifiedpictureurl);
                }
            }
        });
    }
    return self;
}]);

angular.module('mm.addons.mod_forum')
.factory('$mmaModForumHandlers', ["$mmCourse", "$mmaModForum", "$state", "$mmUtil", "$mmContentLinksHelper", "$q", function($mmCourse, $mmaModForum, $state, $mmUtil, $mmContentLinksHelper, $q) {
    var self = {};
        self.courseContent = function() {
        var self = {};
                self.isEnabled = function() {
            return $mmaModForum.isPluginEnabled();
        };
                self.getController = function(module, courseid) {
            return function($scope) {
                $scope.title = module.name;
                $scope.icon = $mmCourse.getModuleIconSrc('forum');
                $scope.action = function(e) {
                    if (e) {
                        e.preventDefault();
                        e.stopPropagation();
                    }
                    $state.go('site.mod_forum', {module: module, courseid: courseid});
                };
            };
        };
        return self;
    };
        self.linksHandler = function() {
        var self = {},
            patterns = ['/mod/forum/view.php', '/mod/forum/discuss.php'];
                function isIndexEnabled(siteId, courseId) {
            return $mmaModForum.isPluginEnabled(siteId).then(function(enabled) {
                if (!enabled) {
                    return false;
                }
                return courseId || $mmCourse.canGetModuleWithoutCourseId(siteId);
            });
        }
                function isDiscEnabled(siteId) {
            return $mmaModForum.isPluginEnabled(siteId);
        }
                self.getActions = function(siteIds, url, courseId) {
            if (url.indexOf(patterns[0]) > -1) {
                return $mmContentLinksHelper.treatModuleIndexUrl(siteIds, url, isIndexEnabled, courseId);
            } else if (url.indexOf(patterns[1]) > -1) {
                var params = $mmUtil.extractUrlParams(url);
                if (params.d != 'undefined') {
                    courseId = courseId || params.courseid || params.cid;
                    return $mmContentLinksHelper.filterSupportedSites(siteIds, isDiscEnabled, false, courseId).then(function(ids) {
                        if (!ids.length) {
                            return [];
                        } else {
                            return [{
                                message: 'mm.core.view',
                                icon: 'ion-eye',
                                sites: ids,
                                action: function(siteId) {
                                    var stateParams = {
                                        discussionid: parseInt(params.d, 10),
                                        cid: courseId
                                    };
                                    $mmContentLinksHelper.goInSite('site.mod_forum-discussion', stateParams, siteId);
                                }
                            }];
                        }
                    });
                }
            }
            return $q.when([]);
        };
                self.handles = function(url) {
            for (var i = 0; i < patterns.length; i++) {
                var position = url.indexOf(patterns[i]);
                if (position > -1) {
                    return url.substr(0, position);
                }
            }
        };
        return self;
    };
    return self;
}]);

angular.module('mm.addons.notifications')
.controller('mmaNotificationsListCtrl', ["$scope", "$mmUtil", "$mmaNotifications", "mmaNotificationsListLimit", function($scope, $mmUtil, $mmaNotifications, mmaNotificationsListLimit) {
    var readCount = 0,
        unreadCount = 0;
    $scope.notifications = [];
    function fetchNotifications(refresh) {
        if (refresh) {
            readCount = 0;
            unreadCount = 0;
        }
        return $mmaNotifications.getUnreadNotifications(unreadCount, mmaNotificationsListLimit).then(function(unread) {
            unreadCount += unread.length;
            if (unread.length < mmaNotificationsListLimit) {
                var readLimit = mmaNotificationsListLimit - unread.length;
                return $mmaNotifications.getReadNotifications(readCount, readLimit).then(function(read) {
                    readCount += read.length;
                    if (refresh) {
                        $scope.notifications = unread.concat(read);
                    } else {
                        $scope.notifications = $scope.notifications.concat(unread).concat(read);
                    }
                    $scope.canLoadMore = read.length >= readLimit;
                }, function(error) {
                    if (unread.length == 0) {
                        if (error) {
                            $mmUtil.showErrorModal(error);
                        } else {
                            $mmUtil.showErrorModal('mma.notifications.errorgetnotifications', true);
                        }
                        $scope.canLoadMore = false;
                    }
                });
            } else {
                if (refresh) {
                    $scope.notifications = unread;
                } else {
                    $scope.notifications = $scope.notifications.concat(unread);
                }
                $scope.canLoadMore = true;
            }
        }, function(error) {
            if (error) {
                $mmUtil.showErrorModal(error);
            } else {
                $mmUtil.showErrorModal('mma.notifications.errorgetnotifications', true);
            }
            $scope.canLoadMore = false;
        });
    }
    fetchNotifications().finally(function() {
        $scope.notificationsLoaded = true;
    });
    $scope.refreshNotifications = function() {
        $mmaNotifications.invalidateNotificationsList().finally(function() {
            fetchNotifications(true).finally(function() {
                $scope.$broadcast('scroll.refreshComplete');
            });
        });
    };
    $scope.loadMoreNotifications = function(){
        fetchNotifications().finally(function() {
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });
    };
}]);

angular.module('mm.addons.notifications')
.directive('mmaNotificationsActions', ["$log", "$mmContentLinksDelegate", function($log, $mmContentLinksDelegate) {
    $log = $log.getInstance('mmaNotificationsActions');
    function link(scope) {
        if (scope.contexturl) {
            $mmContentLinksDelegate.getActionsFor(scope.contexturl, scope.courseid).then(function(actions) {
                scope.actions = actions;
            });
        }
    }
    return {
        link: link,
        restrict: 'E',
        scope: {
            contexturl: '=',
            courseid: '='
        },
        templateUrl: 'modules/moodle/addons/notifications/templates/actions.html',
    };
}]);

angular.module('mm.addons.notifications')
.filter('mmaNotificationsFormat', ["$mmText", function($mmText) {
  return function(text) {
    text = text.replace(/-{4,}/ig, '');
    text = $mmText.replaceNewLines(text, '<br />');
    return text;
  };
}]);

angular.module('mm.addons.notifications')
.factory('$mmaNotificationsHandlers', ["$log", "$mmaNotifications", function($log, $mmaNotifications) {
    $log = $log.getInstance('$mmaNotificationsHandlers');
    var self = {};
        self.sideMenuNav = function() {
        var self = {};
                self.isEnabled = function() {
            return $mmaNotifications.isPluginEnabled();
        };
                self.getController = function() {
                        return function($scope) {
                $scope.icon = 'ion-ios-bell';
                $scope.title = 'mma.notifications.notifications';
                $scope.state = 'site.notifications';
            };
        };
        return self;
    };
    return self;
}]);

angular.module('mm.addons.notifications')
.factory('$mmaNotifications', ["$q", "$log", "$mmSite", "$mmSitesManager", "mmaNotificationsListLimit", function($q, $log, $mmSite, $mmSitesManager, mmaNotificationsListLimit) {
    $log = $log.getInstance('$mmaNotifications');
    var self = {};
    function formatNotificationsData(notifications) {
        angular.forEach(notifications, function(notification) {
            if (notification.contexturl && notification.contexturl.indexOf('/mod/forum/')) {
                notification.mobiletext = notification.smallmessage;
            } else {
                notification.mobiletext = notification.fullmessage;
            }
            var cid = notification.fullmessagehtml.match(/course\/view\.php\?id=([^"]*)/);
            if (cid && cid[1]) {
                notification.courseid = cid[1];
            }
        });
    }
        function getNotificationsCacheKey() {
        return 'mmaNotifications:list';
    };
        self.getNotifications = function(read, limitFrom, limitNumber) {
        limitFrom = limitFrom || 0;
        limitNumber = limitNumber || mmaNotificationsListLimit;
        $log.debug('Get ' + (read ? 'read' : 'unread') + ' notifications from ' + limitFrom + '. Limit: ' + limitNumber);
        var data = {
            useridto: $mmSite.getUserId(),
            useridfrom: 0,
            type: 'notifications',
            read: read ? 1 : 0,
            newestfirst: 1,
            limitfrom: limitFrom,
            limitnum: limitNumber
        };
        var preSets = {
            cacheKey: getNotificationsCacheKey()
        };
        return $mmSite.read('core_message_get_messages', data, preSets).then(function(response) {
            if (response.messages) {
                var notifications = response.messages;
                formatNotificationsData(notifications);
                return notifications;
            } else {
                return $q.reject();
            }
        });
    };
        self.getReadNotifications = function(limitFrom, limitNumber) {
        return self.getNotifications(true, limitFrom, limitNumber);
    };
        self.getUnreadNotifications = function(limitFrom, limitNumber) {
        return self.getNotifications(false, limitFrom, limitNumber);
    };
        self.invalidateNotificationsList = function() {
        return $mmSite.invalidateWsCacheForKey(getNotificationsCacheKey());
    };
        self.isPluginEnabled = function() {
        return $mmSite.wsAvailable('core_message_get_messages');
    };
        self.isPluginEnabledForSite = function(siteid) {
        return $mmSitesManager.getSite(siteid).then(function(site) {
            if (!site.wsAvailable('core_message_get_messages')) {
                return $q.reject();
            }
        });
    };
    return self;
}]);

angular.module('mm.addons.pushnotifications')
.factory('$mmPushNotificationsDelegate', ["$log", function($log) {
    $log = $log.getInstance('$mmPushNotificationsDelegate');
    var handlers = {},
        self = {};
        self.clicked = function(notification) {
        for (var name in handlers) {
            var callback = handlers[name];
            if (typeof callback == 'function') {
                var treated = callback(notification);
                if (treated) {
                    return;
                }
            }
        }
    };
        self.registerHandler = function(name, callback) {
        $log.debug("Registered handler '" + name + "' as push notification handler.");
        handlers[name] = callback;
    };
    return self;
}]);

angular.module('mm.addons.pushnotifications')
.factory('$mmaPushNotifications', ["$mmSite", "$log", "$cordovaPush", "$mmText", "$q", "$cordovaDevice", "$mmUtil", "mmCoreConfigConstants", "$mmApp", "$mmLocalNotifications", "$mmPushNotificationsDelegate", "$mmSitesManager", "mmaPushNotificationsComponent", function($mmSite, $log, $cordovaPush, $mmText, $q, $cordovaDevice, $mmUtil, mmCoreConfigConstants,
            $mmApp, $mmLocalNotifications, $mmPushNotificationsDelegate, $mmSitesManager, mmaPushNotificationsComponent) {
    $log = $log.getInstance('$mmaPushNotifications');
    var self = {},
        pushID;
        self.isPluginEnabled = function() {
        return $mmSite.wsAvailable('core_user_add_user_device')
                && $mmSite.wsAvailable('message_airnotifier_is_system_configured')
                && $mmSite.wsAvailable('message_airnotifier_are_notification_preferences_configured');
    };
        self.notificationClicked = function(data) {
        $mmApp.ready().then(function() {
            $mmPushNotificationsDelegate.clicked(data);
        });
    };
        self.onGCMReceived = function(notification) {
        $log.debug('GCM notification received. Type: '+notification.event);
        switch (notification.event) {
            case 'registered':
                if (notification.regid.length > 0) {
                    pushID = notification.regid;
                    return self.registerDeviceOnMoodle();
                } else {
                    $log.debug('Device NOT registered in GCM, invalid regid');
                    break;
                }
            case 'message':
                notification.payload.foreground = notification.foreground;
                return self.onMessageReceived(notification.payload);
            case 'error':
                $log.debug('Push messages error');
                break;
            default:
                $log.debug('Push unknown message');
        }
    };
        self.onMessageReceived = function(data) {
        var promise;
        if (data && data.site) {
            promise = $mmSitesManager.getSite(data.site);
        } else {
            promise = $q.when();
        }
        promise.then(function() {
            if ($mmUtil.isTrueOrOne(data.foreground)) {
                if ($mmLocalNotifications.isAvailable()) {
                    $mmText.formatText(data.title, true, true).then(function(formattedTitle) {
                        $mmText.formatText(data.message, true, true).then(function(formattedMessage) {
                            var localNotif = {
                                id: 1,
                                title: formattedTitle,
                                message: formattedMessage,
                                at: new Date(),
                                smallIcon: 'res://icon',
                                data: {
                                    notif: data.notif,
                                    site: data.site
                                }
                            };
                            $mmLocalNotifications.schedule(localNotif, mmaPushNotificationsComponent, data.site);
                        });
                    });
                }
            } else {
                self.notificationClicked(data);
            }
        });
    };
        self.registerDevice = function() {
        try {
            if (ionic.Platform.isIOS()) {
                return self._registerDeviceAPNS();
            } else if (ionic.Platform.isAndroid()) {
                return self._registerDeviceGCM();
            }
        } catch(ex) {}
        return $q.reject();
    };
        self._registerDeviceAPNS = function() {
        var options = {
            alert: 'true',
            badge: 'true',
            sound: 'true'
        };
        return $cordovaPush.register(options).then(function(token) {
            pushID = token;
            return self.registerDeviceOnMoodle();
        }, function(error) {
            return $q.reject();
        });
    };
        self._registerDeviceGCM = function() {
        if (mmCoreConfigConstants.gcmpn) {
            return $cordovaPush.register({
                senderID: mmCoreConfigConstants.gcmpn
            });
        }
        return $q.reject();
    };
        self.registerDeviceOnMoodle = function() {
        $log.debug('Register device on Moodle.');
        if (!$mmSite.isLoggedIn() || !pushID || !$mmApp.isDevice()) {
            return $q.reject();
        }
        var data = {
            appid:      mmCoreConfigConstants.app_id,
            name:       ionic.Platform.device().name || '',
            model:      $cordovaDevice.getModel(),
            platform:   $cordovaDevice.getPlatform(),
            version:    $cordovaDevice.getVersion(),
            pushid:     pushID,
            uuid:       $cordovaDevice.getUUID()
        };
        return $mmSite.write('core_user_add_user_device', data);
    };
        self.unregisterDeviceOnMoodle = function(site) {
        if (!site || !$mmApp.isDevice()) {
            return $q.reject();
        }
        $log.debug('Unregister device on Moodle: ' + site.id);
        var data = {
            appid: mmCoreConfigConstants.app_id,
            uuid:  $cordovaDevice.getUUID()
        };
        return site.write('core_user_remove_user_device', data).then(function(response) {
            if (!response || !response.removed) {
                return $q.reject();
            }
        });
    };
    return self;
}]);

angular.module('mm.core')

.constant('mmCoreConfigConstants', {
    "app_id" : "com.moodle.moodlemobile",
    "versioncode" : "2010",
    "versionname" : "2.9",
    "cache_expiration_time" : 300000,
    "default_lang" : "en",
    "languages": {"en": "English", "pt-br": "Português - Brasil"},
    "wsservice" : "moodle_mobile_app",
    "wsextservice" : "local_mobile",
    "demo_sites": {"student": {"url": "http://school.demo.moodle.net", "username": "student", "password": "moodle"}, "teacher": {"url": "http://school.demo.moodle.net", "username": "teacher", "password": "moodle"}, "cva": {"url": "http://mm.cvaconsulting.com/moodle", "username": "student", "password": "student"}},
    "gcmpn": "694767596569",
    "customurlscheme": "moodlemobile"
}
);