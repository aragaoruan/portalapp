angular.module('portal').config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "modules/portal/components/index/tpl/index.html",
      controller: 'IndexCtrl',

      onEnter: function (AuthFactory, $state) {
        if (!AuthFactory.verifyToken()) {
          $state.go('login');
        }
      }
    })
    .state('app.home', {
      url: "/home",
      templateUrl: "modules/portal/components/index/tpl/home.html",
      controller: 'HomeCtrl',
      cache: true
    })
    .state('app.disciplina', {
      url: "/disciplinas",
      templateUrl: "modules/portal/components/disciplina/tpl/index.html",
      cache: true,
    })
    .state('app.disciplina.show', {
      cache: true,
      url: "/disciplina/:id_disciplina",
      views: {
        'tabs-disciplinas': {
          templateUrl: "modules/portal/components/disciplina/tpl/show.html",
          controller: 'DisciplinaShowCtrl'
        }
      }
    })
    .state('app.disciplina.show.conteudo', {
      url: '/conteudo/',
      params: {
        activeButton: 'b'
      },
      views: {
        'tabs-disciplina': {
          templateUrl: 'modules/portal/components/conteudo/tpl/conteudo-list.html',
          controller: 'ConteudoCtrl'
        }
      }
    })
    .state('app.mensagem', {
      url: '/mensagem',
      templateUrl: "modules/portal/components/mensagem/tpl/index.html",
      cache: true
    })
    .state('app.mensagem.list', {
      url: '/mensagem',
      cache: true,
      views: {
        'tabs-mensagens': {
          templateUrl: "modules/portal/components/mensagem/tpl/list.html",
          controller: 'MensagemCtrl'
        }
      }
    })
    .state('app.mensagem.show', {
      url: "/mensagem/:st_mensagem/:dt_cadastromsg/:st_texto",
      cache: true,
      views: {
        'tabs-mensagens': {
          templateUrl: "modules/portal/components/mensagem/tpl/show.html",
          controller: 'MensagemShowCtrl'
        }
      }
    })
    .state('app.notas', {
      url: "/notas/selecionar_curso_notas",
      templateUrl: "modules/portal/components/notas/tpl/index.html",
      cache: true
    })
    .state('app.notas.grade_notas_show', {
      url: '/notas/grade_notas_show',

      views: {
        'tabs-notas': {
          templateUrl: 'modules/portal/components/notas/tpl/grade_notas.html',
          controller: 'GradeNotasCtrl',
        }
      }
    })
    .state('app.assign', {
      url: "/assign/:instanceId",
      templateUrl: "modules/portal/components/assign/tpl/assign.html",
      controller: 'AssignCtrl',
      cache: true
    })
    .state('app.forum', {
      url: "/forum/:instanceId",
      templateUrl: "modules/portal/components/forum/tpl/foruns.html",
      controller: 'ForumCtrl',
      cache: true
    })
    .state('discussion', {
      url: "/discussion/:discussionId",
      templateUrl: "modules/portal/components/forum/tpl/discussion.html",
      controller: 'ForumDiscussionCtrl',
      cache: true
    })
    .state('app.financeiro', {
      url: "/financeiro",
      templateUrl: "modules/portal/components/financeiro/tpl/index.html",
      // controller: 'FinanceiroCtrl',
      cache: false,
    })
    .state('app.financeiro.list', {
      url: "/financeiro",
      cache: true,
      params: {
        action: 'index'
      },
      views: {
        'tabs-financeiro': {
          templateUrl: "modules/portal/components/financeiro/tpl/list.html",
          controller: 'FinanceiroCtrl',
        }
      }
    })
    .state('app.financeiro.detalhar_lancamento', {
      url: "/financeiro/lancamento/:id",
      cache: true,
      params: {
        action: 'detail'
      },
      views: {
        'tabs-financeiro': {
          templateUrl: "modules/portal/components/financeiro/tpl/detail-lancamento.html",
          controller: 'FinanceiroCtrl',
        }
      }
    })
    .state('app.promocoes', {
      url: "/promocoes",
      cache: false,
      templateUrl: "modules/portal/components/promocoes/tpl/index.html",
    })
    .state('app.promocoes.list', {
      url: "/list",
      cache: true,
      params: {
        action: 'index'
      },
      views: {
        'tabs-promocoes': {
          templateUrl: "modules/portal/components/promocoes/tpl/list.html",
          controller: 'PromocoesCtrl',
        }
      }
    })
    .state('app.servico_atencao', {
      url: "/central-atencao",
      cache: false,
      templateUrl: "modules/portal/components/servico-atencao/tpl/index.html",

    })
    .state('app.servico_atencao.list', {
      url: "/list",
      cache: false,
      params: {
        action: 'index'
      },
      views: {
        'tabs-sa': {
          templateUrl: "modules/portal/components/servico-atencao/tpl/list.html"
        }
      }
    })
    .state('app.servico_atencao.add', {
      url: "/central-atencao/add",
      cache: false,
      params: {
        action: 'add'
      },
      views: {
        'tabs-sa': {
          templateUrl: "modules/portal/components/servico-atencao/tpl/add.html",
          controller: 'ServicoAtencaoCtrl',
        }
      }
    })
    .state('app.servico_atencao.detail', {
      url: "/detail/:id",
      cache: false,
      params: {
        action: 'detail'
      },
      views: {
        'tabs-sa': {
          templateUrl: "modules/portal/components/servico-atencao/tpl/detail.html",
          controller: 'ServicoAtencaoCtrl',
        }
      }
    })
    //todo refatorar essas rotas aqui
    .state('app.conteudos_baixados', {
      url: "/conteudo_baixados",
      templateUrl: "modules/portal/components/conteudo/tpl/show-conteudo-baixados.html",
      controller: 'ConteudoBaixadosCtrl',
      cache: false
    })
    .state('conteudo_disciplina_show', {
      url: '/disciplina/conteudo/:id_disciplina/:id_disciplinaconteudo/:isLocal',
      templateUrl: 'modules/portal/components/conteudo/tpl/show-conteudo-html.html',
      controller: 'ConteudoDisciplinaShowCtrl',
      cache: false
    })
    .state('recuperar_senha', {
      url: "/recuperar-senha",
      templateUrl: "modules/portal/components/login/tpl/recuperar-senha.html",
      controller: 'LoginCtrl',
      cache: false
    })
    .state('login', {
      url: "/login",
      templateUrl: "modules/portal/components/login/tpl/login.html",
      controller: 'LoginCtrl',
      cache: false
    })

  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});
