angular.module('portal.services').service('AppService', function (CONFIG_APP, $http,
                                                                  $rootScope, $cordovaLocalNotifications, $state) {
  var that = this;


  this.getUrlApi = function () {
    return CONFIG_APP.url;
  }


  this.getWords = function (str, number) {
    return {
      letters: str.split(/\s+/).slice(0, number).join(" "),
      first_letter: str.charAt(0).toUpperCase()
    }
  };

  this.getColorStatus = function (status) {
    var color;
    switch (status) {
      case 1:
        color = 'balanced';
        break;
      case 2:
        color = 'assertive';
        break;
      case 3:
        color = 'stable';
        break;
      case 4:
        color = 'energized';
        break;
      case 5:
        color = 'calm';
        break;
      default :
        color = '';
        break;
    }
    return color;
  };

  /**
   * Função para retornar alguns icones
   * @param name
   * @returns {*}
   */
  this.getIconesProgresso = function (name) {
    switch (name) {
      case "book":
        return "ion-ios-book"
        break;
      case "chat":
        return "ion-chatboxes"
        break;
      case "forum":
        return "ion-ios-paper"
        break;
      case "questionnaire":
        return "ion-clipboard"
        break;
      default :
        return "";
        break;
    }
  }


  /**
   * Verificacao de existencia e abertura de notificacao registrada pelo app,
   * atraves do sistema de notificacoes do proprio device
   */
  this.notifications = function () {

    $rootScope.$on('$cordovaLocalNotifications:click',
      function (event, notification, state) {

        var params = angular.fromJson(notification.data);
        if (params) {
          $state.go(params.router, params);
        }
      });

    $rootScope.$on('$cordovaLocalNotifications:update', function (event, notification, state) {
      alert(JSON.stringify(event));
      alert(JSON.stringify(notification));
      alert(JSON.stringify(state));
    });
  };

  /**
   * Registrador de notificacoes no device do client
   * @param id
   * @param title
   * @param text
   * @param icon
   * @param data
   * @param smallIcon
   */
  this.registerNotificationSchedule = function (id, title, text, icon, smallIcon, data) {
    return $cordovaLocalNotifications.schedule({
      id: id,
      title: title,
      text: text,
      data: data
    });
  };

  /**
   * Registrador de updates em notificacoes no device do client
   * @param id
   * @param title
   * @param text
   * @param icon
   * @param smallIcon
   * @param data
   * @returns {*}
   */
  /*this.registerUpdateNotification = function(id, title, text, icon, smallIcon, data){
   return $cordovaLocalNotifications.update({
   id: id,
   title: title,
   text: text,
   icon: icon,
   smallIcon: smallIcon,
   data: data,
   sound: false
   });
   };*/

  this.cancelMultipleNotifications = function (ids) {
    if (!ionic.Platform.isIOS()) {
      document.addEventListener('deviceready', function () {
        return $cordovaLocalNotifications.cancel(ids).then(function (result) {
          // ...
        }, function (error) {
          alert(JSON.stringify(error));
        });
      });
    }
  };


  this.formatData = function (data) {
    var split = data.split('-');
    if (split) {
      var splitHora = split[2].split(" ");
      var ano = split[0];
      var mes = split[1];
      var dia = splitHora[0];
      var hora = splitHora[1] ? splitHora[1] : null;
      var horaFormatada = splitHora[1].split('.');
    }
    var date = dia + "/" + mes + "/" + ano + " ás " + horaFormatada[0];
    return date;
  }

  /**
   * Funcao para retornar as classes das cores dos respectivos status para as ocorrencias.
   * @param id
   * @returns {string}
   */
  this.getColorSituacaoOcorrencia = function (id) {
    switch (id) {
      case 98:
      case 112:
        return this.getColorStatus(5);
        break;
      case 97:
        return this.getColorStatus(2);
        break;
      case 99:
        return this.getColorStatus(1);
        break;
      default:
        return "";
        break;
    }
  }

});
