angular.module('portal.services').service('MensagemService', function ($state, CONFIG_APP, $http, AppService,
                                                                       $q, $log, $cordovaPush, $rootScope, UsuarioDeviceFactory, MensagemAgendamentoFactory) {
  var self = this;

  this.dispatchAlertMessagesPenddingUser = function () {

    document.addEventListener('deviceready', function () {

      cordova.plugins.backgroundMode.setDefaults({
        title: 'Portal do Aluno',
        text: 'Background Mode',
        silent: true
      });

      cordova.plugins.backgroundMode.enable();

      cordova.plugins.backgroundMode.onactivate = function () {
        setTimeout(function () {
          //run in background
        }, 9999);
      };

      cordova.plugins.backgroundMode.onfailure = function (errorCode) {
        alert(JSON.stringify(errorCode));
      };

    }, false);
  };

  this.getNovasMensagens = function () {
    return $http.get(CONFIG_APP.url + '/app-api/mensagem-agendamento');
  };

  this.getMensagensSlide = function () {
    var defer = $q.defer()

    MensagemAgendamentoFactory.GetAll({
      // bl_entrega: 0,
      id_evolucao: 42,
    }, false, true).then(function (response) {
      if (response.length) {
        var arrMensagens = [];
        angular.forEach(response, function (mens) {
          var description = angular.element(mens.st_texto).text();
          var mens = {
            title: mens.st_mensagem,
            description: description.replace(/\n/g, "").trim(),
            icon: "icon-icon_chat",
            obj: mens,
            type: 'mensagem',
            priority: mens.bl_importante
          };
          arrMensagens.push(mens);
        });
        defer.resolve(arrMensagens);
      }
    });

    // console.log("mensagens", defer.promise);
    return defer.promise;
  }


});
