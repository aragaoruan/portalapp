angular.module('portal.services')
.service('pendingRequests', function() {
  var pending = [];
  this.get = function() {
    return pending;
  };
  this.add = function(request) {
    pending.push(request);
  };
  this.remove = function(request) {
    pending = _.filter(pending, function(p) {
      return p.url !== request;
    });
  };
  this.cancelAll = function() {
    angular.forEach(pending, function(p) {
      p.canceller.resolve();
    });
    pending.length = 0;
  };
})
// Faz as requisições de forma a poderem ser canceladas
.service('httpService', function($http, $q, pendingRequests) {
  this.get = function(url, params) {
    var canceller = $q.defer();
    pendingRequests.add({
      url: url,
      canceller: canceller
    });

    var requestPromise = $http.get(url, { 
        params: params,
        timeout: canceller.promise 
    });

    requestPromise.finally(function() {
      pendingRequests.remove(url);
    });
    return requestPromise;
  }
});