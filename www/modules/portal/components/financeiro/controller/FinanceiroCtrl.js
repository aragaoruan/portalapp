angular.module('portal.controllers')
.controller('FinanceiroCtrl', function ($rootScope,
                                        CONFIG_APP,
                                        $scope,
                                        $state,
                                        FinanceiroFactory,
                                        $ionicLoading,
                                        AppService,
                                        $analytics,
                                        $ionicModal,
                                        $ionicPopup,
                                        $cordovaSocialSharing,
                                        $http,
                                        AuthFactory) {
  $rootScope.showFilter = false;

  $ionicModal.fromTemplateUrl('modules/portal/components/financeiro/tpl/detail-lancamento.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.modal = modal;
  });


  $analytics.pageTrack('/financeiro');

  $scope.lancamentosPagos = [];
  $scope.lancamentosPendentes = [];
  $scope.lancamentosAtrasados = [];
  $scope.lancamento = {};
  $scope.dados_boleto = {};


  /**
   * Retorna as vendas
   */
  function getVendas() {
    $ionicLoading.show({
      template: '<ion-spinner>Carregando...</ion-spinner>'
    });
    FinanceiroFactory.getVendasUser()
      .success(function (data) {
        angular.forEach(data, function (lancamento) {
          switch (lancamento.id_statuslancamento) {
            case 1:
              $scope.lancamentosPagos.push(lancamento);
              break;
            case 3:
              $scope.lancamentosAtrasados.push(lancamento);
              break;
            case 2:
              $scope.lancamentosPendentes.push(lancamento);
              break;
          }

        });

        //chamando o metodo para salvar log
        // o segundo parametro é null, pq não temos a informação do id_matricula nem da sala
        AuthFactory.gravarLogAcesso($state.current.name, null);

      }).error(function (e) {

      $ionicPopup.alert({
        title: 'Erro!',
        template: e.message
      });

    }).finally(function () {
      $ionicLoading.hide();
    });

  }

  /**
   * Função para carregar os dados do lançamento do storage e setar no scope
   */
  function carregaDadosLancamentoAtivo() {
    $ionicLoading.show({
      template: '<ion-spinner>Carregando...</ion-spinner>'
    });
    $scope.lancamento = FinanceiroFactory.getStorageLancamentoAtivo();


    /*
     * identifica se é um lançamento com o meio de pagamento do tipo boleto e não esta quitado,
     * busca os dados do boleto na api e seta no scope
     */
    if ($scope.lancamento.id_meiopagamento == 2 && !$scope.lancamento.bl_quitado) {
      FinanceiroFactory.getDadosBoleto($scope.lancamento.id_lancamento)
        .success(function (data) {
          $scope.dados_boleto = data;
        }).finally(function () {
      });
    }
    $ionicLoading.hide();
  }

  /**
   * Carrega o arquivo de recibo ou boleto para download
   * @param parcela
   */
  $scope.carregarArquivoFinanceiro = function (parcela, conferido) {
    conferido = typeof conferido == 'undefined'?false:conferido;
    $ionicLoading.show({
      template: '<ion-spinner>Carregando...</ion-spinner>',
      showBackdrop: true
    });
    if (parcela.bl_quitado == true) {
      if (parcela.id_textosistemarecibo) {
        window.open(CONFIG_APP.url + '/' + parcela.st_link.replace('html', 'pdf').replace('/default/', ''), '_system');
      } else {
        $ionicPopup.alert({
          title: 'Atenção!',
          template: 'Não é possível fazer download do recibo, texto de sistema não definido.'
        });
      }
    } else {
      if (parcela.id_meiopagamento == 1) {
        $ionicPopup.alert({
          title: 'Atenção!',
          template: 'Recibo indisponível.'
        });
      } else if (parcela.id_meiopagamento == 2) {
        $ionicLoading.show({
          template: '<ion-spinner>Carregando...</ion-spinner>',
          showBackdrop: true
        });
        //gera o boleto
        FinanceiroFactory.gerarPdfBoleto(parcela.id_lancamento)
        .then(function successCallback(response) {
          if(response.data.st_urlboleto != null){
            window.open(response.data.st_urlboleto, '_system');
            location.reload();
          }else{
            if(conferido == false){
              $ionicLoading.show({
                template: '<ion-spinner>Carregando...</ion-spinner>',
                showBackdrop: true
              });
              setTimeout(function(){
                $scope.carregarArquivoFinanceiro(parcela, true);
              },7000);
            }else{
            var alert = $ionicPopup.alert({
              title: 'Sucesso!',
              template: "Solicitação para geração de boleto enviada com sucesso! Enviaremos a notificação ao e-mail quando estiver disponível para pagamento."
            });

            alert.then(function(res){
              location.reload();
            });
          }
          }
        }, function errorCallback(response) {
          var texto = "Solicitação para geração de boleto enviada com sucesso! Enviaremos a notificação ao e-mail quando estiver disponível para pagamento.";
          var titulo = 'Sucesso!';
          if(response.hasOwnProperty('status') && response.status != 0){
            texto = 'Houve um erro na hora de gerar o boleto, tente novamente mais tarde.';
            titulo = 'Erro!';
          }else if(response.data == null && response.status == 0 && response.statusText == ""){
            texto = "Foi detectada uma conexão de dados lenta. Verifique se o boleto foi gerado corretamente. Caso tenha sido gerado, o mesmo será enviado para o e-mail cadastrado.";
            titulo = "Erro de conexão";
          }
          var alert = $ionicPopup.alert({
            title: titulo,
            template: texto
          });

          if(titulo != 'Erro!'){
            alert.then(function(res){
              location.reload();
            });
          }

        });
      }
    }

  };

  $scope.procuraPalavra = function (frase, palavra) {
    return frase.toLowerCase().search(palavra.toLowerCase());
  };

  $scope.geraBoleto = function(boleto) {
    var emitirBoleto = $ionicPopup.confirm({
      title: 'Confirmar emissão do boleto',
      template: 'Deseja realmente solicitar o boleto?',
      cancelText: 'Cancelar',
      okText:'OK'
    });

    emitirBoleto.then(function(res) {
      if(res) {
        $scope.carregarArquivoFinanceiro(boleto);
      } else {
        console.log('Geração de boleto cancelada.');
      }
    });
  };

  /**
   * Copia o texto para a memoria do device
   * @param value
   */
  $scope.copyText = function (value) {
    //cria um elemento para abriar o código a ser copiado
    var copiar = document.createElement("textarea");
    //adiciona o valor ao elemento criado
    copiar.textContent = value;
    //adiciona o elemento no body
    var body = document.getElementsByTagName('body')[0];
    body.appendChild(copiar);
    //seleciona o elemento
    copiar.select();
    //copia para a área de transferência
    document.execCommand('copy');

    if(value === copiar.value){
      $ionicPopup.alert({
            title: 'Sucesso!',
            template: 'Código de barras copiado para área de transferência.'
          });
    }else{
      $ionicPopup.alert({
            title: 'Erro!',
            template: 'Houve um erro ao tentar copiar código de barras.'
          });
    }

    //remove o elemento criado
    body.removeChild(copiar);
  };


  /**
   * Carrega os dados de recibo ou de boleto e compartilha com os apps do device
   * @param lancemento
   */
  $scope.compartilharArquivoFinanceiro = function (lancamento) {

    //recupera os dados da url da api
    var url = AppService.getUrlApi() + "/";

    //verifica se o lancamento esta quitado para carregar os recibos
    if (lancamento.bl_quitado) {

      //verifica se o texto sistema do recibo não esta definido e para a execução
      if (!lancamento.id_textosistemarecibo) {
        $ionicPopup.alert({
          title: 'Atenção!',
          template: 'Não é possível compartilhar o recibo, texto de sistema não definido.'
        });
        return;
      }


      //faz um replace na string do link do recibo, alterando onde tem html para pdf e onde tem /default/ para vazio
      var diretorioPath = lancamento.st_link.replace('html', 'pdf').replace('/default/', '');

      //carrega o plugin de compartilhamento do device passando o arquivo
      $cordovaSocialSharing
        .share("", "", url + diretorioPath) // Share via native share sheet
        .then(function (result) {
          console.log('shared', result);
        }, function (err) {
          console.log('error', err);
        });

      return;
    }


    if (lancamento.id_meiopagamento == 1 && !lancamento.bl_quitado) {
      $ionicPopup.alert({
        title: 'Atenção!',
        template: 'Recibo indisponível.'
      });
      return;
    }


    //verifica se o meio de pagamento é igual a boleto e se ele não esta quitado
    if (lancamento.id_meiopagamento == 2 && !lancamento.bl_quitado) {
      $ionicLoading.show({
        template: '<ion-spinner>Carregando...</ion-spinner>'
      });
      //gera o pdf do boleto
      FinanceiroFactory.gerarPdfBoleto(lancamento.id_lancamento)
        .success(function (dados) {
          $cordovaSocialSharing
            .share("", "", url + dados.path) // Share via native share sheet
            .then(function (result) {
              console.log('shared', result);
            }, function (err) {
              console.log('error', err);
            });

        }).finally(function () {
        $ionicLoading.hide();
      });
      return;
    }

  };


  /**
   * Seta o objeto do lançamento no storage, carrega os dados do boleto, ou do comprovante e
   * abre o modal.
   * @param lancamento
   */
  $scope.abreLancamento = function (lancamento) {

    /*
     * para abrir o modal de lançamento verifica se o lancamento é diferente do tipo Cartão Recorrente
     * ou o lançamento esta pago
     */
    if (lancamento.id_meiopagamento != 11 || lancamento.id_statuslancamento == 1) {
      FinanceiroFactory.setStorageLancamentoAtivo(lancamento);
      carregaDadosLancamentoAtivo(lancamento.id_lancamento);
      $scope.modal.show();
    }
  };


  /**
   * Fecha o modal
   */
  $scope.closeModal = function () {
    $scope.modal.hide();
  };


  /**
   * verifica se a action é a de index, e carrega todas as vendas.
   */
  if ($state.params.action == 'index') {
    getVendas();
  }


  /**
   * Retorna o formato da date em "01 de Janeiro de 2017"
   */
  $scope.formatDate = function (date) {
    if (date) {
      const split = date.split('-');
      if (split.length == 3) {
        const months = ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'];
        return split[2] + ' de ' + months[split[1] - 1] + ' de ' + split[0];
      }
    }

    return date;
  };

});
