angular.module('portal.controllers').controller('HomeCtrl',
  function ($rootScope, $scope, DisciplinaFactory, $ionicFilterBar, $state,
            $http, CONFIG_APP, $log, CursoAlunoFactory, $analytics, $locale, $ionicLoading,
            MensagemService,$filter,PromocoesFactory,pendingRequests) {

    $analytics.pageTrack('/home');

    $scope.listDisciplinas = [];
    $scope.listEntidade = [];
    $scope.completeRequest = false;
    $scope.dataSlider = [];

    var cacheMinutes = 60;
    
    $scope.$on("$ionicView.beforeLeave", function (event, data) {
      pendingRequests.cancelAll();
    });

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      pendingRequests.cancelAll();
      $ionicLoading.show();
    });

    $scope.$on("$ionicView.afterEnter", function (event, data) {
      listCurso(false);
      $rootScope.showFilter = false;
      $scope.getItensSlide(); //busca os slides
    });

    var listCurso = function (renewCache) {
      
      // param, cacheId, renewCache, cacheTime
      CursoAlunoFactory.GetAll( false, renewCache, cacheMinutes).then(function (entidades) {
        $scope.listEntidade = entidades;
        var cursos = [];
        var matriculas = [];
        $scope.listEntidade.forEach(function (elem, index) {
          var entidade = elem;
          matriculas.push({cursos: elem.cursos});
          entidade.cursos.forEach(function (elem, index) {
            cursos.push({nome:elem.st_curso, id:elem.id_curso});
            $scope.carregaDisciplinas(entidade.id_entidade, elem, renewCache);
          });
          localStorage.setItem('cursos', JSON.stringify(cursos));
        });
        localStorage.setItem('matriculas', JSON.stringify(matriculas)); 
      }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.completeRequest = true;
      });

    }

    $scope.listCurso = listCurso;

    //Abre a barra de pesquisa do header
    // $rootScope.openSearch = function () {
    //   $ionicFilterBar.show({
    //     items: [$scope.listDisciplinas[$scope.listEntidade[0].cursos[0].id_curso]],
    //     update: function (filteredItems, filterText) {
    //       $scope.listDisciplinas[$scope.listEntidade[0].cursos[0].id_curso] = filteredItems;
    //       if (filterText) {
    //       }
    //     }
    //   });
    // };

    // Retorna as disciplinas de um curso.
    $scope.carregaDisciplinas = function (id_entidade, curso, renewCache) {
      var paramsDisc = {};
      paramsDisc.idEntidade = id_entidade;
      paramsDisc.idProjeto = curso.id_curso;
      paramsDisc.idMatricula = curso.id_matricula;
      var cursoid = curso.id_curso;

      // Nesse momento a gente não busca com os dados do moodle por uma questão de desempenho
      DisciplinaFactory.getDisciplinas(paramsDisc, renewCache).then(function (data) {
        $scope.listDisciplinas[cursoid] = data;

        // for (var i = 0; i < $scope.listDisciplinas[cursoid].length; i++) {
        //   var disciplina = $scope.listDisciplinas[cursoid][i];

        /* Transforma as strings de data em objetos Date();
         Necessário para que os filtros de data do angular funcionem
         corretamente. */

        // disciplina.dt_abertura = new Date(disciplina.dt_abertura);
        // disciplina.dt_encerramento = new Date(disciplina.dt_encerramento);
        // disciplina.dt_encerramentosala = new Date(disciplina.dt_encerramentosala);

        // Seta variavel boolean no objeto, indicando se a sala está ou não completa.
        // disciplina.salaCompleta = disciplina.st_statuscomplete == 'Completo';
        // }
        $scope.listDisciplinas[cursoid].forEach(function (elem) {

          /* Transforma as strings de data em objetos Date();
           Necessário para que os filtros de data do angular funcionem
           corretamente. */
          elem.dt_abertura =  $filter("asDate")(elem.dt_abertura, null, false);
          elem.dt_encerramento = $filter("asDate")(elem.dt_encerramento, null, false);//new Date(elem.dt_encerramento);
          elem.dt_encerramentosala = $filter("asDate")(elem.dt_encerramentosala, null, false);//new Date(elem.dt_encerramentosala);

          // Seta variavel boolean no objeto, indicando se a sala está ou não completa.
          elem.salaCompleta = elem.st_statuscomplete == 'Completo';
          $scope.andamentoDisciplina(id_entidade, curso, elem, renewCache);
        });

        //Fecha o spinner de carregamento.
        $ionicLoading.hide();
      });
    };

    // Retorna dados do andamento da disciplina.
    $scope.andamentoDisciplina = function (id_entidade, curso, disciplina, renewCache) {
      var paramsDisc = {};
      paramsDisc.idEntidade = id_entidade;
      paramsDisc.idProjeto = curso.id_curso;
      paramsDisc.idMatricula = curso.id_matricula;
      // localStorage.setItem('id_matricula', curso.id_matricula);
      paramsDisc.idSaladeaula = disciplina.id_saladeaula;
      paramsDisc.blMoodle = 1;
      var cursoid = curso.id_curso;

      // Busca o index do elemento no array princial
      var indexDisciplina = $scope.listDisciplinas[cursoid].indexOf(disciplina);

      //Fazendo a busca para uma disciplina especifica no moodle, adicionando os
      //resultados como um array no $scope.listDisciplinas.
      DisciplinaFactory.getDisciplinas(paramsDisc, renewCache).then(function (data) {
        //se tiver retornado relevantes do moodle, a gente adiciona no scope
        if (data[0].ar_moodle && data[0].ar_moodle.ar_atividades) {
          // $scope.listDisciplinas[cursoid][indexDisciplina].ar_moodle = data[0].ar_moodle;
          disciplina.ar_moodle = data[0].ar_moodle;
          // $scope.listDisciplinas[cursoid][indexDisciplina].st_statuscomplete = data[0].st_statuscomplete;
          disciplina.st_statuscomplete = data[0].st_statuscomplete;
        }
        // $scope.listDisciplinas[cursoid][indexDisciplina].showChart = true;
        disciplina.showChart = true;


      });
    };


    // Função para abrir uma disciplina
    $scope.selecionaDisciplina = function (objEntidade, objCurso, objDisciplina) {
      //Transforma o objeto em string.
      var disciplina = JSON.stringify(objDisciplina);
      var entidade = JSON.stringify(objEntidade);
      var curso = JSON.stringify(objCurso);

      //Seta os objetos no Storage.
      window.localStorage.setItem('entidade_ativa', entidade);
      window.localStorage.setItem('curso_ativo', curso);
      window.localStorage.setItem('disciplina_ativa', disciplina);

      //Seta os parâmetros na rota.
      var params = {
        id_disciplina: objDisciplina.id_disciplina
      };

      //Acessa o caminho da rota.
      $state.go('app.disciplina.show.conteudo', params);
    };


    $scope.getItensSlide = function () {

      MensagemService.getMensagensSlide().then(function (response) {
        $scope.dataSlider = response;
      });

    };

    $scope.redirecionaItemSlide = function (slide) {
      console.log("item do slide -----> ", slide);
      switch (slide.type) {
        case "mensagem":
          $rootScope.acessarMensagem(slide.obj, null, null);
          break;
        default:
          console.log("funcão não definida");
          break
      }
    }

    $scope.addRequests = function(){$scope.listCurso(false)};
    
          $scope.cancelAll = function(){pendingRequests.cancelAll()};
  });
