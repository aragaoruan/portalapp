angular.module('portal.controllers').controller('IndexCtrl', function ($scope, AppService, AuthFactory,PromocoesFactory) {
  $scope.logout = function () {
    AuthFactory.logout();
  };
  $scope.$on("$ionicView.enter", function (event, data) {
    
    var user_name = AppService.getWords(window.localStorage.getItem('user_name'), 2);
    $scope.dataUser = {
      user_name: user_name.letters,
      user_avatar: user_name.first_letter,
      first_letter: user_name.first_letter
    };

    $scope.hasAvatar = false;

    if (window.localStorage.getItem('user_avatar')) {
      $scope.hasAvatar = true;
      $scope.dataUser.user_avatar = window.localStorage.getItem('user_avatar')
    }

  });
});

