angular.module('portal.controllers').controller('ConteudoBaixadosCtrl', function ($scope,
                                                                                  $rootScope,
                                                                                  $ionicFilterBar,
                                                                                  $filter,
                                                                                  CONFIG_APP,
                                                                                  $log,
                                                                                  DisciplinaService,
                                                                                  $ionicLoading,
                                                                                  AppService, $ionicPopup, $cordovaVibration,
                                                                                  $analytics) {

  AppService.cancelMultipleNotifications([1, 2]);
  $scope.conteudos = [];
  $scope.list = [];
  $scope.$on("$ionicView.enter", function(event, data) {
    // Não mostra o icone de pesquisa
    $rootScope.showFilter = false;
    $analytics.pageTrack('/conteudo_offline');



    $scope.conteudos = angular.fromJson(window.localStorage.getItem('conteudo_offline'));
    if ($scope.conteudos) {
      // $scope.list = DisciplinaService.ordenaArrayConteudoOffline($scope.conteudos);
      $scope.list = $scope.organizaArrayConteudo($scope.conteudos);
    }

    //Abre a barra de pesquisa do header
    $rootScope.openSearch = function() {
      $ionicFilterBar.show({
        items: $scope.list,
        update: function(filteredItems, filterText) {
          // $scope.list = filteredItems;
          $scope.list = _.groupBy(filteredItems, 'st_disciplina');
        }
      });
    };
  });

  /**
   * Função para organizar o array com os dados agrupados
   * @param conteudo
   * @returns {Array}
   */
  $scope.organizaArrayConteudo = function(arrConteudos) {

    var arrGroup = []; //cria um array vazio
    //percorre os dados que foram agrupados pelo id_disciplina
    angular.forEach(_.groupBy(arrConteudos, 'id_disciplina'), function(obj, index) {
      //verifia se encontrou algum com o id_discplina
      if (obj.length) {
        //como só precisamos do nome e id_disciplina para agrupar no array, pegamos a posição zero
        disciplina = obj[0];
        //seta os valores no array
        arrGroup.push({
          id_disciplina: disciplina.id_disciplina,
          st_disciplina: disciplina.st_disciplina,
          conteudos: obj
        });
      }
    });
    return arrGroup;
  }

  /**
   * Deleta o conteudo
   * @param conteudo
   * @param $index
   */
  $scope.deleteConteudo = function(conteudo, $index) {
    $ionicPopup.confirm({
      title: 'Atenção!',
      template: 'Deseja remover o conteúdo?'
    }).then(function(res) {
      if (res) {
        DisciplinaService.removeItemConteudoOffline(conteudo)
          .then(function(result) {
            $scope.conteudos = angular.fromJson(window.localStorage.getItem('conteudo_offline'));
            // $scope.list = DisciplinaService.ordenaArrayConteudoOffline($scope.conteudos);
            $scope.list = $scope.organizaArrayConteudo($scope.conteudos);
            //$scope.conteudos.splice($index, 1);
            $cordovaVibration.vibrate(100);
          }, function(error) {
            //alert(JSON.stringify(error));
          });
      } else {
        console.log('Cancelado');
      }
    });
  };


  $scope.abrirConteudo = function(conteudo) {

    $scope.linkFile = CONFIG_APP.url +
      '/disciplina_conteudo/' + conteudo.id_disciplina + '/' + conteudo.id_disciplinaconteudo + '.' + conteudo.st_formato;

    DisciplinaService.verifyExistFile($scope, conteudo, conteudo.st_formato, true).then(function() {
      $ionicLoading.hide();
    });
  };

});
