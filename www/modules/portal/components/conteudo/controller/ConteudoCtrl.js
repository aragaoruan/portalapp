angular.module('portal.controllers')
  .controller('ConteudoCtrl',
    function ($http,
      $scope,
      $log,
      DisciplinaFactory,
      DisciplinaService,
      CONFIG_APP,
      AppService,
      $state,
      $cordovaVibration,
      $rootScope,
      $ionicFilterBar,
      $ionicPopup,
      $controller,
      $q,
      MoodleFactory,
      AuthFactory) {

      //botao da aba que será ativado ao recarregar a rota
      $scope.activeButton = $state.params.activeButton;
      $scope.filesModules = [];

      //dados da disciplina
      $scope.ConteudoDisciplina = [];


      $scope.$on("$ionicView.enter", function (event, data) {
        $rootScope.showFilter = true;
        $scope.storageConteudoOffline = angular.fromJson(window.localStorage.getItem('conteudo_offline')) ? angular.fromJson(window.localStorage.getItem('conteudo_offline')) : [];
        $scope.storageDisciplinaAtiva = angular.fromJson(window.localStorage.getItem('disciplina_ativa'));
        $scope.storageCursoAtivo = angular.fromJson(window.localStorage.getItem('curso_ativo'));

        //Abre a barra de pesquisa do header
        $rootScope.openSearch = function () {
          $ionicFilterBar.show({
            items: $scope.ConteudoDisciplina,
            update: function (filteredItems, filterText) {
              $scope.ConteudoDisciplina = filteredItems;
            }
          });
        };

        if (!($scope.storageDisciplinaAtiva.id_disciplina && $scope.storageCursoAtivo.id_matricula && $scope.storageDisciplinaAtiva.id_saladeaula)) {
          $log.debug('Parametros imcompletos');
        }

        /**
         * [listConteudoDisciplina Lista o conteudo da disciplina]
         * @param  {[type]} {id_disciplina: $scope.storageDisciplinaAtiva.id_disciplina [Codigo da disciplina que está no storage]
         * @param  {[type]} bl_ativo: 1} [Somente conteudos ativos]
         * @return {[type]}           [description]
         */
        DisciplinaFactory.listConteudoDisciplina({
          id_disciplina: $scope.storageDisciplinaAtiva.id_disciplina,
          bl_ativo: 1
        }).then(function (response) {
          $scope.ConteudoDisciplina = response;
          $scope.ConteudoDisciplina.forEach($scope.markIfOffline);

          var st_parametros = {
            idMatriculaAtiva: localStorage.getItem("id_matricula"),
            disciplinaAtiva: localStorage.getItem("disciplina_ativa")
          };

          // //chamando o metodo para salvar log
          AuthFactory.gravarLogAcesso($state.current.name, st_parametros);

        });
        getCourseContents();

      });

      /**
       * [downloadOpenConteudo Funcao para abrir o conteudo]
       * @param  {[type]}  conteudo [oibejto com dados do conteudo]
       * @param  {[type]}  format   [formato do arquivo]
       * @param  {Boolean} isOpen   [Define se o arquivo vai ser aberto]
       * @param  {[type]}  index    [index no array de conteudos]
       * @return {[type]}           [description]
       */
      $scope.downloadOpenConteudo = function (conteudo, format, isOpen, index) {
        //Se o arquivo não  existe offline envia para nova rota para que seja aberto online
        if (conteudo.offline === false && format == 'zip') {
          var conteudo_ativo = JSON.stringify(conteudo);
          window.localStorage.setItem('conteudo_ativo', conteudo_ativo);
          $state.go('conteudo_disciplina_show', {
            id_disciplina: $scope.storageDisciplinaAtiva.id_disciplina,
            id_disciplinaconteudo: conteudo.id_disciplinaconteudo,
            isLocal: false
          });
        }
 else {
          // Se o arquivo existe offline define o caminho
          $scope.linkFile = CONFIG_APP.url + '/disciplina_conteudo/' + $scope.storageDisciplinaAtiva.id_disciplina + '/' + conteudo.id_disciplinaconteudo + '.' + format;

          // Envia para ser aberto o arquivo offline
          DisciplinaService.verifyExistFile($scope, conteudo, format, isOpen);

          // Define que o arquivo está disponivel offline
          $scope.ConteudoDisciplina[index].offline = true;
        }
      };

      $scope.downloadAll = function () {
        $scope.ConteudoDisciplina.forEach(function (elem, index) {
          $scope.checkDownloadDeleteConteudo(elem, false, index);
        });
      };

      /**
       * [checkDownloadDeleteConteudo Função que baixa ou apaga o arquivo]
       * @param  {[type]} conteudo [description]
       * @param  {[type]} format   [description]
       * @param  {[type]} delete   [description]
       * @return {[type]}          [description]
       */
      $scope.checkDownloadDeleteConteudo = function (conteudo, remove, index) {
        var format = conteudo.id_formatoarquivo == 1 ? 'pdf' : 'zip';
        switch (conteudo.offline) {
        case false:
          $scope.downloadConteudo(conteudo, format, index);
          break;
        case true:
          if (remove) {
            $cordovaVibration.vibrate(100); // vibra o celular
            conteudo.st_formato = format; // definie fomrato do arquivo
            DisciplinaService.removeItemConteudoOffline(conteudo); // chama função que apaga o conteudo
          }
          break;
        default:
        }

      };

      // Funcao que faz o dowload de um conteudo
      $scope.downloadConteudo = function (conteudo, format, index) {
        $scope.linkFile = CONFIG_APP.url + '/disciplina_conteudo/' + $scope.storageDisciplinaAtiva.id_disciplina + '/' + conteudo.id_disciplinaconteudo + '.' + format;

        // Chama a função para baixar o arquivo
        $scope.ConteudoDisciplina[index].download = true;
        DisciplinaService.verifyExistFile($scope, conteudo, format, false).then(function () {
          $scope.ConteudoDisciplina[index].offline = true;
          delete $scope.ConteudoDisciplina[index].download;
        });


      };

      /**
       * Marca se um conteudo existe offline no array de conteudos
       * @param  {[type]} conteudo Obejto do conteudo
       * @param  {[type]} index    index no array
       * @return {[type]}          [description]
       */
      $scope.markIfOffline = function (conteudo, index) {
        var isOffline = $scope.checkConteudoOffline(conteudo);
        $scope.ConteudoDisciplina[index].offline = isOffline;
        return isOffline;
      };

      /**
       * checkConteudoOffline verifica se um arquivo existe offline
       * @param  {[type]} conteudo objeto do conteudo
       * @return {[oolean}          true or false
       */
      $scope.checkConteudoOffline = function (conteudo) {
        return $scope.storageConteudoOffline['c' + conteudo.id_disciplinaconteudo] ? true : false;
      };

      /**
       * Busca o conteúdo daquela sala no Moodle
       */
      function getCourseContents() {
        MoodleFactory.getCourseContents($scope.storageCursoAtivo.id_matricula, $scope.storageDisciplinaAtiva.st_codsistemacurso, $scope.storageDisciplinaAtiva.id_saladeaula).then(function (content) {
          $scope.content = content;
          console.log(content);
          $scope.contentLoaded = true;
        });
      }

      /**
       * Alerta para modulos do moodle ainda não implementados
       */
      $scope.showAlertMod = function () {
        var alertPopup = $ionicPopup.alert({
          title: 'Recurso não disponivel',
          template: 'Esse recurso ainda não está disponível no nosso aplicativo mobile. <br> Por favor acesse através de um navegador.'
        });
      };

      /**
       * Ações a serem tomadas ao clicar no modulo do moodle de acordo com o tipo
       * @param {object} - Objeto modulo do moodle
       *
       */


      $scope.goToMod = function (object) {
        switch (object.modname) {
        case 'forum':
          $state.go('app.forum', {
            instanceId: object.instance
          });
          break;

        case 'assign':
          $state.go('app.assign', {
            instanceId: object.instance
          });
          break;

        default:
          $scope.showAlertMod();

        }
      };

      /**
       * Retorna o icone de acordo com o tipo do modulo do Moodle
       * @param {object} - Objeto modulo do moodle
       * @returns {string} - Class css a ser adicionada
       */
      $scope.getModIcon = function (object) {
        switch (object.modname) {
        case 'forum':
          return 'icon-icon_forum fonte_escura';
        case 'page':
          return 'icon-icon_aula';
        case 'resource':
          return 'icon-icon_livro';
        case 'folder':
          return 'icon-icon_livro';
        case 'quiz':
          return 'icon-icon_disc';
        case 'assign':
          return 'icon-icon_disc fonte_escura';
        case 'label':
          return false;
        default:
          return 'icon-icon_aula';

        }
      };
    });
