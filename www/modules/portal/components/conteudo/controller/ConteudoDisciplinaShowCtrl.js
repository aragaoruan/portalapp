angular.module('portal.controllers').controller('ConteudoDisciplinaShowCtrl', function ($scope,
                                                                                        CONFIG_APP,
                                                                                        $http,
                                                                                        $state,
                                                                                        $ionicHistory,
                                                                                        $sce,
                                                                                        $log) {

  //metodo para gerar url baseado nos parametros, e url da config do app
  $scope.gerarUrlConteudoDisciplina = function () {

    var date = new Date();

    $scope.storageConteudoAtivo = angular.fromJson(window.localStorage.getItem('conteudo_ativo'));

    var url = CONFIG_APP.url
      + '/disciplina_conteudo'
      + '/' + $state.params.id_disciplina
      + '/' + $state.params.id_disciplinaconteudo + '/home.html?date=' + date.getTime();

    if ($state.params.isLocal == "true") {
      var localPath = '';
      if (ionic.Platform.isAndroid())
        localPath = cordova.file.externalApplicationStorageDirectory + 'portaldoaluno/';
      else
        localPath = cordova.file.dataDirectory + 'portaldoaluno/';

      url = localPath + '/' + $state.params.id_disciplina + '/' + $state.params.id_disciplinaconteudo + '/home.html';
    }

    //passando pelo angular para gerar uma url confiavel
    $scope.urlConteudoDisciplina = $sce.trustAsResourceUrl(url);
  };
  $scope.myGoBack = function () {

    $ionicHistory.goBack();


  };

  $scope.gerarUrlConteudoDisciplina();
});
