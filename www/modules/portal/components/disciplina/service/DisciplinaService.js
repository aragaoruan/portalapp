angular.module('portal.services').service('DisciplinaService', function ($state, CONFIG_APP, $http, DisciplinaFactory, $cordovaLocalNotifications, $ionicLoading, $cordovaFile, $cordovaZip, $q, AppService, $ionicPopup, $cordovaFileOpener, $cordovaTransfer) {
  var self = this;
  var localPath = CONFIG_APP.url;
  var transfer;

  //TODO ADICIONAR ISSO NUMA FACTORY
  document.addEventListener('deviceready', function() {
    localPath = cordova.file.dataDirectory + 'portaldoaluno/';
    transfer = new $cordovaTransfer();

    if (ionic.Platform.isAndroid()) {
      localPath = cordova.file.externalApplicationStorageDirectory + 'portaldoaluno/';
    }
  });
// Busca as disciplinas
  this.getDisciplinas = function(params) {
    return DisciplinaFactory.getDisciplinas(params);
  };

  this.randomImagesDisciplina = function() {
    return Math.floor(Math.random() * 10) + 1;
  };

  this.downloadConteudo = function($scope, conteudo, format, isOpen) {
    var defer = $q.defer();
    var url = $scope.linkFile;
    var targetPath = localPath + conteudo.id_disciplina + '/' + conteudo.id_disciplinaconteudo + '.' + format;
    var trustHosts = true;

    transfer.download(url, targetPath, null, trustHosts)
      .then(function(object) {

        //manter dados de conteudo baixado offline
        self.mergeStorageConteudoOffline($scope, conteudo, format);

        AppService.registerNotificationSchedule(
          1,
          'Download concluído',
          'Conteúdo baixado com sucesso!',
          'android/ic_action_download.png',
          'android/ic_action_download.png', {
            router: "app.conteudos_baixados"
          }
        );


        switch (format) {
          case 'pdf':
            if (isOpen) {
              $cordovaFileOpener.open(object.nativeURL, 'application/pdf');
            }
            break;
          case 'zip':
            self.descompressConteudo($scope, targetPath, conteudo, isOpen);
        }

        AppService.cancelMultipleNotifications([1, 2]);

        defer.resolve();

      }, function(err) {

        var message = '';

        switch (err.code) {
          case 1:
            message += 'Conteúdo não encontrado, favor entrar em contato com nossa equipe.';
            break;
          case 3:
            message += 'Sua conexão está lenta, mas fique despreocupado, iremos avisar assim que concluirmos o download';
            break;
          default:
            message = JSON.stringify(err);
        }

        AppService.registerNotificationSchedule(
          1,
          'Ops!',
          message
        );
        defer.reject();
      }, function(progress) {
        //callback progress
      });
    return defer.promise;
  };

  /**
   * [verifyExistFile Verifica se o arquivo existe e caso necessário faz o download delete]
   * @param  {[type]}  $scope   [Scope da controller]
   * @param  {[type]}  conteudo [objeto com dados sobre o conteúdo]
   * @param  {[type]}  format   [formato do arquivo]
   * @param  {Boolean} isOpen   [se o arquivo deve ser aberto]
   * @return {[type]}           [promise]
   */
  this.verifyExistFile = function($scope, conteudo, format, isOpen) {

    var defer = $q.defer();

    var fullFilePath = localPath + conteudo.id_disciplina + '/';

    switch (format) {
      case 'pdf':
      fullFilePath += conteudo.id_disciplinaconteudo + '.' + format;
      break;
      case 'zip':
      fullFilePath += conteudo.id_disciplinaconteudo + '/home.html';
    }


      window.resolveLocalFileSystemURL(fullFilePath, function(callback) {
        if (isOpen) {
          switch (format) {
            case 'pdf':
              $cordovaFileOpener.open(callback.nativeURL, 'application/pdf');
              defer.resolve();
              break;
            case 'zip':
              self.openConteudoHtml(conteudo);
              defer.resolve();
          }
        }
      }, function(response){
        var aqui = response;
        self.downloadConteudo($scope, conteudo, format, isOpen).then(function(){
          defer.resolve()
        }
        );
      });


    return defer.promise;
  };
  /**
   * [function description]
   * @param  {[type]}  $scope     [description]
   * @param  {[type]}  targetPath [description]
   * @param  {[type]}  conteudo   [description]
   * @param  {Boolean} isOpen     [description]
   * @return {[type]}             [description]
   */
  this.descompressConteudo = function($scope, targetPath, conteudo, isOpen) {

    var destination = localPath + $scope.storageDisciplinaAtiva.id_disciplina + '/' + conteudo.id_disciplinaconteudo;

    $cordovaZip.unzip(targetPath, destination, function(result) {

      var path = localPath + conteudo.id_disciplina;

      $cordovaFile.removeFile(path, conteudo.id_disciplinaconteudo + '.zip')
        .then(function() {

        }, function(error) {
          //alert(JSON.stringify(error));
        });

      if (result != -1) {
        if (isOpen) {
          self.openConteudoHtml(conteudo);
        }
      } else {
        $ionicPopup.alert({
          title: 'Erro :(',
          template: 'Erro ao tentar descomprimir o conteudo'
        });
      }
    });
  };

  /**
   * Funcao que abre o conteudo HTML ja baixado.
   * @param  {[type]} conteudo objeto do conteudo
   * @return {[type]}          [description]
   */
  this.openConteudoHtml = function(conteudo) {
    var conteudo_ativo = JSON.stringify(conteudo);
    window.localStorage.setItem('conteudo_ativo', conteudo_ativo);
    $state.go('conteudo_disciplina_show', {
      id_disciplina: conteudo.id_disciplina,
      id_disciplinaconteudo: conteudo.id_disciplinaconteudo,
      isLocal: true
    }).then(function() {
      $ionicLoading.hide();
    });
  };

  this.mergeStorageConteudoOffline = function($scope, conteudo, format) {

    var storage = {};

    if (window.localStorage.getItem('conteudo_offline')) {
      storage = angular.fromJson(window.localStorage.getItem('conteudo_offline'));
    }

    storage['c' + conteudo.id_disciplinaconteudo] = {
      id_disciplinaconteudo: conteudo.id_disciplinaconteudo,
      st_descricaoconteudo: conteudo.st_descricaoconteudo,
      st_disciplina: $scope.VwAlunoGradeIntegracao.st_disciplina,
      id_disciplina: conteudo.id_disciplina,
      st_formato: format
    };

    window.localStorage.setItem('conteudo_offline', JSON.stringify(storage));
  };

  this.removeItemConteudoOffline = function(conteudo) {
    var defer = $q.defer(),
      path = null;

    switch (conteudo.st_formato) {
      case 'zip':

        $cordovaFile.removeRecursively(localPath, conteudo.id_disciplina + '/' + conteudo.id_disciplinaconteudo)
          .then(function(success) {

            self.removeItemStorageConteudoOffline(conteudo);
            defer.resolve();

          }, function(error) {
            var message = '';

            switch (error.code) {
              case 1:
                self.removeItemStorageConteudoOffline(conteudo);
                message = 'O arquivo já foi removido';
                defer.resolve();
            }
            $ionicPopup.alert({
              title: 'Ops!',
              template: message ? message : 'Tivemos um problema para excluir os arquivos :('
            });
          });
        break;

      case 'pdf':
        $cordovaFile.removeFile(localPath, conteudo.id_disciplina + '/' + conteudo.id_disciplinaconteudo + '.pdf')
          .then(function() {

            self.removeItemStorageConteudoOffline(conteudo);
            defer.resolve();

          }, function(error) {
            var message = '';

            switch (error.code) {
              case 1:
                self.removeItemStorageConteudoOffline(conteudo);
                message = 'O arquivo já foi removido';
            }
            $ionicPopup.alert({
              title: 'Ops!',
              template: message ? message : 'Tivemos um problema para excluir os arquivos :('
            });

            defer.reject();
          });

        break;
    }

    return defer.promise;
  };

  this.removeItemStorageConteudoOffline = function(conteudo) {
    var storage = angular.fromJson(window.localStorage.getItem('conteudo_offline')) ? angular.fromJson(window.localStorage.getItem('conteudo_offline')) : [];

    var prop = 'c' + conteudo.id_disciplinaconteudo;

    delete storage[prop];

    window.localStorage.setItem('conteudo_offline', JSON.stringify(storage));
  };

  this.ordenaArrayConteudoOffline = function(conteudos) {
    var currentKey = 0;
    var count = 0;
    var list = [];

    angular.forEach(conteudos, function(val, key) {

      if (count === 0) {
        list.push({
          id_disciplina: val.id_disciplina.id_disciplina,
          st_disciplina: val.st_disciplina
        });
      } else {
        if (currentKey != val.id_disciplina.id_disciplina) {
          list.push({
            id_disciplina: val.id_disciplina.id_disciplina,
            st_disciplina: val.st_disciplina
          });
        }
      }

      currentKey = val.id_disciplina.id_disciplina;

      count++;
    });

    return list;
  };

});
