angular.module('portal.controllers').controller('DisciplinaCtrl', function ($rootScope, $scope, $state, DisciplinaService, AppService, $analytics, pendingRequests) {

  $analytics.pageTrack('/disciplina');

  $scope.$on("$ionicView.beforeLeave", function (event, data) {
    pendingRequests.cancelAll();
  });

  /**
   * Pega no storage o curso que foi selecionado na tela de cursos
   * @returns {{st_curso: string}}
   */
  function getCursoAtivo() {
    var curso = JSON.parse(window.localStorage.getItem('curso_ativo'));
    return curso;
  }

  /**
   * Pega no storage a entidade que foi selecionada na tela de cursos
   * @returns {{st_entidade: string, st_urlsite: string, st_logo: string}}
   */
  function getEntidadeAtiva() {
    var entidade = JSON.parse(window.localStorage.getItem('entidade_ativa'));
    return entidade;
  }

  $scope.cursoAtivo = getCursoAtivo();
  $scope.entidadeCurso = getEntidadeAtiva();

  defineAlturaDiv();

  var disciplinas = DisciplinaService.getDisciplinas($state.params);
  disciplinas.then(function (data) {
    angular.forEach(data, function (obj) {
      obj.color_status = AppService.getColorStatus(obj.id_status);
    });
    $scope.listDisciplinas = data;
  });




});
