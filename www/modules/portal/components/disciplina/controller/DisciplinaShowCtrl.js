angular.module('portal.controllers')
  .controller('DisciplinaShowCtrl',
    function ($http, $scope, $log, DisciplinaFactory,
      DisciplinaService, CONFIG_APP, AppService, $state, $cordovaVibration, $ionicPopup, $analytics, MoodleFactory, $rootScope) {


      //url do moodle
      $scope.urlMoodle = "";

      //atividades listadas do moodle
      $scope.atividades = [];
      // Dados do grafico
      $scope.dadosAndamento = {};
      $scope.$on("$ionicView.enter", function (event, data) {

        $analytics.pageTrack('/disciplina');
        $scope.storageConteudoOffline = angular.fromJson(window.localStorage.getItem('conteudo_offline')) ? angular.fromJson(window.localStorage.getItem('conteudo_offline')) : [];
        $scope.storageDisciplinaAtiva = angular.fromJson(window.localStorage.getItem('disciplina_ativa'));
        $scope.storageCursoAtivo = angular.fromJson(window.localStorage.getItem('curso_ativo'));

        if (!($scope.storageDisciplinaAtiva.id_disciplina && $scope.storageCursoAtivo.id_matricula && $scope.storageDisciplinaAtiva.id_saladeaula)) {
          $log.debug('Parametros imcompletos');
        }

        // Retorna os dados gerais da disciplina.
        detailsDisciplinaSala(false);
      });



      var detailsDisciplinaSala = function (renewCache) {
        // Retorna os dados gerais da disciplina.
        return DisciplinaFactory.detailsDisciplinaSala({
          id_disciplina: $scope.storageDisciplinaAtiva.id_disciplina,
          id_matricula: $scope.storageCursoAtivo.id_matricula,
          id_saladeaula: $scope.storageDisciplinaAtiva.id_saladeaula
        }, renewCache, 15).then(function (response) {
          $scope.VwAlunoGradeIntegracao = response;
          DisciplinaFactory.setDisciplinaAtiva(response);
          $scope.andamentoDisciplina(response, renewCache);
        }).finally(function () {
          $scope.$broadcast('scroll.refreshComplete');
          $scope.completeRequest = true;
        });
      }

      $scope.detailsDisciplinaSala = detailsDisciplinaSala;


      // Função que retorna os dados de andamento da disciplina.
      $scope.andamentoDisciplina = function (dados, renewCache) {

        var paramsDisc = {};
        paramsDisc.idEntidade = dados.id_entidade;
        paramsDisc.idProjeto = dados.id_projetopedagogico;
        paramsDisc.idMatricula = dados.id_matricula;
        paramsDisc.idSaladeaula = dados.id_saladeaula;
        paramsDisc.blMoodle = 1;
        //Fazendo a busca para uma disciplina especifica e com os dados do moodle
        DisciplinaFactory.getDisciplinas(paramsDisc, renewCache).then(function (data) {

          //se tiver retornado relevantes do moodle, a gente adiciona no scope
          if (typeof data[0].ar_moodle != "undefined") {
            $scope.dadosAndamento = data[0];
            var dadosAndamento = JSON.stringify(data[0]);

            //Seta os objetos no Storage.
            window.localStorage.setItem('disciplina_andamento', dadosAndamento);
          }
        });
      };


    });
