angular.module('portal.controllers')
  .controller('ForumCtrl',
    function ($http, $scope, $log, CONFIG_APP, $stateParams, $q, $ionicModal, ForumMoodleFactory, MoodleFactory, $ionicHistory, $analytics, $rootScope) {

      $analytics.pageTrack('/forum');
      $rootScope.showFilter = false;

      $scope.forumData = [];
      var instanceId = $stateParams.instanceId;
      $scope.storageDisciplinaAtiva = angular.fromJson(window.localStorage.getItem('disciplina_ativa'));
      $scope.storageCursoAtivo = angular.fromJson(window.localStorage.getItem('curso_ativo'));


      /**
       * Retorna as discussões de um forum do moodle
       */
      function getForumDiscussionsPaginated() {
        ForumMoodleFactory.getForumDiscussionsPaginated($scope.storageCursoAtivo.id_matricula, $scope.storageDisciplinaAtiva.id_saladeaula, $scope.storageDisciplinaAtiva.st_codsistemacurso, instanceId).then(function (foruns) {
          $scope.forumData = foruns;
        });
      }

      /**
       * Verifica se o Forum permite criação de discussão
       * Só funciona a partir do 3.1
       */

      function canAddDiscussion(id) {
        ForumMoodleFactory.canAddDiscussion($scope.storageCursoAtivo.id_matricula, $scope.storageDisciplinaAtiva.id_saladeaula, id).then(function (response) {
          $scope.canAddDiscussion = response.status;

        });
      }

      /**
       * Dispara view do forum
       */
      function viewForum(id) {
        ForumMoodleFactory.triggerViewForum($scope.storageCursoAtivo.id_matricula, $scope.storageDisciplinaAtiva.id_saladeaula, id).then(function () {});
      }

      function getForuns() {
        var idCourses = [
          $scope.storageDisciplinaAtiva.st_codsistemacurso
        ];
        ForumMoodleFactory.getForumsByCourses($scope.storageCursoAtivo.id_matricula, $scope.storageDisciplinaAtiva.id_saladeaula, idCourses, true).then(function (foruns) {

          angular.forEach(foruns, function (forum) {
            //verifia se encontrou algum com o id_discplina
            if (forum.id == instanceId) {
              //como só precisamos do nome e id_disciplina para agrupar no array, pegamos a posição zero
              forum.dtCuttOffDate = moment.unix(forum.cutoffdate);
              forum.dtDueDate = moment.unix(forum.duedate);
              forum.dtTimeModified = moment.unix(forum.timemodified);
              $scope.forum = forum;
              if (moment() > forum.dtCuttOffDate) {
                $scope.dateLimit = true;
              }
              viewForum(forum.id);
              if (forum.cancreatediscussions) {
                $scope.canAddDiscussion = forum.cancreatediscussions;
              }
            };
          });

        });
      }

      $scope.newDiscussion = function (newDiscussion) {
        var params = {
          forumid: instanceId,
          subject: newDiscussion.subject,
          message: newDiscussion.message
        };
        ForumMoodleFactory.forumAddDiscussion($scope.storageCursoAtivo.id_matricula, $scope.storageDisciplinaAtiva.id_saladeaula, params).then(function (response) {
          getForumDiscussionsPaginated(); //recarregando dados
          $scope.closeModal(); // Fechando a modal
        });
      }

      //Instanciando modal de resposta
      $ionicModal.fromTemplateUrl('createDiscussion', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.modal = modal;
      });

      /**
       * Abre a modal de resposta
       * @param {object} post - post que está sendo respondido
       */
      $scope.addDiscussion = function () {
        $scope.modal.show(); // Abre modal
      };
      $scope.closeModal = function () {
        $scope.modal.hide();
      };

      // Cleanup the modal when we're done with it!
      $scope.$on('$destroy', function () {
        $scope.modal.remove();
      });

      // Execute action on hide modal
      $scope.$on('modal.hidden', function () {
        // Execute action
      });

      // Execute action on remove modal
      $scope.$on('modal.removed', function () {
        // Execute action
      });

      //call function
      getForumDiscussionsPaginated();
      getForuns();


    });
