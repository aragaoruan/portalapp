angular.module('portal.controllers')
  .controller('ForumDiscussionCtrl',
    function ($http, $scope, $log, CONFIG_APP, $stateParams, $q, ForumMoodleFactory, $ionicHistory, $analytics, $ionicModal, $ionicScrollDelegate, $location, $rootScope, MoodleFactory, $ionicPlatform) {

      $analytics.pageTrack('/forum');
      $rootScope.showFilter = false;

      $scope.forumData = [];
      var discussionId = $stateParams.discussionId;
      $scope.storageDisciplinaAtiva = angular.fromJson(window.localStorage.getItem('disciplina_ativa'));
      $scope.storageCursoAtivo = angular.fromJson(window.localStorage.getItem('curso_ativo'));

      /**
     * Função para fazer o fetch dos posts de uma discussão
     */
      function getForumDiscussionPosts(noCache) {
        ForumMoodleFactory.getForumDiscussionPosts($scope.storageCursoAtivo.id_matricula, $scope.storageDisciplinaAtiva.id_saladeaula, $scope.storageDisciplinaAtiva.st_codsistemacurso, discussionId, noCache).then(function (response) {
          window.localStorage.setItem('forum_ativo', angular.toJson(response.posts));
          getNestedChildren(response.posts, 0,true).then(function (output){
            console.log(output);
            $scope.posts = output;
            triggerViewForumDiscussion();
          });

        });
      }

      /**
     * Dispara a visualização da discussão para o moodle
     */
      function triggerViewForumDiscussion() {
        ForumMoodleFactory.triggerViewForumDiscussion($scope.storageCursoAtivo.id_matricula, $scope.storageDisciplinaAtiva.id_saladeaula, discussionId).then(function () {

        });
      }

      /**
       * Cria uma resposta para um psot da discussão
       * @param {object} newResponse - Resposta a ser criada
       */
      $scope.postResponse = function(newResponse) {
        var params = {
          postid: $scope.post.id,
          subject: newResponse.subject,
          message: newResponse.message };
        ForumMoodleFactory.forumAddDiscussionPost($scope.storageCursoAtivo.id_matricula, $scope.storageDisciplinaAtiva.id_saladeaula, params).then(function (response) {
          getForumDiscussionPosts(true); //recarregando dados
          $scope.closeModal(); // Fechando a modal
          $scope.newPostId = response.postid; //Seta o novo id para destacar na lista o post criado
          // Fazendo o scroll para p novo post
          $location.hash(response.postid);   //set the location hash
          var handle = $ionicScrollDelegate.$getByHandle('myPageDelegate');
          handle.anchorScroll(true);  // 'true' for animation
        });
      };

      var readDiscussion = function(post){
        var savedDisc = angular.fromJson(window.localStorage.getItem('forum_ativo'));
        var filterDisc = savedDisc.filter(function(disc) {
          return disc.id === post.id;

        })
        post.message = filterDisc[0].message;
      }

      /**
       * Organiza os posts de forma hierarquica
       * @param {array} arr - Array de posts
       * @param {string} parent - Pai dos posts a partir do qual será organizado
       * @param {boolean} promise - Define se será criada uma promise
       */

      function getNestedChildren(arr, parent,promise) {
        if (promise){
          var def = $q.defer();
        }
        var out = [];
        for (var i in arr) {
          arr[i].readDiscussion = readDiscussion;
          arr[i].reply = replyPost;
          delete arr[i].canreply;
          delete arr[i].userid;
          delete arr[i].created;
          delete arr[i].modified;
          delete arr[i].mailed;
          delete arr[i].messageformat;
          delete arr[i].messagetrust;
          delete arr[i].attachment;
          delete arr[i].attachments;
          delete arr[i].totalscore;
          delete arr[i].mailnow;
          delete arr[i].postread;
          if (arr[i].parent){
            delete arr[i].message;
          }
          if (arr[i].parent == parent) {
            var childrens = getNestedChildren(arr, arr[i].id);

            if (childrens.length) {
              arr[i].childrens = childrens;
            }
            out.push(arr[i]);
          }
        }
        if (promise){
          def.resolve(out);
          return def.promise;
        }
 else {
          return out;
        }
      }

      //call function
      getForumDiscussionPosts();

      //Instanciando modal de resposta
      $ionicModal.fromTemplateUrl('discussionReply', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.modal = modal;
      });

      /**
       * Abre a modal de resposta
       * @param {object} post - post que está sendo respondido
       */
      var replyPost = function () {
        $scope.post = this; //Seta no scope para usar os dados
        $scope.newResponse = {
          subject: 'Re: ' + this.subject // Seta um titulo padrão da resposta
        };
        $scope.modal.show(); // Abre modal
      };
      $scope.closeModal = function () {
        $scope.modal.hide();
      };

      // Cleanup the modal when we're done with it!
      $scope.$on('$destroy', function () {
        $scope.modal.remove();
      });

      // Execute action on hide modal
      $scope.$on('modal.hidden', function () {
      // Execute action
      });

      // Execute action on remove modal
      $scope.$on('modal.removed', function () {
      // Execute action
      });

    });
