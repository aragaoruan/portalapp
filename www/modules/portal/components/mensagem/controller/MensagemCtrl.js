angular.module('portal.controllers').controller('MensagemCtrl', function ($scope, $ionicFilterBar, $http, $log, CONFIG_APP, $state, AppService, $ionicLoading,
  MensagemService, $analytics, MensagemAgendamentoFactory, $rootScope, $ionicHistory, CacheFactory, LSFactory, $timeout, AuthFactory) {

  $scope.mensagens = [];
  $scope.mensagensDesagrupadas = [];

  $scope.$on("$ionicView.enter", function (event, data) {

    $rootScope.showFilter = true;
    $analytics.pageTrack('/mensagem_list');

    //Abre a barra de pesquisa do header

    $rootScope.openSearch = function () {
      $ionicFilterBar.show({
        items: $scope.mensagensDesagrupadas,
        update: function (filteredItems, filterText) {
          $scope.mensagens = _.groupBy(filteredItems, 'group');
          if (filterText) {
          }
        }
      });
    };
    //set storage notificacoes lidas
    window.localStorage.setItem('notificado_mensagens', 0);

    $scope.finallyRequest = false;

    //encerrar notificacoes geradas de mensagens
    AppService.cancelMultipleNotifications([2, 3]);

    $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});
    buscarMensagens(false);

  });

  var buscarMensagens = function (renewCache) {
    //param, cacheId, renewCache, cacheTime
    MensagemAgendamentoFactory.GetAll({}, false, renewCache).then(function (response) {

      if (response.length) {
        $scope.mensagensDesagrupadas = response;
        $scope.mensagens = _.groupBy($scope.mensagensDesagrupadas, 'group');
      }

      $scope.$broadcast('scroll.refreshComplete');
    }, function (error) {
    }).finally(function () {

      $timeout(function () {
        $ionicLoading.hide();
      }, 100);

      $scope.finallyRequest = true;
    });

  };


  $scope.doRefresh = buscarMensagens;


  // $scope.$watch('mensagensDesagrupadas', function (oldValue, newValue) {
  //   console.log(oldValue, newValue);
  // });

  $scope.acessarMensagem = function (mensagem) {

    var index = $scope.mensagensDesagrupadas.indexOf(mensagem);

    if (mensagem.id_evolucao == 42) {
      mensagem.id_evolucao = 43;
      $scope.mensagensDesagrupadas[index] = mensagem;

      /**
       * Aqui temos um exemplo de como atualizar os dados no cache, é importante passar para a Factory o tempo definido coo último parâmetro
       */
      LSFactory.set(MensagemAgendamentoFactory.getCacheId(), $scope.mensagensDesagrupadas, MensagemAgendamentoFactory.cacheMinutes, {});

      MensagemAgendamentoFactory.UpdateEvolucao({
        id_enviomensagem: mensagem.id_enviomensagem,
        id_evolucao: mensagem.id_evolucao,
        id_matricula: mensagem.id_matricula
      });

      var st_parametros = {
        idMatriculaAtiva: localStorage.getItem("id_matricula")
      };

      // //chamando o metodo para salvar log
      AuthFactory.gravarLogAcesso($state.current.name, st_parametros);

      $state.go('app.mensagem.show', mensagem);
    } else {
      $state.go('app.mensagem.show', mensagem);
    }
  };
});
