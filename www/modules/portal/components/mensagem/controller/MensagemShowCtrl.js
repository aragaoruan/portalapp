angular.module('portal.controllers').controller('MensagemShowCtrl', function ($scope, $rootScope, $state, $sce, $analytics) {
  $analytics.pageTrack('/mensagem_show');
  $scope.mensagem = $state.params;
  $rootScope.showFilter = false;
});
