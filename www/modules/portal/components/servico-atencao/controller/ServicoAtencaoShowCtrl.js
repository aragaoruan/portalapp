angular.module('portal.controllers')
  .controller('ServicoAtencaoShowCtrl', function ($rootScope,
                                                  $scope,
                                                  $state,
                                                  $ionicLoading,
                                                  ServicoAtencaoService,
                                                  OcorrenciaFactory,
                                                  $cordovaVibration,
                                                  $ionicPopup,
                                                  AppService,
                                                  $analytics,
                                                  $filter,
                                                  $ionicModal,
                                                  $ionicFilterBar,
                                                  AuthFactory) {

    $analytics.pageTrack('/central-atencao');

    // $scope.ocorrencias = [];
    // $scope.ocorrenciasDesagrupadas = [];
    $scope.assuntosOcorrencia = [];
    $scope.subAssuntosOcorrencia = [];
    $scope.ocorrencia = {};
    $scope.interacao = {};
    $scope.cursos = [];
    $scope.salas = [];
    $scope.interacoes = [];

    $scope.maxCaracterTramite = 1000;
    $scope.qtdCaraterTramite = 0;

    //Abre a barra de pesquisa do header
    $rootScope.openSearch = function () {
      $ionicFilterBar.show({
        items: $scope.interacoes,
        update: function (filteredItems, filterText) {
          $scope.interacoes = filteredItems;
          if (filterText) {
          }
        }
      });
    };

    /* Workaround para validação da quantidade máxima de caracteres no <textarea>
     (maxlength do HTML5 funciona de forma inconsistente variando de browser para browser).
     Atualiza também o contador de caracteres na tela.*/

    $scope.validaQtdMaxCaracteresAtualizaContador = function () {
      if (typeof $scope.interacao.st_descricao != 'undefined') {
        if ($scope.interacao.st_descricao.length > $scope.maxCaracterTramite) {
          $scope.interacao.st_descricao = $scope.interacao.st_descricao.substr(0, $scope.maxCaracterTramite);
        }
        $scope.qtdCaraterTramite = $scope.interacao.st_descricao.length;
      } else {
        $scope.qtdCaraterTramite = 0;
      }
    };

    /**
     * Carrega as interações vinculadas a ocorrencia
     * @param id_ocorrencia
     */
    $scope.carregaInteracoesOcorrencia = function (id_ocorrencia) {

      if (id_ocorrencia) {
        $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});
        ServicoAtencaoService.getInteracoesByOcorrencia(id_ocorrencia)
          .success(function (data) {
            //percorre os dados da interacao e resume o nome do usuario
            angular.forEach(data, function (interacao, key) {
              interacao.st_nomecompleto = AppService.getWords(interacao.st_nomecompleto, 2).letters;
            });
            $scope.interacoes = data;
          }).finally(function () {
          $ionicLoading.hide();
        });
      }

    };


    /**
     * Carrega os dados da ocorrencia
     * @param idOcorrencia
     * @param renewCache
     * @returns {boolean}
     */
    $scope.carregaDadosOcorrencia = function (idOcorrencia, renewCache) {

      var st_parametros = {
        id_ocorrencia: idOcorrencia,
      };

      //chamando o metodo para salvar log
      AuthFactory.gravarLogAcesso($state.current.name, st_parametros);

      $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});

      renewCache || (renewCache = false);

      var ocorrenciaAtual = $filter('filter')($rootScope.ocorrencias, {id_ocorrencia: idOcorrencia});
      if (typeof ocorrenciaAtual != 'undefined' && ocorrenciaAtual.length > 0 && !renewCache) {
        $scope.ocorrencia = ocorrenciaAtual[0];
        $scope.ocorrencia.situacao_color = AppService.getColorSituacaoOcorrencia(ocorrenciaAtual[0].id_situacao);
        $scope.carregaInteracoesOcorrencia($scope.ocorrencia.id_ocorrencia);
        $ionicLoading.hide();
        return true;
      }

      OcorrenciaFactory.GetById(idOcorrencia, false, renewCache).then(function (response) {
        //console.log('Buscando do Cache');
        $scope.ocorrencia = response;
        $scope.ocorrencia.situacao_color = AppService.getColorSituacaoOcorrencia(response.id_situacao);
        $scope.carregaInteracoesOcorrencia($scope.ocorrencia.id_ocorrencia);
      }, function (error) {
        //console.log(error);
      }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
        $ionicLoading.hide();

      });
    };


    /**
     * Salva as interacoes da ocorrencia
     */
    $scope.salvarInteracao = function () {
      $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});

      var st_parametros = {
        idMatriculaAtiva: $scope.ocorrencia.id_matricula,
        idSalaDeAula: $scope.ocorrencia.id_saladeaula,
        id_ocorrencia: $scope.ocorrencia.id_ocorrencia,
      };

      // //chamando o metodo para salvar log
      AuthFactory.gravarLogAcesso($state.current.name, st_parametros);

      ServicoAtencaoService.salvarInteracao($scope.interacao.st_descricao, $scope.ocorrencia)
        .then(function (response) {

          //reseta a model de interacao que é utilizada no modal
          $scope.interacao = {};

          /*
           verifica se a resposta é um objeto e se a contagem desse objeto é diferente de undefined,
           isso significa que o retorno da api é um array de objetos
           */
          if (typeof response.data == 'object' && typeof response.data.length != 'undefined') {
            //reescreve a variavel que tem o array com as interacoes
            $scope.interacoes = response.data;
          } else {
            /*
             se a contagem dos objetos for undefined, significa que o retorno da api foi um objeto,
             então pega o objeto retornado e coloca no topo do array das interacoes
             */
            $scope.interacoes.unshift(response.data);
          }


          $ionicPopup.alert({
            title: 'Sucesso!',
            template: 'Interação criada com sucesso.'
          });

        }).finally(function () {
        //fecha o modal
        $scope.closeModal();
        //esconde o loading
        $ionicLoading.hide();
      });
    };

    /**
     * Metodo para encerar ocorrencia
     */
    $scope.encerrarAtendimento = function () {
      $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});


      ServicoAtencaoService.encerrarOcorrencia($scope.ocorrencia.id_ocorrencia)
        .then(function (response) {

          var st_parametros = {
            idMatriculaAtiva: $scope.ocorrencia.id_matricula,
            idSalaDeAula: $scope.ocorrencia.id_saladeaula,
            id_ocorrencia: $scope.ocorrencia.id_ocorrencia,
          };

          // //chamando o metodo para salvar log
          AuthFactory.gravarLogAcesso($state.current.name, st_parametros);

          $scope.ocorrencia = response.data;
          $scope.ocorrencia.situacao_color = AppService.getColorSituacaoOcorrencia($scope.ocorrencia.id_situacao);

          $scope.carregaInteracoesOcorrencia($scope.ocorrencia.id_ocorrencia);

          $ionicLoading.hide();
          $ionicPopup.alert({
            title: 'Sucesso!',
            template: 'Solicitação de encerramento enviada com sucesso.'
          });
        }, function (error) {
          $ionicPopup.alert({
            title: 'Erro!',
            template: 'Ocorreu um erro em sua solicitação, favor tentar mais tarde.'
          });
        });
    };


    /**
     * Metodo para reabrir a ocorrencia
     */
    $scope.reabrirAtendimento = function () {
      $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});
      ServicoAtencaoService.reabrirOcorrencia($scope.ocorrencia.id_ocorrencia)
        .then(function (response) {

          var st_parametros = {
            idMatriculaAtiva: $scope.ocorrencia.id_matricula,
            idSalaDeAula: $scope.ocorrencia.id_saladeaula,
            id_ocorrencia: $scope.ocorrencia.id_ocorrencia,
          };

          // //chamando o metodo para salvar log
          AuthFactory.gravarLogAcesso($state.current.name, st_parametros);

          $scope.ocorrencia = response.data;
          $scope.ocorrencia.situacao_color = AppService.getColorSituacaoOcorrencia($scope.ocorrencia.id_situacao);
          $scope.carregaInteracoesOcorrencia($scope.ocorrencia.id_ocorrencia);

          $ionicLoading.hide();
          $ionicPopup.alert({
            title: 'Sucesso!',
            template: 'Solicitação de reabertura enviada com sucesso.'
          });
        }, function (error) {
          $ionicPopup.alert({
            title: 'Erro!',
            template: 'Ocorreu um erro em sua solicitação, favor tentar mais tarde.'
          });
        });
    };


    /**
     * Abre o modal para criar uma interação
     * @param ocorrencia
     */
    $scope.abreModalInteracao = function (ocorrencia) {
      $ionicModal.fromTemplateUrl('modules/portal/components/servico-atencao/tpl/add-interacao.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    };

    /**
     * Fecha o modal
     */
    $scope.closeModal = function () {
      $scope.modal.hide();
      $scope.qtdCaraterTramite = 0;
    };

    $scope.voltarListaOcorrencias = function () {
      $state.go('app.servico_atencao.list');
    };

    $scope.$on("$ionicView.enter", function (event, data) {
      $analytics.pageTrack('/central_atencao_show');
      $scope.carregaDadosOcorrencia($state.params.id);
    });

  });
