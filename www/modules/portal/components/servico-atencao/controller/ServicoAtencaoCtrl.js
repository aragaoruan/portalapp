angular.module('portal.controllers')
  .controller('ServicoAtencaoCtrl', function ($rootScope,
                                              $scope,
                                              $state,
                                              $ionicLoading,
                                              ServicoAtencaoService,
                                              $cordovaVibration,
                                              $ionicPopup,
                                              AppService,
                                              $timeout,
                                              $analytics,
                                              $filter) {

    /**
     * Carrega todas as ocorrencias do usuario
     * @param cacheName
     * @param renewCache
     * @returns {boolean}
     */
    $scope.getOcorrencias = function (cacheName, renewCache) {
      if (typeof $rootScope.ocorrencias != "undefined" && $rootScope.ocorrencias.length > 0 && !renewCache) {
        return true;
      } else {
        renewCache = true;
        cacheName || (cacheName = 'ocorrencias');
      }


      $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});
      ServicoAtencaoService.getOcorrencias(null, cacheName, renewCache)
        .then(function (data) {
          //pega os dados do retorno e formata as datas de ultimo tramite e de cadastro para o formato en_us
          angular.forEach(data, function (ocorrencia) {
            ocorrencia.dt_ultimotramite = $filter("asDate")(ocorrencia.dt_ultimotramite, "yyyy-MM-dd");
            ocorrencia.dt_cadastro = $filter("asDate")(ocorrencia.dt_cadastro, "yyyy-MM-dd");
            ocorrencia.situacao_color = AppService.getColorSituacaoOcorrencia(ocorrencia.id_situacao);
          });

          $rootScope.ocorrencias = data;
        }, function (error) {
          $ionicPopup.alert({
            title: 'Erro!',
            template: 'Ocorreu um erro em sua solicitação, favor tentar mais tarde.'
          });
        }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
        $ionicLoading.hide();
      });
    };


    $scope.$on("$ionicView.enter", function (event, data) {
      $rootScope.showFilter = false;
      $analytics.pageTrack('/central_atencao_list');
      if (data.stateName == 'app.servico_atencao.list')
        $scope.getOcorrencias('ocorrencias');
    });


    $scope.abreOcorrencia = function (obj) {
      $state.go('app.servico_atencao.detail', {id: obj.id_ocorrencia});
    };

  });
