angular.module('portal.services')
  .service('ServicoAtencaoService', function ($state,
                                              CONFIG_APP,
                                              $http,
                                              OcorrenciaFactory,
                                              $q,
                                              CursoAlunoFactory) {
    var self = this;


    self.getLocalStorange = function (prop) {
      return angular.fromJson(window.localStorage.getItem(prop));
    }


    self.setLocalStorange = function (prop, value) {
      window.localStorage.setItem(prop, value)
    }


    /**
     * Retorna as ocorrencias
     * @returns {HttpPromise}
     */
    self.getOcorrencias = function (where, cacheName, renewCache) {
      where || (where = null);
      renewCache || (renewCache = false);
      return OcorrenciaFactory.GetAll(where, cacheName, renewCache);
    }

    /**
     * Retorna as categorias das ocorrencias
     * @returns {HttpPromise}
     */
    self.getCategorias = function (where) {

      where || ( where = {
        id_tipoocorrencia: 1,
        bl_ativo: 1
      });

      return OcorrenciaFactory.getAllCategoriasOcorrencia(where);
    }

    /**
     * Retorn os assuntos
     * @returns {HttpPromise}
     */
    self.getAssuntos = function (id_assuntocopai, id_entidade) {

      id_assuntocopai || (id_assuntocopai = null);
      id_entidade || (id_entidade = null);

      return OcorrenciaFactory.getAssuntosOcorrencia({
        id_tipoocorrencia: 1,
        bl_ativo: 1,
        bl_abertura: 1,
        id_assuntocopai: id_assuntocopai,
        id_entidade: id_entidade,
        bl_interno: 0
      });

    }

    /**
     * Percorre as entidades e os cursos dessas entidades e atribui todos os cursos em uma posição do array
     * @returns {Array}
     */
    self.getMatriculas = function () {
      var defer = $q.defer();

      CursoAlunoFactory.GetAll()
        .then(function (entidades) {
          var cursos = [];
          angular.forEach(entidades, function (entidade) {
            angular.forEach(entidade.cursos, function (curso) {
              curso.id_entidade = entidade.id_entidade;
              cursos.push(curso);
            });
          });
          defer.resolve(cursos);
        });
      return defer.promise;
    }

    /**
     * Busca as salas de aulas para listar na ocorrencia, baseado na matricula selecionada
     * @param id_matricula
     * @returns {*}
     */
    self.getSalaAulaMatricula = function (id_matricula) {
      return OcorrenciaFactory.getSalaAulaMatriculaByMatricula(id_matricula);
    }

    /**
     * Seta os dados para salvar ocorrencia
     * @param dados
     * @returns {*}
     */
    self.salvarOcorrencia = function (dados) {
      return OcorrenciaFactory.Create(JSON.stringify(dados));
    }


    /**
     * Seta no storage o objeto da ocorrencia ativa, para evitar que faça uma nova requisição ao banco
     * @param ocorrencia
     */
    self.setStorageOcorrenciaAtiva = function (ocorrencia) {
      var ocorrencia = JSON.stringify(ocorrencia);
      window.localStorage.setItem('ocorrencia_ativa', ocorrencia);
    }

    /**
     * Pega do storage a ocorrencia ativa
     * @param id
     * @returns {Object|Array|string|number}
     */
    self.getStorageOcorrenciaAtiva = function (id) {
      var ocorrencia = angular.fromJson(window.localStorage.getItem('ocorrencia_ativa'));
      if (id == ocorrencia.id_ocorrencia)
        return ocorrencia;

    }

    /**
     * Função para buscar os dados das interações.
     * @param idOcorrencia
     * @returns {*}
     */
    self.getInteracoesByOcorrencia = function (idOcorrencia, blVisivel) {
      blVisivel || (blVisivel = true);
      return OcorrenciaFactory.getInteracoesByIdOcorrencia(idOcorrencia, blVisivel);
    }


    /**
     * Função para persistir os dados da interação.
     * @param descricao
     * @param ocorrencia
     * @returns {*}
     */
    self.salvarInteracao = function (descricao, ocorrencia) {
      var tramite = {
        st_tramite: descricao,
        id_entidade: ocorrencia.id_entidade,
        bl_visivel: true
      };

      return OcorrenciaFactory.salvarInteracao(tramite, ocorrencia);

    }

    /**
     * Função para encerrar a ocorrencia.
     * @param ocorrencia
     * @returns {*}
     */
    self.encerrarOcorrencia = function (id_ocorrencia) {
      return OcorrenciaFactory.encerrarOcorrencia(id_ocorrencia);
    }


    /**
     * Função para reabrir a ocorrencia
     * @param id_ocorrencia
     * @returns {*}
     */
    self.reabrirOcorrencia = function (id_ocorrencia) {
      return OcorrenciaFactory.reabrirOcorrencia(id_ocorrencia);
    }


  });
