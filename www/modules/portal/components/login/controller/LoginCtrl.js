angular.module('portal.controllers').controller('LoginCtrl', function ($rootScope,
                                                                       $scope,
                                                                       $timeout,
                                                                       $ionicPopup,
                                                                       $state,
                                                                       AuthFactory,
                                                                       CONFIG_APP,
                                                                       MensagemService,
                                                                       $analytics,
                                                                       $ionicLoading) {

  $analytics.pageTrack('/login');

  $scope.loginData = {
    login: '',
    password: ''
  };


  $scope.loginPattern = /^[a-zA-Z0-9.@]{1,119}$/;
  $scope.emailPattern = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})$/;

  // seleciona o proximo input ao clicar ENTER no login
  $scope.nextInput = function (e){
    var code = e.keyCode || e.which;
    if (code === 13) {
      e.preventDefault();
      var parent = document.querySelector('form#formLogin');
      var inputs = parent.querySelectorAll('input, select, textarea');
      var current = e.target;
      var select = false;
      for (var i in inputs) {
        var input = inputs[i];
        if (select) {
          if (input.focus) {
            input.focus();
          }
          break;
        }
        if (input == current) {
          select = true;
        }
      }
    }
  };

  //aplica o watch para o campo de login

  // $scope.$watch(function (e) {
  //   return $scope.loginData.login;
  // }, function (newValue) {
  //   console.log("login", $scope.loginData.login);
  // });

  $scope.doLogin = function () {
    //verificando se o login e a senha não foi informado parando a execução caso algum deles não tenha sido preenchido
    if (!$scope.loginData.login || !$scope.loginData.senha) {
      return;
    }
    $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});
    AuthFactory.loginAuth($scope.loginData)
      .success(function (data) {
        if (data.retorno == true) {
          $state.go('app.home');

          //chamando o metodo para salvar log
          // o segundo parametro é null, pq não temos a informação do id_matricula nem da sala
          AuthFactory.gravarLogAcesso($state.current.name, null);

        }
      }).finally(function () {
      $ionicLoading.hide();
    });

  };

  $scope.recuperarSenha = function () {


    if (!$scope.loginData.login) {
      $ionicPopup.alert({
        title: 'Atenção!',
        template: 'Informe o e-mail para recuperar sua senha.'
      });
      return;
    }

    $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});
    AuthFactory.recuperarSenha($scope.loginData.login)
      .success(function (data) {

        //cria a variavel com o valor padrão
        var title = "Sucesso!";
        //verifica se o objeto na posição retorno retornou false e altera o valor da variavel
        if (!data.retorno)
          title = "Atenção!";

        //mostra o alerta
        $ionicPopup.alert({
          title: title,
          template: data.message
        });

        //redireciona para o login
        if (data.retorno) {
          $state.go('login');
        }
      }).finally(function () {
      $ionicLoading.hide();
    });


  }
});
