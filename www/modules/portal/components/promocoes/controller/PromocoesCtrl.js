angular.module('portal.controllers')
  .controller('PromocoesCtrl', function ($rootScope,
    CONFIG_APP,
    $scope,
    $state,
    PromocoesFactory,
    $ionicLoading,
    AppService,
    $analytics,
    $ionicModal,
    $ionicPopup,
    $http,
    $timeout,
    $window,
    pendingRequests,
    httpService,
    RestFactory) {

    $scope.promocoes = {};

    $rootScope.showFilter = false;
    $scope.promocoes.url = CONFIG_APP.url+'/';
    $scope.promocoes.imagem = 'assets/img/carregandoimagem.png';

    $scope.promocoes.loadPromocoes = function(){
      var request = PromocoesFactory.recuperaPromocoes(PromocoesFactory.recuperaMatriculaCursoSelecionado());
      if (pendingRequests.get().length != 0){
        $ionicLoading.show({
        });
      }
      request.then(
        function(data){
          $ionicLoading.hide();
          $scope.promocoes.promocoes = data.data;
        },
        function(){
          var alertPopup = $ionicPopup.alert({
            title: 'ERRO!',
            template: 'Não foi possível recuperar as oportunidades do servidor.'
          });
        }
      );
    };

    $scope.$on("$ionicView.leave", function (event, data) {
      pendingRequests.cancelAll();
    });

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      pendingRequests.cancelAll();
    });

    $scope.$on("$ionicView.afterEnter", function (event, data) {
      if (!PromocoesFactory.recuperaMatriculaCursoSelecionado()){
        var alertPopup = $ionicPopup.alert({
          title: 'ERRO!',
          template: 'Selecione algum curso para visualizar suas oportunidades.'
        });
        return true;
      }
      $scope.promocoes.loadPromocoes();
    });

    $ionicModal.fromTemplateUrl('modules/portal/components/promocoes/tpl/detalhe-promocao.html', {
      scope: $scope,
      hardwareBackButtonClose: false,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
    });

    $scope.promocoes.recarregaPromocoes = function (event){
      $state.go($state.current, {}, {
        reload: true
      });
    };

    $scope.promocoes.detalhe_imagem = function detalhe_imagem(item, $event) {
      $event.currentTarget.setAttribute("style", $event.currentTarget.getAttribute('style').replace('box-shadow: 10px 10px 5px #a73866', ''));
      PromocoesFactory.marcarVisualizado(item.id_campanhacomercial);
      $scope.promocoes.titulo = item.st_campanhacomercial;
      $scope.promocoes.imagem = this.url+'/'+item.st_imagem;
      $scope.promocoes.link = item.st_link;
      $scope.promocoes.descricao = item.st_descricao;
      $scope.modal.show();

    };

    $scope.promocoes.fecharModal = function(){
      $scope.promocoes.imagem = 'assets/img/carregandoimagem.png';
      $scope.modal.hide();
    };

    $scope.promocoes.altura = function(detalhe){
      var altura = $window.innerHeight;
      return '250px';
    };

    $scope.promocoes.largura = function(detalhe){
      var largura = $window.innerWidth;
      if (!detalhe){
        largura *= 0.7;

      }
 else if (largura > 480){
        largura = 480;
      }
      return largura+'px';
    };

    $scope.promocoes.abrirLink = function(link) {
      window.open(link, '_system');
    };
  });
