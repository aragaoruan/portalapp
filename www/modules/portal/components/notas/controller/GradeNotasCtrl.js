angular.module('portal.controllers')
  .controller('GradeNotasCtrl', function($scope, $rootScope, $state, $stateParams, $analytics, $http, CONFIG_APP, GradeNotaFactory) {
    $scope.showNotas = false;
    $scope.showNotasDetalhadas = function (value) {
      $scope.showNotas = value;
    }

    $scope.$on("$ionicView.enter", function(event, data) {
      $analytics.pageTrack('/notas');
      $scope.storageDisciplinaAtiva = angular.fromJson(window.localStorage.getItem('disciplina_ativa'));
      $scope.storageDadosAndamento = angular.fromJson(window.localStorage.getItem('disciplina_andamento'));
      $scope.storageCursoAtivo = angular.fromJson(window.localStorage.getItem('curso_ativo'));
      $scope.stTituloExibicaoDisciplina = $scope.storageDisciplinaAtiva.st_disciplina;
      $rootScope.showFilter = false;
      // buscando as notas
      GradeNotaFactory.getGradeNotasAgrupadaDetalhada($scope.storageCursoAtivo.id_matricula, $scope.storageDisciplinaAtiva.id_disciplina).then(

        //success
        function(response) {
          if (response.data) {
            $scope.notas = response.data[Object.keys(response.data)[0]];
            $scope.notas.st_notafinal = ($scope.notas.disciplinas[0].st_notafinal);
            $scope.notas.st_notaead = ($scope.notas.disciplinas[0].st_notaead);
            $scope.notas.st_notatcc = ($scope.notas.disciplinas[0].st_notatcc);
            $scope.notas.nu_notafinal = ($scope.notas.disciplinas[0].nu_notafinal);
            $scope.notas.st_notaatividade = ($scope.notas.disciplinas[0].st_notaatividade);
            $scope.notas.st_notarecuperacao = ($scope.notas.disciplinas[0].st_notarecuperacao);
            $scope.notas.st_notaead_prr = ($scope.notas.disciplinas[0].st_notaead_prr);
          }
        },
        //error
        function(response) {
          $scope.requestError = true;
        });
    });
  });
