angular.module('portal.controllers')
  .controller('ListaDisciplinasController', function ($scope, $rootScope, $http, CONFIG_APP, $stateParams, $state, $analytics, GradeNotaFactory) {


    $scope.storageDisciplinaAtiva = angular.fromJson(window.localStorage.getItem('disciplina_ativa'));
    $scope.$on("$ionicView.enter", function (event, data) {
      $rootScope.showFilter = false;
    }

    GradeNotaFactory.GetAll({
      params: {'id_matricula': $stateParams.idMatricula, 'id_disciplina': $scope.storageDisciplinaAtiva.id_disciplina}
    }).then(
      //success
      function (response) {
        $scope.gradeNotas = response.data;
      },
      //error
      function (response) {
        $scope.requestError = true;
      });

    $scope.selecionaDisciplina = function (disciplina_obj) {
      $state.go('app.notas.grade_notas_show', {
        stTituloExibicaoDisciplina: disciplina_obj.st_tituloexibicaodisciplina,
        stNotaFinal: disciplina_obj.st_notafinal,
        stNotaEad: disciplina_obj.st_notaead,
        stNotaTCC: disciplina_obj.st_notatcc,
        nuNotaFinal: disciplina_obj.nu_notafinal,
        stStatus: disciplina_obj.st_status
      });
    };

  });
