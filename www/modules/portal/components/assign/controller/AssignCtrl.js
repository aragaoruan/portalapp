angular
  .module("portal.controllers")
  .controller("AssignCtrl", function(
    $http,
    $scope,
    $log,
    CONFIG_APP,
    $stateParams,
    $q,
    AtividadesMoodleFactory,
    $ionicModal,
    $ionicHistory,
    $analytics,
    $rootScope,
    $cordovaFileChooser,
    MoodleFactory,
    $cordovaFilePath
  ) {
    $analytics.pageTrack("/assign");
    $rootScope.showFilter = false;
    $scope.newsubmission = {};
    $scope.fileUri;

    $scope.forumData = [];
    var instanceId = $stateParams.instanceId;
    $scope.storageDisciplinaAtiva = angular.fromJson(
      window.localStorage.getItem("disciplina_ativa")
    );
    $scope.storageCursoAtivo = angular.fromJson(
      window.localStorage.getItem("curso_ativo")
    );

    /**
     * Retorna as discussões de um forum do moodle
     */
    $scope.getSubmissionStatus = function() {
      AtividadesMoodleFactory.getSubmissionStatus(
        $scope.storageCursoAtivo.id_matricula,
        $scope.storageDisciplinaAtiva.id_saladeaula,
        instanceId
      ).then(function(submission) {
        $scope.submissionData = submission.lastattempt;
        switch ($scope.submissionData.submission.status) {
          case "submitted":
            $scope.submissionData.statusPt = "Enviado";
        }
      });
    };

    $scope.getAssignments = function() {
      var idCourses = [$scope.storageDisciplinaAtiva.st_codsistemacurso];
      AtividadesMoodleFactory.getAssignments(
        $scope.storageCursoAtivo.id_matricula,
        $scope.storageDisciplinaAtiva.id_saladeaula,
        idCourses,
        true
      ).then(function(assignments) {
        angular.forEach(assignments.courses[0].assignments, function(
          assignment
        ) {

          //verifia se encontrou algum com o id_discplina
          if (assignment.id === parseInt(instanceId)) {
            //como só precisamos do nome e id_disciplina para agrupar no array, pegamos a posição zero
            assignment.dtCuttOffDate = moment.unix(assignment.cutoffdate);
            assignment.dtDueDate = moment.unix(assignment.duedate);
            assignment.dtTimeModified = moment.unix(assignment.timemodified);
            MoodleFactory.changeHtmlLinks(
              assignment.intro,
              $scope.storageCursoAtivo.id_matricula,
              $scope.storageDisciplinaAtiva.id_saladeaula
            ).then(function(response) {
              assignment.intro = response;
              $scope.assignment = assignment;
            }).catch(function(){
              console.log('a  qui');

            });

            if (moment() > assignment.dtCuttOffDate) {
              $scope.dateLimit = true;
            }
            MoodleFactory.getComments(
              $scope.storageCursoAtivo.id_matricula,
              $scope.storageDisciplinaAtiva.id_saladeaula,
              assignment.cmid,
              true
            ).then(function(comments) {
              $scope.comments = comments;
            });
          }
        });
      });
    };
    $scope.chooseFile = function() {
      $cordovaFileChooser.open(function(uri) {
        $scope.fileUri = uri;
        $cordovaFilePath.resolveNativePath(uri).then(function(fileDir) {
          // Nome do arquivo
          $scope.fileName = fileDir.split("/").pop();
        });
      });
    };

    /**
     * Retorna as discussões de um forum do moodle
     */
    $scope.saveSubmission = function(formSubmission) {
      MoodleFactory.filesUpload(
        $scope.storageCursoAtivo.id_matricula,
        $scope.storageDisciplinaAtiva.id_saladeaula,
        $scope.fileUri
      ).then(function(fileUpload) {
        var newSubmission = {
          onlinetext_editor: {
            text: formSubmission.text,
            format: 1,
            itemid: fileUpload[0].itemid
          },
          files_filemanager: fileUpload[0].itemid
        };
        AtividadesMoodleFactory.saveSubmission(
          $scope.storageCursoAtivo.id_matricula,
          $scope.storageDisciplinaAtiva.id_saladeaula,
          instanceId,
          newSubmission
        ).then(function() {
          $scope.modal.hide();
          $scope.getAssignments();
          $scope.getSubmissionStatus();
        });
      });
    };

    //call function
    $scope.getAssignments();
    $scope.getSubmissionStatus();

    //Instanciando modal de submissão
    $ionicModal
      .fromTemplateUrl("submissionSave", {
        scope: $scope,
        animation: "slide-in-up"
      })
      .then(function(modal) {
        $scope.modal = modal;
      });

    /**
     * Abre a modal de submissão
     */
    $scope.submission = function() {
      $scope.modal.show(); // Abre modal
    };
    $scope.closeModal = function() {
      $scope.modal.hide();
    };

    // Cleanup the modal when we're done with it!
    $scope.$on("$destroy", function() {
      $scope.modal.remove();
    });

    // Execute action on hide modal
    $scope.$on("modal.hidden", function() {
      // Execute action
    });

    // Execute action on remove modal
    $scope.$on("modal.removed", function() {
      // Execute action
    });
  });
