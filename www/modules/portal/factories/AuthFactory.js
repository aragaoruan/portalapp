angular.module('portal.factories')
  .service('AuthFactory', function ($ionicHistory, $q, $http, $ionicPopup, $state, CONFIG_APP, jwtHelper, MensagemService, RestFactory) {

    function destroyUserCredentials() {
      window.localStorage.removeItem(CONFIG_APP.local_token);
      window.localStorage.removeItem('login');
      window.localStorage.removeItem('user_name');
      window.localStorage.removeItem('user_avatar');
      window.localStorage.removeItem('id_matricula');
      window.localStorage.removeItem('promo_nao_visualizada');
    }

    /**
     * Destroi o local storage com os dados da entidade
     * que o usuario selecionou na lista de curso
     */
    function destroyStorageEntidadeAtiva() {
      window.localStorage.removeItem('entidade_ativa');
    }

    /**
     * Destroi o local storage com os dados do curso
     * que o usuario selecionou na lista de curso
     */
    function destroyStorageCursoAtivo() {
      window.localStorage.removeItem('curso_ativo');
    }

    /**
     * Destroi o local storage com os dados da disciplina
     * que o usuario selecionou na lista de disciplinas
     */
    function destroyStorageDisciplinaAtiva() {
      window.localStorage.removeItem('disciplina_ativa');
    }

    /**
     * Destroi o local storage com os dados do login do usuário
     * no moodle e o curso
     */
    function destroyStorageCursoMoodle() {
      window.localStorage.removeItem('moodleCredentials');
    }

    /**
     * Destroi o storage com a lista dos cursos do usuario
     */
    function destroyStorageCursos() {
      window.localStorage.removeItem('cursos_usuario');
    }

    /**
     * Destroi o storage com o valor se o menu já foi exibido apos o login do usuario
     */
    function destroyStorageStatusMenu() {
      window.localStorage.removeItem('exibiu_menu');
    }

    function storeUserCredentials(token) {
      window.localStorage.setItem(CONFIG_APP.local_token, token);

      var tokenDecoded = services.verifyToken();

      window.localStorage.setItem('login', tokenDecoded.login);
      window.localStorage.setItem('user_name', tokenDecoded.user_name);
      window.localStorage.setItem('user_avatar', tokenDecoded.user_avatar);
      window.localStorage.setItem('user_id', tokenDecoded.user_id);

      $http.defaults.headers.common['user_token'] = token;
    }

    var services = {
      url: RestFactory.getUrlApi() + '/auth',
      verifyToken: function () {
        try {
          if (window.localStorage.getItem('user_token')) {
            return jwtHelper.decodeToken(window.localStorage.getItem('user_token'));
          }
        } catch (err) {
          console.log('Token salvo inválido!')
          return false;
        }
      },
      logout: function () {
        destroyUserCredentials();
        destroyStorageEntidadeAtiva();
        destroyStorageCursoAtivo();
        destroyStorageDisciplinaAtiva();
        destroyStorageCursoMoodle();
        destroyStorageCursos();
        destroyStorageStatusMenu();
        window.localStorage.clear();
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        $state.go('login');

      },
      loginAuth: function (dataLogin) {
        return $http({
          method: 'POST',
          url: this.url + '/login',
          data: JSON.stringify({
            login: dataLogin.login,
            senha: dataLogin.senha,
            entidadeId: CONFIG_APP.entidadeAppId
          })
        }).success(function (data, status, headers, config) {
          if (data.type == 'success') {
            destroyUserCredentials();
            storeUserCredentials(data.token);
          } else {
            $ionicPopup.alert({
              title: 'Login ou senha inválidos!',
              template: 'Por favor, verifique suas credencias e tente novamente.'
            });
          }
        }).error(function (data, status, headers, config) {
          if (data.type) {
            $ionicPopup.alert({
              title: 'Atenção!',
              template: data.message
            });
          } else {
            $ionicPopup.alert({
              title: 'Erro!',
              template: 'Por favor, verifique sua conexão e tente novamente.'
            });
          }
        });
      },
      recuperarSenha: function (email) {
        return $http({
          url: this.url + '/recuperar-senha',
          method: "POST",
          data: JSON.stringify({
            st_email: email,
            id_entidade: CONFIG_APP.entidadeAppId
          })
        })
          .error(function () {
            $ionicPopup.alert({
              title: 'Erro!',
              template: 'Por favor, verifique sua conexão e tente novamente.'
            });
          });
      },
      gravarLogAcesso: function (rota, params = null) {
        return $http({
          url: RestFactory.getUrlApi() + '/log/create',
          method: "POST",
          data: JSON.stringify({
            id_usuario: window.localStorage.getItem("user_id"),
            id_entidade: CONFIG_APP.entidadeAppId,
            id_sistema: CONFIG_APP.idSistema,
            st_parametros: params,
            rota: rota
          })
        })
          .success(function (response) {})
          .error(function (response) {});
      }
    };

    return angular.extend({}, RestFactory, services);

  });
