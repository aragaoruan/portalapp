angular.module('portal.factories')
  .factory('CursosFactory', function($http, CONFIG_APP, $q, RestFactory) {
    return {
      // Método que busca e retorna dados utilizados na tela de seleção de cursos.
      getListaCursos: function () {
        var def = $q.defer();
        $http.get(RestFactory.getUrlApi() + '/curso')
          .then(function (response) {
            if (typeof response == 'object') {
              def.resolve(response);
            } else {
              def.reject('Não foi possível obter os cursos.');
            }
          }, function (response) {
            def.reject('Não foi possível obter os cursos.');
          });
        return def.promise;
      }
    }
  });
