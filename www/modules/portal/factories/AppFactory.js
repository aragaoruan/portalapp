angular.module('portal.factories')
  .factory('httpInterceptor', function ($q, $rootScope, $location, CONFIG_APP) {
    return {
      request: function (config) {
        if (config.url.indexOf(CONFIG_APP.url) > -1) {
          config.headers = {
            public_token: CONFIG_APP.public_token,
            user_token: window.localStorage.getItem(CONFIG_APP.local_token)
          };
        }
        $rootScope.$broadcast('loading:show');

        return config
      },
      response: function (response) {
        $rootScope.$broadcast('loading:hide');

        return response
      },
      responseError: function (response) {
        $rootScope.$broadcast('loading:hide');

        if (response.status === 500) {
          $location.path('/login')
        }
        return $q.reject(response);
      }
    }
  })
  .config(function ($httpProvider, CONFIG_APP) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.headers.common['public_token'] = CONFIG_APP.public_token;

    // COnfigurações para o moodle

    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.transformRequest = [function (data) {

      return data;
    }];

    if (window.localStorage.getItem(CONFIG_APP.local_token)) {
      $httpProvider.defaults.headers.common['user_token'] = window.localStorage.getItem(CONFIG_APP.local_token);
    }

    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $httpProvider.interceptors.push('httpInterceptor');
  });
