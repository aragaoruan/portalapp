angular.module('portal.factories').factory('RestFactory', function (CONFIG_APP, $http, $q, LSFactory, md5, httpService) {

  var service = {

    /**
     * Retorna url principal
     * @returns {*}
     */
    getUrlBase: function () {
      return CONFIG_APP.url;
    },

    /**
     * Retorna url principal concatenado do caminho do modulo API
     * @returns {string}
     */
    getUrlApi: function () {
      return CONFIG_APP.url + CONFIG_APP.apiModule;
    },

    /**
     * Mensagem padrão de Erro
     * @param response
     * @param reject - reject do promisse
     * @returns {*}
     */
    handleError: function (response, reject) {
      reject(response.data);
    },

    /**
     * Mensagem padrão de sucesso
     * @param response
     * @param status
     * @param headers
     * @param config
     * @param reject  - reject do promisse
     * @param resolve - resolve do promisse
     * @param cacheId - ID do cache
     * @param cacheTime - tempo do cache
     * @param param - parametros usados na consulta
     * @returns {*}
     */
    handleSuccess: function (response, status, headers, config, resolve, reject, cacheId, cacheTime, param) {
      if (angular.isObject(response.data)) {
        //console.log('Retornando da API', cacheId, response.data);
        LSFactory.set(cacheId, response.data, cacheTime, param);
        resolve(response.data);

      } else {
        ////console.log('Tratar Isso');
        reject({
          type: 0,
          message: 'Os registros retornados não estávam em um objeto, favor verificar a requisição.',
          title: 'Erro!'
        });
      }
    },

    /**
     * URL da API, deve ser informada SEMPRE
     * Exemplo: url : CONFIG_APP.url + CONFIG_APP.apiModule + '/app-api/usuario' ou RestFactory.getUrlApi() + '/usuario'
     */
    url: '', // é preciso informar esta propriedade na factory, ex url : CONFIG_APP.url + '/app-api/user/create'

    setUrl: function (url) {
      this.url = url;
    },

    /**
     * Tempo padrão para o cache, 24 horas
     * Pode ser alterado nas Factories filhas
     */
    cacheMinutes: 1440,

    /**
     * ID do cache baseado na URL da API e no Login do Usuário pra garantir a troca do cache quando outro usuário logar no mesmo app
     * Exemplo: mensagem-agendamento.felipe.toffolo
     *
     * @param addString Caso precise add alguma string junto ao ID
     * @returns {*}
     */
    getCacheId: function (addString) {

      addString || (addString = null);

      var ID = this.url.split("/").pop() + '.' + LSFactory.get('login');

      if (addString)
        ID = ID + '.' + addString;

      return ID;


    },

    /**
     * ID de cache padrão para buscas de um único registro, exemplo: mensagem-agendamento.felipe.toffolo.id.123332
     * @param id
     * @returns {string}
     */
    getCacheIdById: function (id) {
      return this.getCacheId() + '.id.' + id;
    },

    /**
     * Método que retorna todos os dados da Model
     *
     * @param param
     * @param cacheId  Nome do ID do cache a ser criado, aopassar false, você está dizendo à factory para usar o getCacheId()
     * @param renewCache  Se true, busca os dados da API ignorando o cache, mas renovando os registros do cache
     * @param cacheTime  Número de minutos que aquele cache vai durar, se for false ou não informado, receberá o valor de cacheMinutes
     * @returns {*|any}
     * @constructor
     */
    GetAll: function (param, cacheId, renewCache, cacheTime) {

      cacheTime || (cacheTime = this.cacheMinutes);

      param || (param = {});

      renewCache || (renewCache = false);

      var that = this;
      if (!cacheId) cacheId = this.getCacheId();

      var cache = LSFactory.get(cacheId, true, param);

      return $q(function (resolve, reject) {

        if (!renewCache && cache && cache.length > 0) {
          resolve(cache);
          return;
        }

        httpService.get(that.url, param).then(
          function (response, status, headers, config) {
            that.handleSuccess(response, status, headers, config, resolve, reject, cacheId, cacheTime, param);
          }, function (response) {
            that.handleError(response, reject);
          });
      });

    },


    /**
     * Método que retorna os dados da Model por páginas
     * @param param.perPage -Quantos registros se espera por página
     * @param param.page - Qual página deseja retornar
     * @param param.sort - Qual campo usar para ordenação
     * @param param.direction - ASC/DESC
     * @param param.MODEL - Json da Model, por exemplo "param.usuario = { st_nomecompleto: "João das Neves" } "
     * @returns {*|any}
     * @constructor
     */
    Paging: function (param) {

      param || (param = {});

      var that = this;

      return $q(function (resolve, reject) {
        httpService.get(that.url + '/paging', param).then(
          function (response, status, headers, config) {
            that.handleSuccess(response, status, headers, config, resolve, reject);
          }, function (response) {
            that.handleError(response, reject);
          });
      });


    },

    /**
     * Retorna os dados pelo ID
     * @param id
     * @constructor
     */
    GetById: function (id, useCache, renewCache) {

      useCache || (useCache = false);

      renewCache || (renewCache = false);

      var that = this;

      return $q(function (resolve, reject) {

        if (!id) reject({type: 0, message: 'Informe o ID', title: 'Erro!'});

        var cacheId = service.getCacheIdById(id);

        if (useCache) {

          var cache = LSFactory.get(cacheId, true);

          if (cache && !renewCache) {
            resolve(cache);
            return;
          }
        }

        httpService.get(that.url + '/get/id/' + id).then(
          function (response, status, headers, config) {
            that.handleSuccess(response, status, headers, config, resolve, reject, cacheId);
          }, function (response) {
            that.handleError(response, reject);
          });
      });

      // return $http.get(this.url + '/get/' + id, {cache: cache}).then(handleSuccess, handleError);
    },


    /**
     * GetOneBy
     *
     * Método usado para recuperar dados da api no método GET (/get || getAction ) usando como parâmetros, mais dados que apenas o ID do usuário.
     * É necessário que no BackEnd isso seja tratado da mesma forma
     * @param params
     * @returns {*|any}
     * @constructor
     */
    GetOneBy: function (params, cacheId, renewCache, cacheTime) {

      cacheTime || (cacheTime = this.cacheMinutes);

      var that = this;

      if (!cacheId) cacheId = this.getCacheId('oneby');

      var cache = LSFactory.get(cacheId, true, params);

      return $q(function (resolve, reject) {

        if (cache && !renewCache) {
          resolve(cache);
          return;
        }

        httpService.get(that.url + '/get', params).then(
          function (response, status, headers, config) {
            that.handleSuccess(response, status, headers, config, resolve, reject, cacheId, cacheTime, params);
          }, function (response) {
            that.handleError(response, reject);
          });
      });

      // return $http.get(this.url + '/get', {params: params, cache: cache}).then(handleSuccess, handleError);
    },

    /**
     * GetOneById
     *
     * Método usado para recuperar dados da api no método GET (/get || getAction ) usando como parâmetro o ID do usuário.
     * @param params
     * @returns {promise}
     * @constructor
     */
    GetOneById: function (id, params) {
      return httpService.get(this.url + '/get/id/' + id, params);
    },

    /**
     * GetAllBy
     *
     * Método usado para recuperar dados da api usando parâmetros.
     * @param params
     * @returns {promise}
     * @constructor
     */
    GetAllBy: function (params) {
      return httpService.get(this.url, params);
    },

    /**
     * Insere um registro
     * @param data
     * @constructor
     */
    Create: function (data) {
      var that = this;

      return $q(function (resolve, reject) {
        $http.post(that.url + '/create', data).then(
          function (response, status, headers, config) {
            that.handleSuccess(response, status, headers, config, resolve, reject);
          }, function (response) {
            that.handleError(response, reject);
          });
      });

    },


    /**
     * Atualiza um registro
     * @param data
     * @param id
     * @returns {*}
     * @constructor
     */
    Update: function (data, id) {
      var that = this;
      return $q(function (resolve, reject) {

        if (typeof id == "undefined") {
          reject({type: 0, message: 'Informe o ID para Atualizar!', title: 'Erro!'});
        }

        var cacheId = that.getCacheIdById(id);

        $http.put(that.url + '/update/id/' + id, data).then(
          function (response, status, headers, config) {
            that.handleSuccess(response, status, headers, config, resolve, reject, cacheId);
          }, function (response) {
            that.handleError(response, reject);
          });
      });

    },


    /**
     * Exclúi um registro pelo ID
     * @param id
     * @constructor
     */
    Delete: function (id) {
      var that = this;

      return $q(function (resolve, reject) {

        if (typeof id == "undefined") {
          reject({type: 0, message: 'Informe o ID para Excluir!', title: 'Erro!'});
        }

        $http.delete(that.url + '/delete/id/' + id).then(
          function (response, status, headers, config) {
            resolve(response);
          }, function (response) {
            reject(response);
          });
      });

    },


    /**
     * Faz uma requisição normal
     * @param url
     * @param method
     * @param data
     * @constructor
     */
    NormalRequest: function (url, method, data) {

      var that = this;

      method || (method = 'POST');

      data || (data = null);

      if (method == 'GET' && data) {
        url = url + '?' + $.param(data);
        data = {}
      }

      return $q(function (resolve, reject) {
        $http({
          method: method,
          url: url,
          data: data,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          transformRequest: function (obj) {
            var str = [];
            for (var p in obj)
              str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            return str.join("&");
          }
        }).then(
          function (response, status, headers, config) {
            that.handleSuccess(response, status, headers, config, resolve, reject);
          }, function (response) {
            that.handleError(response, reject);
          });
      });
    },

  }


  return service;

});
