angular
  .module('portal.factories')
  .factory('GradeNotaFactory', function ($http, RestFactory, CONFIG_APP) {
    var service = {
      url: RestFactory.getUrlApi() + '/grade-nota',
      getGradeNotasAgrupadaDetalhada: function (idMatricula, idDisciplina) {
        return $http.get(this.url + '/get-vw-grade-nota-agrupada-detalhada', {
          params: {
            id_disciplina: idDisciplina,
            id_matricula: idMatricula
          }
        });
      }
    };
    return angular.extend({}, RestFactory, service);
  });
