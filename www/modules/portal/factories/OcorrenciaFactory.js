angular.module('portal.factories')
  .factory('OcorrenciaFactory', function ($http, CONFIG_APP, RestFactory, $q) {

    var service = {

      url: RestFactory.getUrlApi() + '/ocorrencia',

      getAllCategoriasOcorrencia: function (params) {
        return $http.get(this.url + '/categorias', {params: params});
      },
      getAssuntosOcorrencia: function (params) {
        return $http.get(this.url + '/assuntos', {params: params})
      },
      getSalaAulaMatriculaByMatricula: function (id_matricula) {
        var params = {id_matricula: id_matricula};
        return $http.get(RestFactory.getUrlApi() + '/curso-aluno/get-sala-aula-by-matricula', {params: params});
      },
      getInteracoesByIdOcorrencia: function (id_ocorrencia, bl_visivel) {
        var params = {
          id_ocorrencia: id_ocorrencia,
          bl_visivel: bl_visivel
        };
        return $http.get(RestFactory.getUrlApi() + '/interacao', {params: params});
      },
      salvarInteracao: function (tramite, ocorrencia) {
        return $http.post(RestFactory.getUrlApi() + '/interacao/create', JSON.stringify({
          ocorrencia: ocorrencia,
          id_ocorrencia: ocorrencia.id_ocorrencia,
          st_tramite: tramite.st_tramite,
          id_entidade: tramite.id_entidade,
          bl_visivel: tramite.bl_visivel
        }));
      },
      encerrarOcorrencia: function (id_ocorrencia) {
        return $http.post(this.url + '/encerrar', JSON.stringify({
          id_ocorrencia: id_ocorrencia
        }));
      },
      reabrirOcorrencia: function (id_ocorrencia) {
        return $http.post(this.url + '/reabrir', JSON.stringify({id_ocorrencia: id_ocorrencia}));
      },
      error: function (error) {
        console.log(error);
        $log.info('Erro ao tentar requisição');
      },

    };

    return angular.extend({}, RestFactory, service);

  });

