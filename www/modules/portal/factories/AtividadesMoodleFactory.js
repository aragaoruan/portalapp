angular.module('portal.factories')
  .factory('AtividadesMoodleFactory', function (MoodleFactory) {


    /**
     * Returns the courses and assignments for the users capability
     * mod_assign_get_assignments
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param array idCourses
     * @param boolean clearCache
     * @return Promise $q
     */
    this.getAssignments = function (idMatricula, idSalaDeAula, idCourses, clearCache) {

      clearCache = this.getClearCache(clearCache);

      return this.$q(function (resolve, reject) {

        var service = 'mod_assign_get_assignments';
        var cacheServiceId = service + '.' + idSalaDeAula + '.' + idMatricula.toString();
        var courses = this.LsCache.get(cacheServiceId, true);
        if (!clearCache && courses) {
          return resolve(courses);
        }

        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {

            try {
              //adiciona o atributo do token no objeto
              this.doWsCall(service, {
                wstoken: success.token,
                courseids: idCourses
              }, success.siteurl,'get',true)
                .then(function (response) {
                  return resolve(response);
                }.bind(this), function (error) {
                  return reject(error);
                });

            }
            catch (err) {
              return reject({error: err});
            }

          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);

    /**
     * TODO Verificar implementação na versão correta do moodle
     * Get a participant for an assignment, with some summary info about their submissions.
     * Versão 3.1
     * mod_assign_get_participant
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param boolean clearCache
     * @return Promise $q
     */
    this.getParticipant = function (idMatricula, idSalaDeAula, clearCache) {

      clearCache = this.getClearCache(clearCache);//

      return this.$q(function (resolve, reject) {

        var service = 'mod_assign_get_participant';
        var cacheServiceId = service + '.' + idSalaDeAula + '.' + idMatricula.toString();
        var courses = this.LsCache.get(cacheServiceId, true);
        if (!clearCache && courses) {
          return resolve(courses);
        }

        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {

            try {
              //adiciona o atributo do token no objeto
              this.doWsCall(service, {
                wstoken: success.token
              }, success.siteurl)
                .then(function (response) {
                  return resolve(response);
                }.bind(this), function (error) {
                  return reject(error);
                });

            }
            catch (err) {
              return reject({error: err});
            }

          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);


    /**
     * Returns information about an assignment submission status for a given user.
     * mod_assign_get_submission_status
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param int assignid
     * @param boolean clearCache
     * @return Promise $q
     */
    this.getSubmissionStatus = function (idMatricula, idSalaDeAula, assignid, clearCache) {

      clearCache = this.getClearCache(clearCache);//

      return this.$q(function (resolve, reject) {

        var service = 'mod_assign_get_submission_status';
        var cacheServiceId = service + '.' + idSalaDeAula + '.' + idMatricula.toString() + "." + assignid;
        var courses = this.LsCache.get(cacheServiceId, true);
        if (!clearCache && courses) {
          return resolve(courses);
        }

        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {

            try {
              //adiciona o atributo do token no objeto
              this.doWsCall(service, {
                wstoken: success.token,
                assignid: assignid
              }, success.siteurl)
                .then(function (response) {
                  return resolve(response);
                }.bind(this), function (error) {
                  return reject(error);
                });

            }
            catch (err) {
              return reject({error: err});
            }

          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);

    /**
     * Returns the submissions for assignments
     * mod_assign_get_submissions
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param Array assignmentids
     * @param boolean clearCache
     * @return Promise $q
     */
    this.getSubmission = function (idMatricula, idSalaDeAula, assignmentids, clearCache) {
      clearCache = this.getClearCache(clearCache);//


      if (!angular.isArray(assignmentids)) {
        throw "Parametro assignmentids deve ser um array";
      }

      return this.$q(function (resolve, reject) {

        var service = 'mod_assign_get_submissions';
        var cacheServiceId = service + '.' + idSalaDeAula + '.' + idMatricula.toString();
        var courses = this.LsCache.get(cacheServiceId, true);
        if (!clearCache && courses) {
          return resolve(courses);
        }


        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {

            try {
              //adiciona o atributo do token no objeto
              this.doWsCall(service, {
                wstoken: success.token,
                "assignmentids[]": assignmentids
              }, success.siteurl)
                .then(function (response) {
                  return resolve(response);
                }.bind(this), function (error) {
                  return reject(error);
                });

            }
            catch (err) {
              return reject({error: err});
            }

          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);


    /**
     * Save current user submission for a certain assignment.
     * mod_assign_save_submission
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param  {Integer} assignmentId Assign ID.
     * @param  {Object} pluginData   Data to save.
     * @return {Promise} Promise resolved when saved, rejected otherwise.
     */
    this.saveSubmission = function (idMatricula, idSalaDeAula, assignmentId, pluginData) {

      return this.$q(function (resolve, reject) {

        var params = {
          assignmentid: assignmentId,
          plugindata: pluginData
        };

        var service = 'mod_assign_save_submission';
        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {
            params.wstoken = success.token;

            try {
              //adiciona o atributo do token no objeto
              this.doWsCall(service, params, success.siteurl, "GET", true)
                .then(function (response) {
                  return resolve(response);
                }.bind(this), function (error) {
                  return reject(error);
                });

            }
            catch (err) {
              return reject({error: err});
            }

          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);


    /**
     * Trigger the submission status viewed event.
     * mod_assign_view_submission_status
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param int assignid
     * @return Promise $q
     */
    this.viewSubmissionStatus = function (idMatricula, idSalaDeAula, assignid) {

      return this.$q(function (resolve, reject) {
        var service = 'mod_assign_view_submission_status';
        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {
            try {
              //adiciona o atributo do token no objeto
              this.doWsCall(service, {
                wstoken: success.token,
                assignid: assignid
              }, success.siteurl)
                .then(function (response) {
                  return resolve(response);
                }.bind(this), function (error) {
                  return reject(error);
                });

            }
            catch (err) {
              return reject({error: err});
            }

          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);


    return angular.extend(this, MoodleFactory);
  });
