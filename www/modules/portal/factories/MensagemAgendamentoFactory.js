angular.module('portal.factories')
  .factory('MensagemAgendamentoFactory', function ($log, $http, CONFIG_APP, RestFactory) {


    var service = angular.extend({}, RestFactory, {

      url : RestFactory.getUrlApi() + '/mensagem-agendamento',

      cacheMinutes: 120

    });

    service.UpdateEvolucao = function (data) {

      if(typeof data == "object")
        data = JSON.stringify(data);

      console.log(this.url);

      return $http.put(this.url +'/update-evolucao', data);


    };


    return service;

  });
