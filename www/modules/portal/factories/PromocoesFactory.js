angular.module('portal.factories')
  .factory('PromocoesFactory', function ($state,
    CONFIG_APP,
    $http,
    $ionicLoading,
    $q,
    AppService,
    $ionicPopup,
    RestFactory,
    httpService,
    pendingRequests)   {
    var service = {
      url:  RestFactory.setUrl(RestFactory.getUrlApi() + '/campanha-comercial'),

      recuperaPromocoes: function(matricula){
        return RestFactory.GetAllBy(
          {
            bl_mobile: 1,
            id_matricula: matricula,
            order: 'bl_visualizado',
            direction: 'asc',
            'columns[]': ['id_campanhacomercial','st_link', 'st_campanhacomercial', 'st_link', 'st_imagem', 'st_descricao', 'bl_visualizado']
          });
      },

      recuperaPromocoesNaoLidas: function(matricula){
        return RestFactory.GetAllBy(
          {
            bl_mobile: 1,
            id_matricula: matricula,
            'columns[]': ['bl_visualizado']
          });
      },
      atualizaPromocoes: function() {
        var request = this.recuperaPromocoesNaoLidas(this.recuperaMatriculaCursoSelecionado());
        request.then(
          function(data){
            naoVisualizdas = 0;
            for (var i = 0; i < data.data.length;i++){
              if (data.data[i].bl_visualizado == false){
                naoVisualizdas++;
              }
            }
            localStorage.setItem('promo_nao_visualizada', naoVisualizdas);
          },function(erro) {
          }
        );
      },

      marcarVisualizado: function(id){
        RestFactory.GetOneById(id,
          {
            id_campanhacomercial: id,
            id_matricula: this.recuperaMatriculaCursoSelecionado()
          }).then(
          function(){
            this.atualizaPromocoes();
          },
          function(){
            var alertPopup = $ionicPopup.alert({
              title: 'ERRO!',
              template: 'Houve algum erro ao atualizar a promoção.'
            });
            return true;
          });
      },

      recuperaMatriculaCursoSelecionado: function(){
        return localStorage.getItem('id_matricula');
      },

      recuperaMatriculas: function () {
        var matriculas_temp = JSON.parse(localStorage.getItem('matriculas'));
        var matriculas = '';
        for (var i = 0; i < matriculas_temp[0].cursos.length; i++){
          matriculas += matriculas_temp[0].cursos[i].id_matricula;
          if (i+1 != matriculas_temp[0].cursos.length){
            matriculas += ", ";
          }
        }
        return matriculas;
      }
    };

    return angular.extend({}, RestFactory, service);
  });
