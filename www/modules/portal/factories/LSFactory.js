/**
 * Factory para tratar os dados do cache via LocalStorage
 * Ela foi desenvolvida após as inúmeras dificuldades em manipular os dados no angular-cache
 */

angular.module('portal.factories')
  .factory('LSFactory', ['$window', 'md5', function ( $window, md5) {

    var getId = function (key, param) {
        if(param)
          return key + '.' + md5.createHash(JSON.stringify(param));

        return key;
      };



    return {

      clearStorage : function (key) {
        $window.localStorage.removeItem(key);
        $window.localStorage.removeItem(key + '.query');
        $window.localStorage.removeItem(key + '.time');
        $window.localStorage.removeItem(key + '.time.expire');
      },

      /**
       * Salva um valor no LocalStorage
       * Par controlarmos o cache, ao setar algum valor no LocalStorage criamos mais dois registros, o .time e o .time.expire
       * O .time salva o momento em que foi criado o registro e o .time.expire salva a quantidade de minutos que o mesmo deve durar
       *
       * @param key - ID
       * @param val - Valor
       * @param cacheForMinutes - Quantidade de minutos que o cache irá durar
       * @param param - Parâmetros para consulta à API, se informado é gerado um md5 e anexado ao ID para termos cache da consulta
       * @returns {LSFactory}
       */
      set: function (key, val, cacheForMinutes, param) {

        key = getId(key, param);

        cacheForMinutes || (cacheForMinutes = 1440);

        if (typeof val == "object") val = JSON.stringify(val);

        $window.localStorage && $window.localStorage.setItem(key, val);

        var date = moment();
        $window.localStorage.setItem(key + '.time', date.toISOString());
        $window.localStorage.setItem(key + '.time.expire', cacheForMinutes);

        // console.log('Cache salvo: ', key, cacheForMinutes, param);
        return this;
      },




      /**
       * Retorna os dados do cache
       *
       * Neste momento o sistema verifica se temos os registros [key].time e [key].time.expire.
       * Tendo estas duas informações, fazemos a comparação da diferença em minutos do moneto em que foi salvo o cache com o momento atual
       * Caso essa diferença seja maior ou igual à quantidade de minutos informada no momento de criação do cache, o sistema apaga os dados do cache
       * retornando null, fazendo com que o sistema recrie o cache com os dados oriundos da API.
       *
       * Normalmente, quando salvamos dados da API no cache, salvamos uma string JSON, pis é o formato retornado pela nossa API.
       * Para melhorar a forma de tratar estes dados, o usuário pode informar a propriedade [convertToJson], ela irá dizer ao método
       * que o resultado deve ser um Objeto JSON e não uma string.
       *
       * @param key - ID
       * @param convertToJson - Se true, irá retornar o Objeto JSON referentes aos dados do cache
       * @param param - Parâmetros para consulta à API, se informado é gerado um md5 e anexado ao ID para retornarmos o cache da consulta
       * @returns {Storage}
       */
      get: function (key, convertToJson, param) {

        key = getId(key, param);
        // console.log(key);

        if (!$window.localStorage) return;

        var time = $window.localStorage.getItem(key + '.time');
        if (time) {
          var expire = $window.localStorage.getItem(key + '.time.expire');
          if (!expire) expire = 1440;

          var date = moment();
          var cachedDate = moment(time);

          // console.log('Cache acessado: ', key, expire, date.diff(cachedDate, 'minutes'));

          if (expire && (expire <= date.diff(cachedDate, 'minutes'))) {

            // console.log('Apagando o Cache', expire, date.diff(cachedDate, 'minutes'));

            this.clearStorage(key);
            return;
          }

        }

        // console.log('Cache acessado: ', key);
        var cached = $window.localStorage.getItem(key);

        // console.log('Consulta ao cache: ', key,  cached);

        if (cached && convertToJson)
          return JSON.parse(cached);

        return cached;

      }
    };
  }]);
