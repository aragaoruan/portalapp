angular.module('portal.factories')
  .service('FinanceiroFactory', function ($state,
                                          CONFIG_APP,
                                          $http,
                                          $ionicLoading,
                                          $cordovaTransfer,
                                          $cordovaFile,
                                          $cordovaFileOpener,
                                          $q,
                                          AppService,
                                          $ionicPopup,
                                          RestFactory) {

    var localPath = CONFIG_APP.url;

    //TODO ADICIONAR ISSO NUMA FACTORY
    document.addEventListener('deviceready', function () {
      localPath = cordova.file.dataDirectory + 'portaldoaluno/';

      if (ionic.Platform.isAndroid()) {
        localPath = cordova.file.externalApplicationStorageDirectory + 'portaldoaluno/';
      }
    });

    var services = {
      url: '/app-api',

      /**
       * Busca os dados das vendas do usuário
       * @returns {HttpPromise}
       */
      getVendasUser: function () {
        return $http.get(RestFactory.getUrlApi() + '/resumo-financeiro');
      },

      /**
       * Gera o arquivo pdf do boleto no servidor
       * @param idLancamento
       */
      gerarPdfBoleto: function (idLancamento) {
        return $http.get(RestFactory.getUrlApi() + '/boleto/url-boleto?id_lancamento=' + idLancamento, {timeout:3500});
      },

      /**
       * Baixa o conteúdo para o device
       * @param url
       */
      baixarConteudo: function (url) {
        $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});
        var url = RestFactory.getUrlBase() + "/" + url;
        var targetPath = localPath + "files";
        $cordovaTransfer.download(url, targetPath, null, true)
          .then(function (object) {
            $ionicLoading.hide();
            $ionicPopup.alert({
              title: 'Download concluído!',
              template: 'Download do arquivo efetuado com sucesso'
            });
            $cordovaFileOpener.open(object.nativeURL, 'application/pdf');

          }, function (err) {
            $ionicLoading.hide();
            var message = '';
            console.log('error download -----> ', err);
            switch (err.code) {
              case 1:
                message += 'Conteúdo não encontrado, favor entrar em contato com nossa equipe.';
                break;
              case 3:
                message += 'Sua conexão está lenta, mas fique despreocupado, iremos avisar assim que concluirmos o download';
                break;
              default:
                message = JSON.stringify(err);
            }
            $ionicPopup.alert({
              title: 'Ops!',
              template: message
            });

          }, function (progress) {
            //callback progress
          });


      },

      /**
       * Seta o lancamento selecionado no storage
       * @param lancamento
       */
      setStorageLancamentoAtivo: function (lancamento) {
        window.localStorage.setItem('lancamento_ativo', JSON.stringify(lancamento));
      },

      /**
       * Recupera o lançamendo do storage
       * @returns {Object|Array|string|number}
       */
      getStorageLancamentoAtivo: function () {
        return angular.fromJson(window.localStorage.getItem('lancamento_ativo'));
      },

      /**
       * Pega os dados do boleto.
       * @param idLancamento
       * @returns {HttpPromise}
       */
      getDadosBoleto: function (idLancamento) {
        return $http.get(RestFactory.getUrlApi() + '/boleto/retorna-codigo-de-barras', {
          params: {
            funcao: 'getbarcode',
            id_lancamento: idLancamento
          }
        });
      }
    };

    return angular.extend({}, RestFactory, services);

  });
