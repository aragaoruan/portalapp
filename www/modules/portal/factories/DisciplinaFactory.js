angular.module('portal.factories')
  .factory('DisciplinaFactory', function ($log, $http, CONFIG_APP, RestFactory, LSFactory, $q, RestFactory,
    httpService, pendingRequests) {

    var service = angular.extend({}, RestFactory, {

      cacheMinutes: 120,

      url: RestFactory.getUrlApi() + '/disciplina-aluno'

    });
    var disciplinaAtiva = {};


    service.setDisciplinaAtiva = function (disciplina) {
      disciplinaAtiva = disciplina;
    }

    service.getDisciplinaAtiva = function () {
      return disciplinaAtiva;
    }


    service.listConteudoDisciplina = function (params, renewCache, cacheMinutes) {

      pendingRequests.cancelAll();

      var that = this;
      var cacheId = this.getCacheId('list-conteudo-disciplina');
      var cache = LSFactory.get(cacheId, true, params);

      cacheMinutes || (cacheMinutes = this.cacheMinutes);


      return $q(function (resolve, reject) {
        if (cache && !renewCache) {
          resolve(cache);
          return;
        }

        httpService.get(that.url + '/list-conteudo-disciplina', params).then(
          function (response, status, headers, config) {
            that.handleSuccess(response, status, headers, config, resolve, reject, cacheId, cacheMinutes, params);
          }, function (response) {
            that.handleError(response, reject);
          });

      });

    };


    service.detailsDisciplinaSala = function (params, renewCache, cacheMinutes) {

      cacheMinutes || (cacheMinutes = this.cacheMinutes);

      //params, cacheId, renewCache, cacheTime
      return this.GetOneBy(params, null, renewCache, cacheMinutes);
    };

    service.getDisciplinas = function (params, renewCache) {
      //param, cacheId, renewCache, cacheTime
      return this.GetAll({
        id_entidade: params.idEntidade,
        id_matricula: params.idMatricula,
        id_projetopedagogico: params.idProjeto,
        id_saladeaula: params.idSaladeaula,
        bl_moodle: params.blMoodle
      }, this.getCacheId(), renewCache, 30);
    };

    return service;

  });
