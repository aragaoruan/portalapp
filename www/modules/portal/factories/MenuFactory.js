angular.module('portal.factories')
  .factory('MenuFactory', function (CONFIG_APP, $q, $log, $http) {
    var that = this;
    that.currentMenu = null;

    that.setCurrentMenu = function (currentMenu) {
      that.currentMenu = currentMenu;
      return that;
    };

    that.getCurrentMenu = function () {
      var currentMenu = that.currentMenu.split('.');
      currentMenu = currentMenu.length > 0 ? currentMenu[1] : currentMenu[0];
      return currentMenu;
    };

    that.getObjMenu = function () {

      var def = $q.defer();
      var objMenu = [];
      // objeto com os dados do menu selecionado
      var menuData = {};

      $http.get('assets/menu.json').success(function (data) {
        //percorre o objeto retornado pelo json
        angular.forEach(data, function (obj) {
          var name = obj.href.split('.');
          name = name.length > 0 ? name[1] : name[0];
          //checa se é o dado do menu atual e seta na veriavel
          if (name == that.getCurrentMenu()){
            menuData = obj;
          }
          //verifica se no objeto na posição parente existe um index com o nome da rota
          if (obj.parent.indexOf(that.getCurrentMenu()) !== -1) {

            var pattern = new RegExp(that.getCurrentMenu(), "g");

            if (obj.href.match(pattern))
              obj.active = true;

            objMenu.push(obj);//adiciona o objeto no array
          }

        });
        var menu = {
          menuData: menuData,
          itens: objMenu
        }
        def.resolve(menu);
      });

      return def.promise;
    };


    return that;
  });
