angular.module('portal.factories').factory('ForumMoodleFactory', function (MoodleFactory) {
    /**
     * Retorna os forums do curso
     * mod_forum_get_forums_by_courses
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param array idCourses
     * @param boolen clearCache
     * @type Promise
     */
    this.getForumsByCourses = function (idMatricula, idSalaDeAula, idCourses, clearCache) {
      clearCache = this.getClearCache(clearCache);
      return this.$q(function (resolve, reject) {
        var service = 'mod_forum_get_forums_by_courses';
        var cacheServiceId = service + '.' + idSalaDeAula + '.' + idMatricula.toString();
        var courses = this.LsCache.get(cacheServiceId, true);
        if (!clearCache && courses) {
          return resolve(courses);
        }

        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {
            this.doWsCall(service, {
              wstoken: success.token,
              courseids: idCourses
            }, success.siteurl, 'get', true).then(function (response) {
              this.LsCache.set(cacheServiceId, response, 2);
              return resolve(response);
            }.bind(this), function (error) {
              return reject(error);
            });


          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);


    /**
     * Retorna os post de uma discussão no forum
     * mod_forum_get_forum_discussion_posts
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param int courseId
     * @param int discussionId
     * @param boolean clearCache
     * @type Promise $q
     */
    this.getForumDiscussionPosts = function (idMatricula, idSalaDeAula, courseId, discussionId, clearCache) {
      clearCache = this.getClearCache(clearCache);
      return this.$q(function (resolve, reject) {
        var service = 'mod_forum_get_forum_discussion_posts';
        var cacheServiceId = service + '.' + idSalaDeAula + '.' + idMatricula.toString() + '.' + courseId.toString() + "." + discussionId.toString();
        var courses = this.LsCache.get(cacheServiceId, true);
        if (!clearCache && courses) {
          return resolve(courses);
        }

        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {
            this.doWsCall(service, {
              wstoken: success.token,
              discussionid: discussionId
            }, success.siteurl).then(function (response) {
              this.LsCache.set(cacheServiceId, response, 2);
              return resolve(response);
            }.bind(this), function (error) {
              return reject(error);
            });


          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);


    /**
     * mod_forum_get_forum_discussions_paginated
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param int courseId
     * @param int forumid
     * @param boolean clearCache
     * @type Promise $q
     */
    this.getForumDiscussionsPaginated = function (idMatricula, idSalaDeAula, courseId, forumid, clearCache) {
      clearCache = this.getClearCache(clearCache);
      return this.$q(function (resolve, reject) {
        var service = 'mod_forum_get_forum_discussions_paginated';
        var cacheServiceId = service + '.' + idSalaDeAula + '.' + idMatricula.toString() + '.' + courseId.toString() + "." + forumid.toString();
        var courses = this.LsCache.get(cacheServiceId, true);
        if (!clearCache && courses) {
          return resolve(courses);
        }

        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {
            this.doWsCall(service, {
              wstoken: success.token,
              forumid: forumid
            }, success.siteurl).then(function (response) {
              this.LsCache.set(cacheServiceId, response, 2);
              return resolve(response);
            }.bind(this), function (error) {
              return reject(error);
            });


          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);

    /**
     * mod_forum_view_forum_discussion
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param int courseId
     * @param int discussionId
     * @type Promise $q
     */
    this.triggerViewForumDiscussion = function (idMatricula, idSalaDeAula, discussionId) {
      return this.$q(function (resolve, reject) {
        var service = 'mod_forum_view_forum_discussion';

        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {
            this.doWsCall(service, {
              wstoken: success.token,
              discussionid: discussionId
            }, success.siteurl).then(function (response) {
              return resolve(response);
            }.bind(this), function (error) {
              return reject(error);
            });

          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);


    /**
     * mod_forum_view_forum
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param int courseId
     * @param int forumid
     * @type Promise $q
     */
    this.triggerViewForum = function (idMatricula, idSalaDeAula, courseId, forumid) {
      return this.$q(function (resolve, reject) {
        var service = 'mod_forum_view_forum';
        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {
            this.doWsCall(service, {
              wstoken: success.token,
              forumid: forumid
            }, success.siteurl).then(function (response) {
              return resolve(response);
            }.bind(this), function (error) {
              return reject(error);
            });

          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);

    /**
     * Verifica se o usuario pode criar discussão
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param int forumId
     * @type Promise $q
     */
    this.canAddDiscussion = function (idMatricula, idSalaDeAula, forumId) {
      return this.$q(function (resolve, reject) {
        var service = 'mod_forum_can_add_discussion';
        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {
            this.doWsCall(service, {
              wstoken: success.token,
              forumid: forumId
            }, success.siteurl).then(function (response) {
              return resolve(response);
            }.bind(this), function (error) {
              return reject(error);
            });

          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);


    /**
     * Cria uma nova discussão (Verificar se aluno pode criar)
     * mod_forum_add_discussion
     *
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param Object params {
        forumid: <valor>,
        subject:<valor>,
        message:<valor>
      }
     * @type Promise $q
     */
    this.forumAddDiscussion = function (idMatricula, idSalaDeAula, params) {
      return this.$q(function (resolve, reject) {
        var service = 'mod_forum_add_discussion';
        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {

            try {
              this.checkRequiredAttributes(params, [
                "forumid", "subject", "message"
              ]);

              //adiciona o atributo do token no objeto
              params.wstoken = success.token;

              this.doWsCall(service, params, success.siteurl, "POST")
                .then(function (response) {
                  return resolve(response);
                }.bind(this), function (error) {
                  return reject(error);
                });

            } catch (err) {
              return reject({
                error: err
              });
            }

          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);


    /**
     * Cria um novo post em uma discussão
     * mod_forum_add_discussion_post
     *
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param Object params {
        postid: <valor>,
        subject:<valor>,
        message:<valor>
      }
     * @type Promise $q
     */
    this.forumAddDiscussionPost = function (idMatricula, idSalaDeAula, params) {
      return this.$q(function (resolve, reject) {
        var service = 'mod_forum_add_discussion_post';
        this.getUserToken(idMatricula, idSalaDeAula)
          .then(function (success) {

            try {
              this.checkRequiredAttributes(params, [
                "postid", "subject", "message"
              ]);

              //adiciona o atributo do token no objeto
              params.wstoken = success.token;

              this.doWsCall(service, params, success.siteurl)
                .then(function (response) {
                  return resolve(response);
                }.bind(this), function (error) {
                  return reject(error);
                });

            } catch (err) {
              return reject({
                error: err
              });
            }

          }.bind(this), function (error) {
            return reject(error);
          });
      }.bind(this));
    }.bind(this);

    return angular.extend(this, MoodleFactory);

  });
