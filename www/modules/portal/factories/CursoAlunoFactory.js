  angular
    .module('portal.factories')
    .factory('CursoAlunoFactory', function ($http, RestFactory,CONFIG_APP) {
      var service = {
        url : RestFactory.getUrlApi() + '/curso-aluno'
      };
      return angular.extend({}, RestFactory, service);
    });
