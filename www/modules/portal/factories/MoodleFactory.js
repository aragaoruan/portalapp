angular
  .module("portal.factories")
  .factory("MoodleFactory", function(
    CONFIG_APP,
    $q,
    $http,
    LSFactory,
    $httpParamSerializer,
    $cordovaFile,
    $cordovaTransfer,
    $ionicPlatform,
    $cordovaFilePath,
    $rootScope
  ) {
    this.CONFIG = CONFIG_APP;
    this.urlApi = this.CONFIG.url + this.CONFIG.apiModule + "/moodle";

    this.$http = $http;

    this.$q = $q;
    this.wsurl = "webservice/rest/server.php?moodlewsrestformat=json";

    this.LsCache = LSFactory;
    var file = $cordovaFile;

    var localPath;

    /**
     * Faz uma chamada ao Webservice do Moodle
     * @param wsfunction
     * @param params
     * @param siteurl
     * @param method
     * @param serialize
     * @returns Promise
     */
    this.doWsCall = function(wsfunction, params, siteurl, method, serialize) {
      method = angular.isUndefined(method) ? "GET" : method;
      serialize = angular.isUndefined(serialize) ? false : serialize;

      return this.$q(
        function(resolve, reject) {
          var request = {
            method: method,
            url: siteurl + this.wsurl,
            headers: {
              "Content-Type": "application/x-www-form-urlencoded"
            }
          };

          params.wsfunction = wsfunction;
          request.params = params;

          if (serialize) {
            request.paramSerializer = "$httpParamSerializerJQLike";
          }

          //Remove heades que não são aceitos no moodle
          delete this.$http.defaults.headers.common.user_token;
          delete this.$http.defaults.headers.common.public_token;

          this.$http(request).then(
            function(response) {
              var data = response.data;
              if (data.errorcode) {
                return reject(data);
              }
              return resolve(data);
            },
            function(error) {
              return reject(error);
            }
          );
        }.bind(this)
      );
    }.bind(this);

    this.getClearCache = function(clearCache) {
      // return true;
      return angular.isUndefined(clearCache) ? false : clearCache;
    };

    /**
     * Verifica os parametros foram informados
     * @param obj
     * @param arrAtributes
     * @return {boolean}
     */
    this.checkRequiredAttributes = function(obj, arrAtributes) {
      if (!angular.isObject(obj)) {
        throw "O parâmetro passado não é um objeto.";
      }

      angular.forEach(arrAtributes, function(attribute) {
        if (!obj.hasOwnProperty(attribute)) {
          throw "Parâmetro " + attribute + " não informado.";
        }
      });

      return true;
    };

    /**
     * Retorna o token e dados do usuário para integração com o moodle
     * @param int idMatricula
     * @param int idSalaDeAula
     * @param boolean clearCache
     * @return Promise $q
     */
    this.getUserToken = function(idMatricula, idSalaDeAula, clearCache) {
      //verifica se foi passado o parametro
      clearCache = this.getClearCache(clearCache);

      var tokenName =
        "moodle_data." + idSalaDeAula + "." + idMatricula.toString();

      //retorna a Promise
      return this.$q(
        function(resolve, reject) {
          //verifica se existe no cache
          var existToken = this.LsCache.get(tokenName, true);

          //se existir e se não for para limpar o cache retorna o que tá no cache
          if (existToken && !clearCache) {
            return resolve(existToken);
          }

          //busca os dados na api
          this.$http
            .get(this.urlApi + "/get-token", {
              method: "GET",
              params: {
                id_matricula: idMatricula,
                id_saladeaula: idSalaDeAula
              }
            })
            .then(
              function(response) {
                //se não existir o token retorna o erro
                if (angular.isUndefined(response.data.token)) {
                  return reject({
                    error: response
                  });
                }

                //seta no cache os dados e retorna o sucesso
                this.LsCache.set(tokenName, response.data, 1440);
                return resolve(response.data);
              }.bind(this),
              function(error) {
                return reject(error);
              }
            );
        }.bind(this)
      );
    }.bind(this);

    /**
     * Retorna uma URL que da acesso à plataforma Moodle
     * @param int id_matricula
     * @param int id_saladeaula
     * @param boolean clearCache
     * @return Promise $q
     */
    this.getTokenAcesso = function(idMatricula, idSalaDeAula, clearCache) {
      //verifica se foi passado o parametro
      clearCache = this.getClearCache(clearCache);

      return this.$q(
        function(resolve, reject) {
          var cacheServiceId =
            "moodle_token." + idSalaDeAula + "." + idMatricula.toString();
          var token = this.LsCache.get(cacheServiceId, true);
          if (!clearCache && token) {
            return resolve(token);
          }

          this.getUserToken(idMatricula, idSalaDeAula, true).then(
            function(response) {
              this.$http({
                method: "GET",
                url: this.urlApi + "/get-token-acesso",
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                params: {
                  st_loginintegrado: response.st_loginintegrado,
                  id_saladeaula: idSalaDeAula
                }
              }).then(
                function(response) {
                  if (angular.isUndefined(response.data.loginurl)) {
                    return reject({
                      error: response
                    });
                  }

                  this.LsCache.set(cacheServiceId, response.data, 720);
                  return resolve(response.data);
                }.bind(this),
                function(error) {
                  return reject(error);
                }
              );
            }.bind(this),
            function(error) {
              return reject(error);
            }
          );
        }.bind(this)
      );
    }.bind(this);

    /**
     * Retorna os Cursos do usuário
     * @param int idMtricula
     * @param int idSalaDeAula
     * @param boolean clearCache
     * @returns Promise $q
     */
    this.getUserCourses = function(idMtricula, idSalaDeAula, clearCache) {
      //verifica se foi passado o parametro
      clearCache = this.getClearCache(clearCache);
      return this.$q(
        function(resolve, reject) {
          var service = "core_enrol_get_users_courses";
          var cacheServiceId =
            service + "." + idSalaDeAula + "." + idMtricula.toString();
          var courses = this.LsCache.get(cacheServiceId, true);
          if (!clearCache && courses) {
            return resolve(courses);
          }

          this.getUserToken(idMtricula, idSalaDeAula, clearCache).then(
            function(success) {
              this.doWsCall(
                service,
                {
                  userid: success.userid,
                  wstoken: success.token
                },
                success.siteurl
              ).then(
                function(response) {
                  this.LsCache.set(cacheServiceId, response, 2);
                  return resolve(response);
                }.bind(this),
                function(error) {
                  return reject(error);
                }
              );
            }.bind(this),
            function(error) {
              return reject(error);
            }
          );
        }.bind(this)
      );
    }.bind(this);

    /**
     * Retorna o conteúdo de um Curso
     * @param int idMatricula
     * @param int courseId
     * @param int idSalaDeAula
     * @param boolean clearCache
     * @returns Promise $q
     */
    this.getCourseContents = function(
      idMatricula,
      courseId,
      idSalaDeAula,
      clearCache
    ) {
      clearCache = this.getClearCache(clearCache);
      return this.$q(
        function(resolve, reject) {
          var service = "core_course_get_contents";
          var cacheServiceId =
            service +
            "." +
            idSalaDeAula +
            "." +
            idMatricula.toString() +
            "." +
            idSalaDeAula.toString();

          var content = this.LsCache.get(cacheServiceId, true);
          if (!clearCache && content) {
            return resolve(content);
          }

          this.getUserToken(idMatricula, idSalaDeAula, clearCache).then(
            function(success) {
              this.doWsCall(
                service,
                {
                  courseid: courseId,
                  wstoken: success.token
                },
                success.siteurl
              ).then(
                function(response) {
                  this.LsCache.set(cacheServiceId, response, 2);
                  return resolve(response);
                }.bind(this),
                function(error) {
                  return reject(error);
                }
              );
            }.bind(this),
            function(error) {
              return reject(error);
            }
          );
        }.bind(this)
      );
    }.bind(this);

    /**
     * Retorna o conteúdo de um Curso
     * @param int idMatricula
     * @param int courseId
     * @param int idSalaDeAula
     * @param boolean clearCache
     * @returns Promise $q
     */
    this.getCourseModules = function(
      idMatricula,
      idSalaDeAula,
      cmid,
      clearCache
    ) {
      clearCache = this.getClearCache(clearCache);
      return this.$q(
        function(resolve, reject) {
          var service = "core_course_get_course_module ";
          var cacheServiceId =
            service +
            "." +
            idSalaDeAula +
            "." +
            idMatricula.toString() +
            "." +
            cmid.toString();

          var courses = this.LsCache.get(cacheServiceId, true);
          if (!clearCache && courses) {
            return resolve(courses);
          }
          this.getUserToken(idMatricula, idSalaDeAula, clearCache).then(
            function(success) {
              this.doWsCall(
                service,
                {
                  cmid: cmid,
                  wstoken: success.token
                },
                success.siteurl
              ).then(
                function(response) {
                  this.LsCache.set(cacheServiceId, response, 2);
                  return resolve(response);
                }.bind(this),
                function(error) {
                  return reject(error);
                }
              );
            }.bind(this),
            function(error) {
              return reject(error);
            }
          );
        }.bind(this)
      );
    }.bind(this);

    /**
     * Retorna os comentários de um contextId
     * @param int idMatricula
     * @param int courseId
     * @param int idSalaDeAula
     * @param boolean clearCache
     * @returns Promise $q
     */
    this.getComments = function(
      idMatricula,
      idSalaDeAula,
      contextId,
      clearCache
    ) {
      clearCache = this.getClearCache(clearCache);
      return this.$q(
        function(resolve, reject) {
          var service = "core_comment_get_comments";
          var cacheServiceId =
            service +
            "." +
            idSalaDeAula +
            "." +
            idMatricula.toString() +
            ".comment." +
            contextId;
          var comments = this.LsCache.get(cacheServiceId, true);
          if (!clearCache && comments) {
            return resolve(comments);
          }

          this.getUserToken(idMatricula, idSalaDeAula, clearCache).then(
            function(success) {
              this.doWsCall(
                service,
                {
                  contextlevel: "module",
                  instanceid: 4519,
                  component: "assignsubmission_comments",
                  itemid: 5866,
                  area: "submission_comments",

                  wstoken: success.token
                },
                success.siteurl
              ).then(
                function(response) {
                  this.LsCache.set(cacheServiceId, response, 2);
                  return resolve(response);
                }.bind(this),
                function(error) {
                  return reject(error);
                }
              );
            }.bind(this),
            function(error) {
              return reject(error);
            }
          );
        }.bind(this)
      );
    }.bind(this);

    /**
     * Retorna o Status das atividades do Aluno em cada atividade
     * @param int id_matricula
     * @param int courseid
     * @param int id_saladeaula
     * @param boolean clearCache
     * @returns {*}
     */
    this.getActivitiesCompletionStatus = function(
      id_matricula,
      courseid,
      id_saladeaula,
      clearCache
    ) {
      clearCache = this.getClearCache(clearCache);

      return this.$q(
        function(resolve, reject) {
          var service = "core_completion_get_activities_completion_status";
          var cacheServiceId =
            service +
            "." +
            id_saladeaula +
            "." +
            id_matricula.toString() +
            "." +
            courseid.toString();
          var courses = this.LsCache.get(cacheServiceId, true);
          if (!clearCache && courses) {
            return resolve(courses);
          }

          this.getUserToken(id_matricula, id_saladeaula, clearCache).then(
            function(success) {
              this.doWsCall(
                service,
                {
                  userid: success.userid,
                  courseid: courseid,
                  wstoken: success.token
                },
                success.siteurl
              ).then(
                function(response) {
                  this.LsCache.set(cacheServiceId, response, 2);
                  return resolve(response);
                }.bind(this),
                function(error) {
                  return reject(error);
                }
              );
            }.bind(this),
            function(error) {
              return reject(error);
            }
          );
        }.bind(this)
      );
    }.bind(this);

    /**
     * Retorna o Status de conclusão do Aluno no Curso
     * @param int id_matricula
     * @param int courseid
     * @param int id_matricula
     * @param boolean clearCache
     * @returns {*}
     */
    this.getCourseCompletionStatus = function(
      id_matricula,
      courseid,
      id_saladeaula,
      clearCache
    ) {
      clearCache = this.getClearCache(clearCache);
      return this.$q(
        function(resolve, reject) {
          var service = "core_completion_get_course_completion_status";
          var cacheServiceId =
            service +
            "." +
            id_saladeaula +
            "." +
            id_matricula.toString() +
            "." +
            courseid.toString();
          var courses = this.LsCache.get(cacheServiceId, true);
          if (!clearCache && courses) {
            return resolve(courses);
          }
          this.getUserToken(id_matricula, id_saladeaula, clearCache).then(
            function(success) {
              this.doWsCall(
                service,
                {
                  userid: success.userid,
                  courseid: courseid,
                  wstoken: success.token
                },
                success.siteurl
              ).then(
                function(response) {
                  this.LsCache.set(cacheServiceId, response, 2);
                  return resolve(response);
                }.bind(this),
                function(error) {
                  return reject(error);
                }
              );
            }.bind(this),
            function(error) {
              return reject(error);
            }
          );
        }.bind(this)
      );
    }.bind(this);

    this.filesUpload = function(idMatricula, idSalaDeAula, fileUri) {
      return this.$q(
        function(resolve, reject) {
          $rootScope.$broadcast("loading:show");
          this.getUserToken(idMatricula, idSalaDeAula).then(
            function(success) {
              // Recuperando path real do arquivo par ao nome
              $cordovaFilePath
                .resolveNativePath(fileUri)
                .then(function(fileDir) {
                  //Extensão
                  var type = fileDir.split(".").pop();

                  // Nome do arquivo
                  var name = fileDir.split("/").pop();
                  var ftOptions = {};
                  ftOptions.fileKey = type;
                  ftOptions.fileName = name;
                  ftOptions.httpMethod = "POST";
                  ftOptions.params = {
                    token: success.token,
                    filearea: "draft",
                    itemid: 0
                  };
                  ftOptions.chunkedMode = false;
                  ftOptions.headers = {
                    Connection: "close"
                  };

                  var transfer = new $cordovaTransfer();

                  //definindo url de upload
                  var uploadUrl = success.siteurl + "/webservice/upload.php";

                  //fazendo upload
                  transfer.upload(fileUri, uploadUrl, ftOptions, true).then(
                    function(response) {
                      $rootScope.$broadcast("loading:hide");
                      return resolve(JSON.parse(response.response));
                    }.bind(this),
                    function(error) {
                      $rootScope.$broadcast("loading:hide");
                      return reject({
                        error: error
                      });
                    }.bind(this)
                  );
                });
            }.bind(this),
            function(error) {
              return reject(error);
            }
          );
        }.bind(this)
      );
    }.bind(this);

    $ionicPlatform.ready(function() {
      localPath = file.dataDirectory + "portaldoaluno/";
      if (ionic.Platform.isAndroid()) {
        localPath = file.externalApplicationStorageDirectory + "portaldoaluno/";
      }
    });

    /**
     * Função para download de arquivo.
     * Tem q ser melhorada para salvar arquivos localmente
     */
    this.filesDownload = function(idMatricula, idSalaDeAula, url, type) {
      return this.$q(
        function(resolve, reject) {
          this.getUserToken(idMatricula, idSalaDeAula).then(
            function(success) {
              // Recuperando path real do arquivo par ao nome
              //Extensão

              url += "&token=" + success.token;

              // Some webservices returns directly the correct download url, others not.
              if (url.indexOf("/webservice/pluginfile") == -1) {
                url = url.replace("/pluginfile", "/webservice/pluginfile");
              }
              var ftOptions = {};
              ftOptions.encodeURI = false;
              var transfer = new $cordovaTransfer();

              //fazendo upload
              transfer
                .download(url, localPath + "userImage.jpg", ftOptions, true)
                .then(
                  function() {
                    $rootScope.$broadcast("loading:hide");
                    return resolve(localPath + "userImage.jpg");
                  }.bind(this),
                  function() {
                    $rootScope.$broadcast("loading:hide");
                    return reject(false);
                  }.bind(this)
                );
            }.bind(this),
            function(error) {
              return reject(error);
            }
          );
        }.bind(this)
      );
    }.bind(this);

    /**
     * Retorna a url do arquivo para o moodle
     */
    this.filesUrl = function(idMatricula, idSalaDeAula, url, type) {
      return this.$q(
        function(resolve, reject) {
          this.getUserToken(idMatricula, idSalaDeAula).then(
            function(success) {
              // Recuperando path real do arquivo par ao nome
              //Extensão
              if (!url) {
                return "";
              }

              // First check if we need to fix this url or is already fixed.
              if (url.indexOf("token=") != -1) {
                return url;
              }

              // Check if is a valid URL (contains the pluginfile endpoint).
              if (url.indexOf("pluginfile") == -1) {
                return url;
              }

              if (!success.token) {
                return "";
              }

              // In which way the server is serving the files? Are we using slash parameters?
              if (
                url.indexOf("?file=") != -1 ||
                url.indexOf("?forcedownload=") != -1 ||
                url.indexOf("?rev=") != -1
              ) {
                url += "&";
              } else {
                url += "?";
              }

              // Always send offline=1 (for external repositories). It shouldn't cause problems for local files or old Moodles.
              url += "token=" + success.token + "&offline=1";

              // Some webservices returns directly the correct download url, others not.
              if (url.indexOf("/webservice/pluginfile") == -1) {
                url = url.replace("/pluginfile", "/webservice/pluginfile");
              }

              return resolve(url);
            }.bind(this),
            function(error) {
              return reject(error);
            }
          );
        }.bind(this)
      );
    }.bind(this);

    this.convertImg = function() {};

    this.changeHtmlLinks = function(htmlString, idMatricula, idSaladeaula) {
      return this.$q(
        function(resolve, reject, all) {
          var doc = document.createElement("div");
          doc.innerHTML = htmlString;
          var promises = [];

          var links = doc.getElementsByTagName("a");
          var images = doc.getElementsByTagName("img");

          for (i = 0; i < links.length; i++) {
            var actualLink = links[i];
            var promise = this.filesUrl(
              idMatricula,
              idSaladeaula,
              actualLink.getAttribute("href")
            ).then(function(response) {
              actualLink.setAttribute("href", response);
              actualLink.setAttribute("target", "_blank");
              console.log(response,'a');
            });
            promises.push(promise);
          }

          for (v = 0; v < images.length; v++) {
            var actualImage = images[v];
            var promise =  this.filesUrl(
              idMatricula,
              idSaladeaula,
              actualImage.getAttribute("src")
            ).then(function(response) {
              actualImage.setAttribute("src", response);
              console.log(response,'img');
            });
            promises.push(promise);
          }
          this.$q.all(promises).then(function(){
            console.log(new XMLSerializer().serializeToString(doc), "all");
            return resolve(new XMLSerializer().serializeToString(doc));
          });
        }.bind(this)
      );
    };

    return this;
  });
