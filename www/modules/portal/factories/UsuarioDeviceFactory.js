angular.module('portal.factories')
  .factory('UsuarioDeviceFactory', function ($log, $http, CONFIG_APP, RestFactory) {
    var service = {
      url : RestFactory.getUrlApi() + '/usuario-device',
    }

    return angular.extend({}, RestFactory, service);

  });
