angular.module('portal.directives')
  .directive('btnOcorrenciaPortal', [function () {
    return {
      restrict: 'E',
      controller: function ($scope,
                            $state,
                            $ionicModal,
                            $ionicLoading,
                            ServicoAtencaoService,
                            $ionicPopup,
                            DisciplinaFactory,
                            $filter) {


        $scope.categoriasOcorrencia = [];
        $scope.ocorrencia = {};
        $scope.assuntosOcorrencia = [];
        $scope.subAssuntosOcorrencia = [];
        $scope.matriculasAluno = [];
        $scope.salasAluno = [];
        $scope.maxCaracterDescricao = 1000;
        $scope.qtdCaraterDescricao = 0;
        $scope.isCentralDeAtencao = false;

        $scope.$watch(function () {
          return $state.current.name;
        }, function(stateName){
          (function checkIfIsCentralDeAtencao() {
            if(stateName == 'app.servico_atencao.list' || stateName == 'app.servico_atencao.detail'){
              $scope.isCentralDeAtencao = true;
            } else {
              $scope.isCentralDeAtencao = false;
            }
          })();
        });

        /* Workaround para validação da quantidade máxima de caracteres no <textarea>
         (maxlength do HTML5 funciona de forma inconsistente variando de browser para browser).
         Atualiza também o contador de caracteres na tela.*/
        
        $scope.validaQtdMaxCaracteresAtualizaContador = function () {
          if (typeof $scope.ocorrencia.st_ocorrencia != "undefined") {
            if ($scope.ocorrencia.st_ocorrencia.length > $scope.maxCaracterDescricao) {
              $scope.ocorrencia.st_ocorrencia = $scope.ocorrencia.st_ocorrencia.substr(0, $scope.maxCaracterDescricao)
            }
            $scope.qtdCaraterDescricao = $scope.ocorrencia.st_ocorrencia.length;
          } else {
            $scope.qtdCaraterDescricao = 0;
          }
        };

        $ionicModal.fromTemplateUrl('modules/portal/components/servico-atencao/tpl/add.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function (modal) {
          $scope.modalOcorrencia = modal;
        });

        /**
         * Verifica a rota, analisando se é possivel ter os dados referente a disciplina
         */
        var checarRotaVerificarDisciplina = function () {
          if ($state.current.name == 'app.disciplina.show.conteudo'
            || $state.current.name == 'app.notas.grade_notas_show') {
            $scope.disciplinaAtiva = DisciplinaFactory.getDisciplinaAtiva();

            $scope.ocorrencia.id_matricula = $scope.disciplinaAtiva.id_matricula;
            $scope.ocorrencia.id_saladeaula = $scope.disciplinaAtiva.id_saladeaula;
            $scope.ocorrencia.id_entidade = $scope.disciplinaAtiva.id_entidade;
            $scope.getAssuntosOcorrencia();
          } else {
            DisciplinaFactory.setDisciplinaAtiva({});
            $scope.disciplinaAtiva = null;
            $scope.getMatriculasAluno();
          }


        }

        /**
         * Abre o modal e seta os valores iniciais.
         */
        $scope.abreModalOcorrencia = function () {
          $scope.ocorrencia = {};
          $scope.assuntosOcorrencia = [];
          $scope.subAssuntosOcorrencia = [];
          $scope.matriculasAluno = [];
          $scope.salasAluno = [];
          $scope.msgErro = null;
          checarRotaVerificarDisciplina();
          // $scope.getCategoriasOcorrencia();
          // $scope.getAssuntosOcorrencia();

          $scope.modalOcorrencia.show();
        }

        $scope.closeModalOcorrencia = function () {
          $scope.modalOcorrencia.hide();
          $scope.qtdCaraterDescricao = 0;
        };


        /**
         * Recupera as matriculas dos alunos
         */
        $scope.getMatriculasAluno = function () {
          $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});
          ServicoAtencaoService.getMatriculas().then(function (matriculas) {
            $scope.matriculasAluno = matriculas;
          }).finally(function () {
            $ionicLoading.hide();
          });
        }


        $scope.carregaDadosFormulario = function () {
          //busca o objeto da matricula no array das matriculas
          var matricula = $filter('filter')($scope.matriculasAluno, {id_matricula: parseInt($scope.ocorrencia.id_matricula)});

          //verifica se tem resultado
          if (matricula.length) {
            matricula = matricula[0];

            //seta o id_entidade na ocorrencia
            $scope.ocorrencia.id_entidade = matricula.id_entidade;

            $scope.getSalaAulaAlunoByMatricula(matricula);
            $scope.getAssuntosOcorrencia();
          }
        }


        /**
         * Recupera as categorias de ocorrencia
         */
        // $scope.getCategoriasOcorrencia = function () {
        //   $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});
        //   ServicoAtencaoService.getCategorias().then(function (response) {
        //       $scope.categoriasOcorrencia = response.data;
        //
        //     }, function (error) {
        //       //console.log(error);
        //     })
        //     .finally(function () {
        //       $ionicLoading.hide();
        //     });
        // }


        /**
         * Recupra os assuntos da ocorrencia
         */
        $scope.getAssuntosOcorrencia = function () {
          $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});
          ServicoAtencaoService.getAssuntos(null, $scope.ocorrencia.id_entidade)
            .then(function (response) {
              $scope.assuntosOcorrencia = response.data;
            }, function (error) {
              //console.log(error);
            })
            .finally(function () {
              $ionicLoading.hide();
            });
        }

        /**
         * Recupera os sub assuntos da ocorrencia
         */
        $scope.getSubassunto = function () {

          //valida se existe algum item do select selecionado e setado no escopo
          if (!$scope.ocorrencia.id_assuntopai) {
            $scope.subAssuntosOcorrencia = [];
            return;
          }
          var id_assuntocopai = $scope.ocorrencia.id_assuntopai;
          $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});
          ServicoAtencaoService.getAssuntos(id_assuntocopai, $scope.ocorrencia.id_entidade)
            .success(function (data) {
              $scope.subAssuntosOcorrencia = data;
            })
            .finally(function () {
              $ionicLoading.hide();
            });


        }


        /**
         * Recuperar as salas de aula baseado na matricula selecionada
         */
        $scope.getSalaAulaAlunoByMatricula = function (matricula) {
          $scope.msgErro = null;
          $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});
          DisciplinaFactory.getDisciplinas({
              idEntidade: matricula.id_entidade,
              idMatricula: matricula.id_matricula,
              idProjeto: matricula.id_curso
            }, false)
            .then(function (disciplinas) {
              $scope.salasAluno = disciplinas;
            }, function (err) {
              $scope.msgErro = err.message;
            }).finally(function () {
            $ionicLoading.hide();
          });
        }


        /**
         * Salvar a ocorrencia
         */
        $scope.salvarOcorrencia = function () {
          $ionicLoading.show({template: '<ion-spinner>Carregando...</ion-spinner>'});

          ServicoAtencaoService.salvarOcorrencia($scope.ocorrencia)
            .then(function (data) {
              $scope.ocorrencia = {};
              $ionicLoading.hide();
              $ionicPopup.alert({
                title: 'Sucesso!',
                template: 'Ocorrência criada com sucesso.'
              });
              $scope.closeModalOcorrencia();

            }, function (err) {
              $scope.msgErro = err.message;
            });

        }


      }
    }
  }])
;
