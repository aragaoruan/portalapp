angular.module('portal.directives')
  .directive('moodleExternalFile', function (MoodleFactory) {

    /**
     * Directive que seta a url da uma imagem do moodle
     * é necessário informar no img
     * @param idMatricula
     * @param idSaladeaula
     * @param imgSrc
     */
    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        var re = new RegExp("/theme/");
        if (re.test(attrs.imgSrc)){
          elem.attr('src', attrs.imgSrc);
          return;
        }

        // Busca a nova url
        MoodleFactory.filesUrl(attrs.idMatricula, attrs.idSaladeaula, attrs.imgSrc, 1).then(function (response) {
          // seta no src do elemento
          elem.attr('src', response);
        });
      }
    };
  });
