angular.module('portal.directives')
.directive('imagemErroDirective', ['$document', '$window', function ($document, $window) {
    return{
        link: function(scope, element, attrs){
            element.bind('error', function() {
                attrs.$set('src', 'assets/img/semimagem.png');
        });
    }
    }
}])
.directive('imagemErroDetalheDirective', ['$document', '$window', function ($document, $window) {
    return{
        link: function(scope, element, attrs){
            element.bind('error', function() {
                attrs.$set('src', 'assets/img/semimagem.png');
            });
    }
    }
}]);