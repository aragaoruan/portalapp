angular.module('portal.directives')
  .directive('chartPortal', function() {
    return {
      restrict: 'E',
      scope: {
        mysize: '@',
        percent: '=percent',
        color: '@',
        statuscomplete: '=statuscomplete',
        statussala: '=statussala',
        animation: '@'
      },
      templateUrl: 'modules/portal/directives/tpl/chart.html',
      controller: function($scope) {
        var vm = this;
        if (!$scope.percent) {
          $scope.percent = 0;
        }

        var configurarChart = function() {
          $scope.strokeColor = '';
          $scope.salaNaoIniciada = $scope.statussala == 2;

          // Calculando a área transparente do gráfico.
          var result = 100 - $scope.percent;

          // Definindo cores com base no progresso da sala.
          switch ($scope.statuscomplete) {
            case "Adiantado":
              $scope.strokeColor = '#50BCBD';
              break;
            case "Em dia":
              $scope.strokeColor = '#50BCBD';
              break;
            case "Completo":
              $scope.strokeColor = '#7FA2D5';
              $scope.salaCompleta = true;
              $scope.percent = 100;
              result = 0;
              break;
            case "Indefinido":
              $scope.strokeColor = '#79B652';
              break;
            case "Atrasado":
              $scope.strokeColor = '#E84242';
              break;
            case "Futuro":
              $scope.hideNumber = true;
              result = 100;
              $scope.strokeColor = '#FFFFFF';
              break;
            default:
              $scope.strokeColor = '#FFFFFF';
              break;
          }

          /* Aplicando cores definidas acima no chart.
           Remove o # da string da classe, para ser utilizada corretamente
           na classe do template da diretiva.*/

          $scope.colorClass = $scope.strokeColor.substr(1);
          $scope.labels = [" ", " "]; // Deixando labels vazias.
          $scope.data = [$scope.percent, result]; // Definindo dados do gráfico
          $scope.options = {
            segmentShowStroke: false,
            percentageInnerCutout: 66
          };

          if ($scope.animation == 'false')
            $scope.options.animation=false;


          // Aplicando ao gráfico a cor selecionada baseada no status.
          $scope.colours = [{
            strokeColor: $scope.strokeColor
          }, {
            strokeColor: "rgba(207,100,103,0)"
          }];
        };

        //Configura chart na primeira execução da Controller.
        configurarChart();

        //Atualiza dados do chart nas demais execuções.
        $scope.$watchGroup(['percent', 'statuscomplete', 'percent'], function() {
          configurarChart();
        });
      },
      controllerAs: 'vm'
    }
  });
