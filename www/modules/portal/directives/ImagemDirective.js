angular.module('portal.directives')
.directive('imagemDirective', ['$document', '$window', function ($document, $window) {
    return{
        scope: {
            visualizado: '@',
            largura: '@',
            altura: '@'
            },
        link: function(scope, element, attrs){
            attrs.$set('height', scope.altura);
            attrs.$set('width', scope.largura);
            attrs.$set('style', "background-image:url('assets/img/carregandoimagem.png'); background-size: 400px 400px; background-repeat: no-repeat; background-color:#C3C3C3;background-position: center");
            if(scope.visualizado == "false"){
                attrs.$set('style', "box-shadow: 10px 10px 5px #a73866;");
            }
        }
    }
}]);