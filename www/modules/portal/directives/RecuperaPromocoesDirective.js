angular.module('portal.directives')
.directive('recpromo', ['$document', '$window', 'PromocoesFactory', function ($document, $window, PromocoesFactory) {
    return{
        scope: {
            matricula: '@',
            esconder: '@'
            },
        link: function(scope, element, attrs){
            if(scope.esconder == 0){
                localStorage.setItem('id_matricula', scope.matricula);
                PromocoesFactory.atualizaPromocoes();
                return true;
              }
        }
    }
}]);