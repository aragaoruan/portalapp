angular.module('portal.directives')
  .directive('accordion', ['$document', 'PromocoesFactory', function ($document, PromocoesFactory) {
    var self = this;

    return {
      link: function (scope, element, attrs) {
        element.find("accordion-head").on("click", function (e) {
          element.find("accordion-body").toggleClass('hide');
          localStorage.setItem('id_matricula', element.find("accordion-body").attr('matricula'));
          PromocoesFactory.atualizaPromocoes();
        });
      },
      restrict: 'E'
    };
  }]);
