angular.module('portal.directives')
.directive('gerarBoleto', ['$document',
    function($document) {
            return {
              restrict: 'A',
              scope: true,
              link: function(scope, element, attrs) {
                element.on('click', function(e) {
                    e.stopPropagation(); //previne de abrir a tela com os dados do boleto
                }); 
              }
            };
          }
]);
