angular.module('portal.directives')
  .directive('menuPortal', ['$document', '$timeout', function ($document) {
    var that = this;
    return {
      link: function (scope, element, attrs) {
        //atribui o elemento a uma variavel de escopo da diretivaMenuFactory
        that.element = element;
        //posição inicial do menu
        var startX = 0, x = 0;

        //verifica se o menu não te as classes de aberto ou de fechado e atribui a classe de fechado
        if (!element.hasClass('open') || !element.hasClass('closed')) {
          element.addClass('closed');
        }

        //identifica o clique nos itens do menu e fecha o menu
        element.find('ion-item').on('click', function () {
          //verifica se o menu esta aberto e fecha o mesmo
          if (element.hasClass('open')) {
            scope.dragMenuClose();
          }
        });

        element.parent().find('ion-side-menu-content').children().on('click', function () {
          //verifica se o menu esta aberto e fecha o mesmo
          if (element.hasClass('open')) {
            scope.dragMenuClose();
          }
        });

        element.on('mousedown', function (event) {
          // Prevent default dragging of selected content
          event.preventDefault();

          //verifica a posição inicial do clique
          startX = event.pageX - x;

          //escuta os eventos de mover o mouse e de tirar o mouse
          $document.on('mousemove', mousemove);
          $document.on('mouseup', mouseup);
        });

        
        /**
         * Abre ou fecha o menu
         * @param event
         */
        function mousemove(event) {
          x = event.pageX - startX;//calcula a posição do mouse ao ser movido

          //se mover o mouse pra esquerda o valor será positivo, então abriremos o menu
          if (x > 0) {
            scope.dragMenuOpen();
          } else {//caso contrario fecharemos o menu
            scope.dragMenuClose();
          }
          //chama a função que para de escutar os eventos do mouse
          mouseup()

        }

        /**
         * Escuta o evento de mouseup
         */
        function mouseup() {
          $document.off('mousemove', mousemove);
          $document.off('mouseup', mouseup);
        }


      },
      controller: function ($scope, $state, $ionicSideMenuDelegate, $location, MenuFactory, $timeout) {
        $scope.classMenu;
        $scope.$watch(function () {
          return $state.current.name;
        }, function (newValue) {
          var menuObj = MenuFactory.setCurrentMenu(newValue).getObjMenu();
          menuObj.then(function (menu) {
            $scope.itensMenu = menu.itens;
            //classe para ser adicionada no menu
            $scope.classMenu = menu.menuData.classmenu;
          });
        });

        $scope.$watch(function () {
          return window.localStorage.getItem('user_name');
        }, function (newValue) {

          var statusMenu = window.localStorage.getItem('exibiu_menu');
          if (newValue && statusMenu != 'sim') {
            window.localStorage.setItem('exibiu_menu', 'sim')
                $scope.dragMenuClose();
          }


        });

        $scope.$watch(function () {
          return localStorage.getItem('promo_nao_visualizada');
        }, function (promocoes) {
          $scope.qtdPromocoes = localStorage.getItem('promo_nao_visualizada');
        });


        //Abra o menu
        $scope.dragMenuOpen = function () {
          //pega o elemento
          var element = that.element;
          //adiciona a classe de aberto no mesmo
          element.addClass('open');
          //remove a classe de fechado
          element.removeClass('closed');
          //faz o movimento de abertura do menu
          element.css({
            "transform": "translate3d(233px,0,0)",
          });
        }

        //fecha o menu
        $scope.dragMenuClose = function () {
          var element = that.element;
          element.removeClass('open');
          element.addClass('closed');
          element.css({
            "transform": "translate3d(0,0,0)",
          });
        }


      },
      restrict: 'E',
      spoce: {
        autoShow: "@",
        delay: "=delay"
      },
      templateUrl: 'modules/portal/directives/tpl/menu.html'
    }
  }]);
