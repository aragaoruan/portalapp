angular.module('portal.filters')
    .filter('asDate', ['$filter', function ($filter) {
        return function (input, formatPatner, format) {
            if (input == null) {
                return "";
            }

            if (typeof format == 'undefined') {
                format = true;
            }


            //verifica se a string da data tem horas e separa
            var splitedDate = input.split(" ");

            //verifica se separou
            if (splitedDate.length) {

                //quebra a data
                var dataArr = splitedDate[0].split("-");
                //cria uma nova variavel somente com a data
                var dateTime = new Date(splitedDate[0]);

                //seta cada parte da data
                if (dataArr.length) {
                    var dateTime = new Date();
                    dateTime.setDate(dataArr[2]);
                    dateTime.setMonth((dataArr[1] - 1));
                    dateTime.setYear(dataArr[0]);
                }


                //verifica se existe a segunda posição criada pelo split
                if (typeof splitedDate[1] != 'undefined') {

                    //separa do . que define datetime2
                    var splitedHours = splitedDate[1].split(".");

                    //verifica se existia o separador de datetime2
                    if (splitedHours.length) {

                        //separa as horas, minutos e segundos
                        var splitTime = splitedHours[0].split(":");

                        //verifica se separou
                        if (splitTime.length) {
                            //seta os valores no datime
                            dateTime.setHours(splitTime[0]);
                            dateTime.setMinutes(splitTime[1]);
                            dateTime.setSeconds(splitTime[2]);
                        }
                    }
                }

                if (format) {
                    return $filter('date')(dateTime, formatPatner ? formatPatner : 'dd/MM/yyyy HH:mm:ss');
                }
                return dateTime;
            }

            var dateTime = new Date(input);
            dateTime.setDate(30);
            if (format) {
                return $filter('date')(dateTime, formatPatner ? formatPatner : 'dd/MM/yyyy HH:mm:ss');
            }
            return dateTime;
        };
    }])
    .filter('parseHtml', function ($sce) {
        return function (value) {
            if (!value) {
                return '';
            }
            return $sce.trustAsHtml(value.replace(/\n/g, '<br>'));
        };
    })

    .filter('removeHtmlTags', function () {
        return function (text) {
          var text = String(text).replace(/<(?!p\s*\/?)[^>]+>/g, "");
            // var text = String(text).replace(/<[^>]+>/gm, '');
            text = String(text).replace(/<!DOCTYPE[^>[]*(\[[^]]*\])?>/gm, '');
            text = String(text).replace(/<html[^>[]*(\[[^]]*\])?>/gm, '');
            return text;
        };
    })

    .filter('removeImgTags', function () {
        return function (text) {

          var text = String(text).replace(/<img .*?>/g, "");
            // var text = String(text).replace(/<[^>]+>/gm, '');
            return text;
        };
    })
    .filter('removeHtmlTagsExceptImg', function () {
        return function (text) {
            // Remove todas as TAGs exceto tags <img>
            var text = String(text).replace(/<(?!img\s*\/?)[^>]+>/g, "");
            // Remove largura e altura absolutas da imagem
            text = String(text).replace(/width="\d+" height="\d+"/g, "");
            /* Adiciona classe de imagem responsiva.
             Para que a imagem seja exibida corretamente, a TAG parent deve
             ter conter a classe .responsive-container. */
            text = String(text).replace(/<img/g, "<img class='responsive-img' ");
            return text;
        }
    })
    .filter('sliceString', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || '…');
        };
    }).filter('capitalize', function () {
    return function (input, all) {
        if (!input)
            return;

        // Precisa mesmo do parâmetro all ?
        var alwaysToLower = ["em", "de", "e", "é", "a", "o", "da", "do", "dos", "das", "na", "com", "por", "para"]; // adicionar aqui todas as palavras que normalmente não precisam de alterações
        var words = input.split(' ');
        var capitalized = words.map(function (word) {
            if (alwaysToLower.indexOf(word.toLowerCase().trim()) > -1) return word.toLowerCase();
            return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
        });
        return capitalized.join(' ');
    }
}).filter('groupBy', function () {
    var memoized = _.memoize(function (items, field) {
        var orded = _.groupBy(items, field);
        return orded;
    });

    return memoized;


    // return _.memoize(function(items, field) {
    //     return _.groupBy(items, field);
    //   }
    // );
});
