#!/bin/bash

#Verifica se os 2 parâmetros foi passado junto com o comando.
if [ $# -lt 2 ]; then
  echo "A passagem dos 2 parâmetros é obrigatória! Verifique o script para maiores detalhes."
  exit 1
fi

#Copio o arquivo AppConstante.js da pasta padrão, para ser usado.
cp templates/dist/AppConstant.js www/modules/portal/config/

#swith case para verificar qual será o ambiente de apontamento do apk
case $1 in
  producao) sed -i'.bkp' "s/AMBIENTE/http:\/\/g2s.unyleya.com.br\//g" www/modules/portal/config/AppConstant.js ;;
  release) sed -i'.bkp' "s/AMBIENTE/http:\/\/g2release.unyleya.xyz\//g" www/modules/portal/config/AppConstant.js ;;
  evolutiva) sed -i'.bkp' "s/AMBIENTE/http:\/\/g2evolutiva.unyleya.xyz\//g" www/modules/portal/config/AppConstant.js ;;
  staging) sed -i'.bkp' "s/AMBIENTE/http:\/\/g2staging.unyleya.xyz\//g" www/modules/portal/config/AppConstant.js ;;
  desenvolvimento) sed -i'.bkp' "s/AMBIENTE/http:\/\/dev5g2.unyleya.xyz\//g" www/modules/portal/config/AppConstant.js ;;
  *) "Parâmetro desconhecido." ;;
esac

#swith case para verificar qual será a entidade de apontamento do apk
case $2 in
  unyleya) ENTIDADE=14 ;;
  ucam) ENTIDADE=262 ;;
  padrao) ENTIDADE=514 ;;
  *) "Parâmetro desconhecido" ;;
esac

#roda o comando que substitui a entidade do arquivo AppConstant
sed -i'.bkp' "s/ENTIDADE/$ENTIDADE/g" www/modules/portal/config/AppConstant.js

#Apaga a copia gerada pelo comando sed
rm -rf www/modules/portal/config/AppConstant.js.bkp

#Fazendo uma copia do arquivo config.xml para ser aplicado os comandos de substituição.
cp templates/dist/config.xml .

#Gerando uma versão e o id automaticamente
sed -i'.bkp' "s/APP-VERSION/$(date +%y).$(date +%m%d).$(date +%H)/g" config.xml

#swith case para gerar o id automaticamente
case $2 in
   unyleya) sed -i'.bkp' "s/APP-ID/com.unyleya.portalapp/g" config.xml ;;
   ucam) sed -i'.bkp' "s/APP-ID/com.unyleya.ucam/g" config.xml ;;
   *) "Parâmetro desconhecido" ;;
esac


#Apagando o arquivo de backup, pois temos o nosso padrão dentro de /templates/dist/config.xml
rm -rf config.xml.bkp

#Gerando a identidade visual da UNYLEYA
gulp set-layout --layout $2

#swith case para gerar o nome do apk
case $2 in
  unyleya) NOME_APK="PortalAluno"-$(date +%Y%m%d%H%M) ;;
  ucam) NOME_APK="PortalUcam"-$(date +%Y%m%d%H%M) ;;
  padrao) NOME_APK="PortalIMP"-$(date +%Y%m%d%H%M) ;;
  *) "Parâmetro desconhecido" ;;
esac


#gerando nova versao do release
#ionic cordova build --release android
ionic cordova build android --minifycss --minifyjs --aot --device --optimizejs --release

#keystore nao necessario executar a nao ser que tenha certeza do que esta fazendo
#rm my-release-key.keystore
#keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000

#removendo arquivo apk descartada
rm -rf $NOME_APK.apk

#passphrase unyleya00
#Assinatura da apk

#Tive que colocar o path completo para funcionar, caso rode em outra maquina lembre-se de trocar o caminho!
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore /Users/alexalexandre/Documents/dev/portalapp/platforms/android/build/outputs/apk/release/android-release-unsigned.apk alias_name
#/cygdrive/c/Program\ Files/Java/jdk1.8.0_144/bin/jarsigner.exe -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore platforms/android/build/outputs/apk/app-release-unsigned.apk alias_name

#zip align

#Tive que colocar o path completo para funcionar, caso rode em outra maquina lembre-se de trocar o caminho!
/Users/alexalexandre/Library/Android/sdk/build-tools/26.0.2/zipalign -v 4 /Users/alexalexandre/Documents/dev/portalapp/platforms/android/build/outputs/apk/release/android-release-unsigned.apk $NOME_APK.apk

#/opt/Android/Sdk/build-tools/24.0.2/zipalign -v 4 platforms/android/build/outputs/apk/app-release-unsigned.apk $NOME_APK.apk
#/cygdrive/c/Users/felip/AppData/Local/Android/sdk/build-tools/26.0.1/zipalign.exe -v 4 platforms/android/build/outputs/apk/app-release-unsigned.apk $NOME_APK.apk
# /Users/fpastor/Library/Android/sdk/build-tools/23.0.0/zipalign -v 4 platforms/android/build/outputs/apk/app-release-unsigned.apk $NOME_APK.apk

# git commit -a -m "update version" && git push

