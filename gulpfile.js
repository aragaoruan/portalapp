var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');

var insert = require('gulp-insert');
var stripComments = require('gulp-strip-comments');
var removeEmptyLines = require('gulp-remove-empty-lines');
var clipEmptyFiles = require('gulp-clip-empty-files');
var tap = require('gulp-tap');
var fs = require('fs');
var through = require('through');
var path = require('path');
var File = gutil.File;
var gulpSlash = require('gulp-slash');
var ngAnnotate = require('gulp-ng-annotate');

var license = '' +
        '// (C) Copyright 2015 Martin Dougiamas\n' +
        '//\n' +
        '// Licensed under the Apache License, Version 2.0 (the "License");\n' +
        '// you may not use this file except in compliance with the License.\n' +
        '// You may obtain a copy of the License at\n' +
        '//\n' +
        '//     http://www.apache.org/licenses/LICENSE-2.0\n' +
        '//\n' +
        '// Unless required by applicable law or agreed to in writing, software\n' +
        '// distributed under the License is distributed on an "AS IS" BASIS,\n' +
        '// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n' +
        '// See the License for the specific language governing permissions and\n' +
        '// limitations under the License.\n\n',
    buildFile = 'mm.bundle.js';

var modules = {
    portal: './www/modules/portal'
}

var argv = require('yargs')
    .option('l', {
        alias: 'layout',
        demand: true,
        default: 'padrao',
        describe: 'Layout do App',
        type: 'string'
    });

var paths = {
    build: './www/build',
    sass: {
        custom: [
            './scss/*.scss'
        ]
    },
};

gulp.task('default', ['sass']);

gulp.task('set-layout', function () {
    var params = argv.argv;

    gulp.src('./templates/' + params.layout + '/_variables.scss')
      .pipe(gulp.dest('./scss/'))
      .on('end', function () {
        gulp.start('sass');
      });

    gulp.src('./templates/' + params.layout + '/images/*.png')
        .pipe(gulp.dest('./www/assets/img'));

    gulp.src('./templates/' + params.layout + '/images/resources/*.png')
        .pipe(gulp.dest('./resources/'));

    gulp.src('./templates/' + params.layout + '/images/resources/android/icon/*.png')
      .pipe(gulp.dest('./resources/android/icon'));

    gulp.src('./templates/' + params.layout + '/images/resources/android/splash/*.png')
      .pipe(gulp.dest('./resources/android/splash'));

});


gulp.task('sass', function (done) {
    gulp.src(paths.sass.custom)
        .pipe(sass({
            errLogToConsole: true
        }))
        .pipe(gulp.dest('./www/assets/css/'))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest('./www/assets/css/'))
        .on('end', done);
});

gulp.task('watch', function () {
    gulp.watch(paths.sass.custom, ['sass']);
});

gulp.task('install', ['git-check'], function () {
    return bower.commands.install()
        .on('log', function (data) {
            gutil.log('bower', gutil.colors.cyan(data.id), data.message);
        });
});

gulp.task('git-check', function (done) {
    if (!sh.which('git')) {
        console.log(
            '  ' + gutil.colors.red('Git is not installed.'),
            '\n  Git, the version control system, is required to download Ionic.',
            '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
            '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
        );
        process.exit(1);
    }
    done();
});
